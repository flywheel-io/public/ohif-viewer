from datetime import datetime, timedelta
import json
import os
import shlex
import shutil
import subprocess
import sys
import urllib.parse
import urllib.request

GITLAB_PROJECT = 'flywheel-io/public/ohif-viewer'
STORAGE_ROOT = 'gs://fw-ohif'
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
PIPELINE_OFFSET = timedelta(days=int(os.environ.get('BRANCH_ACTIVE_DAYS', '7')))
TAG_OFFSET = timedelta(days=int(os.environ.get('TAG_ACTIVE_DAYS', '90')))
PINNED_REFS = json.loads(os.environ.get('PINNED_REFS', '[]'))

def main(args):
    if not GITLAB_TOKEN:
        print('missing GITLAB_TOKEN environment variable', file=sys.stderr)
        sys.exit(1)
    if len(args) < 1:
        print('missing branch directory argument', file=sys.stderr)
        sys.exit(1)
    branch_directory = args[0]
    branch_listing = set(os.listdir(branch_directory))

    quoted_project = urllib.parse.quote(GITLAB_PROJECT, safe='')
    project = request(f'projects/{quoted_project}')

    active_refs = set(PINNED_REFS)
    updated_after = datetime.strftime(datetime.now() - PIPELINE_OFFSET, '%Y-%m-%dT%H:%M:%SZ')
    for pipeline in request_all(f'projects/{project["id"]}/pipelines', {'updated_after': updated_after}):
        if pipeline['status'] not in ('success', 'running'):
            continue
        ref = pipeline['ref']
        if not ref.startswith('refs/'):
            active_refs.add(ref)
    tag_start = datetime.now() - TAG_OFFSET
    for tag in request_all(f'projects/{project["id"]}/repository/tags'):
        tag_created = datetime.strptime(tag['commit']['created_at'].split('T')[0], '%Y-%m-%d')
        if tag_start < tag_created:
            active_refs.add(tag['name'])

    delete_refs = branch_listing - active_refs
    for ref in delete_refs:
        shutil.rmtree(os.path.join(branch_directory, ref))

    for ref in active_refs:
        local_dir = os.path.join(branch_directory, ref)
        os.makedirs(local_dir, exist_ok=True)
        subprocess.run(['gsutil', '-m', 'rsync', '-d', '-r', f'{STORAGE_ROOT}/{ref}', local_dir], check=True)

def request(path, params={}):
    query = urllib.parse.urlencode(params)
    request = urllib.request.Request(f'https://gitlab.com/api/v4/{path}?{query}', headers={'Private-Token': GITLAB_TOKEN})
    data = urllib.request.urlopen(request)
    return json.load(data)

def request_all(path, params={}, page=1):
    per_page = 100
    params = params.copy()
    params.update({'page': page, 'per_page': per_page})
    data = request(path, params)
    if isinstance(data, list):
        for item in data:
            yield item
        if len(data) >= per_page:
            yield from request_all(path, params, page=page + 1)
    else:
        yield data

if __name__ == '__main__':
    main(sys.argv[1:])

# ohif-viewer

![Version: 1.0.0](https://img.shields.io/badge/Version-1.0.0-informational?style=flat-square)

Flywheel ohif-viewer chart for Kubernetes

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| image.pullPolicy | string | `"IfNotPresent"` | policy for pulling the runtime image |
| image.repository | string | `"flywheel/ohif-viewer"` | Docker repository for the runtime image |
| image.tag | string | `"master"` | image tag for the runtime image |
| global.podAntiAffinityTopologyKey | string | `"topology.kubernetes.io/zone"` | The optional alternative key to use for anti-affinity in pod-scheduling |
| ohifConfig.showStudyList | bool | `true` | show study list for projects |
| ohifConfig.prefetchStudies | bool | `true` | eagerly load study images |
| ohifConfig.enableFileAdapter | bool | `true` | enable file loader support: nifti or web image |
| ohifConfig.filterQueryParam | bool | `true` | enable filtering on url query param support |
| ohifConfig.enableStudyLazyLoad | bool | `true` | enable lazy load support |
| ohifConfig.enableAutoFullDynamicWWWC | bool | `false` | enable auto apply full dynamic wwwc |
| ohifConfig.enableMeasurementTableWarning | bool | `false` | enable warnings on measurement table |
| ohifConfig.enableMeasurementSaveBtn | bool | `false` | enable measurement save btn on measurement table |
| ohifConfig.wadoUriRoot | string | `"/io-proxy/wado"` | path where the WADO-URI API is served |
| ohifConfig.qidoRoot | string | `"/io-proxy/wado"` | path where the QIDO-RS API is served |
| ohifConfig.wadoRoot | string | `"/io-proxy/wado"` | path where the WADO-RS API is served |
| ohifConfig.qidoSupportsIncludeField | bool | `false` | enables "include" query support for QIDO-RS |
| ohifConfig.imageRendering | string | `"wadors"` | protocol used to render images |
| ohifConfig.thumbnailRendering | string | `"wadors"` | protocol used to render thumbnails |
| replicas | int | `1` | number of pods maintained for this deployment |
| servicePort | int | `80` | port exposed by the service |
| servicePrefix | string | `"/ohif-viewer"` | path where the service is mounted |
| contentSecurityPolicy.frameAncestors | list | `["'self'"]` | which origins can load this app in an iframe |
| resources.requests.memory | string | `"64Mi"` | requested memory for the pod |
| resources.requests.cpu | string | `"0.1"` | requested cpu for the pod where 1.0 is one "core" or hyperthread |
| resources.limits.memory | string | `"512Mi"` | absolute limit of memory for the pod |
| branchBrowser.enabled | bool | `false` | enable the branch browser route and cronjob |
| branchBrowser.gcloudServiceAccount | string | `nil` | service account json with read access to the fw-ohif bucket |
| branchBrowser.gitlabToken | string | `nil` | access token with api scope for the Gitlab API |
| branchBrowser.syncInterval | string | `"5"` | interval to check for updates in minutes |
| branchBrowser.branchActiveDays | string | `"7"` | number of days back to consider a branch as active |
| branchBrowser.tagActiveDays | string | `"90"` | number of days back to consider a tag as active |
| branchBrowser.pinnedRefs | list | `[]` | tags or branches to always show in the list |
| branchBrowser.resources.requests.memory | string | `"64Mi"` | memory is the requested memory for the pod |
| branchBrowser.resources.requests.cpu | string | `"0.1"` | cpu is the requested cpu for the pod where 1.0 is one "core" or hyperthread |
| branchBrowser.resources.requests.ephemeralStorage | string | `"4Gi"` | ephemeralStorage is the requested ephemeral storage capacity allocated to a container |
| branchBrowser.resources.limits.memory | string | `"512Mi"` | memory is the absolute limit of memory for the pod |
| branchBrowser.resources.limits.ephemeralStorage | string | `"12Gi"` | ephemeralStorage is the absolute limit of ephemeral storage capacity allocated to a container |
| extraEnv | object | `{}` | Additional environment variables |
| ingress | object | `{"domain":"","enabled":false,"proxy":{"bodySize":"50g","buffering":"off","readTimeout":"280","requestBuffering":"off","sendTimeout":"280"},"tlsEnabled":false,"tlsSecretName":""}` | configuration for the ingress resource, this should only be enabled for Central clusters, otherwise ingress is deployed in umbrella |
| ingress.enabled | bool | `false` | enable the ingress resource |
| ingress.domain | string | `""` | domain to use for the ingress |
| ingress.tlsEnabled | bool | `false` | enable tls for the ingress |
| ingress.tlsSecretName | string | `""` | name of the tls secret |
| ingress.proxy.bodySize | string | `"50g"` | bodySize is the maximum allowed size of the client request body |
| ingress.proxy.buffering | string | `"off"` | buffering is the toggle to turn on/off buffering of responses from the proxy server |
| ingress.proxy.readTimeout | string | `"280"` | readTimeout is the allowed timeout for reading the response from the proxy server |
| ingress.proxy.requestBuffering | string | `"off"` | requestBuffering is the toggle to turn on/off buffering of a client request body |
| ingress.proxy.sendTimeout | string | `"280"` | sendTimeout is the allowed timeout for transmitting a request to the proxy server |
| podDisruptionBudget | object | `{"enabled":true,"maxUnavailable":1,"minAvailable":null,"unhealthyPodEvictionPolicy":null}` | configuration for the pod disruption budget |
| podDisruptionBudget.enabled | bool | `true` | enable the pod disruption budget |
| podDisruptionBudget.maxUnavailable | int | `1` | maximum number of unavailable pods |
| podDisruptionBudget.minAvailable | string | `nil` | minimum number of available pods |
| podDisruptionBudget.unhealthyPodEvictionPolicy | string | `nil` | unhealthy pod eviction policy - AlwaysAllow or IfHealthyBudget - requires k8s 1.27+ |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.13.1](https://github.com/norwoodj/helm-docs/releases/v1.13.1)

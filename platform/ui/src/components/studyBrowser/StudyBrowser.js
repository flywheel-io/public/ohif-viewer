import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Thumbnail } from './Thumbnail.js';
import { ToolbarButton } from '@ohif/ui';
import store from '@ohif/viewer/src/store/index.js';
import './StudyBrowser.styl';
import {
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const {
  isCurrentNifti,
  isCurrentVolumetricImage,
  isCurrentWebImage,
  getRouteParams,
} = FlywheelCommonUtils;

const baseURL = window.location.origin;

function StudyBrowser(props) {
  const {
    studies,
    onThumbnailClick,
    onThumbnailDoubleClick,
    supportsDrag,
  } = props;

  const state = store.getState();
  const projectConfig = state.flywheel?.projectConfig;
  const showSessionLink = projectConfig?.showSessionLink;
  const acquisitions = useSelector(store => {
    return store?.flywheel?.acquisitions;
  });

  const getSessionLink = () => {
    if (studies) {
      const { projectId, fileContainerId, StudyInstanceUID } = getRouteParams();
      if (projectId) {
        let sessionId = state.flywheel?.launchedSessionId;
        if (sessionId) {
          return `${baseURL}/#/projects/${projectId}/sessions/${sessionId}`;
        }
        if (
          isCurrentNifti() ||
          isCurrentVolumetricImage() ||
          isCurrentWebImage()
        ) {
          if (fileContainerId) {
            const acquisition = acquisitions?.[fileContainerId];
            sessionId = acquisition?.parents?.session;
          }
        } else {
          const association = state.flywheel?.associations[StudyInstanceUID];
          sessionId = association.session_id;
        }
        return sessionId
          ? `${baseURL}/#/projects/${projectId}/sessions/${sessionId}`
          : null;
      }
    }
  };

  const openSessionLink = () => {
    const sessionLink = getSessionLink();
    if (sessionLink) {
      window.open(sessionLink, '_blank');
    }
  };

  return (
    <div className="study-browser">
      {showSessionLink && Object.keys(acquisitions)?.length && (
        <div className="session-link">
          <a onClick={openSessionLink} target="_blank" title="Open Session">
            <ToolbarButton
              label=""
              key="NewTab"
              icon="open_in_new"
              class="material-icons"
            ></ToolbarButton>
          </a>
        </div>
      )}
      <div className="scrollable-study-thumbnails">
        {studies
          .map((study, studyIndex) => {
            const { StudyInstanceUID } = study;
            return study.thumbnails.map((thumb, thumbIndex) => {
              // TODO: Thumb has more props than we care about?
              const {
                altImageText,
                displaySetInstanceUID,
                imageId,
                InstanceNumber,
                numImageFrames,
                SeriesDescription,
                SeriesNumber,
                stackPercentComplete,
                imageRotation,
              } = thumb;

              return (
                <div
                  key={thumb.displaySetInstanceUID}
                  className="thumbnail-container"
                  data-cy="thumbnail-list"
                >
                  <Thumbnail
                    supportsDrag={supportsDrag}
                    key={`${studyIndex}_${thumbIndex}`}
                    id={`${studyIndex}_${thumbIndex}`} // Unused?
                    // Study
                    StudyInstanceUID={StudyInstanceUID} // used by drop
                    // Thumb
                    altImageText={altImageText}
                    imageId={imageId}
                    InstanceNumber={InstanceNumber}
                    displaySetInstanceUID={displaySetInstanceUID} // used by drop
                    numImageFrames={numImageFrames}
                    SeriesDescription={SeriesDescription}
                    SeriesNumber={SeriesNumber}
                    stackPercentComplete={stackPercentComplete}
                    imageRotation={imageRotation}
                    // Events
                    onClick={onThumbnailClick.bind(
                      undefined,
                      displaySetInstanceUID
                    )}
                    onDoubleClick={onThumbnailDoubleClick}
                  />
                </div>
              );
            });
          })
          .flat()}
      </div>
    </div>
  );
}

const noop = () => {};

StudyBrowser.propTypes = {
  studies: PropTypes.arrayOf(
    PropTypes.shape({
      StudyInstanceUID: PropTypes.string.isRequired,
      thumbnails: PropTypes.arrayOf(
        PropTypes.shape({
          altImageText: PropTypes.string,
          displaySetInstanceUID: PropTypes.string.isRequired,
          imageId: PropTypes.string,
          InstanceNumber: PropTypes.number,
          numImageFrames: PropTypes.number,
          SeriesDescription: PropTypes.string,
          SeriesNumber: PropTypes.number,
          stackPercentComplete: PropTypes.number,
        })
      ),
    })
  ).isRequired,
  supportsDrag: PropTypes.bool,
  onThumbnailClick: PropTypes.func,
  onThumbnailDoubleClick: PropTypes.func,
};

StudyBrowser.defaultProps = {
  studies: [],
  supportsDrag: true,
  onThumbnailClick: noop,
  onThumbnailDoubleClick: noop,
};

// Improve the performance by skipping unwanted rendering of the
// series panel thumbnails and allow update only study/series loading update
// or during the color map change
const areEqual = (prevProps, nextProps) => {
  let isSame = true;
  (nextProps.studies || []).forEach((study, index) => {
    const prevStudy = prevProps.studies?.[index];
    if (!prevStudy || !isSame) {
      isSame = false;
      return;
    }
    study.thumbnails.forEach((thumbnail, thumbIndex) => {
      const prevThumbnail = prevStudy.thumbnails?.[thumbIndex];
      if (!prevThumbnail) {
        isSame = false;
        return;
      }
      if (
        prevThumbnail.stackPercentComplete !== thumbnail.stackPercentComplete ||
        prevThumbnail.colorMapId !== thumbnail.colorMapId
      ) {
        isSame = false;
      }
    });
  });
  return isSame; // If false, then allow re-render else skip the re-rendering
};

const studyBrowserMemo = React.memo(StudyBrowser, areEqual);

export { studyBrowserMemo as StudyBrowser };

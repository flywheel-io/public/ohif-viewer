import React, { memo } from "react";
import classnames from 'classnames';

import { preventPropagation } from "./Utils";
import { Icon } from './../../elements/Icon';

import './UndoRedo.styl';

function UndoRedo(props) {
  const { onClickUndo, onClickRedo } = props;

  return (<div className="display-flex undo-redo-container">
    <div className="display-flex-column-center margin-right" onClick={onClickUndo}>
      <Icon name={"undo"} onClick={(e) => {
        preventPropagation(e);
        onClickUndo();
      }} className={classnames('caret', 'material-icons', 'undo')}
        width="14px" height="14px" />
      <span className='undo-redo-label'>Undo</span>
    </div>
    <div className="display-flex-column-center" onClick={onClickRedo}>
      <Icon name={"redo"} onClick={(e) => {
        preventPropagation(e);
        onClickRedo();
      }} className={classnames('caret', 'material-icons', 'undo')}
        width="14px" height="14px" />
      <span className='undo-redo-label'>Redo</span>
    </div>
  </div>);
}

export default memo(UndoRedo);

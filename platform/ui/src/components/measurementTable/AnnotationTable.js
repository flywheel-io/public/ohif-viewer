import './AnnotationTable.styl';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as FlywheelCommon from '@flywheel/extension-flywheel-common';
import { toolbarModule } from '@ohif/extension-cornerstone';
import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { withTranslation } from '../../contextProviders';
import { Icon } from './../../elements/Icon';
import { OverlayTrigger } from './../overlayTrigger';
import { ScrollableArea } from './../../ScrollableArea/ScrollableArea.js';
import { TableList } from './../tableList';
import { Tooltip } from './../tooltip';
import {
  createChildComponents,
  getQuestionKeysByToolTypes,
  isMeasurementsHidden,
  updateMeasurementVisibility,
  getSeriesData,
} from './Utils';
import { ComponentTypes } from './AnnotationTableComponents';
import UndoRedo from './UndoRedo';
import { CollapseButton } from './CollapseButton/CollapseButton';

const FlywheelCommonUtils = FlywheelCommon.Utils;

const {
  isCurrentFile,
  hasAnonymousReaderTask,
  getTaskByTaskID,
} = FlywheelCommonUtils;

const { selectCurrentWebImage } = FlywheelCommon.Redux.selectors;

const ColorPicker = require('a-color-picker');
const defaultColor = 'rgba(255, 255, 0, 0.2)';

class AnnotationTable extends Component {
  static propTypes = {
    measurementCollection: PropTypes.array.isRequired,
    timepoints: PropTypes.array.isRequired,
    overallWarnings: PropTypes.object.isRequired,
    readOnly: PropTypes.bool,
    onItemClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
    onDeleteGroupMeasurementClick: PropTypes.func,
    onVisibleClick: PropTypes.func,
    onChangeColor: PropTypes.func,
    onEditLabelDescriptionClick: PropTypes.func,
    onEditSubFormClick: PropTypes.func,
    onClickUndo: PropTypes.func,
    onClickRedo: PropTypes.func,
    getImageOrientation: PropTypes.func,
    selectedMeasurementNumber: PropTypes.number,
    t: PropTypes.func,
    saveFunction: PropTypes.func,
    onSaveComplete: PropTypes.func,
    appConfig: PropTypes.object,
    measurements: PropTypes.any,
    dispatchUpdateRoiColor: PropTypes.func,
    dispatchUpdatePaletteColors: PropTypes.func,
    palette: PropTypes.array.isRequired,
    paletteIndex: PropTypes.number.isRequired,
  };

  static defaultProps = {
    overallWarnings: {
      warningList: [],
    },
    readOnly: false,
  };

  state = {
    selectedKey: null,
    selectedColor: defaultColor,
    showColorPicker: false,
    colorPickCoordinates: { left: 0, top: 0 },
    measurementData: null,
    selectedSession: {},
    isMouseEntered: false,
    componentState: {},
  };

  updateState(newState) {
    const newComponentState = { ...this.state.componentState, ...newState };
    this.setState({ componentState: newComponentState });
  }

  componentDidMount() {
    onclick = () => {
      if (this.state.showColorPicker) {
        this.closeColorPicker();
      }

      const event = new CustomEvent('annotationPanel');
      document.dispatchEvent(event);
    };
  }

  render() {
    const { overallWarnings, saveFunction, t, appConfig = {} } = this.props;
    const { enableMeasurementSaveBtn = true } = appConfig;
    const hasOverallWarnings = overallWarnings.warningList.length > 0;

    return (
      <div className="annotationTable">
        <div className="measurementTableHeader">
          {hasOverallWarnings && (
            <OverlayTrigger
              key={'overwall-warning'}
              placement="left"
              overlay={
                <Tooltip
                  placement="left"
                  className="in tooltip-warning"
                  id="tooltip-left"
                >
                  <div className="warningTitle">
                    {t('Criteria nonconformities')}
                  </div>
                  <div className="warningContent">
                    {this.getWarningContent()}
                  </div>
                </Tooltip>
              }
            >
              <span className="warning-status">
                <span className="warning-border">
                  <Icon name="exclamation-triangle" />
                </span>
              </span>
            </OverlayTrigger>
          )}
        </div>
        <div>
          {this.props.measurementCollection.map((measureGroup, index) => (
            <TableList
              key={index}
              customHeader={this.getCustomHeader(measureGroup)}
            >
              <></>
            </TableList>
          ))}
        </div>
        <ScrollableArea>
          <div className="footer-bottom-margin hide-footer-height">
            {this.getMeasurementsGroups()}
          </div>
        </ScrollableArea>
        <div className="measurementTableFooter">
          {saveFunction && enableMeasurementSaveBtn && (
            <button
              onClick={this.saveFunction}
              className="saveBtn"
              data-cy="save-measurements-btn"
            >
              <Icon name="save" width="14px" height="14px" />
              Save measurements
            </button>
          )}
        </div>
        {this.state.showColorPicker && this.getColorSelectorDialog()}
      </div>
    );
  }

  saveFunction = async event => {
    const { saveFunction, onSaveComplete } = this.props;
    if (saveFunction) {
      try {
        const result = await saveFunction();
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: result.message,
            type: 'success',
          });
        }
      } catch (error) {
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: error.message,
            type: 'error',
          });
        }
      }
    }
  };

  getMeasurementsGroups = () => {
    return this.props.measurementCollection.map((measureGroup, index) => {
      return (
        <TableList key={index} customHeader={null}>
          {this.getMeasurementsByUser(measureGroup)}
        </TableList>
      );
    });
  };

  setSubjectList(
    subjectList,
    item,
    subjectId,
    subjectLabel,
    sessionId,
    sessionLabel,
    sliceNumber,
    allSeries,
    studyInstanceUid,
    seriesInstanceUid
  ) {
    if (sliceNumber) {
      item.sliceNumber = sliceNumber;
    }

    const subject = subjectList.find(x => x.key === subjectId) || {
      label: subjectLabel,
      key: subjectId,
      typeOfComponent: ComponentTypes.ContainerTile,
      children: [],
    };
    if (!subjectList.includes(subject)) {
      subjectList.push(subject);
    }

    const session = subject.children.find(s => s.key === sessionId) || {
      label: sessionLabel,
      key: sessionId,
      typeOfComponent: ComponentTypes.ContainerTile,
      children: [],
    };
    if (!subject.children.includes(session)) {
      subject.children.push(session);
    }

    if (FlywheelCommonUtils.isCurrentWebImage()) {
      this.getUserWiseMeasurementListBySession(item, session.children);
    } else {
      const seriesId = studyInstanceUid + '_' + seriesInstanceUid;
      let series =
        session.children.find(s => s.key === seriesId) ||
        getSeriesData(allSeries, studyInstanceUid, seriesInstanceUid);
      if (!session.children.includes(series)) {
        session.children.push(series);
      }
      this.getUserWiseMeasurementListBySession(item, series.children);
    }
    return subjectList;
  }

  getUserWiseMeasurementListBySession(measurement, userWiseMeasurementList) {
    const isCurrentUserRestricted = hasAnonymousReaderTask(store.getState());
    const user = userWiseMeasurementList.find(
      u =>
        u.key === measurement.task_id ||
        (measurement.user && u.key === measurement.user?.email)
    );
    if (measurement.task_id) {
      const task = getTaskByTaskID(measurement.task_id);
      this.groupByUserOrTask(
        user,
        isCurrentUserRestricted,
        measurement,
        userWiseMeasurementList,
        task
      );
    } else {
      this.groupByUserOrTask(
        user,
        isCurrentUserRestricted,
        measurement,
        userWiseMeasurementList
      );
    }

    return userWiseMeasurementList;
  }

  groupByUserOrTask(
    user,
    isCurrentUserRestricted,
    measurement,
    userWiseMeasurementList,
    readerTask = null
  ) {
    // to create new user
    if (!user) {
      const allUsers = store.getState().flywheel.allUsers;
      const userObj = {};
      userObj.label = isCurrentUserRestricted
        ? measurement.task_id
        : measurement.task_id
        ? readerTask?.assignee
        : measurement.user?.email || '';
      userObj.key = measurement.task_id || userObj.label;
      userObj.typeOfComponent = ComponentTypes.UserTile;
      userObj.data =
        measurement?.user?.email !== userObj.label
          ? allUsers[readerTask?.assignee]
          : { ...measurement.user };
      userObj.children = [];
      userWiseMeasurementList.push(userObj);
      user = userObj;
    }

    // to create label object
    if (!user.children.some(x => x.key === measurement.location)) {
      const newObj = {};
      newObj.label = measurement.label;
      newObj.key = measurement.location;
      newObj.typeOfComponent = ComponentTypes.LabelTile;
      newObj.data = { ...measurement };
      newObj.children = [];
      user.children.push(newObj);
    }

    const label = user.children.find(x => x.key === measurement.location);
    // to create measurement object
    if (!label.children.find(x => x.key === measurement.measurementId)) {
      label.children.push(measurement);
    }
  }

  getMeasurementList(measureGroup) {
    const state = store.getState();
    const measurements = state.timepointManager.measurements;
    const projectConfig = state.flywheel?.projectConfig;
    const measurementCategories = this.getMeasurementCategories();
    const isFile = isCurrentFile(state);

    if (FlywheelCommonUtils.isCurrentWebImage()) {
      const allSessions = state.flywheel.allSessions;
      const sessionId = selectCurrentWebImage(state)?.parents?.session;
      measureGroup.measurements.forEach(item => {
        Object.keys(measurements).forEach(measurementKey => {
          let m = measurements[measurementKey];
          m = m.find(x => x._id === item.measurementId);
          let sliceNumber = null;
          if (!isFile && projectConfig?.bSlices && m) {
            sliceNumber = m.sliceNumber;
          }
          if (m) {
            const parentCategory = measurementCategories.find(x =>
              x.types.includes(item.toolType)
            );
            this.setSubjectList(
              parentCategory.children,
              item,
              allSessions[sessionId]?.parents?.subject,
              allSessions[sessionId]?.subject?.label,
              sessionId,
              allSessions[sessionId]?.label,
              sliceNumber
            );
          }
        });
      });
    } else {
      const studyAssociations = {};
      measureGroup.measurements.forEach(item => {
        Object.keys(measurements).forEach(measurementKey => {
          let m = measurements[measurementKey];
          m = m.find(x => x._id === item.measurementId);
          let sliceNumber = null;
          if (!isFile && projectConfig?.bSlices && m) {
            sliceNumber = m.sliceNumber;
          }
          if (m) {
            const studyInstanceUid = m.StudyInstanceUID || m.studyInstanceUid;
            const seriesInstanceUid =
              m.SeriesInstanceUID || m.seriesInstanceUID;
            let studyAssociation = studyAssociations?.[studyInstanceUid];

            if (!studyAssociation) {
              studyAssociation = state.flywheel.allAssociations.filter(
                assoc => assoc.study_uid === studyInstanceUid
              );
              studyAssociations[studyInstanceUid] = studyAssociation;
            }
            const association = studyAssociation.find(
              assoc => assoc.series_uid === seriesInstanceUid
            );
            if (association?.study_uid) {
              const sessions = state.flywheel.allSessions;
              const session = sessions[association.session_id];
              const { studyMetadataManager } = OHIF.utils;
              const study = studyMetadataManager.get(studyInstanceUid);
              const allSeries = study?.getSeries?.() || [];
              if (session) {
                const parentCategory = measurementCategories.find(x =>
                  x.types.includes(item.toolType)
                );
                const subject = session.subject;
                this.setSubjectList(
                  parentCategory?.children,
                  item,
                  subject._id,
                  subject.label,
                  session._id,
                  session.label,
                  sliceNumber,
                  allSeries,
                  studyInstanceUid,
                  seriesInstanceUid
                );
              }
            }
          }
        });
      });
    }
    return measurementCategories;
  }

  getMeasurementListForVolumetricImage(measureGroup, volumetricImages) {
    const state = store.getState();
    const allSessions = state.flywheel.allSessions;
    const measurements = state.timepointManager.measurements;
    const measurementCategories = this.getMeasurementCategories();
    (volumetricImages || []).forEach(volumetricImage => {
      if (
        volumetricImage.parents &&
        allSessions[volumetricImage.parents.session]
      ) {
        const session = allSessions[volumetricImage.parents.session];
        const subject = session.subject;

        measurementCategories.forEach(category => {
          let isNewSession = false;
          let isNewSubject = false;
          const subjectObj =
            category.children.find(child => child.key === subject._id) || {};
          const sessionObj =
            subjectObj.children?.find(
              child => child.key === volumetricImage.parents.session
            ) || {};
          let seriesObj = {};
          if (!subjectObj.key) {
            subjectObj.key = subject._id;
            subjectObj.label = subject.label;
            subjectObj.typeOfComponent = ComponentTypes.ContainerTile;
            subjectObj.children = [];
            isNewSubject = true;
          }

          if (!sessionObj.key) {
            sessionObj.key = volumetricImage.parents.session;
            sessionObj.label = session.label;
            sessionObj.typeOfComponent = ComponentTypes.ContainerTile;
            sessionObj.children = [];
            isNewSession = true;
          }

          measureGroup.measurements.map(measurement => {
            if (category.types.includes(measurement.toolType)) {
              const measureInfo = (
                measurements[measurement.toolType] || []
              ).find(x => x._id === measurement.measurementId);
              if (measureInfo) {
                const studyInstanceUID =
                  measureInfo.StudyInstanceUID || measureInfo.studyInstanceUid;
                const seriesInstanceUID =
                  measureInfo.SeriesInstanceUID ||
                  measureInfo.seriesInstanceUID;
                const { studyMetadataManager } = OHIF.utils;
                const study = studyMetadataManager.get(studyInstanceUID);
                const allSeries = study?.getSeries?.() || [];
                const series = allSeries
                  .find(
                    s => s?.getData?.().SeriesInstanceUID === seriesInstanceUID
                  )
                  ?.getData();
                const seriesId = studyInstanceUID + '_' + seriesInstanceUID;
                const seriesLabel = series?.SeriesDescription
                  ? series.SeriesDescription
                  : series?.SeriesNumber !== undefined
                  ? series.SeriesNumber
                  : series?.SeriesInstanceUID || seriesInstanceUID;
                seriesObj =
                  sessionObj.children?.find(child => child.key === seriesId) ||
                  {};

                if (!seriesObj.key) {
                  seriesObj.key = seriesId;
                  seriesObj.label = seriesLabel;
                  seriesObj.typeOfComponent = ComponentTypes.ContainerTile;
                  seriesObj.children = [];
                }

                if (
                  studyInstanceUID?.toLowerCase() ===
                  volumetricImage.filename?.toLowerCase()
                ) {
                  this.getUserWiseMeasurementListBySession(
                    measurement,
                    seriesObj.children
                  );
                  if (!sessionObj.children.includes(seriesObj)) {
                    sessionObj.children.push(seriesObj);
                  }
                }
              }
            }
          });
          if (isNewSession && sessionObj.children?.length) {
            subjectObj.children.push(sessionObj);
          }
          if (isNewSubject && subjectObj.children?.length) {
            category.children.push(subjectObj);
          }
        });
      }
    });
    return measurementCategories;
  }

  getMeasurementCategories() {
    let toolbarDefinitions = [];
    if (toolbarModule.definitions) {
      toolbarDefinitions = toolbarModule.definitions;
    }
    const measurementGroupTypes = [];
    const roiObj = {};
    const measureObj = {};
    const annotationObj = {};

    roiObj.label = 'Region of Interest';
    roiObj.key = 'ROI';
    roiObj.typeOfComponent = ComponentTypes.HeaderTile;
    roiObj.types = [];
    roiObj.children = [];

    measureObj.label = 'Measurements';
    measureObj.key = 'Measure';
    measureObj.typeOfComponent = ComponentTypes.HeaderTile;
    measureObj.types = [];
    measureObj.children = [];

    annotationObj.label = 'Annotations';
    annotationObj.key = 'Annotate';
    annotationObj.typeOfComponent = ComponentTypes.HeaderTile;
    annotationObj.types = [];
    annotationObj.children = [];

    const annotationDefinitions = toolbarDefinitions.find(
      button => button.id === 'AnnotateMenu'
    );
    roiObj.types = this.getToolsByGroup(annotationDefinitions.buttons, 'ROI');
    measureObj.types = this.getToolsByGroup(
      annotationDefinitions.buttons,
      'Measure'
    );
    annotationObj.types = this.getToolsByGroup(
      annotationDefinitions.buttons,
      'SinglePoint'
    );
    measurementGroupTypes.push(roiObj);
    measurementGroupTypes.push(measureObj);
    measurementGroupTypes.push(annotationObj);
    return measurementGroupTypes;
  }

  getToolsByGroup(toolDefentitions, group) {
    return toolDefentitions
      .filter(
        button =>
          button.options?.group === group && button.commandOptions?.toolName
      )
      .map(button => button.commandOptions.toolName);
  }

  onMouseEnter(e) {
    if (!this.state.isMouseEntered) {
      this.setState({ isMouseEntered: true }, () => {
        this.props.setMeasurementColorIntensity(true);
      });
    }
  }

  onMouseLeave(e) {
    this.setState({ isMouseEntered: false }, () => {
      this.props.setMeasurementColorIntensity(false);
    });
  }

  getMeasurementsByUser = measureGroup => {
    const state = store.getState();
    let currentVolumetricImage =
      state.flywheel.currentMetaImage || state.flywheel.currentNiftis;
    let annotationCategories = [];
    const volumetricImages = Array.isArray(currentVolumetricImage)
      ? currentVolumetricImage
      : currentVolumetricImage
      ? [currentVolumetricImage]
      : [];
    if (volumetricImages?.length > 0) {
      annotationCategories = this.getMeasurementListForVolumetricImage(
        measureGroup,
        volumetricImages
      );
    } else {
      annotationCategories = this.getMeasurementList(measureGroup);
    }

    const newProps = {
      parentProps: {
        ...this.props,
        onClickColorPicker: this.onClickColorPicker,
        isMouseEntered: this.state.isMouseEntered,
        componentState: this.state.componentState,
        updateState: newState => this.updateState(newState),
        onItemClick: this.onItemClick,
        closeColorPicker: this.closeColorPicker,
        questionKeysByToolTypes: getQuestionKeysByToolTypes(),
        onMouseEnter: this.onMouseEnter.bind(this),
        onMouseLeave: this.onMouseLeave.bind(this),
        tableType: 'ANNOTATION_TABLE',
      },
    };
    const childComponents = createChildComponents(annotationCategories, {
      ...newProps,
    });
    return <div className="subject-container">{childComponents}</div>;
  };

  onItemClick = (event, measurementData) => {
    this.closeColorPicker();
    if (this.props.readOnly) return;

    const { measurements } = this.props;

    const measurement = measurements[measurementData.toolType].find(
      m => m.measurementNumber === measurementData.measurementNumber
    );

    this.setState({
      selectedKey: measurementData.measurementNumber,
      selectedColor: measurement.color || defaultColor,
      measurementData,
    });

    if (this.props.onItemClick) {
      this.props.onItemClick(event, measurementData);
    }
  };

  onVisibleClick(e, measurements) {
    updateMeasurementVisibility(e, measurements, this.props.onVisibleClick);
  }

  getCustomHeader = measureGroup => {
    const isVisible = !isMeasurementsHidden(measureGroup.measurements);
    const dim = isVisible ? '' : 'dim';
    const iconStyle = `icon-color ${dim}`;
    return (
      <React.Fragment>
        <div className="tableListHeaderTitle flex-align-center">
          <CollapseButton />
          Annotations{' '}
          <div className="total-measurement-count">
            {measureGroup.measurements.length}
          </div>
        </div>
        {measureGroup.maxMeasurements && (
          <div className="maxMeasurements">
            {this.props.t('MAX')} {measureGroup.maxMeasurements}
          </div>
        )}
        <div
          className={`${iconStyle} flex-align-center margin-right`}
          onClick={e => this.onVisibleClick(e, measureGroup.measurements)}
        >
          <Icon
            className="caret"
            name={isVisible ? 'eye' : 'eye-closed'}
            width="20px"
            height="20px"
          />
        </div>
        <UndoRedo
          onClickUndo={this.props.onClickUndo}
          onClickRedo={this.props.onClickRedo}
        />
      </React.Fragment>
    );
  };

  getWarningContent = () => {
    const { warningList = '' } = this.props.overallWarnings;

    if (Array.isArray(warningList)) {
      const listedWarnings = warningList.map((warn, index) => {
        return <li key={index}>{warn}</li>;
      });

      return <ol>{listedWarnings}</ol>;
    } else {
      return <React.Fragment>{warningList}</React.Fragment>;
    }
  };

  // To Show the color selector dialog
  onClickColorPicker = (event, measurementData) => {
    const screenHeight = window.innerHeight;

    // Align dialog 20px above or below from clicked point to not overlap the picker dialog over the palette and other buttons.
    const offset = 20;

    const pickerSize = {
      height: 297, // Color selector dialog height
      width: 232, // Color selector dialog width
    };

    const colorPickCoordinates = {
      left: event.clientX - pickerSize.width / 2,
      top:
        screenHeight / 2 > event.clientY
          ? event.clientY + offset
          : event.clientY - offset - pickerSize.height,
    };

    let selectedColor = defaultColor;
    let currentData = {};
    if (Array.isArray(measurementData)) {
      currentData = measurementData[0];
    } else {
      currentData = measurementData;
    }
    const measure = this.props.measurements[currentData.toolType].find(
      x => x._id === currentData.measurementId
    );
    if (measure) {
      selectedColor = measure.color;
    }

    this.setState({ showColorPicker: false }, () => {
      this.setState(
        {
          showColorPicker: true,
          colorPickCoordinates,
          measurementData,
          selectedColor,
        },
        () => this.onColorChangeHandler()
      );
    });
  };

  closeColorPicker = () => {
    const { measurements } = this.props;
    this.props.dispatchUpdatePaletteColors(measurements);
    this.setState({ showColorPicker: false });
  };

  onColorChangeHandler = () => {
    ColorPicker.from('.picker').on('change', picker => {
      const { palette } = this.props;
      const selectedColor = `rgba(${picker.rgba.join(',')})`;
      this.setState(
        {
          selectedColor,
        },
        () => {
          if (Array.isArray(this.state.measurementData)) {
            this.state.measurementData.forEach(x =>
              this.props.onChangeColor(selectedColor, x)
            );
          } else {
            this.props.onChangeColor(selectedColor, this.state.measurementData);
          }

          this.props.dispatchUpdateRoiColor({
            current: selectedColor,
            next: palette[this.props.paletteIndex],
          });
        }
      );
    });
  };

  getColorSelectorDialog = () => {
    return (
      <div
        className="color-picker"
        style={{
          left: this.state.colorPickCoordinates.left,
          top: this.state.colorPickCoordinates.top,
        }}
        onClick={e => e.stopPropagation()}
      >
        <div
          className="picker"
          acp-color={this.state.selectedColor}
          acp-palette={this.props.palette.join('|')}
          acp-show-rgb="no"
          acp-show-hsl="no"
          acp-show-hex="no"
          acp-show-alpha="yes"
          acp-palette-editable="no"
          acp-use-alpha-in-palette="no"
        ></div>
      </div>
    );
  };
}

const connectedComponent = withTranslation(['AnnotationTable', 'Common'])(
  AnnotationTable
);
export { connectedComponent as AnnotationTable };
export default connectedComponent;

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

function extractStyleChunks(isProdBuild) {
  return [
    {
      test: /\.styl$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader,
        },
        { loader: 'css-loader' },
        { loader: 'stylus-loader' },
      ],
    },
    {
      test: /\.(sa|sc|c)ss$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader,
        },
        'css-loader',
        'postcss-loader',
        // 'sass-loader',
      ],
    },
  ];
}

module.exports = extractStyleChunks;




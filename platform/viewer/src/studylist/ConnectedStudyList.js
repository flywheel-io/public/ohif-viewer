import { connect } from 'react-redux';

import StudyListRoute from './StudyListRoute.js';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';

const { selectStudySessions, selectStudyList } = FlywheelRedux.selectors;
const {
  setStudySort,
  setStudyFilters,
  setStudyOrdering,
} = FlywheelRedux.actions;

const isActive = a => a.active === true;

const mapStateToProps = state => {
  const activeServer = state.servers.servers.find(isActive);

  return {
    filtersAndSort: selectStudyList(state),
    server: activeServer,
    studySessions: selectStudySessions(state),
    user: state.oidc.user,
  };
};

const mapDispatchToProps = { setStudySort, setStudyFilters, setStudyOrdering };

const ConnectedStudyList = connect(
  mapStateToProps,
  mapDispatchToProps
)(StudyListRoute);

export default ConnectedStudyList;

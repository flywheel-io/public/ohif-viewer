import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';

import OHIF, { utils } from '@ohif/core';
import { servicesManager as appServiceManager } from './../../App.js';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';
import { smAnnotationHandler } from '@ohif/extension-dicom-microscopy';
import getMeasurementLocationCallback from './getMeasurementLocationCallback';
import store from '../../store/index.js';
import {
  displaySetContainsSopInstance,
  syncMeasurementsAndToolData,
} from './Utils';
const triggerEvent = cornerstoneTools.importInternal('util/triggerEvent');
const { DeleteDialog, PermissionDialog } = FlywheelComponents;
const {
  MeasurementApi,
  getImageIdForImagePath,
  getModality,
  MeasurementHandlers,
} = OHIF.measurements;
const {
  cornerstoneUtils,
  getDefaultROILabelFromHPLayout,
} = FlywheelCommonUtils;

const { studyMetadataManager } = utils;
const {
  setActiveTool,
  setAvailableLabels,
  setHoveredMeasurement,
  subFormRoiState,
  setActiveQuestion,
} = FlywheelCommonRedux.actions;
const {
  annotationOperations,
  handleAnnotationChange,
  editLabel,
} = smAnnotationHandler;

const {
  isCurrentWebImage,
  getQuestionAndAnswerForMeasurement,
  activateToolsAndLabelsForAnswer,
  getBSliceSettings,
  getToolsAndToolTypeForSelectedOption,
} = FlywheelCommonUtils;

const { EVENTS } = cornerstoneTools;

const popupFromLeft = 175;
const popupFromTop = 220;
let measurementDeleteDispatchTimer = 0;

const triggerRoiContourUpdateEvent = (
  measurementData,
  eventType,
  enabledElement = null
) => {
  const enabledElements = enabledElement
    ? [enabledElement]
    : cornerstone.getEnabledElements();
  enabledElements.forEach(enabledElement => {
    const viewportSeriesUID = cornerstone.metaData.get(
      'SeriesInstanceUID',
      enabledElement.image.imageId
    );
    if (measurementData?.SeriesInstanceUID !== viewportSeriesUID) {
      return;
    }
    const eventName =
      measurementData.toolType + 'measurementpropertychangeevent';
    triggerEvent(enabledElement.element, eventName, {
      measurementData,
      eventType,
    });
  });
};

const triggerMeasurementRemoveEvent = measurementData => {
  const eventType = EVENTS.MEASUREMENT_REMOVED;
  const eventData = {
    toolName: measurementData.toolType,
    toolType: measurementData.toolType,
    measurementData: measurementData,
  };
  const enabledElements = cornerstone.getEnabledElements();
  enabledElements.forEach(enabledElement => {
    triggerEvent(enabledElement.element, eventType, eventData);
  });
};

export const deleteAnnotations = (measurements, toolActivationContext) => {
  const state = store.getState();
  const allMeasurements = state.timepointManager.measurements;
  const defaultRoiLabelDetails = getDefaultROILabelFromHPLayout();
  const viewportsState = state.viewports;
  const timepointManagerState = state.timepointManager;
  let isFOVGridUpdate = false;
  let defaultRoiLabelsForUpdate;

  MeasurementHandlers.navigateToMeasurementImage(
    measurements[0],
    viewportsState,
    timepointManagerState
  );
  measurements = measurements.reverse();

  measurements.forEach(measurement => {
    const toolType = measurement.toolType;
    let SeriesInstanceUID;

    Object.keys(allMeasurements).forEach(tool => {
      if (tool === toolType) {
        allMeasurements[tool]?.forEach(annotation => {
          if (annotation.uuid === measurement.uuid) {
            SeriesInstanceUID = annotation.SeriesInstanceUID;
          }
        });
      }
    });

    const defaultRoiLabels = defaultRoiLabelDetails[SeriesInstanceUID];
    const fovGridUpdate = measurements.some(measurement => {
      return defaultRoiLabels?.includes(measurement.location);
    });

    if (fovGridUpdate) {
      isFOVGridUpdate = fovGridUpdate;
      defaultRoiLabelsForUpdate = defaultRoiLabels;
    }
  });

  const measurementApi = MeasurementApi.Instance;
  const alreadyRemovedContours = [];
  measurements.forEach(measurement => {
    const collection = measurementApi.tools[measurement.toolType];
    const measurementData = collection.find(
      t => t._id === measurement.measurementId
    );

    if (getModality(store.getState()) === 'SM') {
      handleAnnotationChange(annotationOperations.DELETE_ANNOTATION, {
        toolType: measurement.toolType,
        measurementData: {
          _id: measurement.measurementId,
          lesionNamingNumber: measurement.lesionNamingNumber,
          measurementNumber: measurement.measurementNumber,
        },
      });
    }

    if (
      measurement.ROIContourUid &&
      alreadyRemovedContours.indexOf(measurement.ROIContourUid) === -1 &&
      measurementData
    ) {
      // If contour roi type, then delete only one, the remaining will be deleted as sub action
      alreadyRemovedContours.push(measurement.ROIContourUid);
      triggerMeasurementRemoveEvent(measurementData);
      triggerRoiContourUpdateEvent(measurementData, 'Delete');
    } else if (!measurement.ROIContourUid && measurementData) {
      triggerMeasurementRemoveEvent(measurementData);
    }

    const skipToolActivation =
      !toolActivationContext ||
      measurement.uuid === toolActivationContext?.uuid;

    if (skipToolActivation) {
      clearTimeout(measurementDeleteDispatchTimer);
      const { question, selectedOption } = getQuestionAndAnswerForMeasurement(
        measurement
      );
      measurementDeleteDispatchTimer = setTimeout(() => {
        activateToolsAndLabelsForAnswer(
          question,
          selectedOption,
          measurement.sliceNumber
        );
        if (question && selectedOption) {
          const isSubForm = measurement.isSubForm;
          store.dispatch(
            setActiveQuestion({
              ...question,
              answer: measurement.answer,
              questionKey: measurement.questionKey,
              isSubForm: isSubForm || false,
              subFormName: isSubForm ? measurement.subFormName : '',
              subFormAnnotationId: isSubForm
                ? measurement.subFormAnnotationId
                : '',
              question: isSubForm ? measurement.question : '',
            })
          );
        }
        const { toolType } = getToolsAndToolTypeForSelectedOption(
          measurement,
          selectedOption
        );
        store.dispatch(setActiveTool(toolType));
      }, 1);
    }
  });

  if (isFOVGridUpdate) {
    const { commandsManager } = window.ohif.app;
    const plugin = 'cornerstone';
    const propertiesToSync = {
      defaultRoiLabels: defaultRoiLabelsForUpdate,
    };
    commandsManager?.runCommand('crossViewportSync', {
      plugin,
      propertiesToSync,
    });
  }
};

const dispatchActions = {
  onItemClick: (event, measurementData, propsFromState, propsFromDispatch) => {
    const viewportsState = propsFromState.viewports;
    const timepointManagerState = propsFromState.timepointManager;

    const options = {
      invertViewportTimepointsOrder: false,
      childToolKey: null,
    };

    propsFromDispatch.dispatchJumpToRowItem(
      measurementData,
      viewportsState,
      timepointManagerState,
      options
    );
    // Allow measurement editing, when available tools have data, in case of like study summary updates
    const tools = store.getState().infusions.availableTools;
    if (tools && tools.length > 0) {
      store.dispatch(setActiveTool(measurementData.toolType));
    }
  },
  getImageOrientation: (measurementData, reduxData) => {
    const { timepointManager } = reduxData;
    const measure = timepointManager.measurements[
      measurementData.toolType
    ].find(item => item._id === measurementData.measurementId);
    let orientation = 'N/A';
    const dataStudyInstanceUID = measure.StudyInstanceUID;
    const study = studyMetadataManager.get(dataStudyInstanceUID);
    if (measure && study) {
      const imageId = getImageIdForImagePath(measure.imagePath);
      if (
        ['Axial', 'Coronal', 'Sagittal'].includes(measure.SeriesInstanceUID)
      ) {
        orientation = measure.SeriesInstanceUID;
      } else if (imageId) {
        orientation = cornerstoneUtils.getNormalAxisOrientation(imageId);
      }
      if (orientation === 'Axial') {
        orientation = 'AXI';
      } else if (orientation === 'Coronal') {
        orientation = 'COR';
      } else if (orientation === 'Sagittal') {
        orientation = 'SAG';
      }
    }
    return orientation;
  },
  onDeleteGroupMeasurementClick: (event, measurements, data) => {
    const { servicesManager } = data;
    const { UIDialogService } = servicesManager.services;
    let helpDialogId = null;

    if (helpDialogId) {
      UIDialogService.dismiss({ id: helpDialogId });
      helpDialogId = null;
      return;
    }

    const bSliceSettings = getBSliceSettings();
    const measurementListForDelete = [...measurements];
    const toolActivationContext = measurements[measurements.length - 1];
    const state = store.getState();
    const studyForm = state.flywheel.projectConfig?.studyForm;
    measurements.length &&
      measurements.forEach(measurement => {
        if (!measurement.isSubForm && studyForm?.components?.length) {
          const question = studyForm.components.find(
            component => component.key === measurement.questionKey
          );
          if (question?.values?.length > 0) {
            const answer =
              typeof measurement.answer === 'object'
                ? measurement.answer.value
                : measurement.answer;
            const subForm = question.values.find(
              value => value.value === answer
            )?.subForm;
            if (subForm) {
              const allMeasurements = state.timepointManager.measurements;
              Object.keys(allMeasurements).forEach(key => {
                allMeasurements[key].forEach(tool => {
                  if (
                    tool.question === measurement.questionKey &&
                    (!bSliceSettings ||
                      tool.sliceNumber === measurement.sliceNumber) &&
                    tool.subFormAnnotationId === measurement.uuid
                  ) {
                    tool.measurementId = tool._id;
                    measurementListForDelete.push(tool);
                  }
                });
              });
            }
          }
        }
      });

    const deleteDialogEvent = new CustomEvent('deleteDialog');
    document.dispatchEvent(deleteDialogEvent);

    helpDialogId = UIDialogService.create({
      content: DeleteDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: true,
      contentProps: {
        measurements: measurementListForDelete,
        servicesManager: servicesManager,
        label: 'Do you want to delete this measurement group?',
        onClose: () => {
          UIDialogService.dismiss({ id: helpDialogId });
          helpDialogId = null;
        },
        onConfirm: () => {},
        onDelete: deleteAnnotations,
        toolActivationContext,
      },
    });
  },
  onDeleteClick: (event, measurementData, data, skipToolActivation = false) => {
    const state = store.getState();
    const { MeasurementHandlers } = OHIF.measurements;
    const bSliceSettings = getBSliceSettings();
    const servicesManager = data?.servicesManager || appServiceManager;
    let subFormLabel = '';
    const studyForm = state.flywheel.projectConfig?.studyForm;
    const measurementListForDelete = [measurementData];
    const toolActivationContext = skipToolActivation ? {} : measurementData;

    if (!measurementData.isSubForm && studyForm?.components?.length) {
      const question = studyForm.components.find(
        component => component.key === measurementData.questionKey
      );
      if (question?.values?.length > 0) {
        const answer =
          typeof measurementData.answer === 'object'
            ? measurementData.answer.value
            : measurementData.answer;
        const subForm = question.values.find(value => value.value === answer)
          ?.subForm;

        if (subForm) {
          const measurements = state.timepointManager.measurements;
          Object.keys(measurements).forEach(key => {
            measurements[key].forEach(tool => {
              if (
                tool.question === measurementData.questionKey &&
                (!bSliceSettings ||
                  tool.sliceNumber === measurementData.sliceNumber) &&
                tool.subFormAnnotationId === measurementData.uuid
              ) {
                tool.measurementId = tool._id;
                measurementListForDelete.push(tool);
              }
            });
          });
          subFormLabel =
            'Do you want to delete this annotation? This annotation contains a sub form response.';
        }
      }
    }

    if (subFormLabel !== '') {
      const { UIDialogService } = servicesManager.services;
      let helpDialogId = null;

      if (helpDialogId) {
        UIDialogService.dismiss({ id: helpDialogId });
        helpDialogId = null;
        return;
      }

      const deleteDialogEvent = new CustomEvent('deleteDialog');
      document.dispatchEvent(deleteDialogEvent);

      helpDialogId = UIDialogService.create({
        content: PermissionDialog,
        defaultPosition: {
          x: window.innerWidth / 2 - popupFromLeft,
          y: window.innerHeight / 2 - popupFromTop,
        },
        showOverlay: true,
        contentProps: {
          measurements: measurementListForDelete,
          label: subFormLabel,
          onClose: () => {
            UIDialogService.dismiss({ id: helpDialogId });
            helpDialogId = null;
          },
          onConfirm: () => {},
          onDelete: deleteAnnotations,
          toolActivationContext: toolActivationContext,
        },
      });
    } else {
      deleteAnnotations([measurementData], toolActivationContext);
    }
  },
  onChangeColor: (color, measurementData) => {
    const { MeasurementHandlers } = OHIF.measurements;
    const measurementApi = MeasurementApi.Instance;
    const collection = measurementApi.tools[measurementData.toolType];
    const measurement = collection.find(
      t => t._id === measurementData.measurementId && color !== t.color
    );
    // If already updated the color then return(ex: in case of contour roi, applying to whole group).
    if (!measurement) {
      return;
    }

    if (getModality(store.getState()) === 'SM') {
      handleAnnotationChange(annotationOperations.CHANGE_COLOR, {
        toolType: measurementData.toolType,
        measurementData: {
          _id: measurementData.measurementId,
          lesionNamingNumber: measurementData.lesionNamingNumber,
          measurementNumber: measurementData.measurementNumber,
          color: color,
        },
      });
      MeasurementHandlers.onModified({
        detail: {
          toolType: measurementData.toolType,
          measurementData: {
            _id: measurementData.measurementId || measurementData._id,
            lesionNamingNumber: measurementData.lesionNamingNumber,
            measurementNumber: measurementData.measurementNumber,
            color: color,
          },
        },
      });
    } else {
      const enabledElements = cornerstone.getEnabledElements();
      enabledElements.forEach(enabledElement => {
        const image = cornerstone.getImage(enabledElement.element);
        const viewportSeriesUID = cornerstone.metaData.get(
          'SeriesInstanceUID',
          image.imageId
        );
        if (measurement.SeriesInstanceUID !== viewportSeriesUID) {
          return;
        }
        MeasurementHandlers.onModified({
          detail: {
            toolType: measurementData.toolType,
            element: enabledElement.element,
            measurementData: {
              _id: measurementData.measurementId || measurementData._id,
              lesionNamingNumber: measurementData.lesionNamingNumber,
              measurementNumber: measurementData.measurementNumber,
              color: color,
            },
          },
        });
        if (measurementData.ROIContourUid) {
          triggerRoiContourUpdateEvent(measurement, 'Modify', enabledElement);
        }

        Object.keys(measurementApi.tools).forEach(toolType => {
          const measurements = measurementApi.tools[toolType];

          measurements.forEach(measurement => {
            measurement.active = false;
          });
        });

        measurementApi.syncMeasurementsAndToolData();
        cornerstone.updateImage(enabledElement.element);
      });
    }
  },
  dispatchJumpToRowItem: (
    measurementData,
    viewportsState,
    timepointManagerState,
    options
  ) => {
    MeasurementHandlers.onItemClick(measurementData, options);
  },
  dispatchEditLabelDescription: (
    event,
    measurementData,
    viewportsState,
    tableType
  ) => {
    event.persist();

    const { toolType, measurementId } = measurementData;
    const tool = MeasurementApi.Instance.tools[toolType].find(measurement => {
      return measurement._id === measurementId;
    });

    const options = {
      editLabelDescriptionOnDialog: true,
      tableType,
    };

    if (getModality(store.getState()) === 'SM') {
      editLabel(event, tool, options, viewportsState);
    } else {
      const activeViewportIndex =
        (viewportsState && viewportsState.activeViewportIndex) || 0;
      const enabledElements = cornerstone.getEnabledElements();
      if (!enabledElements || enabledElements.length <= activeViewportIndex) {
        OHIF.log.error('Failed to find the enabled element');
        return;
      }
      const { element } = enabledElements[activeViewportIndex];

      const eventData = {
        event: {
          clientX: event.clientX,
          clientY: event.clientY,
        },
        element,
      };

      getMeasurementLocationCallback(eventData, tool, options);
    }
  },
  dispatchEditSubForm: measurementData => {
    store.dispatch(
      subFormRoiState({ status: true, measurementData: measurementData })
    );
  },
  changeVisibilityInViewerScope: (measurementData, notifyUpdateAndSync) => {
    // This will neither mark measurement field dirty as true, nor make the visibility changes undo-able.
    const measurementApi = MeasurementApi.Instance;
    const collection = measurementApi.tools[measurementData.toolType];
    const measurement = collection.find(
      t => t._id === measurementData.measurementId
    );
    const { lesionNamingNumber, timepointId } = measurement;
    const measurementTypeId =
      measurementApi.toolsGroupsMap[measurementData.toolType];
    measurementApi.setMeasurementVisibility(
      measurementData.toolType,
      measurementTypeId,
      {
        lesionNamingNumber,
        timepointId,
      },
      measurementData.visible,
      notifyUpdateAndSync
    );
  },
  onVisibleClick: (event, measurementData, notifyUpdateAndSync = true) => {
    const { MeasurementHandlers } = OHIF.measurements;
    const measurementApi = MeasurementApi.Instance;
    const collection = measurementApi.tools[measurementData.toolType];
    const measurement = collection.find(
      t => t._id === measurementData.measurementId
    );
    if (getModality(store.getState()) === 'SM') {
      handleAnnotationChange(annotationOperations.TOGGLE_VISIBILITY, {
        toolType: measurementData.toolType,
        measurementData: {
          _id: measurementData.measurementId,
          lesionNamingNumber: measurementData.lesionNamingNumber,
          measurementNumber: measurementData.measurementNumber,
        },
      });
    }

    MeasurementHandlers.onVisible(
      {
        detail: {
          toolType: measurementData.toolType,
          measurementData: {
            _id: measurementData.measurementId,
            lesionNamingNumber: measurementData.lesionNamingNumber,
            measurementNumber: measurementData.measurementNumber,
          },
        },
      },
      !measurementData.visible,
      notifyUpdateAndSync
    );
    if (measurementData.ROIContourUid) {
      triggerRoiContourUpdateEvent(measurement, 'Modify');
    }
  },
  dispatchActiveHoveredMeasurements: (measurements, dispatch) => {
    const state = store.getState();
    if (getModality(state) === 'SM') {
      handleAnnotationChange(
        annotationOperations.HiGHLIGHT_HOVERED_ANNOTATIONS,
        measurements
      );
    } else {
      dispatch(setHoveredMeasurement(measurements));
    }
    if (measurements.length) {
      const storeMeasurements = state.timepointManager.measurements;
      const measurementData = storeMeasurements[measurements[0].toolType].find(
        x => x._id === measurements[0].measurementId
      );
      let enableSync = false;
      if (isCurrentWebImage()) {
        enableSync = true;
      } else if (measurementData) {
        const dataStudyInstanceUID = measurementData.StudyInstanceUID;
        const study = studyMetadataManager.get(dataStudyInstanceUID);
        if (study) {
          const dataSOPInstanceUID =
            measurementData.SOPInstanceUID || measurementData.sopInstanceUid;
          const displaySet = study.findDisplaySet(displaySet => {
            return displaySetContainsSopInstance(
              displaySet,
              dataSOPInstanceUID
            );
          });
          if (displaySet) {
            enableSync = true;
          }
        }
      }
      if (enableSync) {
        syncMeasurementsAndToolData();
      }
    } else {
      syncMeasurementsAndToolData();
    }
  },
};

export default dispatchActions;

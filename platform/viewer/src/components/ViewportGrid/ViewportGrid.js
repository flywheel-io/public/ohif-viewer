import './ViewportGrid.css';

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { utils } from '@ohif/core';
import { useSnackbarContext } from '@ohif/ui';
import { useDispatch, useSelector } from 'react-redux';
//
import ViewportPane from './ViewportPane.js';
import DefaultViewport from './DefaultViewport.js';
import EmptyViewport from './EmptyViewport.js';

import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
const {
  setLoaderDisplayStatus,
  removeSegmentationData,
} = FlywheelCommonRedux.actions;
const { selectLoaderDisplayStatus } = FlywheelCommonRedux.selectors;

const { loadAndCacheDerivedDisplaySets } = utils;

const ViewportGrid = function(props) {
  const {
    activeViewportIndex,
    availablePlugins,
    defaultPlugin: defaultPluginName,
    layout,
    numRows,
    numColumns,
    setViewportData,
    studies,
    viewportData,
    overlayViewportData,
    children,
    isStudyLoaded,
    viewportSpanData,
  } = props;

  const validData = (viewportSpanData || []).find(data => data !== null);

  const totalRows = validData ? validData.totalSpanRows : numRows;
  const totalColumns = validData ? validData.totalSpanColumns : numColumns;

  const rowSize = 100 / totalRows;
  const colSize = 100 / totalColumns;

  // http://grid.malven.co/
  if (!viewportData || !viewportData.length) {
    return null;
  }

  const dispatch = useDispatch();
  const snackbar = useSnackbarContext();
  const enableLoader = useSelector(store => {
    return selectLoaderDisplayStatus(store);
  });

  useEffect(() => {
    if (isStudyLoaded) {
      viewportData.forEach(displaySet => {
        const promises = loadAndCacheDerivedDisplaySets(displaySet, studies);

        if (promises.length > 0) {
          Promise.all(promises)
            .then(() => {
              if (enableLoader) {
                dispatch(setLoaderDisplayStatus(false));
              }
            })
            .catch(error => {
              if (enableLoader) {
                const failedSeg = error.segInfo
                  ? {
                      ...error.segInfo,
                      ...{
                        referenceUID: error.segInfo.referencedDisplaySetUID,
                        originalId: error.segInfo.segmentationIndex,
                      },
                    }
                  : null;
                if (failedSeg) {
                  dispatch(removeSegmentationData(failedSeg));
                }
                dispatch(setLoaderDisplayStatus(false));
              }
              snackbar.hideAll();
              snackbar.show({
                title: 'Error loading derived display set:',
                message: error.message,
                type: 'error',
                error,
                autoClose: false,
              });
            });
        } else {
          if (enableLoader) {
            dispatch(setLoaderDisplayStatus(false));
          }
        }
      });
    }
  }, [studies, viewportData, isStudyLoaded, snackbar]);

  const getViewportPanes = () =>
    layout.viewports.map((layout, viewportIndex) => {
      const displaySet = viewportData[viewportIndex];
      const overlayDisplaySet = (overlayViewportData || [])[0];

      if (!displaySet) {
        return null;
      }

      const data = {
        displaySet,
        studies,
        overlayDisplaySet,
      };

      // JAMES TODO:

      // Use whichever plugin is currently in use in the panel
      // unless nothing is specified. If nothing is specified
      // and the display set has a plugin specified, use that.
      //
      // TODO: Change this logic to:
      // - Plugins define how capable they are of displaying a SopClass
      // - When updating a panel, ensure that the currently enabled plugin
      // in the viewport is capable of rendering this display set. If not
      // then use the most capable available plugin

      const pluginName =
        !layout.plugin && displaySet && displaySet.plugin
          ? displaySet.plugin
          : layout.plugin;

      const ViewportComponent = _getViewportComponent(
        data, // Why do we pass this as `ViewportData`, when that's not really what it is?
        viewportIndex,
        children,
        availablePlugins,
        pluginName,
        defaultPluginName
      );

      return (
        <ViewportPane
          displaySet={displaySet}
          overlayDisplaySet={overlayDisplaySet}
          onDrop={setViewportData}
          viewportIndex={viewportIndex} // Needed by `setViewportData`
          className={classNames('viewport-container', {
            active: activeViewportIndex === viewportIndex,
          })}
          spanData={
            viewportSpanData && viewportSpanData[viewportIndex]
              ? viewportSpanData[viewportIndex]
              : null
          }
          key={viewportIndex}
        >
          {ViewportComponent}
        </ViewportPane>
      );
    });

  const ViewportPanes = React.useMemo(getViewportPanes, [
    layout,
    viewportData,
    studies,
    children,
    availablePlugins,
    defaultPluginName,
    setViewportData,
    activeViewportIndex,
  ]);

  return (
    <div
      data-cy="viewprt-grid"
      style={{
        display: 'grid',
        gridTemplateRows: `repeat(${totalRows}, ${rowSize}%)`,
        gridTemplateColumns: `repeat(${totalColumns}, ${colSize}%)`,
        height: '100%',
        width: '100%',
      }}
    >
      {ViewportPanes}
    </div>
  );
};

ViewportGrid.propTypes = {
  viewportData: PropTypes.array.isRequired,
  overlayViewportData: PropTypes.array,
  supportsDrop: PropTypes.bool.isRequired,
  activeViewportIndex: PropTypes.number.isRequired,
  layout: PropTypes.object.isRequired,
  availablePlugins: PropTypes.object.isRequired,
  setViewportData: PropTypes.func.isRequired,
  studies: PropTypes.array,
  children: PropTypes.node,
  defaultPlugin: PropTypes.string,
  numRows: PropTypes.number.isRequired,
  numColumns: PropTypes.number.isRequired,
};

ViewportGrid.defaultProps = {
  viewportData: [],
  numRows: 1,
  numColumns: 1,
  layout: {
    viewports: [{}],
  },
  activeViewportIndex: 0,
  supportsDrop: true,
  availablePlugins: {
    DefaultViewport,
  },
  defaultPlugin: 'defaultViewportPlugin',
};

/**
 *
 *
 * @param {*} plugin
 * @param {*} viewportData
 * @param {*} viewportIndex
 * @param {*} children
 * @returns
 */
function _getViewportComponent(
  viewportData,
  viewportIndex,
  children,
  availablePlugins,
  pluginName,
  defaultPluginName
) {
  if (viewportData.displaySet) {
    pluginName = pluginName || defaultPluginName;
    const ViewportComponent = availablePlugins[pluginName];

    if (!ViewportComponent) {
      throw new Error(
        `No Viewport Component available for name ${pluginName}.
         Available plugins: ${JSON.stringify(availablePlugins)}`
      );
    }

    return (
      <ViewportComponent
        viewportData={viewportData}
        viewportIndex={viewportIndex}
        children={[children]}
      />
    );
  }

  return <EmptyViewport />;
}

export default ViewportGrid;

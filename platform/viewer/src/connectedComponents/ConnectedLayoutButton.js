import { LayoutButton } from '@ohif/ui';
import OHIF from '@ohif/core';
import { connect } from 'react-redux';
import store from '../store';
import { resetLabellingAndContextMenuAction } from '../appExtensions/MeasurementsPanel/actions';

const {
  setLayout,
  setViewportActive,
  setViewportLayoutAndData,
} = OHIF.redux.actions;

const mapStateToProps = state => {
  return {
    currentLayout: state.viewports.layout,
    activeViewportIndex: state.viewports.activeViewportIndex,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // TODO: Change if layout switched becomes more complex
    onChange: (selectedCell, currentLayout, activeViewportIndex) => {
      const state = store?.getState() || {};
      if (state.ui?.contextMenu) {
        store.dispatch(resetLabellingAndContextMenuAction());
      }
      const viewports = [];
      let numRows = selectedCell.row + 1;
      let numColumns = selectedCell.col + 1;
      let layoutUpdated = false;
      if (
        currentLayout.numRows !== numRows ||
        currentLayout.numColumns !== numColumns
      ) {
        const multiSessionData = state?.multiSessionData;
        if (!!multiSessionData && multiSessionData.isMultiSessionViewEnabled) {
          const acquisitionCount = multiSessionData.acquisitionData.length;
          if (numColumns === 1 && numRows === 1) {
            numColumns *= acquisitionCount;
          } else {
            if (numRows === 1 && numColumns % acquisitionCount === 0) {
              // do nothing
            } else {
              if (numRows < acquisitionCount) {
                numRows += acquisitionCount - numRows;
              } else {
                numRows -= numRows - acquisitionCount;
              }
            }
          }

          const displaySets = [];
          const currentViewports = state.viewports;
          Object.keys(currentViewports.viewportSpecificData).forEach(key => {
            if (
              !displaySets.find(
                ds =>
                  ds.displaySetInstanceUID ===
                  currentViewports.viewportSpecificData[key]
                    .displaySetInstanceUID
              )
            ) {
              displaySets.push(currentViewports.viewportSpecificData[key]);
            }
          });
          const viewportSpecificData = {};
          let plugin = 'cornerstone';
          let dsIndex = 0;
          let index = 0;
          const numViewports = numRows * numColumns;
          const viewportsPerDS = numViewports / displaySets.length;
          for (let i = 0; i < viewportsPerDS; i++) {
            viewports.push({
              plugin,
            });
            viewportSpecificData[index] = displaySets[dsIndex];
            viewportSpecificData[index].plugin = plugin;
            if (i + 1 === viewportsPerDS) {
              i = -1;
              dsIndex += 1;
            }
            index++;
            if (index === numViewports) break;
          }
          const maxActiveIndex = numViewports - 1;
          if (activeViewportIndex > maxActiveIndex) {
            dispatch(setViewportActive(0));
          }

          dispatch(
            setViewportLayoutAndData(
              {
                numRows,
                numColumns,
                viewports,
              },
              viewportSpecificData
            )
          );
          layoutUpdated = true;
        }
      }
      if (!layoutUpdated) {
        const numViewports = numRows * numColumns;

        for (let i = 0; i < numViewports; i++) {
          // Hacky way to allow users to exit MPR "mode"
          const viewport = currentLayout.viewports[i];
          let plugin = viewport && viewport.plugin;
          if (viewport && viewport.vtk) {
            plugin = 'cornerstone';
          }

          viewports.push({
            plugin,
          });
        }
        const layout = {
          numRows,
          numColumns,
          viewports,
        };

        const maxActiveIndex = numViewports - 1;
        if (activeViewportIndex > maxActiveIndex) {
          dispatch(setViewportActive(0));
        }

        dispatch(setLayout(layout));
      }
    },
  };
};

const mergeProps = (propsFromState, propsFromDispatch) => {
  const onChangeFromDispatch = propsFromDispatch.onChange;
  const { currentLayout, activeViewportIndex } = propsFromState;

  return {
    onChange: selectedCell => {
      onChangeFromDispatch(selectedCell, currentLayout, activeViewportIndex);
      store.dispatch(setViewportActive(0));
    }
  };
};

const ConnectedLayoutButton = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(LayoutButton);

export default ConnectedLayoutButton;

import { ToolsUtils as FlywheelCommonToolsUtils } from '@flywheel/extension-flywheel-common';
import getImagePropertyForImagePath from '../lib/getImagePropertyForImagePath';
const displayFunction = data => {
  let lengthValue = '';
  if (data.length && !isNaN(data.length)) {
    const pixelSpacing = getImagePropertyForImagePath(
      data.imagePath,
      'PixelSpacing'
    );

    lengthValue =
      data.length.toFixed(2) +
      ' ' +
      FlywheelCommonToolsUtils.unitUtils.getLengthUnit(
        pixelSpacing,
        data.length
      );
  }
  return lengthValue;
};

export const length = {
  id: 'Length',
  name: 'Length',
  toolGroup: 'allTools',
  cornerstoneToolType: 'Length',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};

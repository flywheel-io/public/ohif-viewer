import cornerstone from 'cornerstone-core';
import { MeasurementApi } from '../classes';
import log from '../../log';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';

const {
  getDefaultROILabelFromHPLayout,
  getAllFovealRois,
} = FlywheelCommonUtils;

export default function({ eventData, tool, toolGroupId, toolGroup }) {
  const measurementApi = MeasurementApi.Instance;
  if (!measurementApi) {
    log.warn('Measurement API is not initialized');
  }

  const { measurementData } = eventData;

  const collection = measurementApi.tools[tool.parentTool];

  // Stop here if the tool data shall not be persisted (e.g. temp tools)
  if (!collection) return;

  log.info('CornerstoneToolsMeasurementModified');

  const measurement = collection.find(t => t._id === measurementData._id);
  let childMeasurement = measurement && measurement[tool.attribute];

  // Stop here if the measurement is already deleted
  if (!childMeasurement) return;

  childMeasurement = Object.assign(childMeasurement, measurementData);
  childMeasurement.viewport = cornerstone.getViewport(eventData.element);

  // Update the parent measurement
  measurement[tool.attribute] = childMeasurement;
  measurementApi.updateMeasurement(tool.parentTool, measurement);

  // TODO: Notify about the last activated measurement

  if (MeasurementApi.isToolIncluded(tool)) {
    // TODO: Notify that viewer suffered changes
  }

  const defaultRoiLabelDetails = getDefaultROILabelFromHPLayout();
  const defaultRoiLabels = defaultRoiLabelDetails[measurement.SeriesInstanceUID];
  const annotations = getAllFovealRois(defaultRoiLabels);
  const annotationsForSeries = annotations?.filter(annotation => annotation.SeriesInstanceUID === measurement.SeriesInstanceUID);
  const isFOVGridUpdate = annotationsForSeries?.length === 1 &&
  defaultRoiLabels?.includes(measurementData.location);

  if (isFOVGridUpdate) {
    const { commandsManager } = window.ohif.app;
    const plugin = 'cornerstone';
    const propertiesToSync = {
      defaultRoiLabels: defaultRoiLabels
    };
    commandsManager?.runCommand('crossViewportSync', {
      plugin,
      propertiesToSync,
    });
  }
}

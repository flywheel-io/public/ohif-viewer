import studyMetadataManager from '../../utils/studyMetadataManager';

export default function (imagePath, thumbnail = false) {
  const [
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID,
    frameIndex,
  ] = imagePath.split('$$$');
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);
  const series =
    studyMetadata && studyMetadata.getSeriesByUID(SeriesInstanceUID);
  const instance = series && series.getInstanceByUID(SOPInstanceUID);

  if (instance && instance.getTagValue && (instance.getTagValue("NumberOfFrames") > 1)) {
    // Handle the multi-frame cases(ex: OPT) where each image share the same SOP Instance UID
    return (instance && instance.getImageId(frameIndex - 1, thumbnail)) || '';
  }
  return (instance && instance.getImageId(frameIndex, thumbnail)) || '';
}

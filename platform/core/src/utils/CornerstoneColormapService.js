import cornerstone from 'cornerstone-core';
class CornerstoneColormapService {
  constructor() {
    this.listeners = {};
    this.viewportsAdapted = [];
    this.currentColormapId;
  }
  loadAndUpdateImage(imageId, isReset, colormap, colormapName) {
    return cornerstone.loadImage(imageId).then(image => {
      if (isReset && !colormapName) {
        this._recoverOriginalImage(image);
      } else {
        this._adaptImage(image, colormap);
      }
    });
  }
  getNextColormap(colormapId) {
    const _colormap = cornerstone.colors.getColormap(colormapId);
    const colormapName = _colormap.getColorSchemeName() || undefined;
    const colormap = colormapName ? _colormap : undefined;

    return {
      colormap,
      colormapName,
    };
  }
  setColormapId(colormapId) {
    this.currentColormapId = colormapId;
  }

  bindOnNewImage(viewportIndex, element) {
    if (!element) {
      return;
    }
    if (this.listeners[viewportIndex]) {
      element.removeEventListener(
        cornerstone.EVENTS.NEW_IMAGE,
        this.listeners[viewportIndex]
      );
    }

    this.listeners[viewportIndex] = this._onNewImageListener.bind(
      this,
      viewportIndex
    );

    element.addEventListener(
      cornerstone.EVENTS.NEW_IMAGE,
      this.listeners[viewportIndex]
    );
  }

  onChangeColormap(element, isReset, nextColormap, nextColormapName) {
    const updateImagesRenderMethod = (element, isReset) => {
      const promises = [];
      const enabledElement = cornerstone.getEnabledElement(element);
      if (enabledElement.image?.imageId) {
        if (isReset && !nextColormapName) {
          this._recoverOriginalImage(enabledElement.image);
        }
        promises.push(
          this.loadAndUpdateImage(
            enabledElement.image.imageId,
            isReset,
            nextColormap,
            nextColormapName
          ).catch(error => {
            console.log(error);
          })
        );
      }
      return promises;
    };

    Promise.all(updateImagesRenderMethod(element, isReset)).then(() => {
      const enabledElement = (cornerstone.getEnabledElements() || []).find(
        enabledElement => enabledElement.element === element
      );
      if (enabledElement?.image?.imageId) {
        const viewport = cornerstone.getViewport(element);
        this._updateViewportColormap(element, viewport, nextColormap);
      }
    });
  }

  _onNewImageListener(viewportIndex, event = {}) {
    const { image, viewport, element } = event.detail;
    if (
      this.currentColormapId &&
      viewport.colormap !== this.currentColormapId
    ) {
      const { colormap } = this.getNextColormap(this.currentColormapId);
      this._adaptImage(image, colormap, viewportIndex);
      this._updateViewportColormap(element, viewport, colormap);
      this.viewportsAdapted[viewportIndex] = true;
    }
  }

  _recoverOriginalImage(image) {
    image.render = image.originalRender;
    delete image.originalRender;
    image.colormap = image.originalColormap;
    delete image.originalColormap;
  }

  _updateViewportColormap(element, viewport, colormap) {
    if (viewport.colormap && !colormap) {
      cornerstone.resetColorMap(element);
    }
    viewport.colormap = colormap;
    cornerstone.setViewport(element, viewport);
  }

  _adaptImage(image, colormap, viewportIndex) {
    //store original render method
    if (typeof image.originalRender !== 'function') {
      image.originalRender = image.render;
    }

    if (this.viewportsAdapted[viewportIndex] && colormap) {
      if (typeof image.originalColormap !== 'object') {
        image.originalColormap = image.colormap;
      }
      image.colormap = colormap;
    }

    image.render = null;
  }
}

const cornerstoneColormapService = new CornerstoneColormapService();

export default cornerstoneColormapService;

/**
 * Sort 4D imageSet based on ImagePositionPatient (0020,0032), then by TriggerTime (0018,1060)
 * @param {*} imageSet
 */
export default function sort4DImageSetByPosition(imageSet) {
  const getTriggerTime = image =>
    parseInt(image.getData().metadata.TriggerTime, 10);
  const triggerTime = getTriggerTime(imageSet.images[0]);
  const isTimeSortable4DStack = !isNaN(triggerTime);
  if (isTimeSortable4DStack) {
    imageSet.sortBy((a, b) => getTriggerTime(a) - getTriggerTime(b));
  }
  imageSet.sortByImagePositionPatient();
}

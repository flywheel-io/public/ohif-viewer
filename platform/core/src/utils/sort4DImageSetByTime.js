/**
 * Sort 4D imageSet based on TriggerTime (0018,1060) then by ImagePositionPatient (0020,0032)
 * @param {*} imageSet
 */
export default function sort4DImageSetByTime(imageSet) {
  imageSet.sortByImagePositionPatient();
  const getTriggerTime = image =>
    parseInt(image.getData().metadata.TriggerTime, 10);
  const triggerTime = getTriggerTime(imageSet.images[0]);
  const isTimeSortable4DStack = !isNaN(triggerTime);
  if (isTimeSortable4DStack) {
    imageSet.sortBy((a, b) => getTriggerTime(a) - getTriggerTime(b));
  }
}

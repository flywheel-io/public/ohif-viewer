import studyMetadataManager from './studyMetadataManager';
import cornerstoneTools from 'cornerstone-tools';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
const { configuration } = cornerstoneTools.getModule('segmentation');
const {
  rearrangeSegmentationData,
  setSegmentationData,
} = FlywheelCommonRedux.actions;

/**
 * Get derived display sets.
 * @param {Array} derivedDisplaySets
 * @param {string[]} displaySetInstanceUIDs
 * @returns void
 */
const getDerivedDisplaySets = (derivedDisplaySets, displaySetInstanceUIDs) => {
  let displaySets = [];
  if (!derivedDisplaySets.length && derivedDisplaySets.length <= 0) {
    return displaySets;
  }
  derivedDisplaySets.every(segDisplaySet => {
    if (
      !segDisplaySet.isDeleted &&
      displaySetInstanceUIDs?.includes(segDisplaySet.displaySetInstanceUID)
    ) {
      displaySets.push(segDisplaySet);
      return false;
    }
    return true;
  });
  return displaySets;
};

/**
 * Get derived display set collection.
 * @param {Object} studyMetadata
 * @param {string} SeriesInstanceUID
 * @param {string} modality - segmentation overlay derived series modality
 * @returns void
 */
const getDerivedDisplaySetArrayByReferencedSeries = (
  studyMetadata,
  SeriesInstanceUID,
  modality
) => {
  const derivedDisplaySets = studyMetadata.getDerivedDatasets({
    referencedSeriesInstanceUID: SeriesInstanceUID,
    Modality: modality,
  });
  return derivedDisplaySets;
};

/**
 * Find the removed item id from from the segment collection.
 * @param {Array} previousSegmentData - previous segment data collection
 * @param {Array} newSegmentData - new segment data collection
 * @returns number - item id
 */
const findRemovedItem = (previousSegmentData, newSegmentData) => {
  let removedItem = {};
  previousSegmentData.every(prevSegData => {
    let isRemoved = true;
    newSegmentData.every(segData => {
      if (
        segData.fileId === prevSegData.fileId &&
        segData.originalId === prevSegData.originalId
      ) {
        isRemoved = false;
        return false;
      }
      return true;
    });
    if (isRemoved) {
      removedItem = prevSegData;
      return false;
    }
    return true;
  });
  return removedItem;
};

/**
 * Find the order changed segment id from the segment collection.
 * @param {Array} previousSegmentData - previous segment data collection
 * @param {Array} newSegmentData - new segment data collection
 * @returns number - item id
 */
const findOrderChangeItem = (previousSegmentData, newSegmentData) => {
  let prevOrderData = {};
  let newOrderData = {};
  let itemId = 0;
  previousSegmentData.forEach((segData, index) => {
    prevOrderData[segData.originalId] = {};
    prevOrderData[segData.originalId].prev =
      index === 0 ? -1 : previousSegmentData[index - 1].originalId;
    prevOrderData[segData.originalId].next =
      index >= previousSegmentData.length - 1
        ? -1
        : previousSegmentData[index + 1].originalId;
  });
  newSegmentData.forEach((segData, index) => {
    newOrderData[segData.originalId] = {};
    newOrderData[segData.originalId].prev =
      index === 0 ? -1 : newSegmentData[index - 1].originalId;
    newOrderData[segData.originalId].next =
      index >= newSegmentData.length - 1
        ? -1
        : newSegmentData[index + 1].originalId;
  });
  for (const key of Object.keys(newOrderData)) {
    const newData = newOrderData[key];
    const prevData = prevOrderData[key];
    if (prevData && newData) {
      if (newData.prev !== prevData.prev && newData.next !== prevData.next) {
        itemId = parseInt(key);
        break;
      }
    }
  }
  return itemId;
};

/**
 * Remove all segments of the study.
 * @param {string} StudyInstanceUID
 * @param {Study[]} studies Collection of studies
 * @returns void
 */
const removeAllSegments = (StudyInstanceUID, studies) => {
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);

  if (!studyMetadata) {
    return;
  }
  const displaySets = studyMetadata.getDisplaySets();
  displaySets.forEach(displaySet => {
    if (displaySet.isDerived) {
      return;
    }
    const derivedDisplaySets = studyMetadata.getDerivedDatasets({});
    derivedDisplaySets.forEach(segDisplayset => {
      (segDisplayset.segmentationIndexes || []).forEach(segIndex => {
        if (segIndex > 0 && !segDisplayset.isDeleted) {
          segDisplayset.removeSegment(displaySet, studies, segIndex);
        }
      });
      segDisplayset.isDeleted = true;
    });
  });
};

/**
 * Update order changed segments or atrributes of segment or remove segment from the segment collection.
 * @param {string} StudyInstanceUID
 * @param {Study[]} studies Collection of studies
 * @param {string} referenceSeriesInstanceUID - reference series UID
 * @param {Array} previousSegmentData - previous segment data collection
 * @param {Array} newSegmentData - new segment data collection
 * @param {string} modality - segmentation overlay derived series modality
 * @param {boolean} isRemoved - update the item removal or not
 * @returns {string[]} - collection of removed series instance UID's  of removed segments
 */
const updateSegmentDisplayData = (
  StudyInstanceUID,
  studies,
  referenceUID,
  previousSegmentData,
  newSegmentData,
  modality,
  isRemoved = false
) => {
  const removedItem = findRemovedItem(previousSegmentData, newSegmentData);
  const removedIndex = isRemoved ? removedItem.originalId - 1 : -1;
  const changedSegId = isRemoved
    ? 0
    : findOrderChangeItem(previousSegmentData, newSegmentData);

  const promises = [];

  const removedSeriesInstanceUIDs = [];
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);

  if (!studyMetadata) {
    return promises;
  }
  const displaySets = studyMetadata.getDisplaySets();
  displaySets.forEach(displaySet => {
    const { SeriesInstanceUID } = displaySet;
    // For DICOM study, the reference UID will be active viewport series instance UID
    // and for Nifti study, the reference UID will be loaded study instance UID
    if (
      referenceUID !== SeriesInstanceUID &&
      referenceUID !== StudyInstanceUID
    ) {
      return;
    }
    const derivedDisplaySets = getDerivedDisplaySetArrayByReferencedSeries(
      studyMetadata,
      SeriesInstanceUID,
      modality
    );
    if (isRemoved && removedIndex >= 0) {
      const segDisplaysets = getDerivedDisplaySets(
        derivedDisplaySets,
        removedItem?.displaySetInstanceUIDs
      );
      if (segDisplaysets.length === 0) {
        return promises;
      }
      segDisplaysets.forEach(segDisplayset => {
        if (segDisplayset === null || !segDisplayset.labelmapIndexes?.length) {
          return;
        }
        segDisplayset.removeSegment(displaySet, studies, removedIndex + 1);
        if (segDisplayset.segmentationIndexes.length > 0) {
          segDisplayset.segmentationIndexes.forEach(segIndex => {
            segDisplayset.removeSegment(displaySet, studies, segIndex);
          });
        }
        removedSeriesInstanceUIDs.push(segDisplayset.SeriesInstanceUID);
        segDisplayset.isDeleted = true;
      });
    }
    const isPropertyUpdate =
      changedSegId > 0 || (isRemoved && removedIndex >= 0) ? false : true;
    // Always skip 0th index as that label map preserved for manual drawn segments.
    let labelMapIndex = newSegmentData.length;
    newSegmentData.every((segData, index) => {
      const segDisplaysets = getDerivedDisplaySets(
        derivedDisplaySets,
        segData?.displaySetInstanceUIDs
      );
      if (segDisplaysets.length > 0) {
        segDisplaysets.forEach(segDisplayset => {
          if (segDisplayset !== null && segDisplayset.labelmapIndexes?.length) {
            // If display set mapped to multiple label map/segment then
            // find index to the corresponding in that display set.
            let itemIndex = (segDisplayset.segmentationIndexes || []).findIndex(
              segIndex => segIndex === segData.originalId
            );
            if (itemIndex === -1) {
              itemIndex = 0;
            }
            if (!isPropertyUpdate) {
              if (segData.originalId === changedSegId) {
                segDisplayset.rePositionSegment(
                  displaySet,
                  studies,
                  segData.modality === 'RTSTRUCT' ? index : labelMapIndex,
                  itemIndex
                );
              }
              segDisplayset.labelmapIndexes[itemIndex] = labelMapIndex;
              labelMapIndex--;
              if (segData.modality === 'RTSTRUCT') {
                segDisplayset.labelmapIndexes = [index];
              }
            } else {
              segDisplayset.setSegmentVisibility(
                segData.isVisible,
                displaySet,
                itemIndex
              );
            }
          }
        });
      }
      return true;
    });
    updateOpacity(displaySet, derivedDisplaySets, newSegmentData, modality);
  });
  return removedSeriesInstanceUIDs;
};

/**
 * Update segmentation opacity configuration.
 * @param {Object} referenceDisplaySet -reference displayset
 * @param {Object} derivedDisplaySets - displaysets corresponding to segmentation data
 * @param {Array} segmentList - segmentation data collection
 * @param {string} modality - segmentation modality
 * @returns void
 */
const updateOpacity = (
  referenceDisplaySet,
  derivedDisplaySets,
  segmentList,
  modality
) => {
  configuration.renderOutline = false;
  if (modality === 'RTSTRUCT') {
    const module = cornerstoneTools.getModule('rtstruct');
    segmentList.forEach(segData => {
      const fillAlpha = parseFloat(segData.opacity) / 100;
      if (
        !module.configuration.opacity ||
        !isNaN(module.configuration.opacity)
      ) {
        module.configuration.opacity = {};
      }
      module.configuration.opacity[segData.seriesInstanceUID] = fillAlpha;
    });
  } else {
    const imageId = referenceDisplaySet.images.map(image =>
      image.getImageId()
    )[0];
    if (configuration.fillAlphaPerLabelMap[imageId]?.length > 0) {
      configuration.fillAlphaPerLabelMap[imageId].splice(1);
    } else {
      configuration.fillAlphaPerLabelMap[imageId] = [];
    }
    segmentList.forEach((segData, index) => {
      const segDisplaysets = getDerivedDisplaySets(
        derivedDisplaySets,
        segData?.displaySetInstanceUIDs
      );
      segDisplaysets.forEach(segDisplayset => {
        if (segDisplayset !== null && segDisplayset.labelmapIndexes?.length) {
          let itemIndex = (segDisplayset.segmentationIndexes || []).findIndex(
            segIndex => segIndex === segData.originalId
          );
          if (itemIndex === -1) {
            itemIndex = 0;
          }
          const labelMapIndex = segDisplayset.labelmapIndexes[itemIndex];
          const fillAlpha = parseFloat(segData.opacity) / 100;
          configuration.fillAlphaPerLabelMap[imageId][
            labelMapIndex
          ] = fillAlpha;
        }
      });
    });
  }
};

/**
 * Reorder the auto-loaded segmentations in the reverse order of label map index
 * for the reference series as label map readjust skipping for auto-loading scenario
 * @param {string} studyInstanceUID
 * @param {string} referenceUID
 * @returns void
 */
const reOrderAutoLoadedSegmentationBasedOnLabelMapindex = (
  studyInstanceUID,
  referenceUID
) => {
  const studyMetadata = studyMetadataManager.get(studyInstanceUID);

  if (!studyMetadata) {
    return promises;
  }
  const displaySets = studyMetadata.getDisplaySets();
  displaySets.forEach(displaySet => {
    const { SeriesInstanceUID } = displaySet;
    // For DICOM study, the reference UID will be active viewport series instance UID
    // and for Nifti study, the reference UID will be loaded study instance UID
    if (
      referenceUID !== SeriesInstanceUID &&
      referenceUID !== studyInstanceUID
    ) {
      return;
    }
    const derivedDisplaySets = studyMetadata.getDerivedDatasets({
      referencedSeriesInstanceUID: SeriesInstanceUID,
    });
    if (derivedDisplaySets.find(segDisplaySet => !segDisplaySet.isLoaded)) {
      return;
    }
    const labelmaps = [];
    const segmentationData = [];
    derivedDisplaySets.forEach(segDisplaySet => {
      segDisplaySet.labelmapIndexes.forEach(labelMapIndex =>
        labelmaps.push(labelMapIndex)
      );
    });
    labelmaps.sort((a, b) => b - a);
    labelmaps.forEach(labelMapIndex => {
      const segDisplaySet = derivedDisplaySets.find(segDs =>
        segDs.labelmapIndexes.includes(labelMapIndex)
      );
      const itemIndex = segDisplaySet.labelmapIndexes.findIndex(
        labelIndex => labelIndex === labelMapIndex
      );
      const matchingSegment = getMatchingSegment(segDisplaySet, itemIndex);
      if (matchingSegment) {
        if (!segmentationData.includes(matchingSegment)) {
          segmentationData.push(matchingSegment);
        }
      }
    });
    store.dispatch(setSegmentationData([...segmentationData], referenceUID));
  });
};

const getMatchingSegment = (displaySet, itemIndex) => {
  const segmentationData = window.store.getState()?.segmentation
    ?.segmentationData;
  const segIndex =
    displaySet.segmentationIndexes[itemIndex] ||
    displaySet.segmentationIndexes[0];
  let matchingSegment = null;
  Object.keys(segmentationData).forEach(key => {
    const segments = segmentationData[key];
    if (segments?.length) {
      matchingSegment =
        matchingSegment ||
        segments.find(
          seg =>
            seg.seriesInstanceUID === displaySet.SeriesInstanceUID &&
            seg.originalId === segIndex
        );
    }
  });
  return matchingSegment;
};

/**
 * Reset the reference UID if there is mismatch in ReferencedSeriesSequence .
 * @param {Array} newSegmentData - segmentation data collection
 * @param {string} modality - segmentation modality
 * @returns void
 */
const resetReferenceUID = (newSegmentData, modality) => {
  newSegmentData.forEach(segData => {
    const studyMetadata = studyMetadataManager.get(segData.studyInstanceUID);
    const derivedDisplaySets = studyMetadata.getDerivedDatasets({
      Modality: modality,
    });
    const segDisplaysets = getDerivedDisplaySets(
      derivedDisplaySets,
      segData?.displaySetInstanceUIDs
    );
    segDisplaysets.forEach(segDisplayset => {
      if (!segDisplayset.isLoaded) {
        return;
      }
      const ReferencedSeriesSequence = Array.isArray(
        segDisplayset.metadata.ReferencedSeriesSequence
      )
        ? segDisplayset.metadata.ReferencedSeriesSequence
        : [segDisplayset.metadata.ReferencedSeriesSequence];
      const referenceSeriesUID = ReferencedSeriesSequence[0].SeriesInstanceUID;
      if (referenceSeriesUID !== segData.referenceUID) {
        store.dispatch(
          rearrangeSegmentationData(
            segData,
            segData.referenceUID,
            referenceSeriesUID,
            null,
            null
          )
        );
      }
    });
  });
};

const setDisplaySetInstanceUIDForSegments = (studyMetadata, segments) => {
  const updatedSegments = [];
  (segments || []).forEach(segment => {
    const segDisplaySet = studyMetadata
      .getData()
      .derivedDisplaySets.find(
        ds => ds.SeriesInstanceUID === segment.seriesInstanceUID
      );
    const segmentationData = store.getState().segmentation.segmentationData[
      segment.referenceUID
    ];
    const index = segmentationData.findIndex(
      data =>
        data.seriesInstanceUID === segment.seriesInstanceUID &&
        !segment.displaySetInstanceUIDs?.length
    );
    if (index > -1 && segDisplaySet) {
      const newSegmentData = segmentationData[index];
      const updatedSegmentData = { ...newSegmentData };
      updatedSegmentData.displaySetInstanceUIDs = [
        segDisplaySet.displaySetInstanceUID,
      ];
      if (updatedSegmentData.modality !== segDisplaySet.Modality) {
        store.dispatch(
          rearrangeSegmentationData(
            updatedSegmentData,
            null,
            null,
            updatedSegmentData.modality,
            segDisplaySet.Modality
          )
        );
        updatedSegmentData.modality = segDisplaySet.Modality;
        updatedSegments.push(updatedSegmentData);
      } else {
        let newMaskArray = [...segmentationData];
        newMaskArray[index] = updatedSegmentData;
        updatedSegments.push(updatedSegmentData);

        store.dispatch(setSegmentationData(newMaskArray, segment.referenceUID));
      }
    } else {
      updatedSegments.push(segment);
    }
  });
  return updatedSegments;
};

export {
  updateSegmentDisplayData,
  removeAllSegments,
  resetReferenceUID,
  setDisplaySetInstanceUIDForSegments,
  reOrderAutoLoadedSegmentationBasedOnLabelMapindex,
};

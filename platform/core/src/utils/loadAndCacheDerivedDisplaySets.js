import studyMetadataManager from './studyMetadataManager';

/**
 * Study schema
 *
 * @typedef {Object} Study
 * @property {Array} series -
 * @property {Object} seriesMap -
 * @property {Object} seriesLoader -
 * @property {string} wadoUriRoot -
 * @property {string} wadoRoot -
 * @property {string} qidoRoot -
 * @property {string} PatientName -
 * @property {string} PatientID -
 * @property {number} PatientAge -
 * @property {number} PatientSize -
 * @property {number} PatientWeight -
 * @property {string} AccessionNumber -
 * @property {string} StudyDate -
 * @property {string} StudyTime -
 * @property {string} modalities -
 * @property {string} StudyDescription -
 * @property {string} imageCount -
 * @property {string} StudyInstanceUID -
 * @property {string} institutionName -
 * @property {Array} displaySets -
 */

const derivedDisplaysetLoadStatus = {};

/**
 * Factory function to load and cache derived display sets.
 *
 * @param {object} referencedDisplaySet Display set
 * @param {string} referencedDisplaySet.displaySetInstanceUID Display set instance uid
 * @param {string} referencedDisplaySet.SeriesDate
 * @param {string} referencedDisplaySet.SeriesTime
 * @param {string} referencedDisplaySet.SeriesInstanceUID
 * @param {string} referencedDisplaySet.SeriesNumber
 * @param {string} referencedDisplaySet.seriesDescription
 * @param {number} referencedDisplaySet.numImageFrames
 * @param {string} referencedDisplaySet.frameRate
 * @param {string} referencedDisplaySet.Modality
 * @param {boolean} referencedDisplaySet.isMultiFrame
 * @param {number} referencedDisplaySet.InstanceNumber
 * @param {boolean} referencedDisplaySet.isReconstructable
 * @param {string} referencedDisplaySet.StudyInstanceUID
 * @param {Array} referencedDisplaySet.sopClassUIDs
 * @param {Study[]} studies Collection of studies
 * @returns void
 */
const loadAndCacheDerivedDisplaySets = (referencedDisplaySet, studies) => {
  const { StudyInstanceUID, SeriesInstanceUID } = referencedDisplaySet;

  const promises = [];

  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);

  if (!studyMetadata) {
    return promises;
  }

  const derivedDisplaySets = studyMetadata.getDerivedDatasets({
    referencedSeriesInstanceUID: SeriesInstanceUID,
  });

  if (!derivedDisplaySets.length) {
    return promises;
  }

  // Filter by type
  const displaySetsPerModality = {};

  derivedDisplaySets.forEach(displaySet => {
    const Modality = displaySet.Modality;

    if (displaySetsPerModality[Modality] === undefined) {
      displaySetsPerModality[Modality] = [];
    }

    if (!isSegmentationAutoLoaded(displaySet)) {
      displaySetsPerModality[Modality].push(displaySet);
    }
  });

  // For each type, see if any are loaded, if not load the most recent.
  Object.keys(displaySetsPerModality).forEach(key => {
    const displaySets = displaySetsPerModality[key];
    // TODO - Needs to remove the special handling for SEG
    let displaySetIndex = 0;
    if (key === 'SEG' || key === 'RTSTRUCT') {
      // Check if any of the seg displaysets pending to load, then continue to load that
      const isPendingLoaded = displaySets.some(
        displaySet => !displaySet.isLoaded
      );

      if (!isPendingLoaded) {
        return;
      } else {
        const alreadyLoadingDisplaySets = displaySets.filter(
          displaySet => displaySet.isLoading && !displaySet.isLoaded
        );
        if (alreadyLoadingDisplaySets.length) {
          alreadyLoadingDisplaySets.forEach(loadingDs => {
            promises.push(
              derivedDisplaysetLoadStatus[loadingDs.SeriesInstanceUID]
            );
          });
          return;
        }
      }
      displaySetIndex = displaySets.length - 1;
    } else {
      const isLoaded = displaySets.some(displaySet => displaySet.isLoaded);

      if (isLoaded) {
        return;
      }
    }

    if (displaySets.some(displaySet => displaySet.loadError)) {
      return;
    }

    // find most recent and load it.
    let recentDateTime = 0;
    let recentDisplaySet = displaySets[displaySetIndex];

    displaySets.forEach((displaySet, index) => {
      if (!displaySet.isLoaded) {
        const dateTime = Number(
          `${displaySet.SeriesDate}${displaySet.SeriesTime}`
        );
        if (dateTime > recentDateTime) {
          recentDateTime = dateTime;
          recentDisplaySet = displaySet;
          displaySetIndex = index;
        }
      }
      if (key === 'SEG' && displaySet.isLoaded) {
        adjustLabelMapIndex(displaySet, 1);
      }
    });

    recentDisplaySet.isLoading = true;
    let segIndex = getSegmentationIndexForAddedDisplaySet(recentDisplaySet);
    segIndex = segIndex === -1 ? displaySetIndex + 1 : segIndex; // Segmentation index to set for newly loading segmentation

    const promise = recentDisplaySet.load(
      referencedDisplaySet,
      studies,
      segIndex
    );
    promises.push(promise);
    derivedDisplaysetLoadStatus[recentDisplaySet.SeriesInstanceUID] = promise;
    if (key === 'SEG') {
      promises[0].then(() => {
        delete derivedDisplaysetLoadStatus[recentDisplaySet.SeriesInstanceUID];
        if (recentDisplaySet.labelmapIndexes?.length === 1) {
          return; // Already adjusted, no need for further adjust
        }
        // If more than one label map for a display set, then adjust the display sets again.
        displaySets.forEach(displaySet => {
          if (
            displaySet.displaySetInstanceUID !==
            recentDisplaySet.displaySetInstanceUID
          ) {
            adjustLabelMapIndex(
              displaySet,
              recentDisplaySet.labelmapIndexes?.length - 1
            );
          }
        });
      });
    }
  });

  return promises;
};

/**
 * Function to load and cache derived display sets which are pending to load.
 *
 * @param {Study[]} studies Collection of studies
 * @param {string} modality
 * @param {boolean}} isAutoSegmentation
 * @returns void
 */
const loadPendingDerivedDisplaySets = (
  studies,
  modality,
  isAutoSegmentation = false
) => {
  const promises = [];

  studies.forEach(study => {
    const studyInstanceUID = study.studyInstanceUID || study.StudyInstanceUID;
    const studyMetadata = studyMetadataManager.get(studyInstanceUID);

    if (!studyMetadata) {
      return;
    }

    const derivedDisplaySets = studyMetadata.getDerivedDatasets({
      Modality: modality,
    });

    if (!derivedDisplaySets.length) {
      return;
    }

    // Filter by type
    const isPendingLoaded = derivedDisplaySets.some(
      displaySet => !displaySet.isLoaded
    );

    if (!isPendingLoaded) {
      return;
    }
    const loadInfos = [];
    const loadedDisplaySets = [];
    derivedDisplaySets.forEach((displaySet, index) => {
      if (displaySet.isLoaded) {
        if (!isAutoSegmentation && displaySet.Modality === 'SEG') {
          adjustLabelMapIndex(displaySet, 1);
          loadedDisplaySets.push(displaySet);
        }
        return;
      }
      if (displaySet.isLoading) {
        return;
      }
      // Auto-loading enabled for the DICOM segments, so avoiding the chance of loading the segment label map
      // before actually placing the segmentation entry in the store.
      const isDicomSeg = isDicomSegment(displaySet);
      if (!isDicomSeg || (isDicomSeg && !!getSegmentData(displaySet))) {
        displaySet.isLoading = true;
        let segIndex = getSegmentationIndexForAddedDisplaySet(
          displaySet,
          false
        );
        segIndex = segIndex === -1 ? index + 1 : segIndex; // Segmentation index to set for newly loading segmentation
        loadInfos.push({ displaySet, segIndex });
      }
    });

    loadInfos.forEach(loadInfo => {
      const promise = loadInfo.displaySet.load(
        loadInfo.displaySet,
        studies,
        loadInfo.segIndex,
        isAutoSegmentation
      );
      if (promise) {
        promise.then(() => {
          if (!isAutoSegmentation) {
            loadedDisplaySets.forEach(displaySet => {
              if (loadInfo.displaySet.labelmapIndexes?.length === 1) {
                return; // Already adjusted, no need for further adjust
              }
              // If more than one label map for a display set, then adjust the display sets again.
              adjustLabelMapIndex(
                displaySet,
                loadInfo.displaySet.labelmapIndexes?.length - 1
              );
            });
            // Adjust the display sets which are trigerred for loading together, and already loaded.
            const filteredDatas = loadInfos.filter(
              data =>
                data.displaySet.displaySetInstanceUID !==
                  loadInfo.displaySet.displaySetInstanceUID &&
                data.displaySet.isLoaded &&
                data.displaySet.Modality === 'SEG'
            );
            filteredDatas.forEach(data => {
              adjustLabelMapIndex(
                data.displaySet,
                loadInfo.displaySet.labelmapIndexes?.length
              );
            });
          }
        });
        promises.push(promise);
      }
    });
  });

  return promises;
};

const adjustLabelMapIndex = (displaySetToAdust, adjustIndex = 1) => {
  // Shifting all the loaded segmentation label map index to 1 position up for adjusting the rendering order
  (displaySetToAdust.labelmapIndexes || []).forEach((mapIndex, index) => {
    displaySetToAdust.labelmapIndexes[index] = mapIndex + adjustIndex;
  });
};

const isDicomSegment = displaySet => {
  return (
    displaySet.Modality === 'RTSTRUCT' ||
    (displaySet.Modality === 'SEG' && !displaySet?.dataProtocol)
  );
};

const getSegmentData = displaySet => {
  const segmentationData = window.store.getState()?.segmentation
    ?.segmentationData;
  let segment = null;
  Object.keys(segmentationData).forEach(key => {
    const segments = segmentationData[key];
    if (segments?.length) {
      segment =
        segment ||
        segments.find(
          seg => seg.seriesInstanceUID === displaySet.SeriesInstanceUID
        );
    }
  });
  return segment;
};

const isSegmentationAutoLoaded = displaySet => {
  if (!isDicomSegment(displaySet) || displaySet.isLoaded) {
    return false;
  }
  const segment = getSegmentData(displaySet);
  return !segment || segment.isLoadedInitially;
};

const getSegmentationIndexForAddedDisplaySet = (
  displaySet,
  isLatestLoadIndex = true
) => {
  let segIndex = -1;
  const derivedSuffix = '-derived'; // Derived segmentation series suffix for Nifti overlay
  if (displaySet.SeriesInstanceUID.indexOf(derivedSuffix) >= 0) {
    segIndex = parseInt(displaySet.SeriesInstanceUID.split(derivedSuffix)[1]);
  } else if (isDicomSegment(displaySet)) {
    const segmentationData = window.store.getState()?.segmentation
      ?.segmentationData;
    Object.keys(segmentationData).forEach(key => {
      const segments = segmentationData[key];
      if (!!segments && segments.length > 0) {
        if (!isLatestLoadIndex) {
          const segment = segments.find(
            seg => seg.seriesInstanceUID === displaySet.SeriesInstanceUID
          );
          segIndex = segment?.originalId || segIndex;
        } else {
          segments.forEach(segData => {
            if (segData.modality === displaySet.Modality) {
              segIndex =
                segData.originalId > segIndex ? segData.originalId : segIndex;
            }
          });
        }
      }
    });
  }
  return segIndex;
};

export { loadAndCacheDerivedDisplaySets, loadPendingDerivedDisplaySets };

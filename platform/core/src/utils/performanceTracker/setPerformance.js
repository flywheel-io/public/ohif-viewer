import store from '@ohif/viewer/src/store';
import redux from '../../redux';

const {
  setPerformanceStartEntry,
  setPerformanceEndEntry,
  setPerformanceAddEntry,
  setPerformanceRemoveEntry,
  clearPerformanceEntry,
} = redux.actions;

const performanceStart = entry => {
  store.dispatch(setPerformanceStartEntry(entry));
};

const performanceEnd = entry => {
  store.dispatch(setPerformanceEndEntry(entry));
};

const performanceAdd = entry => {
  store.dispatch(setPerformanceAddEntry(entry));
};

const performanceRemove = entry => {
  store.dispatch(setPerformanceRemoveEntry(entry));
};

const performanceClear = () => {
  store.dispatch(clearPerformanceEntry());
};

const setPerformance = {
  performanceStart,
  performanceEnd,
  performanceAdd,
  performanceRemove,
  performanceClear,
};

export default setPerformance;

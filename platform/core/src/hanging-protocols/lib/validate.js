import validate from 'validate.js';
const { validators } = validate;

function getCustomizedValidators() {
  validators.equals = function (value, options, key, attributes) {
    if (options && value !== options.value) {
      return key + 'must equal ' + options.value;
    }
  };

  validators.doesNotEqual = function (value, options, key) {
    if (options && value === options.value) {
      return key + 'cannot equal ' + options.value;
    }
  };

  validators.contains = function (value, options, key) {
    let regex = new RegExp(options.value, "i");
    if (regex.test(value)) {
      return;
    }
    if (options && value.indexOf && value.indexOf(options.value) === -1) {
      return key + 'must contain ' + options.value;
    }
  };

  validators.doesNotContain = function (value, options, key) {
    if (options && value.indexOf && value.indexOf(options.value) !== -1) {
      return key + 'cannot contain ' + options.value;
    }
  };

  validators.startsWith = function (value, options, key) {
    if (options && value.startsWith && !value.startsWith(options.value)) {
      return key + 'must start with ' + options.value;
    }
  };

  validators.endsWith = function (value, options, key) {
    if (options && value.endsWith && !value.endsWith(options.value)) {
      return key + 'must end with ' + options.value;
    }
  };
  return validators;
}

export { validate, getCustomizedValidators };

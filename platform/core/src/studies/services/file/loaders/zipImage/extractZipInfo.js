import { HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';

const { getZipInfo } = FlywheelCommonHTTP.services;

// Sorting the zip files in natural sort order
// e.g. A1, A2, A3, A10, A11 instead of the alphanumeric A1, A10, A11, A2, A3
const sortZipMembers = members => {
  return members.sort((firstMember, secondMember) =>
    firstMember.path.localeCompare(secondMember.path, undefined, {
      numeric: true,
    })
  );
};

const extractZipInfo = fileId => {
  const promise = new Promise(async (resolve, reject) => {
    try {
      const zipInfo = await getZipInfo(fileId).catch(error => {
        reject(error);
      });
      const members = zipInfo.members;
      sortZipMembers(members);
      resolve(members);
    } catch (error) {
      reject(error);
    }
  });
  return {
    promise,
  };
};

export default extractZipInfo;

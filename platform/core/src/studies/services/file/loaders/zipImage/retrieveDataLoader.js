import { utils } from '@ohif/core';
import _addInstancesToStudy from './addInstanceToStudy';
import WebImageDataLoaderSync from '../webImage/retrieveDataLoader';

const { ZIP_IMAGES } = utils.regularExpressions;

export default class ZipImageDataLoaderSync extends WebImageDataLoaderSync {
  static extensionRegExp = ZIP_IMAGES;

  constructor(server, containerId, fileName) {
    super(server, containerId, fileName);
    this.DATA_PROTOCOL = 'zip';
  }

  _getSOPInstanceUIDFromUrl(url = '') {
    const imagePath = url.replace(/^zip:/, '');
    const imageData = imagePath.split('$$$');
    const SOPInstanceUID = imageData[2];

    return SOPInstanceUID;
  }

  async addInstancesToStudy(study, dataProtocol) {
    return await _addInstancesToStudy(
      study,
      dataProtocol,
      this._getSOPInstanceUIDFromUrl
    );
  }
}

import metadataProvider from '../../../../../classes/MetadataProvider';
import extractZipInfo from './extractZipInfo';
import {
  decorateSOPInstance,
  getAndStoreFileMetadata,
  getContainerIdFromUrl,
  getFilteredZipInfo,
} from '../utils';

export default async function addInstancesToStudy(
  study = {},
  dataProtocol,
  _getSOPInstanceUIDFromUrl
) {
  if (!study.series || study.series.length) {
    return;
  }
  const baseUrl = study.baseURL;
  const containerId = getContainerIdFromUrl(baseUrl);
  const fileName = baseUrl.substring(baseUrl.lastIndexOf('/') + 1);
  const fileMetadata = await getAndStoreFileMetadata(containerId, fileName);
  const fileId = fileMetadata.file_id;
  const zipInfo = await extractZipInfo(fileId).promise;
  const filteredZipInfo = getFilteredZipInfo(zipInfo);
  let instances = [];

  filteredZipInfo.forEach(info => {
    const imagePath = [containerId, fileName, info.path, 0].join('$$$');
    const instance = {
      url: `zip:${imagePath}`,
      Columns: '',
      frame: 0,
      Rows: '',
      SOPClassUID: '0.0.0.0.0',
      StudyInstanceUID: containerId,
    };
    instances.push(instance);

    decorateSOPInstance(instance, fileName, 'file', _getSOPInstanceUIDFromUrl);
  });

  study.series = [
    {
      SeriesInstanceUID: study.StudyInstanceUID,
      SeriesDescription: 'file',
      SeriesNumber: 1,
      Modality: '',
      SeriesDate: '',
      SeriesTime: '',
      instances,
      dataProtocol,
    },
  ];
  study.StudyInstanceUID = containerId;

  metadataProvider.updateFromStudy(study);
}

export const NIFTI_DATA_PROTOCOL = 'nifti';
export const META_IMAGE_DATA_PROTOCOL = 'metaimage';
export const DEFAULT_DATA_PROTOCOL = 'viewer';

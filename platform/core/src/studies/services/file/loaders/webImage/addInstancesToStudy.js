import metadataProvider from '../../../../../classes/MetadataProvider';
import { decorateSOPInstance } from '../utils';

export default function addInstancesToStudy(
  study = {},
  dataProtocol,
  _getSOPInstanceUIDFromUrl
) {
  if (!study.series || study.series.length) {
    return;
  }

  // Fill this up in case io-proxy sends study metadata
  const instance = {
    url: study.baseURL,
    Columns: '',
    frame: '',
    Rows: '',
    SOPClassUID: '0.0.0.0.0',
    StudyInstanceUID: study.StudyInstanceUID,
  };

  decorateSOPInstance(instance, '0.0.0.0', 'file', _getSOPInstanceUIDFromUrl);

  // Fill this up in case io-proxy sends study metadata
  study.series = [
    {
      SeriesInstanceUID: '0.0.0.0',
      SeriesDescription: 'file',
      SeriesNumber: 0,
      Modality: '',
      SeriesDate: '',
      SeriesTime: '',
      instances: [instance],
      dataProtocol,
    },
  ];

  metadataProvider.updateFromStudy(study);
}

import { MODULE_TYPES, utils } from '@ohif/core';
import {
  getReferencedDisplaySet,
  getSourceDisplaySet,
} from './getSourceDisplaySet';
import OHIF from '@ohif/core';
import dcmjs from 'dcmjs';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import loadSegmentation from './loadSegmentation';
import omit from 'lodash/omit';

const { addSegmentationData } = FlywheelCommonRedux.actions;
const { segmentationUtils } = FlywheelCommonUtils;
const { DicomLoaderService } = OHIF.utils;
const { DicomMessage, DicomMetaDictionary } = dcmjs.data;

// TODO: Should probably use dcmjs for this
const SOP_CLASS_UIDS = {
  DICOM_SEG: '1.2.840.10008.5.1.4.1.1.66.4',
};

const sopClassUIDs = Object.values(SOP_CLASS_UIDS);

function mergeBufferList(segArrayBufferList) {
  const bufferList = Array.isArray(segArrayBufferList)
    ? segArrayBufferList
    : [segArrayBufferList];
  let totalLength = 0;
  bufferList.forEach(buffer => (totalLength += buffer.byteLength));
  let mergedBuffer = new Uint8Array(totalLength);
  let offset = 0;
  for (let i = 0; i < bufferList.length; i++) {
    mergedBuffer.set(new Uint8Array(bufferList[i]), offset);
    offset += bufferList[i].byteLength;
  }
  return mergedBuffer;
}

export default function getSopClassHandlerModule({ servicesManager }) {
  // TODO: Handle the case where there is more than one SOP Class Handler for the
  // same SOP Class.
  return {
    id: 'OHIFDicomSegSopClassHandler',
    type: MODULE_TYPES.SOP_CLASS_HANDLER,
    sopClassUIDs,
    getDisplaySetFromSeries: function(
      series,
      study,
      dicomWebClient,
      authorizationHeaders
    ) {
      const instance = series.getFirstInstance();
      const metadata = instance.getData().metadata;

      const {
        SeriesDate,
        SeriesTime,
        SeriesDescription,
        FrameOfReferenceUID,
        SOPInstanceUID,
        SeriesInstanceUID,
        StudyInstanceUID,
        SeriesNumber,
      } = metadata;

      const segDisplaySet = {
        Modality: 'SEG',
        displaySetInstanceUID: utils.guid(),
        wadoRoot: study.getData().wadoRoot,
        wadoUri: instance.getData().wadouri,
        SOPInstanceUID,
        SeriesInstanceUID,
        StudyInstanceUID,
        FrameOfReferenceUID,
        authorizationHeaders,
        isDerived: true,
        referencedDisplaySetUID: null, // Assigned when loaded.
        labelmapIndexes: [], // Assigned when loaded.
        isLoaded: false,
        hasOverlapping: false,
        SeriesDate,
        SeriesTime,
        SeriesNumber,
        SeriesDescription,
        metadata,
      };

      segDisplaySet.getSourceDisplaySet = function(
        studies,
        skipUpdate = false
      ) {
        return getSourceDisplaySet(studies, segDisplaySet, skipUpdate);
      };

      segDisplaySet.setSegmentVisibility = function(
        isVisible,
        referencedDisplaySet,
        itemIndex = 0
      ) {
        return segmentationUtils.setSegmentVisibility(
          segDisplaySet.labelmapIndexes[itemIndex],
          segDisplaySet.segmentationIndexes[itemIndex],
          isVisible
        );
      };

      segDisplaySet.removeSegment = function(
        referencedDisplaySet,
        studies,
        currentSegIndex,
        newSegIndex = 0,
        newLabelmapIndex = -1
      ) {
        return segmentationUtils.removeSegment(
          segDisplaySet,
          referencedDisplaySet,
          studies,
          currentSegIndex,
          newSegIndex,
          newLabelmapIndex
        );
      };

      segDisplaySet.rePositionSegment = function(
        referencedDisplaySet,
        studies,
        newLabelmapIndex,
        itemIndex
      ) {
        return segmentationUtils.rePositionSegment(
          segDisplaySet,
          referencedDisplaySet,
          studies,
          newLabelmapIndex,
          itemIndex
        );
      };

      segDisplaySet.load = async function(
        referencedDisplaySet,
        studies,
        segmentationIndex,
        skipLabelMapReadjust = false
      ) {
        if (
          segDisplaySet.SeriesInstanceUID ===
          referencedDisplaySet.SeriesInstanceUID
        ) {
          referencedDisplaySet = getReferencedDisplaySet(
            segDisplaySet,
            studies
          );
        }
        const segmentData = getSegments(segDisplaySet, referencedDisplaySet)[0];
        segDisplaySet.segmentationIndexes = [];
        const { StudyInstanceUID } = referencedDisplaySet;
        const segArrayBufferList = await DicomLoaderService.findDicomRleEncodedDataPromise(
          segDisplaySet,
          studies
        );
        let segArrayBuffer = mergeBufferList(segArrayBufferList).buffer;
        const {
          PerFrameFunctionalGroupsSequence,
          SharedFunctionalGroupsSequence,
        } = segDisplaySet.metadata;

        // Special handling for supporting SEG with DerivationImageSequence(source image/series information)
        // in SharedFunctionalGroupsSequence instead of PerFrameFunctionalGroupsSequence.
        // dcmjs library expects and retrieve the DerivationImageSequence from PerFrameFunctionalGroupsSequence
        // So here, creating the segmentation buffer by copying/updating those tags.
        let dataset = null;
        const isSourceMultiFrame =
          referencedDisplaySet.numImageFrames > 1 &&
          referencedDisplaySet.isMultiFrame;
        if (
          PerFrameFunctionalGroupsSequence?.length &&
          !PerFrameFunctionalGroupsSequence[0].DerivationImageSequence &&
          SharedFunctionalGroupsSequence?.DerivationImageSequence
        ) {
          dataset = getDICOMDataSetFromBuffer(segArrayBuffer);
          const DerivationImageSequence =
            SharedFunctionalGroupsSequence.DerivationImageSequence;
          const SourceImageSequence =
            DerivationImageSequence?.SourceImageSequence || [];
          if (
            PerFrameFunctionalGroupsSequence.length ===
            SourceImageSequence.length
          ) {
            SourceImageSequence.forEach((sourceSequence, index) => {
              const modifiedSourceSequence = isSourceMultiFrame
                ? {
                    SourceImageSequence: sourceSequence,
                  }
                : {
                    SourceImageSequence: omit(
                      sourceSequence,
                      'ReferencedFrameNumber'
                    ),
                  };
              PerFrameFunctionalGroupsSequence[
                index
              ].DerivationImageSequence = modifiedSourceSequence;
            });
          }
          dataset.PerFrameFunctionalGroupsSequence =
            segDisplaySet.metadata.PerFrameFunctionalGroupsSequence;
        }

        let firstSourceImage = null;
        if (PerFrameFunctionalGroupsSequence?.length > 0) {
          firstSourceImage = getSourceImage(
            PerFrameFunctionalGroupsSequence,
            SharedFunctionalGroupsSequence,
            0
          );
          if (isSourceMultiFrame) {
            dataset = dataset || getDICOMDataSetFromBuffer(segArrayBuffer);
            // Special handling to adjust the frame number to match with the dcmjs usage of comparing
            // frame number with stack image ids.
            dataset.PerFrameFunctionalGroupsSequence.forEach(
              (perFrameFunctionalGroup, index) => {
                const sourceImage = getSourceImage(
                  dataset.PerFrameFunctionalGroupsSequence,
                  dataset.SharedFunctionalGroupsSequence,
                  index
                );
                if (sourceImage.ReferencedFrameNumber > 0) {
                  sourceImage.ReferencedFrameNumber =
                    sourceImage.ReferencedFrameNumber + 1;
                }
              }
            );
          }
          // Special handling for test datas to remove the SourceImageSequence coming as
          // an object instead of array in root data set, if it is already present in per frame
          // sequence.
          if (
            !Array.isArray(segDisplaySet.metadata.SourceImageSequence || [])
          ) {
            dataset = dataset || getDICOMDataSetFromBuffer(segArrayBuffer);
            if (firstSourceImage && dataset.SourceImageSequence) {
              delete dataset.SourceImageSequence;
            }
          }
        }

        if (dataset) {
          segArrayBuffer =
            dcmjs.data.datasetToBuffer(dataset)?.buffer || segArrayBuffer;
        }
        const imageIds = segmentationUtils.getImageIdsForDisplaySet(
          studies,
          StudyInstanceUID,
          referencedDisplaySet.SeriesInstanceUID
        );
        let referenceImageIds = imageIds;
        let skipOverlapping = false;
        // Adjust the image id to change to original image id format from the
        // customized format.
        if (isSourceMultiFrame) {
          referenceImageIds = [];
          imageIds.forEach(imageId => {
            const targetImageId = imageId.replace('frames/', 'frame=');
            referenceImageIds.push(targetImageId);
          });
          if (PerFrameFunctionalGroupsSequence.length !== imageIds.length) {
            skipOverlapping = true;
          }
        }
        return new Promise(async (resolve, reject) => {
          let results;
          try {
            results = _parseSeg(
              segArrayBuffer,
              referenceImageIds,
              skipOverlapping
            );
          } catch (error) {
            segDisplaySet.isLoaded = false;
            segDisplaySet.loadError = true;
            reject(error);
          }
          const {
            labelmapBufferArray,
            segMetadata,
            segmentsOnFrame,
            segmentsOnFrameArray,
          } = results;

          const segIndexes = [segmentationIndex];
          if (labelmapBufferArray.length > 1 && segmentData) {
            const storeUpdatePromise = waitForStoreUpdate(
              segDisplaySet,
              referencedDisplaySet,
              labelmapBufferArray.length
            );
            for (let i = 1; i < labelmapBufferArray.length; i++) {
              store.dispatch(
                addSegmentationData({
                  referenceUID: segmentData.referenceUID,
                  name: segmentData.name + '_' + i,
                  parent_ref: { id: segmentData.containerId },
                  file_id: segmentData.fileId,
                  modality: segmentData.modality,
                  isLoadedInitially: true,
                  studyInstanceUID: segmentData.studyInstanceUID,
                  seriesInstanceUID: segmentData.seriesInstanceUID,
                  displaySetInstanceUIDs: segmentData.displaySetInstanceUIDs,
                  dataProtocol: 'dicom',
                })
              );
            }
            await storeUpdatePromise;
          }
          const segments = getSegments(segDisplaySet, referencedDisplaySet);
          segments.forEach(seg => {
            if (seg.originalId !== segmentationIndex) {
              segIndexes.push(seg.originalId);
            }
          });
          correctSegmentationIndexInInputData(results, segIndexes);

          let labelmapIndex;
          if (labelmapBufferArray.length > 1) {
            let labelmapIndexes = [];
            for (let i = 0; i < labelmapBufferArray.length; ++i) {
              labelmapIndexes.push(
                await loadSegmentation(
                  segDisplaySet,
                  referencedDisplaySet,
                  studies,
                  labelmapBufferArray[i],
                  segMetadata,
                  segmentsOnFrame,
                  segmentsOnFrameArray[i],
                  segIndexes[i],
                  skipLabelMapReadjust
                )
              );
            }
            /**
             * Since overlapping segmentations have virtual labelmaps,
             * originLabelMapIndex is used in the panel to select the correct dropdown value.
             */
            segDisplaySet.hasOverlapping = true;
            segDisplaySet.originLabelMapIndex = labelmapIndexes[0];
            labelmapIndex = labelmapIndexes[0];
            console.warn('Overlapping segmentations!');
          } else {
            labelmapIndex = await loadSegmentation(
              segDisplaySet,
              referencedDisplaySet,
              studies,
              labelmapBufferArray[0],
              segMetadata,
              segmentsOnFrame,
              segmentsOnFrameArray[0],
              segIndexes[0],
              skipLabelMapReadjust
            );
          }
          segDisplaySet.isLoaded = true;
          resolve(labelmapIndex);
        });
      };

      return segDisplaySet;
    },
  };
}

async function waitForStoreUpdate(
  segDisplaySet,
  referencedDisplaySet,
  labelMapCount
) {
  return new Promise((resolve, reject) => {
    const unsubscribeFn = store.subscribe(() => {
      if (
        labelMapCount === getSegmentCount(segDisplaySet, referencedDisplaySet)
      ) {
        unsubscribeFn();
        resolve();
      }
    });
  });
}

// Update the segmentation index to sync with the viewer existing segments
function correctSegmentationIndexInInputData(segmentationResults, segIndexes) {
  const {
    segMetadata,
    segmentsOnFrame,
    segmentsOnFrameArray,
    labelmapBufferArray,
  } = segmentationResults;
  const currentSegIndexs = [];
  const currentSegDatas = [];
  // Clearing the data from existing segmentation index and move it to the position of
  // new segmentation index passing to sync with lready loaded segments and store segment data
  (segMetadata?.data || []).forEach((data, index) => {
    if (data) {
      currentSegIndexs.push(index);
      currentSegDatas.push(data);
      segMetadata.data[index] = undefined;
    }
  });
  if (currentSegIndexs.length === segIndexes.length) {
    currentSegIndexs.forEach((curSegIndex, index) => {
      const newSegIndex = segIndexes[index];
      segMetadata.data[newSegIndex] = currentSegDatas[index];
      segMetadata.data[newSegIndex].SegmentNumber = newSegIndex;
    });
    correctSegmentsOnFrame(segmentsOnFrame, currentSegIndexs, segIndexes);
    segmentsOnFrameArray.forEach(segmentOnFrameArray => {
      if (!segmentsOnFrameArray) {
        return;
      }
      correctSegmentsOnFrame(segmentOnFrameArray, currentSegIndexs, segIndexes);
    });
  }
}

function correctSegmentsOnFrame(segmentsOnFrame, currentSegIndexs, segIndexes) {
  (segmentsOnFrame || []).forEach(segments => {
    if (!segments) {
      return;
    }
    (segments || []).forEach((segment, index) => {
      const segmentOnFrameIndex = currentSegIndexs.findIndex(
        currentSegIndex => segment === currentSegIndex
      );
      if (segmentOnFrameIndex >= 0) {
        const newSegIndex = segIndexes[segmentOnFrameIndex];
        segments[index] = newSegIndex;
      }
    });
  });
}

function _parseSeg(arrayBuffer, imageIds, skipOverlapping = false) {
  return dcmjs.adapters.Cornerstone.Segmentation.generateToolState(
    imageIds,
    arrayBuffer,
    cornerstone.metaData,
    skipOverlapping
  );
}

function getDICOMDataSetFromBuffer(segArrayBuffer) {
  const dicomData = DicomMessage.readFile(segArrayBuffer);
  const dataset = DicomMetaDictionary.naturalizeDataset(dicomData.dict);
  dataset._meta = DicomMetaDictionary.namifyDataset(dicomData.meta);
  return dataset;
}

function getSourceImage(
  perFrameFunctionalGroupsSequence,
  sharedFunctionalGroupsSequence,
  frameIndex = 0
) {
  const sourceImageSequence =
    perFrameFunctionalGroupsSequence[frameIndex]?.DerivationImageSequence
      ?.SourceImageSequence ||
    (sharedFunctionalGroupsSequence.DerivationImageSequence
      ?.SourceImageSequence || [])[frameIndex];
  return sourceImageSequence?.[0] || sourceImageSequence;
}

function getSegments(segDisplaySet, referencedDisplaySet) {
  const segmentationData =
    store.getState().segmentation.segmentationData[
      referencedDisplaySet.SeriesInstanceUID
    ] || [];
  const segments = segmentationData.filter(data =>
    data.displaySetInstanceUIDs?.includes(segDisplaySet.displaySetInstanceUID)
  );
  return segments || [];
}

function getSegmentCount(segDisplaySet, referencedDisplaySet) {
  const segmentationData =
    store.getState().segmentation.segmentationData[
      referencedDisplaySet.SeriesInstanceUID
    ] || [];
  const segments = segmentationData.filter(data =>
    data.displaySetInstanceUIDs?.includes(segDisplaySet.displaySetInstanceUID)
  );
  return segments.length;
}

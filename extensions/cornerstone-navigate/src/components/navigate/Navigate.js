import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { connect } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import { Icon } from '@ohif/ui';
import { redux, utils } from '@ohif/core';
import './Navigate.styl';
import classnames from 'classnames';
import {
  Redux as FlywheelCommonRedux,
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import { Redux as FlywheelRedux, Utils as FlywheelUtils } from '@flywheel/extension-flywheel';
const { setLayout } = redux.actions;

const { getRouteParams } = FlywheelCommonUtils;
const {
  getAcquisitionAssociation,
  getConfig,
  getProject,
  getProjectAssociations,
  getSession,
  listAcquisitions,
  listSessionAnalyses,
  listSessionsBySubjectId,
  listSubjectAnalyses,
  listSubjects,
} = FlywheelCommonHTTP.services;
const {
  addMultiSessionData,
  clearMultiSessionData,
  setLoaderDisplayStatus
} = FlywheelCommonRedux.actions;
const {
  setAllSessions,
  setAssociations,
  setFileAssociations,
  setCurrentNifti,
  setActiveProtocolStore
} = FlywheelRedux.actions;
const {
  selectActiveSessions,
  selectAllSessions,
  selectAllAssociations,
  selectActiveProtocolStore
} = FlywheelRedux.selectors;
const { selectCurrentNiftis } = FlywheelCommonRedux.selectors;
const { studyMetadataManager } = utils;
const { setViewportLayoutAndData, setViewportActive } = redux.actions;
const { resetLayoutToHP } = FlywheelUtils;

const Navigate = props => {
  const allAssociations = useSelector(store => {
    return selectAllAssociations(store);
  });
  const allSessions = useSelector(store => {
    return selectAllSessions(store);
  });
  const sessions = useSelector(store => {
    return selectActiveSessions(store);
  });

  let niftiSessions = [];
  const currentNiftis = selectCurrentNiftis(store.getState());
  if (currentNiftis.length > 0) {
    currentNiftis.forEach(currentNifti => {
      niftiSessions.push(allSessions[currentNifti.parents.session]);
    })
  }

  const currentViewports = useSelector(store => {
    return store.viewports;
  });
  const protocolLayout = useSelector(store => {
    return store.infusions?.protocolLayout;
  });

  const dispatch = useDispatch();
  const [loadedNavigationData, setLoadedNavigationData] = useState({});
  const [data, setData] = useState({});
  const [activeAcquisitionData, setActiveAcquisitionData] = useState([]);
  const [isWaitForDisplaySet, setIsWaitForDisplaySet] = useState(false);
  const [isRequiredDisplaySetUpdate, setIsRequiredDisplaySetUpdate] = useState(
    false
  );
  const [isRequiredLayoutUpdate, setIsRequiredLayoutUpdate] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isSessionsInitialized, setIsSessionsInitialized] = useState({});
  const [isAcquisitionsInitialized, setIsAcquisitionsInitialized] = useState({});
  const [isMultiSubjectView, setIsMultiSubjectView] = useState(false);

  const [subjectAnalyses, setSubjectAnalyses] = useState([]);
  const [sessionAnalyses, setSessionAnalyses] = useState([]);
  const [projectAnalysesInput, setProjectAnalysesInput] = useState([]);
  const [projectAnalysesOutput, setProjectAnalysesOutput] = useState([]);
  const [headerItem] = useState([{}]);

  const params = getRouteParams(history.location);

  useEffect(() => {
    if ((loadedNavigationData.subject && loadedNavigationData.subject.length === 0) &&
      (sessions && (sessions.length > 0) || niftiSessions && (niftiSessions.length > 0))) {
      initializeSubjects(sessions || niftiSessions);
    }
  }, [loadedNavigationData]);

  useEffect(() => {
    if ((sessions && (sessions.length > 0) || niftiSessions && (niftiSessions.length > 0))
      && (activeAcquisitionData.length === 0)) {
      if (loadedNavigationData.subject && loadedNavigationData.subject.length === 0) {
        initializeSubjects(sessions || niftiSessions);
      }
      initializeActiveAcquisitions(sessions || niftiSessions);
    }
  }, [sessions || niftiSessions]);

  useEffect(() => {
    setLoading(true);
    let projectId = params.projectId;
    getConfig().then(settingsConfig => {
      setIsMultiSubjectView(settingsConfig && settingsConfig.features.ohif_multi_subject_view ? true : false);
      getProject(projectId).then(res_project => {
        if (res_project) {
          let newData = {
            project: res_project.label,
            projectId: res_project._id,
            subject: [],
            analyses: res_project.analyses,
          };
          const stateData = cloneDeep(newData);
          setData(stateData);
          setLoadedNavigationData({ ...newData });
        }
      });
    });
  }, []);

  const getSubjectsAnalyses = (subjectId) => {
    listSubjectAnalyses(subjectId).then(res => {
      if (res) {
        let result = [...res];
        let navigateData = { ...data };
        const resultNavigateData = navigateData.subject.find(
          item => item.subjectId === subjectId
        );
        result = result.filter((item) => {
          item.files = item.files.filter((data) => {
            return data.type === "dicom" || data.type === "image" || data.type === "nifti";
          });
          item.inputs = item.inputs.filter((data) => {
            return data.type === "dicom" || data.type === "image" || data.type === "nifti";
          });
          if (item.files.length > 0 || item.inputs.length > 0) {
            return item;
          }
        });
        resultNavigateData.subjectAnalyses = result;
        resultNavigateData.subjectAnalyses && resultNavigateData.subjectAnalyses.map((item) => {
          item.analysis = [];
        });
        setSubjectAnalyses(result);
      }
    });
  }

  const getSessionAnalyses = (subjectId, sessionId) => {
    listSessionAnalyses(sessionId).then(res => {
      if (res) {
        let result = [...res];
        let navigateData = { ...data };
        const resultNavigateData = navigateData.subject.find(
          item => item.subjectId === subjectId
        ).session.find((data) =>
          data.sessionId === sessionId
        );
        result = result.filter((item) => {
          item.files = item.files.filter((data) => {
            return data.type === "dicom" || data.type === "image" || data.type === "nifti";
          });
          item.inputs = item.inputs.filter((data) => {
            return data.type === "dicom" || data.type === "image" || data.type === "nifti";
          });
          if (item.files.length > 0 || item.inputs.length > 0) {
            return item;
          }
        });
        resultNavigateData.sessionAnalyses = result;
        resultNavigateData.sessionAnalyses && resultNavigateData.sessionAnalyses.map((item) => {
          item.analysis = [];
        });
        setSessionAnalyses(result);
      }
    });
  }

  const getActiveAssociation = () => {
    const { viewportSpecificData, activeViewportIndex } = currentViewports;
    const activeViewportData = viewportSpecificData[activeViewportIndex];
    const associationKey = Object.keys(allAssociations).find(key => (allAssociations[key].study_uid === activeViewportData.StudyInstanceUID) &&
      (allAssociations[key].series_uid === activeViewportData.SeriesInstanceUID));
    let activeAssociation = allAssociations[associationKey];
    if (currentNiftis.length > 0) {
      activeAssociation = {
        subjectId: currentNiftis[0].parents.subject,
        session_id: currentNiftis[0].parents.session,
        acquisition_id: currentNiftis[0].parents.acquisition,
        study_uid: activeViewportData.StudyInstanceUID,
        series_uid: activeViewportData.SeriesInstanceUID,
        containerId: currentNiftis[0].containerId,
      };
    }
    return activeAssociation;
  }

  const initializeSubjects = (activeSessions) => {
    // To retrieve the subjects
    listSubjects(loadedNavigationData.projectId).then(res_subject => {
      setLoading(false);
      if (res_subject) {
        let subjectData = { ...loadedNavigationData };
        if (isMultiSubjectView) {
          const subjectsData = (res_subject || []).map(subjectItem => ({
            subjectLabel: subjectItem.label,
            subjectId: subjectItem._id,
            session: [],
          }));
          let projectAnalyses = [];
          subjectData.analyses.map((item) => {
            item.files = item.files.filter((data) => {
              return data.type === "dicom" || data.type === "image" || data.type === "nifti";
            });
            item.inputs = item.inputs.filter((data) => {
              return data.type === "dicom" || data.type === "image" || data.type === "nifti";
            });
            if (item.files.length > 0 || item.inputs.length > 0) {
              const projectAnalysis = {
                analysisLabel: subjectData.analyses[0].label,
                analysisId: subjectData.analyses[0]._id,
                analysisFiles: subjectData.analyses[0].files,
                analysisInuts: subjectData.analyses[0].inputs,
              }
              projectAnalyses.push(projectAnalysis);
            }
          });
          subjectData.subject = [...subjectsData, ...projectAnalyses];
        } else {
          const activeAssociation = getActiveAssociation();
          let activeSubjectId = "";
          if (activeAssociation && allSessions[activeAssociation.session_id]) {
            activeSubjectId = allSessions[activeAssociation.session_id].parents.subject;
          } else {
            activeSubjectId = activeSessions[0].parents.subject;
          }
          const subject = (res_subject || []).find(subjectItem => subjectItem._id === activeSubjectId);
          const subjectsData = [{
            subjectLabel: subject.label,
            subjectId: subject._id,
            session: [],
          }];
          let projectAnalyses = [];
          subjectData.analyses.map((item) => {
            item.files = item.files.filter((data) => {
              return data.type === "dicom" || data.type === "image" || data.type === "nifti";
            });
            item.inputs = item.inputs.filter((data) => {
              return data.type === "dicom" || data.type === "image" || data.type === "nifti";
            });
            if (item.files.length > 0 || item.inputs.length > 0) {
              const projectAnalysis = {
                analysisLabel: subjectData.analyses[0].label,
                analysisId: subjectData.analyses[0]._id,
                analysisFiles: subjectData.analyses[0].files,
                analysisInuts: subjectData.analyses[0].inputs,
              }
              projectAnalyses.push(projectAnalysis);
            }
          });
          subjectData.subject = [...subjectsData, ...projectAnalyses];
        }
        const stateData = cloneDeep(subjectData);
        setData(stateData);
        setLoadedNavigationData({ ...subjectData });
      }
    });
  }

  const initializeActiveAcquisitions = (activeSessions) => {
    if (multiSessionData && multiSessionData.isMultiSessionViewEnabled) {
      const activeData = (multiSessionData.acquisitionData || []).map(acquisition => ({
        projectId: acquisition.projectId,
        subjectId: acquisition.subjectId,
        sessionId: acquisition.sessionId,
        acquisitionId: acquisition.acquisitionId,
        studyInstanceUID: acquisition.studyUid,
        seriesInstanceUID: acquisition.seriesUid,
        containerId: acquisition.containerId || '',
      }));
      setActiveAcquisitionData(activeData);
      return;
    }

    if (!activeSessions.length) {
      return;
    }

    const { viewportSpecificData, activeViewportIndex } = currentViewports;
    const activeViewportData = viewportSpecificData[activeViewportIndex];
    const activeAssociation = getActiveAssociation();
    if (activeAssociation && allSessions[activeAssociation.session_id]) {
      const currentAcqData = {
        projectId: params.projectId,
        subjectId: allSessions[activeAssociation.session_id].parents.subject,
        sessionId: activeAssociation.session_id,
        acquisitionId: activeAssociation.acquisition_id,
        studyInstanceUID: activeAssociation.study_uid,
        seriesInstanceUID: activeAssociation.series_uid,
        containerId: activeAssociation.containerId || '',
      };
      setActiveAcquisitionData([currentAcqData]);
    } else {
      const currentSessionId = activeSessions[0]._id;
      loadSessionAndAssociations(currentSessionId);
      listAcquisitions(currentSessionId)
      .filter(item => item.acquisition_id !== 'None')
      .then(res => {
        if (res) {
          res &&
            res.length > 0 &&
            res.map(item => {
              getAcquisitionAssociation(params.projectId, item._id).then(response => {
                if (response) {
                  if (
                    response.study_uid === activeViewportData.StudyInstanceUID &&
                    response.series_uid === activeViewportData.SeriesInstanceUID
                  ) {
                    const currentAcqData = {
                      projectId: params.projectId,
                      subjectId: activeSessions[0].parents.subject,
                      sessionId: currentSessionId,
                      acquisitionId: item._id,
                      studyInstanceUID: activeViewportData.StudyInstanceUID,
                      seriesInstanceUID: activeViewportData.SeriesInstanceUID,
                    };
                    setActiveAcquisitionData([currentAcqData]);
                  }
                }
              });
            });
        }
      });
    }
  }

  const studyData = useSelector(store => {
    return store?.studies?.studyData;
  });
  const multiSessionData = useSelector(store => {
    return store?.multiSessionData;
  });
  useEffect(() => {
    // Handles the multi session view - wait for display set is ready
    if (
      isRequiredDisplaySetUpdate
    ) {
      if (!isWaitForDisplaySet) {
        setTimeout(() => { updateOnDisplaySetReady() }, 0);
      } else {
        setTimeout(() => { setIsWaitForDisplaySet(false) }, 100);
      }
    }
  }, [isWaitForDisplaySet]);

  const updateOnDisplaySetReady = () => {
    if (
      multiSessionData &&
      multiSessionData.acquisitionData
    ) {
      const studies = studyMetadataManager.all();
      if (!multiSessionData.isMultiSessionViewEnabled) {
        const study = studies.find(
          study =>
            study.studyInstanceUID ===
            activeAcquisitionData[0].studyInstanceUID
        );
        if (study && study._displaySets.length > 0) {
          const displaySet = study._displaySets.find(
            ds =>
              ds.SeriesInstanceUID ===
              activeAcquisitionData[0].seriesInstanceUID
          );
          setIsRequiredDisplaySetUpdate(false);
          updateLayout([displaySet]);
          dispatch(setLoaderDisplayStatus(false));
        }
      } else {
        const acquisitionsData = multiSessionData.acquisitionData;
        const study = studies.find(
          study =>
            study.studyInstanceUID ===
            acquisitionsData[acquisitionsData.length - 1].studyUid
        );

        if (study && study._displaySets.length > 0) {
          const displaySets = [];
          let isClearMultiSession = false;
          acquisitionsData.forEach(acquisition => {
            const acquisitionStudy = studies.find(
              study => study.studyInstanceUID === acquisition.studyUid
            );
            const ds = acquisitionStudy._displaySets.find(
              ds => ds.SeriesInstanceUID === acquisition.seriesUid
            );
            if (currentNiftis.length > 0) {
              acquisitionStudy._displaySets.forEach(
                displaysSet => {
                  if (displaysSet && displaysSet.images && displaysSet.images.length > 0 &&
                    displaysSet.StudyInstanceUID === acquisition.studyUid) {
                    displaySets.push(displaysSet);
                  }
                  else if (displaysSet && !displaysSet.images) {
                    isClearMultiSession = true;
                  }
                }
              );
            } else {
              if (ds && ds.images && ds.images.length > 0) {
                displaySets.push(ds);
              } else if (ds && !ds.images) {
                // To handle selection of non-displayable images like overlays
                isClearMultiSession = true;
              }
            }
          });
          if ((currentNiftis.length > 0 && displaySets.length === acquisitionsData.length * 3) ||
            (displaySets.length === acquisitionsData.length)) {
            setIsRequiredDisplaySetUpdate(false);
            updateLayout(displaySets);
            dispatch(setLoaderDisplayStatus(false));
          } else {
            if (isClearMultiSession) {
              let activeAcqData = cloneDeep(activeAcquisitionData);
              clearMultiSessionView(activeAcqData, 1, false);
              dispatch(setLoaderDisplayStatus(false));
            } else {
              setIsWaitForDisplaySet(true);
            }
          }
        } else {
          setIsWaitForDisplaySet(true);
        }
      }
    }
  };

  useEffect(() => {
    // Handles the multi session view acquisition selection update
    if (
      isRequiredDisplaySetUpdate &&
      multiSessionData &&
      multiSessionData.acquisitionData
    ) {
      setTimeout(() => { updateOnDisplaySetReady() }, 0);
    }
  }, [studyData, isRequiredDisplaySetUpdate]);

  const calculateLayout = (currentRows, currentColumns, isInMprMode) => {
    let numColumns = currentNiftis.length > 0 ? currentColumns : currentRows;
    let numRows = currentNiftis.length > 0 ? currentRows : currentColumns;
    const acquisitionsData = multiSessionData.acquisitionData;
    if (!multiSessionData.isMultiSessionViewEnabled) {
      numColumns = currentNiftis.length > 0 ? currentColumns : 1;
      numRows = 1;
    } else {
      if (isInMprMode) {
        numColumns = acquisitionsData.length;
        numRows = 1;
      } else {
        if (isRequiredLayoutUpdate) {
          if (
            currentColumns === 1 &&
            currentRows === 1
          ) {
            numColumns *= acquisitionsData.length;
          } else if (currentColumns > 1) {
            numRows *= acquisitionsData.length;
          } else if (currentRows > 1) {
            numColumns *= acquisitionsData.length;
          }
        }
      }
    }
    return { numRows: numRows, numColumns: numColumns };
  }

  const updateLayout = displaySets => {
    const viewports = [];
    const state = store.getState();
    const isCurrentNifti = currentNiftis.length > 0;
    const plugin = isCurrentNifti ? 'cornerstone::nifti' : 'cornerstone';
    let currentLayout = currentViewports.layout;
    let isInMprMode = currentLayout.viewports.find(
      viewport => viewport && viewport.vtk
    );
    let numColumns = currentViewports.numColumns;
    let numRows = currentViewports.numRows;
    let numViewports = currentLayout.viewports.length;
    let layouts = [];
    for (let i = 0; i < numViewports; i++) {
      layouts.push({ plugin })
    }
    const protocolStore = selectActiveProtocolStore(state);
    // Clear the protocol layout before switching from/to the multisession layout
    if (protocolLayout) {
      numColumns = isCurrentNifti ? 3 : 1;
      numRows = 1;
      numViewports = isCurrentNifti ? 3 : 1;
      currentLayout = {
        numRows,
        numColumns,
        viewports: layouts,
      };
      isInMprMode = false;
      dispatch(setLayout(currentLayout));
    } else if (protocolStore && !multiSessionData.isMultiSessionViewEnabled) {
      resetLayoutToHP(props.commandsManager, store, protocolStore);
    }
    if (isRequiredLayoutUpdate || isInMprMode) {
      const layout = calculateLayout(numRows, numColumns, isInMprMode);
      numRows = layout.numRows;
      numColumns = layout.numColumns;
      numViewports = numRows * numColumns;
      setIsRequiredLayoutUpdate(false);
    }
    const viewportSpecificData = {};
    let dsIndex = 0;
    let index = 0;
    const viewportsPerDS = numViewports / displaySets.length;
    for (let i = 0; i < viewportsPerDS; i++) {
      viewports.push({
        plugin,
      });
      viewportSpecificData[index] = displaySets[dsIndex];
      viewportSpecificData[index].plugin = plugin;
      if (i + 1 === viewportsPerDS) {
        i = -1;
        dsIndex += 1;
      }
      index++;
      if (index === numViewports) {
        break;
      }
    }
    const maxActiveIndex = numViewports - 1;
    if (currentViewports.activeViewportIndex > maxActiveIndex) {
      dispatch(setViewportActive(0));
    }

    dispatch(
      setViewportLayoutAndData(
        {
          numRows,
          numColumns,
          viewports,
        },
        viewportSpecificData
      )
    );
  };

  const clearMultiSessionView = (activeAcqData, index, updateStatus = true) => {
    if (activeAcqData && (index >= 0)) {
      setIsRequiredLayoutUpdate(updateStatus);
      activeAcqData.splice(index, 1);
      setActiveAcquisitionData(activeAcqData);
      dispatch(clearMultiSessionData());
      setIsRequiredDisplaySetUpdate(updateStatus);
    } else {
      dispatch(setLoaderDisplayStatus(false));
      dispatch(clearMultiSessionData());
    }
  }

  const updateStateAcquisitions = (subjectId, sessionId, acquisitions) => {
    let stateData = { ...data };
    let stateResult = stateData.subject.find(item => item.subjectId === subjectId);
    let stateResultSession = stateResult.session.find(acquisitionItem => acquisitionItem.sessionId === sessionId);
    stateResultSession.acquisition = cloneDeep(acquisitions);
    setData(stateData);
  }

  const loadAcquisitions = (subjectId, sessionId, updateState = false) => {
    if (loadedNavigationData.subject && loadedNavigationData.subject.length > 0) {
      let result = loadedNavigationData.subject.find(item => item.subjectId === subjectId);
      if (result && result.session.length > 0) {
        let resultSession = result.session.find(acquisitionItem => acquisitionItem.sessionId === sessionId);
        if (resultSession && resultSession.acquisition.length > 0) {
          if (updateState) {
            updateStateAcquisitions(subjectId, sessionId, resultSession.acquisition);
          }
          return;
        }
      }
    }

    loadSessionAndAssociations(sessionId);
    listAcquisitions(sessionId).then(res => {
      if (res) {
        let acquisitionList = []
        if (currentNiftis.length > 0) {
          res.forEach(acquisition => {
            acquisition.files.forEach(file => acquisitionList.push({
              acquisitionId: acquisition._id,
              acquisitionLabel: file.name,
              fileName: file.name,
              fileId: file.file_id,
              containerId: file.parent_ref.id
            }))
          })
        } else {
          acquisitionList = (res || []).map(item => ({
            acquisitionId: item._id,
            acquisitionLabel: item.label,
          }));
        }
        const result = loadedNavigationData.subject.map(item => {
          if (item.subjectId === subjectId) {
            item.session.map(acquisitionItem => {
              if (acquisitionItem.sessionId === sessionId) {
                acquisitionItem.acquisition = acquisitionList;
              }
            });
          }
          return item;
        });
        setLoadedNavigationData(loadedNavigationData);
        if (updateState
        ) {
          updateStateAcquisitions(subjectId, sessionId, acquisitionList);
        }
      }
    });
  }

  const loadSessionAndAssociations = sessionId => {
    getProjectAssociations(params.projectId, { session_id: sessionId }).then(associations => {
      dispatch(setAssociations(associations));
    });

    if (currentNiftis.length > 0) {
      const { fileContainerId, filename } = getRouteParams(window.location);
      dispatch(setFileAssociations(
        { containerId: fileContainerId, filename: filename }));
    }
    if (!(sessionId in allSessions)) {
      getSession(sessionId).then(session => {
        setAllSessions({
          [session._id]: session
        });
      });
    }
  }

  const expandAcquisition = (event, subjectId, sessionId) => {
    let navigateData = { ...data };
    const resultNavigateData = navigateData.subject.find(
      item => item.subjectId === subjectId
    );
    if (resultNavigateData.session.length !== 0) {
      const resultSessionData = resultNavigateData.session.find(
        item => item.sessionId === sessionId
      );
      if (resultSessionData.acquisition.length === 0) {
        loadAcquisitions(subjectId, sessionId, true);
        getSessionAnalyses(subjectId, sessionId);
      } else {
        let result = navigateData.subject.map(item => {
          if (item.subjectId === subjectId) {
            item.session.map(acquisitionItem => {
              if (acquisitionItem.sessionId === sessionId) {
                acquisitionItem.acquisition = [];
                acquisitionItem.sessionAnalyses = [];
              }
            });
          }
          return item;
        });
        setData(navigateData);
      }
    }
  };

  const selectAcquisition = (
    event,
    projectId,
    subjectId,
    sessionId,
    acquisitionId,
    acquisitionLabel = null,
    containerId = null
  ) => {
    let alreadyActiveIndex = -1;
    if (activeAcquisitionData && activeAcquisitionData.length > 0) {
      alreadyActiveIndex = activeAcquisitionData.findIndex(
        acquisitionData =>
        (currentNiftis.length > 0 ? (!acquisitionLabel || acquisitionData.studyInstanceUID === acquisitionLabel)
          : (acquisitionData.acquisitionId === acquisitionId)
        )
      );
      if (alreadyActiveIndex === 0) { // never remove the current loaded session acquisition
        dispatch(setLoaderDisplayStatus(false));
        return;
      }
    }
    if (
      !multiSessionData ||
      !multiSessionData.acquisitionData ||
      multiSessionData.acquisitionData.length === 0
    ) {
      if (activeAcquisitionData && activeAcquisitionData.length > 0) {
        const currentData = activeAcquisitionData[0];
        dispatch(
          addMultiSessionData(
            currentData.projectId,
            currentData.subjectId,
            currentData.sessionId,
            currentData.acquisitionId,
            currentData.studyInstanceUID,
            currentData.seriesInstanceUID,
            currentData.containerId,
            0
          )
        );
      }
    }

    let activeAcqData = cloneDeep(activeAcquisitionData);
    dispatch(setLoaderDisplayStatus(true));
    if (alreadyActiveIndex >= 0) {
      clearMultiSessionView(activeAcqData, alreadyActiveIndex);
      if (currentNiftis.length) {
        const clearExisiting = currentNiftis.length === 1;
        currentNiftis.forEach(file => {
          dispatch(setCurrentNifti(
            file.containerId,
            file.filename,
            file.file_id,
            file.info,
            file.parents,
            clearExisiting
          ));
        })
      }
    } else {
      try {
        const isMultiSessionViewEnabled = multiSessionData
          ? multiSessionData.isMultiSessionViewEnabled
          : false;
        if (!containerId) {
          getAcquisitionAssociation(projectId, acquisitionId).then(res => {
            if (res) {
              setIsRequiredLayoutUpdate(!isMultiSessionViewEnabled);
              const obj = {
                projectId: projectId,
                subjectId: subjectId,
                sessionId: sessionId,
                acquisitionId: acquisitionId,
                studyInstanceUID: res.study_uid,
                seriesInstanceUID: res.series_uid
              };
              activeAcqData.splice(1, activeAcqData.length - 1, obj);
              setActiveAcquisitionData(activeAcqData);
              dispatch(
                addMultiSessionData(
                  projectId,
                  subjectId,
                  sessionId,
                  acquisitionId,
                  res.study_uid,
                  res.series_uid,
                  '',
                  1
                )
              );
              setIsRequiredDisplaySetUpdate(true);
            } else {
              // If the acquisition selected is not valid, then reset and clear multi-session data
              if (isMultiSessionViewEnabled) {
                clearMultiSessionView(activeAcqData, alreadyActiveIndex);
              } else {
                clearMultiSessionView();
              }
            }
          });
        }
        else {
          setIsRequiredLayoutUpdate(!isMultiSessionViewEnabled);
          const obj = {
            projectId: projectId,
            subjectId: subjectId,
            sessionId: sessionId,
            acquisitionId: acquisitionId,
            studyInstanceUID: acquisitionLabel,
            seriesInstanceUID: '',
            containerId: containerId
          };
          activeAcqData.splice(1, activeAcqData.length - 1, obj);
          setActiveAcquisitionData(activeAcqData);
          dispatch(
            addMultiSessionData(
              projectId,
              subjectId,
              sessionId,
              acquisitionId,
              acquisitionLabel,
              '',
              containerId,
              1
            )
          );
          setIsRequiredDisplaySetUpdate(true);
        }
      } catch (error) {
        clearMultiSessionView();
      }
    }
  };

  const isExpanded = (subjectId, sessionId) => {
    const subject = data.subject.find(
      item => item.subjectId === subjectId
    );
    if (!subject) {
      return false;
    } else if (!sessionId) {
      return (subject.session.length > 0) ? true : false;
    }
    const session = subject.session.find(
      item => item.sessionId === sessionId
    );
    if (!session) {
      return false;
    }
    return (session.acquisition.length > 0) ? true : false;
  };

  const isActiveSession = (sessionId) => {
    return (activeAcquisitionData.find(acq => (acq.sessionId === sessionId)));
  }

  const isActiveAcquisition = (acquisitionId, acquisitionLabel) => {
    return (activeAcquisitionData.find(acq =>
    (currentNiftis.length > 0 ? (!acquisitionLabel || acq.studyInstanceUID === acquisitionLabel)
      : (acq.acquisitionId === acquisitionId))
    ));
  }

  const returnElement = (index, classList, name) => {
    return (
      <li key={index}>
        <div className={classList}>
          {name}
        </div>
      </li>
    )
  };

  function treeView(isCurrentProject = true) {
    return (
      <ul className="subjectActive">
        {data.subject &&
          data.subject.map((subjectItem, index) => {
            if (subjectItem.subjectId === undefined) {
              if (isCurrentProject) {
                return (
                  <li key={index} className={'listCenter'}>
                    <div className="alignItemsCenter">
                      <Icon
                        className={classnames(
                          'caret',
                          'material-icons',
                          'treeViewIcon'
                        )}
                        name={(projectAnalysesInput.length > 0 || projectAnalysesOutput.length > 0) ? 'remove_circle_out' : 'add_circle_out'}
                        width="15px"
                        height="15px"
                        onClick={e => expandProjectAnalysis(e, subjectItem.analysisId)}
                      />
                      {subjectItem.analysisLabel}
                    </div>
                    <ul className="projectAnalysisActive">
                      {
                        projectAnalysesInput.length > 0 &&
                        headerItem.map(() => {
                          return returnElement(0, "alignItemsCenter analysis-Header padding-Left", "Input");
                        })
                      }
                      {
                        projectAnalysesInput.map((item, projectAnalysesIndex) => {
                          return returnElement(projectAnalysesIndex, "alignItemsCenter padding-Left", item.name);
                        })
                      }
                      {
                        projectAnalysesOutput.length > 0 &&
                        headerItem.map(() => {
                          return returnElement(0, "alignItemsCenter analysis-Header padding-Left", "Output");
                        })
                      }
                      {
                        projectAnalysesOutput.map((item, projectAnalysesIndex) => {
                          return returnElement(projectAnalysesIndex, "alignItemsCenter padding-Left", item.name);
                        })
                      }
                    </ul>
                  </li>
                );
              }
            }
            else {
              if ((
                isCurrentProject &&
                activeAcquisitionData[0].subjectId === subjectItem.subjectId
              ) || (!isCurrentProject && activeAcquisitionData[0].subjectId !== subjectItem.subjectId)) {
                return (
                  <li key={index} className={'listCenter'}>
                    <div className="alignItemsCenter">
                      <Icon
                        className={classnames(
                          'caret',
                          'material-icons',
                          'treeViewIcon'
                        )}
                        name={isExpanded(subjectItem.subjectId) ? 'remove_circle_out' : 'add_circle_out'}
                        width="15px"
                        height="15px"
                        onClick={e => expand(e, subjectItem.subjectId)}
                      />
                      {subjectItem.subjectLabel}
                    </div>
                    <ul className="sessionActive">
                      {subjectItem.session &&
                        subjectItem.session.map((session, sessionIndex) => {
                          return (
                            <li key={sessionIndex}>
                              <div className="alignItemsCenter">
                                <Icon
                                  className={classnames(
                                    'caret',
                                    'material-icons',
                                    'treeViewIcon'
                                  )}
                                  name={isExpanded(subjectItem.subjectId, session.sessionId) ? 'remove_circle_out' : 'add_circle_out'}
                                  width="15px"
                                  height="15px"
                                  onClick={e =>
                                    expandAcquisition(
                                      e,
                                      subjectItem.subjectId,
                                      session.sessionId
                                    )
                                  }
                                />
                                <Icon
                                  className={classnames(
                                    'caret',
                                    'material-icons',
                                    'treeViewIcon'
                                  )}
                                  name={
                                    isActiveSession(session.sessionId)
                                      ? 'visibility'
                                      : 'visibility_off'
                                  }
                                  width="15px"
                                  height="15px"
                                />
                                {session.sessionLabel}
                              </div>
                              <ul className="aqactive">
                                {session.acquisition &&
                                  session.acquisition.map(
                                    (acquisition, acquisitionIndex) => {
                                      return (
                                        <li
                                          key={acquisitionIndex}
                                          className={
                                            isActiveAcquisition(
                                              acquisition.acquisitionId,
                                              acquisition.acquisitionLabel)
                                              ? 'selectedAcquisition'
                                              : ''
                                          }
                                          onClick={e =>
                                            selectAcquisition(
                                              e,
                                              data.projectId,
                                              subjectItem.subjectId,
                                              session.sessionId,
                                              acquisition.acquisitionId,
                                              acquisition.acquisitionLabel,
                                              acquisition.containerId
                                            )
                                          }
                                        >
                                          <div className="alignItemsCenter">
                                            <Icon
                                              className={classnames(
                                                'caret',
                                                'material-icons',
                                                'treeViewIcon'
                                              )}
                                              name={
                                                isActiveAcquisition(
                                                  acquisition.acquisitionId,
                                                  acquisition.acquisitionLabel)
                                                  ? 'visibility'
                                                  : 'visibility_off'
                                              }
                                              width="15px"
                                              height="15px"
                                            />
                                            {acquisition.acquisitionLabel}
                                          </div>
                                        </li>
                                      );
                                    }
                                  )}
                                {
                                  session.sessionAnalyses && session.sessionAnalyses.map((item, index) => {
                                    return (
                                      <li key={index} className={'listCenter'}>
                                        <div className="alignItemsCenter">
                                          <Icon
                                            className={classnames(
                                              'caret',
                                              'material-icons',
                                              'treeViewIcon'
                                            )}
                                            name={item.analysis.length > 0 ? 'remove_circle_out' : 'add_circle_out'}
                                            width="15px"
                                            height="15px"
                                            onClick={e => expandSessionAnalysis(e, subjectItem.subjectId, session.sessionId, item._id)}
                                          />
                                          {item.label}
                                        </div>
                                        <ul className="sessionAnalysisActive">
                                          {
                                            item.analysis.length > 0 && item.inputs.length > 0 &&
                                            headerItem.map(() => {
                                              return returnElement(0, "alignItemsCenter analysis-Header padding-Left", "Input");
                                            })
                                          }
                                          {
                                            item.analysis.length > 0 && item.inputs.length > 0 && item.inputs.map((newitem, sessionAnalysesIndex) => {
                                              return returnElement(sessionAnalysesIndex, "alignItemsCenter padding-Left", newitem.name);
                                            })
                                          }
                                          {
                                            item.analysis.length > 0 && item.files.length > 0 &&
                                            headerItem.map(() => {
                                              return returnElement(0, "alignItemsCenter analysis-Header padding-Left", "Output");
                                            })
                                          }
                                          {
                                            item.analysis.length > 0 && item.files.length > 0 && item.files.map((newitem, sessionAnalysesIndex) => {
                                              return returnElement(sessionAnalysesIndex, "alignItemsCenter padding-Left", newitem.name);
                                            })
                                          }
                                        </ul>
                                      </li>
                                    );
                                  })
                                }
                              </ul>
                            </li>
                          );
                        })}
                      {
                        subjectItem.subjectAnalyses && subjectItem.subjectAnalyses.map((item, index) => {
                          return (
                            <li key={index} className={'listCenter'}>
                              <div className="alignItemsCenter">
                                <Icon
                                  className={classnames(
                                    'caret',
                                    'material-icons',
                                    'treeViewIcon'
                                  )}
                                  name={item.analysis.length > 0 ? 'remove_circle_out' : 'add_circle_out'}
                                  width="15px"
                                  height="15px"
                                  onClick={e => expandSubjectAnalysis(e, subjectItem.subjectId, item._id)}
                                />
                                {item.label}
                              </div>
                              <ul className="subjectAnalysisActive">
                                {
                                  item.analysis.length > 0 && item.inputs.length > 0 &&
                                  headerItem.map(() => {
                                    return returnElement(0, "alignItemsCenter analysis-Header padding-Left", "Input");
                                  })
                                }
                                {
                                  item.analysis.length > 0 && item.inputs && item.inputs.map((newitem, subjectAnalysesIndex) => {
                                    return returnElement(subjectAnalysesIndex, "alignItemsCenter padding-Left", newitem.name);
                                  })
                                }
                                {
                                  item.analysis.length > 0 && item.files.length > 0 &&
                                  headerItem.map(() => {
                                    return returnElement(0, "alignItemsCenter analysis-Header padding-Left", "Output");
                                  })
                                }
                                {
                                  item.analysis.length > 0 && item.files && item.files.map((newitem, subjectAnalysesIndex) => {
                                    return returnElement(subjectAnalysesIndex, "alignItemsCenter padding-Left", newitem.name);
                                  })
                                }
                              </ul>
                            </li>
                          );
                        })
                      }
                    </ul>
                  </li>
                );
              }
            }
          })}
      </ul>
    );
  }

  const updateStateSessions = (subjectId, sessions) => {
    let stateData = { ...data };
    if (stateData.subject) {
      let stateResult = stateData.subject.find(item => item.subjectId === subjectId);
      if (!sessions || sessions.length === 0) {
        const index = stateData.subject.findIndex(subject => subject.subjectId === subjectId);
        if (index >= 0) {
          stateData.subject.splice(index, 1);
        }
      } else {
        stateResult.session = cloneDeep(sessions);
        stateResult.session.forEach((session, index) => {
          session.acquisition = [];
          stateResult.session[index] = session;
        });
      }
      setData(stateData);
    }
  }

  const loadSessions = (subjectId, updateState = false) => {
    if (loadedNavigationData.subject && loadedNavigationData.subject.length > 0) {
      const result = loadedNavigationData.subject.find(item => item.subjectId === subjectId);
      if (result && (result.session.length > 0)) {
        if (updateState) {
          updateStateSessions(subjectId, result.session);
        }
        return;
      }
    }
    listSessionsBySubjectId(subjectId).then(res => {
      if (res) {
        const sessionList = (res || []).map(item => ({
          sessionId: item._id,
          sessionLabel: item.label,
          acquisition: [],
        }));

        if (sessionList.length === 0) {
          const index = loadedNavigationData.subject.findIndex(subject => subject.subjectId === subjectId);
          if (index >= 0) {
            loadedNavigationData.subject.splice(index, 1);
            // Update state as a special case for removing empty session subject even updateState flag is false
            updateStateSessions(subjectId, sessionList);
          }
        } else {
          const result = loadedNavigationData.subject.map(item => {
            if (item.subjectId === subjectId) {
              item.session = sessionList;
            }
            return item;
          });
        }
        setLoadedNavigationData({ ...loadedNavigationData });
        if (updateState
        ) {
          updateStateSessions(subjectId, sessionList);
        }
      }
    });
  }

  const expand = (event, subjectId) => {
    let navigateData = { ...data };
    const resultNavigateData = navigateData.subject.find(
      item => item.subjectId === subjectId
    );

    if (resultNavigateData.session.length === 0) {
      loadSessions(subjectId, true);
      getSubjectsAnalyses(subjectId);
    } else {
      let result = navigateData.subject.map(item => {
        if (item.subjectId === subjectId) {
          item.session = [];
          item.subjectAnalyses = [];
        }
        return item;
      });
      setData(navigateData);
    }
  };

  const expandSessionAnalysis = (event, subjectId, sessionId, id) => {
    let navigateData = { ...data };
    const resultNavigateData = navigateData.subject.find(
      item => item.subjectId === subjectId
    ).session.find((data) =>
      data.sessionId === sessionId
    );
    const result = resultNavigateData.sessionAnalyses.find((item) => {
      return item._id === id;
    });
    if (result.analysis.length > 0) {
      resultNavigateData.sessionAnalyses.find((item) => {
        if (item._id === id) {
          item.analysis = [];
        }
      });
      setData(navigateData);
    } else {
      resultNavigateData.sessionAnalyses.find((item) => {
        if (item._id === id) {
          item.analysis = [...item.inputs, ...item.files];
        }
      });
      setData(navigateData);
    }
  }

  const expandSubjectAnalysis = (event, subjectId, id) => {
    let navigateData = { ...data };
    const resultNavigateData = navigateData.subject.find(
      item => item.subjectId === subjectId
    );
    const result = resultNavigateData.subjectAnalyses.find((item) => {
      return item._id === id;
    });
    if (result.analysis.length > 0) {
      resultNavigateData.subjectAnalyses.find((item) => {
        if (item._id === id) {
          item.analysis = [];
        }
      });
      setData(navigateData);
    } else {
      resultNavigateData.subjectAnalyses.find((item) => {
        if (item._id === id) {
          item.analysis = [...item.inputs, ...item.files];
        }
      });
      setData(navigateData);
    }
  }

  const expandProjectAnalysis = (event, analysisId) => {
    if (projectAnalysesInput.length > 0 || projectAnalysesOutput.length > 0) {
      setProjectAnalysesInput([]);
      setProjectAnalysesOutput([]);
    } else {
      data.subject.map((item) => {
        if (item.analysisId !== undefined && item.analysisId === analysisId) {
          item.analysisInuts.map((data) => {
            data.analysisType = 'input';
          });
          item.analysisFiles.map((data) => {
            data.analysisType = 'output';
          });
          setProjectAnalysesInput([...item.analysisInuts]);
          setProjectAnalysesOutput([...item.analysisFiles]);
        }
      });
    }
  };

  return (
    <div className='navigate-panel'>
      {isLoading && <div className="navigate-panel-loading-indicator"></div>}
      <ul className="ulPosition">
        {data.project && (data.subject.length > 0) && (activeAcquisitionData.length > 0) && (
          <ul className="rootActive">
            <li className={'listCenter'}>
              <div className="alignItemsCenter">
                <Icon
                  className={classnames(
                    'caret',
                    'material-icons',
                    'treeViewIcon'
                  )}
                  name={'remove_circle_out'}
                  width="15px"
                  height="15px"
                />
                This Subject
              </div>

              {treeView()}
            </li>
            {isMultiSubjectView && (
              <li className={'listCenter'}>
                <div className="alignItemsCenter">
                  <Icon
                    className={classnames(
                      'caret',
                      'material-icons',
                      'treeViewIcon'
                    )}
                    name={'remove_circle_out'}
                    width="15px"
                    height="15px"
                  />
                  Other Subjects
                </div>
                {treeView(false)}
              </li>
            )}
          </ul>
        )}
      </ul>
    </div>

  );
};

Navigate.propTypes = {
  data: PropTypes.object,
  currentData: PropTypes.object,
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  selectActiveSessions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigate);

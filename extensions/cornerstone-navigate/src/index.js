import Navigate from './components/navigate/Navigate';
import initExtension from './init';
import commandsModule from './commandsModule';
import toolbarModule from './toolbarModule';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

export default {
  id: 'navigate-panel',
  preRegistration({ commandsManager, servicesManager, appConfig }) {
    initExtension({ commandsManager, servicesManager, appConfig });
  },
  getCommandsModule({
    commandsManager,
    servicesManager,
    hotkeysManager,
    appConfig,
  }) {
    return commandsModule({
      commandsManager,
      servicesManager,
      hotkeysManager,
      appConfig,
    });
  },
  getToolbarModule({
    commandsManager,
    servicesManager,
    hotkeysManager,
    appConfig,
  }) {
    return toolbarModule({
      commandsManager,
      servicesManager,
      hotkeysManager,
      appConfig,
    });
  },
  getPanelModule() {
    return panelModule;
  },
};

const panelModule = {
  menuOptions: [
    {
      icon: 'list',
      label: 'Nav',
      target: 'navigate-panel',
      context: ['ACTIVE_VIEWPORT::CORNERSTONE'],
      isDisabled: studies => {
        if (!studies) {
          return true;
        }

        return FlywheelCommonUtils.isCurrentWebImage();
      },
    },
  ],
  components: [
    {
      id: 'navigate-panel',
      component: Navigate,
    },
  ],
  defaultContext: ['VIEWER'],
};

import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import createSegmentNiftiFile from './createSegmentNiftiFile';
import {
  Utils as FlywheelCommonUtils,
  HTTP as FlywheelCommonHTTP,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import {
  getActiveLabelmapIndex,
  getActiveReferenceUID,
} from '@flywheel/extension-cornerstone-infusions';

const segmentationModule = cornerstoneTools.getModule('segmentation');
const { cornerstoneUtils } = FlywheelCommonUtils;
const {
  listSessionAnalyses,
  createAnalyses,
  uploadAnalysesFile,
  replaceFileInContainer,
} = FlywheelCommonHTTP.services;
const {
  getActiveFile,
  getSmartCTRangeList,
  getPixelDetailsInTargetSeries,
} = FlywheelCommonUtils;
const saveRequestTracker = {};
const { setSegmentationData } = FlywheelCommonRedux.actions;

/**
 * Get the cornerstone viewport matching the filtering params
 * @param {Object} filterParams
 * @returns {Object} matching cornerstone element
 */
const getCornerstoneViewport = filterParams => {
  let filteredViewport = null;
  cornerstone.getEnabledElements().forEach(enabledElement => {
    const imageId = enabledElement.image.imageId;
    const orientation = cornerstoneUtils.getNormalAxisOrientation(imageId);
    const instance = cornerstone.metaData.get('instance', imageId);
    if (
      orientation === filterParams.Orientation &&
      instance.StudyInstanceUID === filterParams.StudyInstanceUID
    ) {
      filteredViewport = enabledElement.element;
    }
  });
  return filteredViewport;
};

/**
 * Save the active viewport segments as NiFTI files, each segment is saved as separate file.
 * @param {Object} activeViewport
 * @param {String} sourceDataProtocol - source image data protocol
 * @returns {Promise} - Nifti save request promise
 */
export default function saveSegmentsAsNiftiFiles(
  activeViewport,
  sourceDataProtocol
) {
  const activeEnabledElement = cornerstone.getEnabledElement(activeViewport);
  const {
    editedSegmentFileName,
    editedSegmentFileExtension,
    containerId,
  } = getCurrentEditedSegment();
  let labelmap3D = [];
  let isUploadingNewSegment = true;

  const isContainerIdEmpty = containerId === '';
  if (editedSegmentFileName) {
    const activeLabelmapIndex = getActiveLabelmapIndex();
    labelmap3D = segmentationModule.getters.labelmap3D(
      activeViewport,
      activeLabelmapIndex
    );
    isUploadingNewSegment = isContainerIdEmpty;
  } else {
    const { labelmaps3D } = segmentationModule.getters.labelmaps3D(
      activeViewport
    );
    labelmap3D = labelmaps3D?.[0];
  }
  const instance = cornerstone.metaData.get(
    'instance',
    activeEnabledElement.image.imageId
  );
  const sourceToolState = cornerstoneTools.getToolState(
    activeViewport,
    'stack'
  );
  const sourceStackData = sourceToolState.data[0];

  let axialViewport = null;
  if (sourceDataProtocol !== 'dicom') {
    axialViewport = getCornerstoneViewport({
      Orientation: 'Axial',
      StudyInstanceUID: instance.StudyInstanceUID,
    });
  }
  const toolState = cornerstoneTools.getToolState(
    axialViewport || activeViewport,
    'stack'
  );
  const stackData = toolState.data[0];
  const imageId = stackData.imageIds[0];

  return new Promise(async (resolve, reject) => {
    try {
      if (!labelmap3D) {
        resolve({ savedFiles: [], failedFiles: [] });
        return;
      }
      if (saveRequestTracker[imageId]) {
        resolve({
          savedFiles: [],
          failedFiles: [],
          warningInfo: { title: 'Save already in progress' },
        });
        return;
      }
      const activeFile = await getActiveFile(store.getState());
      if (!activeFile) {
        resolve({ savedFiles: [], failedFiles: [] });
        return;
      }
      saveRequestTracker[imageId] = true;
      const orientation = cornerstoneUtils.getNormalAxisOrientation(imageId);
      const metaData = cornerstone.metaData.get('imagePlaneModule', imageId);
      if (!metaData.imagePositionPatient || !metaData.imageOrientationPatient) {
        delete saveRequestTracker[imageId];
        resolve({
          savedFiles: [],
          failedFiles: [],
          warningInfo: {
            title: 'Unable to create the NiFTI file',
            message:
              'Image position or orientation missing for the current image',
          },
        });
        return;
      }

      let sliceSpacing = [1, 1, 1];
      if (sourceDataProtocol === 'dicom') {
        const noOfImages = stackData.imageIds.length;
        const { imagePositionPatient } = cornerstone.metaData.get(
          'imagePlaneModule',
          stackData.imageIds[noOfImages - 1]
        );
        sliceSpacing = [
          (imagePositionPatient[0] - metaData.imagePositionPatient[0]) /
            noOfImages,
          (imagePositionPatient[1] - metaData.imagePositionPatient[1]) /
            noOfImages,
          (imagePositionPatient[2] - metaData.imagePositionPatient[2]) /
            noOfImages,
        ];
      }

      const baseSeriesMetaInfo = {
        metaData: { ...metaData, sliceSpacing },
        numberOfFrames: stackData.imageIds.length,
      };
      const { file_id, parents } = activeFile;
      const analysesInputInfo = getAnalysesInputInfo(activeFile);
      const extension = `.${editedSegmentFileExtension || 'nii.gz'}`;

      const allAnalyses = await listSessionAnalyses(parents.session);
      const checkAnalyses = (analysis) => {
        return (
          analysis.job === null &&
          (analysis.inputs || []).find(file => file.file_id === file_id)
        );
      };
      let maskAnalysis = (allAnalyses || []).find(
        analysis =>
        checkAnalyses(analysis) &&
          analysis.label === 'Mask Analyses'
      );

      if (!maskAnalysis) {
        maskAnalysis = (allAnalyses || []).find(
          analysis =>
          checkAnalyses(analysis)
        );
        if(!maskAnalysis){
          maskAnalysis = await createAnalyses(parents.session, analysesInputInfo);
        }
      }
      
      const segmentsMap = getSegmentationDataMap(
        labelmap3D.labelmaps2D,
        `Mask_${orientation}`,
        extension,
        maskAnalysis,
        editedSegmentFileName
      );

      const targetSegmentsMap =
        !axialViewport || axialViewport === activeViewport
          ? {}
          : getSegmentationPixelMapInTarget(
              labelmap3D.labelmaps2D,
              segmentsMap,
              sourceStackData.imageIds,
              stackData.imageIds,
              baseSeriesMetaInfo
            );

      const savePromises = [];
      const savedFiles = [];
      const failedFiles = [];

      Object.keys(segmentsMap).forEach(async key => {
        const segmentDetails = targetSegmentsMap[key] || segmentsMap[key];

        const file = createSegmentNiftiFile(
          baseSeriesMetaInfo,
          segmentDetails,
          labelmap3D.labelmaps2D,
          true,
          null,
          sourceDataProtocol,
          extension
        );
        const promise = uploadFile(
          file,
          file.name,
          isUploadingNewSegment ? maskAnalysis._id : containerId,
          isUploadingNewSegment
        )
          .then(fileResponse => {
            if (fileResponse) {
              if (isContainerIdEmpty) {
                setContainerIdForNewSegment(
                  `${editedSegmentFileName}.${editedSegmentFileExtension}`,
                  maskAnalysis._id,
                  fileResponse[0]?.file_id
                );
              }
              savedFiles.push(file.name);
            }
          })
          .catch(error => {
            failedFiles.push(file.name);
            console.log(error);
          });

        savePromises.push(promise);
      });
      await Promise.all(savePromises);
      delete saveRequestTracker[imageId];
      resolve({ savedFiles, failedFiles });
    } catch (err) {
      delete saveRequestTracker[imageId];
      reject(err);
    }
    return;
  });
}

const setContainerIdForNewSegment = (mask, containerId, fileId) => {
  const activeReferenceUID = getActiveReferenceUID();
  const segmentationData = store.getState().segmentation.segmentationData[
    activeReferenceUID
  ];
  const index = segmentationData.findIndex(data => data.name === mask);
  if (index > -1) {
    const newSegmentData = segmentationData[index];
    const editedSegmentData = { ...newSegmentData };
    editedSegmentData.containerId = containerId;
    editedSegmentData.fileId = fileId;
    const newMaskArray = [...segmentationData];
    newMaskArray[index] = editedSegmentData;

    if (newSegmentData?.modality !== 'RTSTRUCT') {
      store.dispatch(
        setSegmentationData([...newMaskArray], activeReferenceUID)
      );
    }
  }
};

const getCurrentEditedSegment = () => {
  const state = store.getState();
  const referenceUID = getActiveReferenceUID();
  const segmentationData = state.segmentation.segmentationData?.[referenceUID];
  const editedSegment = segmentationData?.find(data => data.isEditing);
  const splitFileName = editedSegment?.name?.split('.');
  const editedSegmentFileExtension =
    splitFileName?.length > 2
      ? `${splitFileName?.[1]}.${splitFileName?.[2]}`
      : splitFileName?.[1];
  const containerId = editedSegment?.containerId;

  let lastIndex = editedSegment?.name?.lastIndexOf('.');
  const fileExtension = editedSegment?.name?.substring(lastIndex);
  let editedSegmentFileName = editedSegment?.name?.substring(0, lastIndex);
  if (fileExtension === '.gz') {
    lastIndex = editedSegmentFileName.lastIndexOf('.');
    editedSegmentFileName = editedSegment?.name?.substring(0, lastIndex);
  }

  return { editedSegmentFileName, editedSegmentFileExtension, containerId };
};

// Get the analyses input info with file details for creating analyses
const getAnalysesInputInfo = activeFile => {
  const { name, filename, parents, version } = activeFile;
  const containerId = activeFile.containerId || activeFile.parent_ref.id;
  const containerType = Object.keys(parents).find(
    key => parents[key] === containerId
  );
  const inputFileInfo = {
    type: containerType,
    id: containerId,
    name: name || filename,
    version: version,
  };

  return {
    label: 'Mask Analyses',
    inputs: [inputFileInfo],
    description: '',
    info: {},
  };
};

// Upload the file to the mask analyses container
const uploadFile = (file, fileName, analysesId, isUploadingNewSegment) => {
  const formData = new FormData();
  formData.append('input_data', file, fileName);
  if (isUploadingNewSegment) {
    return uploadAnalysesFile(analysesId, formData);
  } else {
    return replaceFileInContainer(analysesId, formData);
  }
};

const getCurrentDateTimeString = () => {
  const format = val => String(val).padStart(2, '0');
  const d = new Date();
  const date = [
    d.getFullYear(),
    format(d.getMonth()),
    format(d.getDate()),
  ].join('');
  const time = [
    format(d.getHours()),
    format(d.getMinutes()),
    format(d.getSeconds()),
  ].join('');
  return `${date}_${time}`;
};

// Retrieve the segment information from the label map and create a map with each segment detailed information
const getSegmentationDataMap = (
  labelMaps2D,
  namePrefix,
  fileExtension,
  maskAnalyses,
  editedSegmentFileName = null
) => {
  const segmentsMap = {};
  const smartCTRanges = getSmartCTRangeList();
  (labelMaps2D || []).forEach((labelMap, index) => {
    (labelMap.segmentsOnLabelmap || []).forEach(segmentIndex => {
      if (segmentIndex > 0) {
        const segmentName =
          smartCTRanges[segmentIndex - 1]?.label || `Tissue${segmentIndex}`;
        let fileName =
          editedSegmentFileName ||
          `${namePrefix}_${segmentName}_${getCurrentDateTimeString()}`;
        segmentsMap[segmentIndex] = segmentsMap[segmentIndex] || {
          segmentIndex,
          start: Number.MAX_VALUE,
          end: -1,
          slices: [],
          name: fileName,
          fileExtension,
        };
      } else {
        return;
      }
      segmentsMap[segmentIndex].start =
        segmentsMap[segmentIndex].start > index
          ? index
          : segmentsMap[segmentIndex].start;
      segmentsMap[segmentIndex].end =
        segmentsMap[segmentIndex].end < index
          ? index
          : segmentsMap[segmentIndex].end;
      if (!segmentsMap[segmentIndex].slices.includes(index)) {
        segmentsMap[segmentIndex].slices.push(index);
      }
    });
  });
  return segmentsMap;
};

// Retrieve the segment information in the target images corresponding
// to source images and source segmentation detail and it is calculated using the source
// image plane information
const getSegmentationPixelMapInTarget = (
  labelMaps2D,
  sourceSegmentsMap,
  sourceImageIds,
  targetImageIds,
  targetMetaData
) => {
  const targetSegmentsMap = {};
  const { metaData, numberOfFrames } = targetMetaData;
  const imageSize = metaData.columns * metaData.rows;
  const totalBufferSize = imageSize * numberOfFrames;
  Object.keys(sourceSegmentsMap).forEach(async key => {
    const segmentDetails = sourceSegmentsMap[key];
    let fileData = new Int8Array(totalBufferSize);
    for (let i = segmentDetails.start; i <= segmentDetails.end; i++) {
      if (segmentDetails.slices.includes(i)) {
        const sourceImagePlane = cornerstone.metaData.get(
          'imagePlaneModule',
          sourceImageIds[i]
        );
        labelMaps2D[i].pixelData.forEach((value, index) => {
          if (value === segmentDetails.segmentIndex) {
            const targetImageDetails = getPixelDetailsInTargetSeries(
              index,
              sourceImagePlane,
              targetImageIds
            );
            const { sliceIndex, imagePoint } = targetImageDetails;
            const sliceBaseIndex = sliceIndex * imageSize;
            const pixelIndex =
              sliceBaseIndex +
              metaData.columns * Math.round(imagePoint.y) +
              Math.round(imagePoint.x);
            fileData[pixelIndex] = value;
            targetSegmentsMap[value] = targetSegmentsMap[value] || {
              segmentIndex: value,
              start: Number.MAX_VALUE,
              end: -1,
              slices: [],
              name: segmentDetails.name,
              fileExtension: segmentDetails.fileExtension,
              fileData,
            };
            targetSegmentsMap[value].start =
              targetSegmentsMap[value].start > sliceIndex
                ? sliceIndex
                : targetSegmentsMap[value].start;
            targetSegmentsMap[value].end =
              targetSegmentsMap[value].end < sliceIndex
                ? sliceIndex
                : targetSegmentsMap[value].end;
            if (!targetSegmentsMap[value].slices.includes(sliceIndex)) {
              targetSegmentsMap[value].slices.push(sliceIndex);
            }
          }
        });
      }
    }
  });
  return targetSegmentsMap;
};

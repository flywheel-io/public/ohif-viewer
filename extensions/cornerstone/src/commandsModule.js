import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import OHIF from '@ohif/core';
import pick from 'lodash/pick';
import store from '../../../platform/viewer/src/store';

import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import {
  Redux as FlywheelRedux,
  Utils as FlywheelUtils,
  Components as FlywheelComponents,
} from '@flywheel/extension-flywheel';
import {
  cornerstoneUtils,
  updateOtherViewports,
  scrollToCenter,
  getActiveReferenceUID,
  getActiveLabelmapIndex,
} from '@flywheel/extension-cornerstone-infusions';

import setCornerstoneLayout from './utils/setCornerstoneLayout.js';
import { getEnabledElement } from './state';
import CornerstoneViewportDownloadForm from './CornerstoneViewportDownloadForm';
import loadFovIndicators from './utils/loadFovIndicators';
import loadFundusRingOverlay from './utils/loadFundusRingOverlay';
import {
  loadScaleIndicator,
  clearScaleIndicator,
} from './utils/loadScaleIndicator.js';
import saveSegments from './utils/segmentSave/saveSegments.js';
import proximityCursor from '@flywheel/extension-flywheel-common/src/tools/utils/proximityCursor';

// TODO move this to flywheel infusion.
const {
  setActiveTool,
  setProximityCursorProperties,
  subFormQuestionAnswered,
  setActiveQuestion,
} = FlywheelCommonRedux.actions;
const { measurementTools } = FlywheelCommonRedux.selectors;
const { selectActiveProtocolStore } = FlywheelRedux.selectors;
const { resetLayoutToHP } = FlywheelUtils;
const {
  getCondensedProjectConfig,
  undoRedoHandlers,
  getSubAnnotations,
  getDefaultROILabelFromHPLayout,
} = FlywheelCommonUtils;
const scroll = cornerstoneTools.import('util/scroll');
const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');
const { studyMetadataManager } = OHIF.utils;
const { setViewportActive } = OHIF.redux.actions;
const { SaveSegmentationDialog, PermissionDialog } = FlywheelComponents;
const { getProximityCursorProperties } = proximityCursor;
const triggerEvent = cornerstoneTools.importInternal('util/triggerEvent');

const popupFromLeft = 175;
const popupFromTop = 220;

const getImageIdToolState = viewports => {
  const element = getEnabledElement(viewports.activeViewportIndex);
  if (!element) {
    return;
  }

  const enabledElement = cornerstone.getEnabledElement(element);
  if (!enabledElement || !enabledElement.image) {
    return;
  }

  const toolState = cornerstoneTools.globalImageIdSpecificToolStateManager.saveToolState();
  if (
    !toolState ||
    toolState.hasOwnProperty(enabledElement.image.imageId) === false
  ) {
    return;
  }

  return toolState[enabledElement.image.imageId];
};

const refreshCornerstoneViewports = () => {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    if (enabledElement.image) {
      cornerstone.updateImage(enabledElement.element);
    }
  });
};

const onSaveSegmentsFiles = (servicesManager, viewports) => {
  const { UINotificationService } = servicesManager.services;
  const activeViewport = getEnabledElement(viewports.activeViewportIndex);
  const viewportSpecificData =
    viewports.viewportSpecificData[viewports.activeViewportIndex];
  const state = store.getState();
  const saveAsDicomSeg = state.flywheel?.projectConfig?.saveAsDicomSeg;
  const sourceDataProtocol = viewportSpecificData.dataProtocol;
  const fileSaveType =
    sourceDataProtocol !== 'nifti' && saveAsDicomSeg ? 'dicom' : 'nifti';

  saveSegments(activeViewport, sourceDataProtocol, fileSaveType).then(
    ({ savedFiles, failedFiles, warningInfo }) => {
      if (savedFiles.length) {
        UINotificationService.show({
          title: `Saved segments as ${fileSaveType} files successfully`,
          message: `Saved files: ${savedFiles.join(',\n')}`,
          type: 'info',
          autoClose: true,
        });
      }

      if (failedFiles.length) {
        UINotificationService.show({
          title: `Failed to save segments as ${fileSaveType} files`,
          message: `Failed files: ${failedFiles.join(',\n')}`,
          type: 'error',
          autoClose: false,
        });
      }

      if (warningInfo) {
        UINotificationService.show({
          title: warningInfo.title || '',
          message: warningInfo.message || '',
          type: 'warning',
          autoClose: true,
        });
      }
    },
    err => {
      UINotificationService.show({
        title: `Failed to save segments as ${fileSaveType} files`,
        message: err.message || err,
        type: 'error',
        autoClose: false,
      });
    }
  );
};
const commandsModule = ({ servicesManager, commandsManager }) => {
  const actions = {
    rotateViewport: ({ viewports, rotation }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.rotation += rotation;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    flipViewportHorizontal: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.hflip = !viewport.hflip;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    flipViewportVertical: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.vflip = !viewport.vflip;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    stretchViewport: ({ viewports, isVertical, isIncrement }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        const DEFAULT_STRETCH = 1;
        const MAX_STRETCH = 8;
        let viewport = cornerstone.getViewport(enabledElement);
        if (!viewport.stretch) {
          viewport.stretch = { x: DEFAULT_STRETCH, y: DEFAULT_STRETCH };
        }
        let stretch = isVertical ? viewport.stretch.y : viewport.stretch.x;

        if (isIncrement) {
          stretch = stretch < MAX_STRETCH ? stretch * 2 : stretch;
        } else {
          stretch = stretch > DEFAULT_STRETCH ? stretch / 2 : DEFAULT_STRETCH;
        }
        if (isVertical) {
          viewport.stretch.x = DEFAULT_STRETCH;
          viewport.stretch.y = stretch;
        } else {
          viewport.stretch.x = stretch;
          viewport.stretch.y = DEFAULT_STRETCH;
        }

        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    scaleViewport: ({ direction, viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      const step = direction * 0.15;

      if (enabledElement) {
        if (step) {
          let viewport = cornerstone.getViewport(enabledElement);
          viewport.scale += step;
          cornerstone.setViewport(enabledElement, viewport);
        } else {
          cornerstone.fitToWindow(enabledElement);
        }
      }
    },
    resetViewport: ({ viewports }) => {
      const isColorMapExist = viewports?.colormapId;
      const isColorChannelExist = viewports?.channelId;
      Object.keys(viewports.viewportSpecificData).forEach(viewportIndex => {
        const enabledElement = getEnabledElement(viewportIndex);
        if (enabledElement) {
          if (isColorMapExist) {
            const viewport = cornerstone.getViewport(enabledElement);
            viewport.colormap = undefined;
            cornerstone.resetColorMap(enabledElement);
          }
          cornerstone.reset(enabledElement);
          scrollToCenter([enabledElement]);
          const state = store.getState();
          const protocolStore = selectActiveProtocolStore(state);
          // Reset to hanging protocol layout if a valid protocol already applied and if the current layout
          // or the viewport displaySet is different
          if (protocolStore) {
            resetLayoutToHP(
              commandsManager,
              store,
              protocolStore,
              isAlreadyMatching => {
                if (isAlreadyMatching) {
                  const viewportSpecificData =
                    viewports.viewportSpecificData[
                      viewports.activeViewportIndex
                    ];
                  updateOtherViewports(
                    commandsManager,
                    ['Wwwc', 'StackScroll', 'Zoom'],
                    viewportSpecificData,
                    enabledElement,
                    true
                  );
                }
              }
            );
          } else {
            const viewportSpecificData =
              viewports.viewportSpecificData[viewports.activeViewportIndex];
            updateOtherViewports(
              commandsManager,
              ['Wwwc', 'StackScroll', 'Zoom'],
              viewportSpecificData,
              enabledElement,
              true
            );
          }
        }
      });
      if (isColorMapExist || isColorChannelExist) {
        // TODO: Temporary hack to reflect the color map & color channel change to all viewports if the layout is changed
        // to HP layout as part of reset
        setTimeout(() => {
          if (isColorMapExist) {
            commandsManager?.runCommand('changeColormap', {
              reset: true,
            });
          } else {
            commandsManager?.runCommand('changeColorChannel', {
              reset: true,
            });
          }
        }, 1000);
      }
      store.dispatch(setViewportActive(0));
    },
    invertViewport: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);
        viewport.invert = !viewport.invert;
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    // TODO: this is receiving `evt` from `ToolbarRow`. We could use it to have
    //       better mouseButtonMask sets.
    setToolActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }

      const state = store.getState();
      const projectConfig = state.flywheel?.projectConfig;
      const question = state.infusions.currentSelectedQuestion;
      if (
        (projectConfig?.studyFormWorkflow === 'ROI' &&
          !state.subForm.isSubFormPopupEnabled &&
          measurementTools.includes(toolName)) ||
        (projectConfig?.studyFormWorkflow === 'Mixed' &&
          measurementTools.includes(toolName) &&
          !state.subForm.isSubFormPopupEnabled &&
          !question?.isSubForm)
      ) {
        store.dispatch(subFormQuestionAnswered(false));
        store.dispatch(setActiveQuestion(null));
      }
      store.dispatch(setActiveTool(toolName));
    },
    updateViewportDisplaySet: ({ direction }) => {
      // TODO
      console.warn('updateDisplaySet: ', direction);
    },
    deleteMeasurement: ({ viewports }) => {
      const imageIdToolState = getImageIdToolState(viewports);
      if (Object.keys(imageIdToolState || {}).length === 0) {
        return;
      }

      const measurementsByType = Object.keys(imageIdToolState).map(toolType => {
        return imageIdToolState[toolType].data.map(measurement =>
          Object.assign({}, measurement, { toolType })
        );
      });
      const measurements = [].concat(...measurementsByType);
      const selectedMeasurement = measurements.find(measure => measure.active);

      if (selectedMeasurement.readonly) {
        return;
      }

      if (selectedMeasurement && selectedMeasurement._id) {
        const dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
          .default;

        dispatchActions.onDeleteClick(
          {},
          {
            ...selectedMeasurement,
            ...{ measurementId: selectedMeasurement?._id },
          },
          { servicesManager }
        );
      }
    },
    clearAnnotations: ({ viewports }) => {
      const imageIdToolState = getImageIdToolState(viewports);
      const element = getEnabledElement(viewports.activeViewportIndex);
      if (Object.keys(imageIdToolState || {}).length === 0) {
        return;
      }

      const measurementsToRemove = [];
      const defaultRoiLabelDetails = getDefaultROILabelFromHPLayout();
      let isFOVGridUpdate = false;
      let defaultRoiLabels;

      Object.keys(imageIdToolState).forEach(toolType => {
        const { data } = imageIdToolState[toolType];

        data.forEach(measurementData => {
          const { _id, readonly } = measurementData;
          if (!_id || readonly) {
            return;
          }
          defaultRoiLabels =
            defaultRoiLabelDetails[measurementData.SeriesInstanceUID];
          isFOVGridUpdate = defaultRoiLabels?.includes(
            measurementData.location
          );
          const measurement = {
            ...measurementData,
            ...{ measurementId: measurementData?._id },
          };

          measurementsToRemove.push(measurement);
        });
      });

      if (measurementsToRemove?.length) {
        triggerEvent(element, `fwCornerstoneMeasurementDeleting`, {
          measurement: measurementsToRemove,
        });
      }

      if (isFOVGridUpdate) {
        const plugin = 'cornerstone';
        const propertiesToSync = {
          defaultRoiLabels: defaultRoiLabels,
        };
        commandsManager?.runCommand('crossViewportSync', {
          plugin,
          propertiesToSync,
        });
      }
    },
    performUndo: ({ viewports }) => {
      undoRedoHandlers.handleUndoRedoOperation(true, viewports);
      refreshCornerstoneViewports();
    },
    performRedo: ({ viewports }) => {
      undoRedoHandlers.handleUndoRedoOperation(false, viewports);
      refreshCornerstoneViewports();
    },
    nextImage: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      scroll(enabledElement, 1);
      const viewportSpecificData =
        viewports.viewportSpecificData[viewports.activeViewportIndex];
      updateOtherViewports(
        commandsManager,
        ['StackScroll'],
        viewportSpecificData,
        enabledElement,
        true
      );
    },
    toggleScaleDisplay: () => {
      loadScaleIndicator();
    },
    toggleFundusRing: () => {
      loadFundusRingOverlay();
    },
    clearScaleDisplay: () => {
      clearScaleIndicator();
    },
    previousImage: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      scroll(enabledElement, -1);
      const viewportSpecificData =
        viewports.viewportSpecificData[viewports.activeViewportIndex];
      updateOtherViewports(
        commandsManager,
        ['StackScroll'],
        viewportSpecificData,
        enabledElement,
        true
      );
    },
    nextBSlice: ({ viewports }) => {
      const viewportSpecificData =
        viewports.viewportSpecificData[viewports.activeViewportIndex];
      if (
        viewportSpecificData &&
        viewportSpecificData.plugin.includes('cornerstone')
      ) {
        const config = getCondensedProjectConfig();

        const sliceSettings = config?.bSlices?.settings;
        if (sliceSettings) {
          const enabledElement = getEnabledElement(
            viewports.activeViewportIndex
          );
          const toolState = cornerstoneTools.getToolState(
            enabledElement,
            'stack'
          );
          const stackData = toolState.data[0];
          const curIndex = stackData.currentImageIdIndex;
          const numSlices = stackData.imageIds.length;
          let firstIndex = -1;
          let nextIndex = -1;

          // Find the next b slice index(if bottom then to first b slice index).
          for (let index = 0; index < numSlices; index++) {
            const currentSliceNumber = `${index + 1}`;
            if (sliceSettings[currentSliceNumber]) {
              if (firstIndex < 0) {
                firstIndex = index;
              }
              if (index > curIndex) {
                nextIndex = index;
                break;
              }
            }
          }
          nextIndex = nextIndex < 0 ? firstIndex : nextIndex;
          if (nextIndex >= 0) {
            const scrollFactor = nextIndex - curIndex;
            scroll(enabledElement, scrollFactor);
            updateOtherViewports(
              commandsManager,
              ['StackScroll'],
              viewportSpecificData,
              enabledElement,
              true
            );
          }
        }
      }
    },
    getActiveViewportEnabledElement: ({ viewports }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);
      return enabledElement;
    },
    showDownloadViewportModal: ({ title, viewports }) => {
      const activeViewportIndex = viewports.activeViewportIndex;
      const { UIModalService } = servicesManager.services;
      if (UIModalService) {
        UIModalService.show({
          content: CornerstoneViewportDownloadForm,
          title,
          contentProps: {
            activeViewportIndex,
            onClose: UIModalService.hide,
          },
        });
      }
    },
    updateTableWithNewMeasurementData({
      toolType,
      measurementNumber,
      location,
      description,
      color,
    }) {
      // Update all measurements by measurement number
      const measurementApi = OHIF.measurements.MeasurementApi.Instance;
      const measurements = measurementApi.tools[toolType].filter(
        m => m.measurementNumber === measurementNumber
      );

      measurements.forEach(measurement => {
        measurement.location = location;
        measurement.description = description;
        if (color) {
          measurement.color = color;
        }

        measurementApi.updateMeasurement(measurement.toolType, measurement);
      });

      measurementApi.syncMeasurementsAndToolData();

      refreshCornerstoneViewports();
    },
    getNearbyToolData({ element, canvasCoordinates, availableToolTypes }) {
      const nearbyTool = {};
      let pointNearTool = false;

      availableToolTypes.forEach(toolType => {
        const elementToolData = cornerstoneTools.getToolState(
          element,
          toolType
        );

        if (!elementToolData) {
          return;
        }

        elementToolData.data.forEach((toolData, index) => {
          let elementToolInstance = cornerstoneTools.getToolForElement(
            element,
            toolType
          );

          if (!elementToolInstance) {
            elementToolInstance = cornerstoneTools.getToolForElement(
              element,
              `${toolType}Tool`
            );
          }

          if (!elementToolInstance) {
            console.warn('Tool not found.');
            return undefined;
          }

          if (
            elementToolInstance.pointNearTool(
              element,
              toolData,
              canvasCoordinates
            )
          ) {
            pointNearTool = true;
            nearbyTool.tool = toolData;
            nearbyTool.index = index;
            nearbyTool.toolType = toolType;
          }
        });

        if (pointNearTool) {
          return false;
        }
      });

      return pointNearTool ? nearbyTool : undefined;
    },
    removeToolState: ({ element, toolType, tool }) => {
      cornerstoneTools.removeToolState(element, toolType, tool);
      cornerstone.updateImage(element);
    },
    setCornerstoneLayout: () => {
      setCornerstoneLayout();
    },
    setWindowLevel: ({ viewports, window, level }) => {
      const enabledElement = getEnabledElement(viewports.activeViewportIndex);

      if (enabledElement) {
        let viewport = cornerstone.getViewport(enabledElement);

        viewport.voi = {
          windowWidth: Number(window),
          windowCenter: Number(level),
        };
        cornerstone.setViewport(enabledElement, viewport);
      }
    },
    jumpToImage: ({
      StudyInstanceUID,
      SOPInstanceUID,
      frameIndex,
      activeViewportIndex,
    }) => {
      const study = studyMetadataManager.get(StudyInstanceUID);

      const displaySet = study.findDisplaySet(ds => {
        return (
          ds.images &&
          ds.images.find(i => i.getSOPInstanceUID() === SOPInstanceUID)
        );
      });

      displaySet.SOPInstanceUID = SOPInstanceUID;
      displaySet.frameIndex = frameIndex;

      window.store.dispatch(setViewportActive(activeViewportIndex));

      refreshCornerstoneViewports();
    },
    updateViewportProperties: ({ viewports, propertiesToSync }) => {
      let enabledElements = cornerstone.getEnabledElements();
      let sourceEnabledElement = null;
      if (propertiesToSync.viewportIndex >= 0) {
        const sourceElement = getEnabledElement(propertiesToSync.viewportIndex);
        sourceEnabledElement = sourceElement
          ? cornerstone.getEnabledElement(sourceElement)
          : null;
      }
      enabledElements.forEach((enabledElement, index) => {
        if (
          !enabledElement.image ||
          (sourceEnabledElement && sourceEnabledElement !== enabledElement)
        ) {
          return;
        }
        const imageId = enabledElement.image.imageId;
        const instance = cornerstone.metaData.get('instance', imageId);
        if (!instance) {
          return;
        }

        const {
          StudyInstanceUID,
          SeriesInstanceUID,
          SOPInstanceUID,
        } = instance;

        const study = studyMetadataManager.get(StudyInstanceUID);
        if (
          StudyInstanceUID === propertiesToSync.studyInstanceUID &&
          (SeriesInstanceUID === propertiesToSync.seriesInstanceUID ||
            (study.displaySets.length &&
              study.displaySets[0].plugin === 'cornerstone::nifti'))
        ) {
          let viewport = cornerstone.getViewport(enabledElement.element);

          if (propertiesToSync?.windowLevel) {
            viewport.voi = propertiesToSync.windowLevel;
          }
          viewport.rotation = isNaN(propertiesToSync.rotation)
            ? viewport.rotation
            : propertiesToSync.rotation;
          viewport.hflip = propertiesToSync.hflip || viewport.hflip;
          viewport.vflip = propertiesToSync.vflip || viewport.vflip;
          viewport.invert = propertiesToSync.invert || viewport.invert;
          if (propertiesToSync?.zoom) {
            const defaultViewport = cornerstone.getDefaultViewport(
              enabledElement.canvas,
              enabledElement.image
            );
            const config = window.store.getState()?.flywheel?.projectConfig;
            const defaultMinZoom = 0.25;
            const defaultMaxZoom = 20;
            const configuration = {
              minScale:
                config && config.zoomLevel
                  ? config.zoomLevel.minimum
                  : defaultMinZoom,
              maxScale:
                config && config.zoomLevel
                  ? config.zoomLevel.maximum
                  : defaultMaxZoom,
            };

            let updatedScale = viewport.scale;
            if (propertiesToSync.zoom.type === 'exact') {
              updatedScale = propertiesToSync.zoom.scale;
            } else if (propertiesToSync.zoom.type === 'rescale') {
              if (
                propertiesToSync.zoom.viewportDim &&
                propertiesToSync.zoom.scale
              ) {
                const canvasWidth = enabledElement.canvas.width;
                const canvasHeight = enabledElement.canvas.height;
                const relWidthChange =
                  canvasWidth / propertiesToSync.zoom.viewportDim.width;
                const relHeightChange =
                  canvasHeight / propertiesToSync.zoom.viewportDim.height;
                const relChange = Math.sqrt(relWidthChange * relHeightChange);
                updatedScale = relChange * propertiesToSync.zoom.scale;
              } else {
                updatedScale =
                  defaultViewport.scale + propertiesToSync.zoom.scale;
              }
            } else {
              updatedScale =
                defaultViewport.scale + propertiesToSync.zoom.scale;
            }

            updatedScale =
              updatedScale < configuration.minScale
                ? configuration.minScale
                : updatedScale > configuration.maxScale
                ? configuration.maxScale
                : updatedScale;
            viewport.scale = updatedScale;
          }
          if (propertiesToSync?.pan) {
            viewport.translation =
              propertiesToSync.zoom.type === 'exact'
                ? propertiesToSync.pan.translation
                : {
                    x: propertiesToSync.pan.translation.x / viewport.scale,
                    y: propertiesToSync.pan.translation.y / viewport.scale,
                  };
          }
          cornerstone.setViewport(enabledElement.element, viewport);
          if (
            propertiesToSync?.patientPos ||
            propertiesToSync?.scrollIndex >= 0
          ) {
            const toolState = cornerstoneTools.getToolState(
              enabledElement.element,
              'stack'
            );
            const stackData = toolState.data[0];
            if (propertiesToSync.scrollIndex >= 0) {
              const newImageIdIndex = Math.min(
                propertiesToSync.scrollIndex,
                stackData.imageIds.length - 1
              );
              scrollToIndex(enabledElement.element, newImageIdIndex);
            } else {
              const newImageIdIndex = cornerstoneUtils.getNearestSliceIndex(
                enabledElement,
                propertiesToSync.patientPos
              );
              scroll(
                enabledElement.element,
                newImageIdIndex - stackData.currentImageIdIndex
              );
            }
          }
          if (propertiesToSync?.fovReferencesInfo) {
            loadFovIndicators(
              enabledElement,
              propertiesToSync.fovReferencesInfo
            );
          }
        }
      });
    },
    saveSegments: async ({ viewports, saveNewSegment }) => {
      const state = store.getState();
      const activeLabelmapIndex = getActiveLabelmapIndex();
      const referenceUID = getActiveReferenceUID();
      const segmentationData =
        state.segmentation.segmentationData?.[referenceUID];
      const index = segmentationData?.findIndex(data => data.isEditing);

      if (activeLabelmapIndex > 0 && index >= 0 && saveNewSegment) {
        const { UIDialogService } = servicesManager.services;
        const popupFromLeft = 175;
        const popupFromTop = 220;
        const saveConfirmDialogId = UIDialogService.create({
          content: SaveSegmentationDialog,
          defaultPosition: {
            x: window.innerWidth / 2 - popupFromLeft,
            y: window.innerHeight / 2 - popupFromTop,
          },
          showOverlay: true,
          contentProps: {
            label:
              'Please save the segmentation .This will update the existing file with new version.',
            onClose: () => UIDialogService.dismiss({ id: saveConfirmDialogId }),
            onSave: () => {
              onSaveSegmentsFiles(servicesManager, viewports);
              UIDialogService.dismiss({ id: saveConfirmDialogId });
            },
          },
        });
      } else {
        onSaveSegmentsFiles(servicesManager, viewports);
      }
    },
    toggleProximityRoiCursorStatus: ({}) => {
      const proximityCursorProperties = getProximityCursorProperties();
      const toggleEvent = 'keydown';
      store.dispatch(
        setProximityCursorProperties(
          !proximityCursorProperties?.display,
          proximityCursorProperties?.shape,
          proximityCursorProperties?.size,
          proximityCursorProperties?.lineStyle
        )
      );
      cornerstone.getEnabledElements().forEach(enabledElement => {
        triggerEvent(enabledElement.element, toggleEvent, enabledElement);
      });
    },
  };

  const definitions = {
    jumpToImage: {
      commandFn: actions.jumpToImage,
      storeContexts: [],
      options: {},
    },
    getNearbyToolData: {
      commandFn: actions.getNearbyToolData,
      storeContexts: [],
      options: {},
    },
    removeToolState: {
      commandFn: actions.removeToolState,
      storeContexts: [],
      options: {},
    },
    updateTableWithNewMeasurementData: {
      commandFn: actions.updateTableWithNewMeasurementData,
      storeContexts: [],
      options: {},
    },
    showDownloadViewportModal: {
      commandFn: actions.showDownloadViewportModal,
      storeContexts: ['viewports'],
      isDisabled: () => {
        return !FlywheelCommonUtils.hasPermissionForFileDownload();
      },
      options: {},
      disabledStoreContexts: ['viewports'],
    },
    getActiveViewportEnabledElement: {
      commandFn: actions.getActiveViewportEnabledElement,
      storeContexts: ['viewports'],
      options: {},
    },
    rotateViewportCW: {
      commandFn: actions.rotateViewport,
      storeContexts: ['viewports'],
      options: { rotation: 90 },
    },
    rotateViewportCCW: {
      commandFn: actions.rotateViewport,
      storeContexts: ['viewports'],
      options: { rotation: -90 },
    },
    invertViewport: {
      commandFn: actions.invertViewport,
      storeContexts: ['viewports'],
      options: {},
    },
    flipViewportVertical: {
      commandFn: actions.flipViewportVertical,
      storeContexts: ['viewports'],
      options: {},
    },
    flipViewportHorizontal: {
      commandFn: actions.flipViewportHorizontal,
      storeContexts: ['viewports'],
      options: {},
    },
    incrementVerticalStretch: {
      commandFn: actions.stretchViewport,
      storeContexts: ['viewports'],
      options: { isVertical: true, isIncrement: true },
    },
    decrementVerticalStretch: {
      commandFn: actions.stretchViewport,
      storeContexts: ['viewports'],
      options: { isVertical: true, isIncrement: false },
    },
    incrementHorizontalStretch: {
      commandFn: actions.stretchViewport,
      storeContexts: ['viewports'],
      options: { isVertical: false, isIncrement: true },
    },
    decrementHorizontalStretch: {
      commandFn: actions.stretchViewport,
      storeContexts: ['viewports'],
      options: { isVertical: false, isIncrement: false },
    },
    scaleUpViewport: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: 1 },
    },
    scaleDownViewport: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: -1 },
    },
    fitViewportToWindow: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: 0 },
    },
    resetViewport: {
      commandFn: actions.resetViewport,
      storeContexts: ['viewports'],
      options: {},
    },
    deleteMeasurement: {
      commandFn: actions.deleteMeasurement,
      storeContexts: ['viewports'],
      options: {},
    },
    clearAnnotations: {
      commandFn: actions.clearAnnotations,
      storeContexts: ['viewports'],
      options: {},
    },
    nextImage: {
      commandFn: actions.nextImage,
      storeContexts: ['viewports'],
      options: {},
    },
    toggleScaleDisplay: {
      commandFn: actions.toggleScaleDisplay,
      storeContexts: ['viewports'],
      options: {},
    },
    toggleFundusRing: {
      commandFn: actions.toggleFundusRing,
      storeContexts: ['viewports'],
      options: {},
    },
    clearScaleDisplay: {
      commandFn: actions.clearScaleDisplay,
      storeContexts: [],
      options: {},
      context: 'VIEWER',
    },
    previousImage: {
      commandFn: actions.previousImage,
      storeContexts: ['viewports'],
      options: {},
    },
    nextBSlice: {
      commandFn: actions.nextBSlice,
      storeContexts: ['viewports'],
      options: {},
    },
    // TOOLS
    setToolActive: {
      commandFn: actions.setToolActive,
      storeContexts: [],
      options: {},
    },
    setZoomTool: {
      commandFn: actions.setToolActive,
      storeContexts: [],
      options: { toolName: 'Zoom' },
    },
    setCornerstoneLayout: {
      commandFn: actions.setCornerstoneLayout,
      storeContexts: [],
      options: {},
      context: 'VIEWER',
    },
    setWindowLevel: {
      commandFn: actions.setWindowLevel,
      storeContexts: ['viewports'],
      options: {},
    },
    update2DViewport: {
      commandFn: actions.updateViewportProperties,
      storeContexts: ['viewports'],
      options: {},
      context: 'VIEWER',
    },
    performUndo: {
      commandFn: actions.performUndo,
      storeContexts: ['viewports'],
      options: {},
    },
    performRedo: {
      commandFn: actions.performRedo,
      storeContexts: ['viewports'],
      options: {},
    },
    saveSegments: {
      commandFn: actions.saveSegments,
      storeContexts: ['viewports'],
      options: { saveNewSegment: true },
    },
    toggleProximityRoiCursorStatus: {
      commandFn: actions.toggleProximityRoiCursorStatus,
      storeContexts: ['viewports'],
      options: {},
    },
  };

  return {
    actions,
    definitions,
    defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE',
  };
};

export default commandsModule;

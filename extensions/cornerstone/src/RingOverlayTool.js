import csTools from 'cornerstone-tools';
import TOOL_NAMES from './toolNames';
import IndicatorDisplayTool from './IndicatorDisplayTool';
import { getRingCount } from './utils/RingOverlay';

const ellipseUtils = csTools.importInternal('util/ellipseUtils');
const moveAnnotation = csTools.importInternal('manipulators/moveAnnotation');
const { pointInEllipse } = ellipseUtils;

/**
 * @class RingOverlayTool - Render the ring overlay on 2D viewport.
 * @extends IndicatorDisplayTool
 */
export default class RingOverlayTool extends IndicatorDisplayTool {
  constructor(props = {}) {
    const defaultProps = {
      mixins: [],
      name: TOOL_NAMES.RING_OVERLAY_TOOL,
      supportedInteractionTypes: ['Mouse'],
    };
    const initialProps = Object.assign(defaultProps, props);
    super(initialProps);
  }

  // Overriding from parent class.P
  getToolData(element) {
    const toolState = csTools.getToolState(element, this.name);

    return toolState?.data || [];
  }

  clearToolData() {
    this.data.length = 0;
  }

  toolSelectedCallback(evt, annotation, interactionType = 'mouse') {
    const ringCount = getRingCount();
    this.options.deleteIfHandleOutsideImage = false;
    const { element } = evt.detail;
    const toolState = csTools.getToolState(element, this.name);
    for (let i = 0; i < ringCount; i++) {
      moveAnnotation(evt, this, toolState.data[i], interactionType);
    }
  }

  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    return;
  }

  mouseMoveCallback = evt => {
    const { element, currentPoints } = evt.detail;
    const coords = currentPoints.canvas;
    const toolState = csTools.getToolState(element, this.name);
    let imageNeedsUpdate = false;
    const ringCount = getRingCount();
    for (let i = 0; i < ringCount; i++) {
      const data = toolState.data[i];
      const nearToolAndNotMarkedActive =
        this.pointNearTool(element, data, coords, 'mouse') && !data?.active;
      const notNearToolAndMarkedActive =
        !this.pointNearTool(element, data, coords, 'mouse') && data?.active;

      if (nearToolAndNotMarkedActive || notNearToolAndMarkedActive) {
        data.active = !data.active;
        imageNeedsUpdate = true;
      }
    }
    return imageNeedsUpdate;
  };

  pointNearTool(
    element,
    data,
    coords,
    interactionType = 'mouse',
    mouseProximity = 10
  ) {
    let isHighlight = false;

    if (!(data?.handles?.start && data?.handles?.end)) {
      return false;
    }

    const distance = interactionType === 'mouse' ? mouseProximity : 25;
    const startCanvas = cornerstone.pixelToCanvas(element, data.handles.start);
    const endCanvas = cornerstone.pixelToCanvas(element, data.handles.end);
    const minorEllipse = {
      left: Math.min(startCanvas.x, endCanvas.x) + distance / 2,
      top: Math.min(startCanvas.y, endCanvas.y) + distance / 2,
      width: Math.abs(startCanvas.x - endCanvas.x) - distance,
      height: Math.abs(startCanvas.y - endCanvas.y) - distance,
    };
    const majorEllipse = {
      left: Math.min(startCanvas.x, endCanvas.x) - distance / 2,
      top: Math.min(startCanvas.y, endCanvas.y) - distance / 2,
      width: Math.abs(startCanvas.x - endCanvas.x) + distance,
      height: Math.abs(startCanvas.y - endCanvas.y) + distance,
    };
    const pointInMinorEllipse = pointInEllipse(minorEllipse, coords);
    const pointInMajorEllipse = pointInEllipse(majorEllipse, coords);

    if (pointInMajorEllipse && !pointInMinorEllipse) {
      if (!isHighlight) {
        isHighlight = true;
      }
    }
    return isHighlight;
  }
}

import { importInternal, getToolState } from 'cornerstone-tools';
import TOOL_NAMES from './toolNames';
import { getIndicatorLabel } from './utils/indicatorUtils/utils';

// Cornerstone 3rd party dev kit imports
const draw = importInternal('drawing/draw');
const drawCircle = importInternal('drawing/drawCircle');
const drawEllipse = importInternal('drawing/drawEllipse');
const drawTextBox = importInternal('drawing/drawTextBox');
const drawJoinedLines = importInternal('drawing/drawJoinedLines');
const getNewContext = importInternal('drawing/getNewContext');
const BaseTool = importInternal('base/BaseTool');

/**
 * @class IndicatorDisplayTool - Render the indicators
 * @extends cornerstoneTools.BaseTool
 */
export default class IndicatorDisplayTool extends BaseTool {
  constructor(props = {}) {
    const defaultProps = {
      mixins: ['enabledOrDisabledBinaryTool'],
      name: TOOL_NAMES.INDICATOR_DISPLAY_TOOL,
    };

    const initialProps = Object.assign(defaultProps, props);

    super(initialProps);

    this._indicatorModule = cornerstoneTools.getModule('indicator');
  }

  getToolData(element) {
    const toolState = getToolState(element, this.name);

    if (!toolState) {
      return;
    }
    return toolState.data;
  }

  renderToolData(evt) {
    const eventData = evt.detail;
    const indicatorModule = this._indicatorModule;
    const imageId = evt.detail.image.imageId;

    const toolData = this.getToolData(evt.currentTarget);
    if (!toolData) {
      return;
    }
    const indicatorSets = indicatorModule.getters.indicatorSets();

    const { lineWidth } = indicatorModule.configuration;

    // We have tool data for this element - iterate over each one and draw it
    const context = getNewContext(eventData.canvasContext.canvas);

    for (let index = 0; index < indicatorSets.length; index++) {
      const indicatorSet = indicatorSets[index];

      for (let i = 0; i < toolData.length; i++) {
        const data = toolData[i];

        if (
          data.indicatorSeriesInstanceUid !== indicatorSet.SeriesInstanceUID
        ) {
          continue;
        }

        // Don't render if entire indicatorSet is hidden.
        if (!indicatorSet.visible) {
          continue;
        }

        const indicatorData = indicatorModule.getters.indicator(
          indicatorSet.indicatorSetLabel,
          data.indicatorSeriesInstanceUid,
          imageId,
          data.indicatorNumber
        );
        if (!indicatorData) {
          continue;
        }
        const isMatchingIndicator =
          getIndicatorLabel(this.name) === indicatorSet.indicatorSetLabel;

        // Don't render if indicator is hidden or tool and indicator are not same.
        if (!indicatorData.visible || !isMatchingIndicator) {
          continue;
        }

        const points = data.handles.points || [
          data.handles.start,
          data.handles.end,
        ];

        if (!points.length) {
          continue;
        }

        const color = 'green';

        let options = { color, lineWidth };

        switch (data.type) {
          case 'CLOSED_PLANAR':
            this._renderClosedPlanar(
              context,
              eventData.element,
              points,
              options
            );
            break;
          case 'POINT':
            this._renderPoint(context, eventData.element, points, options);
            break;
          case 'OPEN_PLANAR':
            if (data.color) {
              options.color = data.color;
            }
            if (data.lineWidth) {
              options.lineWidth = data.lineWidth;
            }
            if (data.value !== undefined) {
              options.value = data.value;
            }
            if (data.pointPosition) {
              options.pointPosition = data.pointPosition;
            }
            if (data.textPosition) {
              options.textPosition = data.textPosition;
            }
            options.coordSystem = data?.coordSystem || 'pixel';
            this._renderOpenPlanar(context, eventData.element, points, options);
            break;
          case 'CIRCULAR':
            if (data.isDottedLine) {
              options.lineDash = [10, 10];
              options.lineWidth = 4;
            }
            if (toolData.find(data => data.active === true)) {
              options.color = '#01fc2a';
            }
            this._renderEllipse(context, eventData.element, points, options);
        }
      }
    }
  }

  _renderClosedPlanar(context, element, points, options) {
    draw(context, context => {
      drawJoinedLines(
        context,
        element,
        points[points.length - 1],
        points,
        options
      );
    });
  }

  _renderEllipse(context, element, points, options) {
    draw(context, context => {
      drawEllipse(context, element, points[0], points[1], options);
    });
  }

  _renderPoint(context, element, points, options) {
    draw(context, context => {
      drawCircle(context, element, points[0], 3, options);
    });
  }

  _renderOpenPlanar(context, element, points, options) {
    const coordSystem = options.coordSystem || 'pixel';
    draw(context, context => {
      drawJoinedLines(
        context,
        element,
        points[0],
        points,
        options,
        coordSystem
      );
      if (options.value !== undefined && options.textPosition) {
        const { value } = options;

        drawTextBox(
          context,
          value,
          options.textPosition.x,
          options.textPosition.y,
          options.color
        );
      }
    });
  }
}

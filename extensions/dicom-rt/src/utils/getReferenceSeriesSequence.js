export function deriveReferencedSeriesSequenceFromFrameOfReferenceSequence(
  ReferencedFrameOfReferenceSequence,
  studyMetaData
) {
  const ReferencedSeriesSequence = [];

  if (!studyMetaData) {
    throw new Error(
      'Unable to load the RT Struct for the active series since the RT Struct reference series is different'
    );
    return;
  }

  _getSequenceAsArray(ReferencedFrameOfReferenceSequence).forEach(
    referencedFrameOfReference => {
      const { RTReferencedStudySequence } = referencedFrameOfReference;

      _getSequenceAsArray(RTReferencedStudySequence).forEach(
        rtReferencedStudy => {
          const { RTReferencedSeriesSequence } = rtReferencedStudy;

          _getSequenceAsArray(RTReferencedSeriesSequence).forEach(
            rtReferencedSeries => {
              const ReferencedInstanceSequence = [];
              const {
                ContourImageSequence,
                SeriesInstanceUID,
              } = rtReferencedSeries;

              _getSequenceAsArray(ContourImageSequence).forEach(
                contourImage => {
                  ReferencedInstanceSequence.push({
                    ReferencedSOPInstanceUID:
                      contourImage.ReferencedSOPInstanceUID,
                    ReferencedSOPClassUID: contourImage.ReferencedSOPClassUID,
                  });
                }
              );

              const referencedSeries = {
                SeriesInstanceUID,
                ReferencedInstanceSequence,
              };
              // This is a special handling to correct the SeriesInstanceUID if the sop instance UID
              // referring to a series in the study but the SeriesInstanceUID got was a wrong one
              let series =
                studyMetaData &&
                studyMetaData.getSeriesByUID(SeriesInstanceUID);
              if (!series) {
                // Finding the correct series by iterating through the image sop instance UID.
                series = getSeriesForInstanceUID(
                  studyMetaData,
                  referencedSeries
                );
                if (series) {
                  referencedSeries.SeriesInstanceUID = series.getSeriesInstanceUID();
                }
              }

              ReferencedSeriesSequence.push(referencedSeries);
            }
          );
        }
      );
    }
  );

  return ReferencedSeriesSequence;
}

function _getSequenceAsArray(sequence) {
  return Array.isArray(sequence) ? sequence : [sequence];
}

function getSeriesForInstanceUID(studyMetaData, referencedSeries) {
  const referencedInstanceSequence =
    referencedSeries.ReferencedInstanceSequence;
  const series = studyMetaData._series.find(series =>
    referencedInstanceSequence.find(instance =>
      series.getInstanceByUID(instance.ReferencedSOPInstanceUID)
    )
  );
  return series;
}

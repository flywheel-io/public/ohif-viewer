import store from '@ohif/viewer/src/store';
import { Utils, Redux } from '@flywheel/extension-flywheel-common';

const {
  selectCurrentNifti,
  selectCurrentVolumetricImage,
  selectCurrentWebImage,
} = Redux.selectors;
const { isCurrentNifti, isCurrentVolumetricImage, isCurrentWebImage } = Utils;

const getContainers = ids => {
  if (!ids) {
    return {};
  }
  const state = store.getState();
  const { acquisitionId, sessionId, fileId } = ids;
  const { acquisitions, project } = state.flywheel;
  const activeAcquisition = acquisitions[acquisitionId];
  const activeSession = findActiveSession(sessionId);
  const activeSubject = activeSession?.subject;
  const activeFile = findActiveFileFromAcquisition(activeAcquisition, fileId);

  const customContainers = {
    Project: project,
    Acquisition: activeAcquisition,
    Session: activeSession,
    Subject: activeSubject,
    File: activeFile,
  };
  return customContainers;
};

const getContainersIds = () => {
  const state = store.getState();
  const viewport = state.viewports;
  const viewportIndex = viewport.activeViewportIndex;
  const viewportData = viewport.viewportSpecificData[viewportIndex];

  if (!viewportData) {
    return;
  }
  const {
    SeriesInstanceUID: seriesInstanceUid,
    StudyInstanceUID: studyInstanceUid,
  } = viewportData;
  const associations = state.flywheel.allAssociations;
  let containersId;
  let currentFile;

  if (isCurrentNifti(state)) {
    currentFile = selectCurrentNifti(state);
  } else if (isCurrentVolumetricImage(state)) {
    currentFile = selectCurrentVolumetricImage(state);
  } else if (isCurrentWebImage()) {
    currentFile = selectCurrentWebImage(state);
  } else {
    associations.find(association => {
      const seriesId = association.series_uid;
      const studyId = association.study_uid;
      const isActiveAssociation =
        seriesInstanceUid === seriesId && studyInstanceUid === studyId;

      if (isActiveAssociation) {
        const { acquisition_id, file_id, project_id, session_id } = association;
        containersId = {
          acquisitionId: acquisition_id,
          fileId: file_id,
          projectId: project_id,
          sessionId: session_id,
        };
      }
      return isActiveAssociation;
    });
  }
  if (currentFile) {
    containersId = {
      acquisitionId: currentFile.containerId,
      fileId: currentFile.file_id,
      projectId: currentFile.parents.project,
      sessionId: currentFile.parents.session,
    };
  }
  return containersId;
};

const findActiveSession = sessionId => {
  const state = store.getState();
  const { sessions } = state.flywheel;
  const activeSession = sessions?.find(session => sessionId === session._id);

  return activeSession;
};

const findActiveFileFromAcquisition = (acquisition, fileId) => {
  const activeFile = acquisition?.files?.find(
    file => fileId === file.file_id || fileId === file._id
  );
  return activeFile;
};

export {
  getContainers,
  getContainersIds,
  findActiveSession,
  findActiveFileFromAcquisition,
};

import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import { calculateMinMaxMean } from './cornerstoneUtils';

const getLuminance = cornerstoneTools.import('util/getLuminance');

const MIN_WINDOW_WIDTH = 1;

/**
 * It applies full dynamic window width and window center on given enabledElement.
 * New values consist of:
 *  window center: it will be the mean value of a set of luminance values for given image
 *  window width: it will be the range between min and max values from a set of luminance values for given image
 *
 * It mutates given param enabledElement
 *
 * @param {Object} enabledElement cornerstone enabled element
 * @param {Object} config configuration with some default values
 */
const applyFullDynamicWWWC = (enabledElement, config = {}) => {
  const left = 0;
  const top = 0;
  const { image = {}, viewport, element } = enabledElement;
  const { width, height } = image;

  if (!width || !height) {
    return;
  }

  // Get luminance of given cs element
  const pixelLuminanceData = getLuminance(element, left, top, width, height);

  // Calculate the minimum and maximum pixel values
  const minMaxMean = calculateMinMaxMean(
    pixelLuminanceData,
    image.minPixelValue,
    image.maxPixelValue
  );

  if (config.minWindowWidth === undefined) {
    config.minWindowWidth = MIN_WINDOW_WIDTH;
  }

  // window width should be at minimum MIN_WINDOW_WIDTH (or what ever is on config)
  viewport.voi.windowWidth = Math.max(
    Math.abs(minMaxMean.max - minMaxMean.min),
    config.minWindowWidth
  );
  viewport.voi.windowCenter = minMaxMean.mean;

  // Unset any existing VOI LUT
  viewport.voiLUT = undefined;

  cornerstone.setViewport(element, viewport);
  cornerstone.updateImage(element);
};

export default applyFullDynamicWWWC;

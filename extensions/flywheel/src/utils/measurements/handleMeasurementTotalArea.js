import store from '@ohif/viewer/src/store';
import { getTotalArea } from '@ohif/ui/src/components/measurementTable/Utils.js';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const { measurementToolUtils } = FlywheelCommonUtils;
const { roiTools } = measurementToolUtils;
const CLOSED_ROI_TOOLS = [
  roiTools.EllipticalRoi,
  roiTools.CircleRoi,
  roiTools.RectangleRoi,
  roiTools.FreehandRoi,
];

function groupByMultipleProperties(object, prop1, prop2) {
  const groupedObject = {};
  object.forEach(obj => {
    const key = obj[prop1] + '_' + obj[prop2];
    if (!groupedObject[key]) {
      groupedObject[key] = [];
    }
    groupedObject[key].push(obj);
  });
  return groupedObject;
}

function getMeasurementsByLabel(measurement) {
  const measuresList = measurement.reduce(
    (measuresList, item) => ({
      ...measuresList,
      [item.location]: [...(measuresList[item.location] || []), item],
    }),
    {}
  );
  return measuresList;
}

export const getCumulativeAreaOfMeasurementLabels = taskId => {
  const closedRoiMeasurementFromStore = getClosedRoiMeasurementFromTimepointManager();
  let annotationStatsInfo = {};

  // Filter by TaskId
  const measurementFilteredByTaskId = closedRoiMeasurementFromStore.filter(
    s => s.task_id === taskId
  );
  if (!measurementFilteredByTaskId.length) {
    return annotationStatsInfo;
  }

  // Grouped by StudyInstanceUID & SeriesInstanceUID
  const measurementsGroupedByStudyAndSeriesUID = groupByMultipleProperties(
    measurementFilteredByTaskId,
    'StudyInstanceUID',
    'SeriesInstanceUID'
  );
  if (!Object.keys(measurementsGroupedByStudyAndSeriesUID).length) {
    return annotationStatsInfo;
  }

  Object.keys(measurementsGroupedByStudyAndSeriesUID).forEach(
    measurementKey => {
      let measurements = measurementsGroupedByStudyAndSeriesUID[measurementKey];

      if (measurements) {
        const measurementsByLabel = getMeasurementsByLabel(measurements);

        Object.keys(measurementsByLabel).forEach(labelKey => {
          const measures = measurementsByLabel[labelKey];
          const StudyInstanceUID =
            measures[0].StudyInstanceUID || measures[0].studyInstanceUid;
          const SeriesInstanceUID =
            measures[0].SeriesInstanceUID || measures[0].seriesInstanceUID;
          const totalArea = getTotalArea(measures, false);

          if (!annotationStatsInfo[StudyInstanceUID]) {
            annotationStatsInfo[StudyInstanceUID] = {};
          }

          if (!annotationStatsInfo[StudyInstanceUID]?.[SeriesInstanceUID]) {
            annotationStatsInfo[StudyInstanceUID][SeriesInstanceUID] = {};
          }

          annotationStatsInfo[StudyInstanceUID][SeriesInstanceUID].labels = {
            ...annotationStatsInfo[StudyInstanceUID][SeriesInstanceUID].labels,
            [labelKey]: { area: totalArea },
          };
        });
      }
    }
  );

  return annotationStatsInfo;
};

const getClosedRoiMeasurementFromTimepointManager = () => {
  const state = store.getState();
  if (state?.timepointManager?.measurements) {
    const measurements = state.timepointManager.measurements;
    const toolTypes = Object.keys(measurements);
    const toolData = [];
    toolTypes.forEach(toolType => {
      if (CLOSED_ROI_TOOLS.includes(toolType)) {
        measurements[toolType].forEach(measures => {
          toolData.push(measures);
        });
      }
    });
    return toolData;
  }
  return null;
};

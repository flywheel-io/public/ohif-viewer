import { ErrorTypes } from '../components/ReIndexing/Types';

export const getErrorTypeDisplay = errorType => {
  if (Object.keys(ErrorTypes).includes(errorType)) {
    return ErrorTypes[errorType];
  }
};

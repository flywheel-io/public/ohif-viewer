export function selectProjectConfig(state) {
  return state.flywheel.projectConfig;
}

export function selectActiveProject(state) {
  return state.flywheel.project;
}

export async function selectReaderTask(state) {
  return await state.flywheel.readerTask;
}

export async function hasPermission(state, permission) {
  const permissions = await state.flywheel.permissions;
  const featureConfig = state.flywheel.featureConfig;
  if (!featureConfig?.features?.reader_tasks) {
    return true;
  }
  return permissions.indexOf(permission) > -1;
}

import common from './common';
import studyList from './studyList';
import multipleReaderTask from './multipleReaderTask';

const reducers = {
  common,
  studyList,
  multipleReaderTask,
};

export default reducers;

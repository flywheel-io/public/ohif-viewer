import actions from './actions';
import reducers from './reducers';
import selectors from './selectors';
import * as constants from './constants/ActionTypes';

const redux = {
  actions,
  constants,
  reducers,
  selectors,
};

export default redux;

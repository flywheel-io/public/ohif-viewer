import {
  SET_STUDY_SORT,
  SET_STUDY_FILTERS,
  SET_STUDY_ORDERING,
} from '../constants/ActionTypes';

export const setStudySort = sort => ({ type: SET_STUDY_SORT, sort });
export const setStudyFilters = filters => ({
  type: SET_STUDY_FILTERS,
  filters,
});
export const setStudyOrdering = ordering => ({
  type: SET_STUDY_ORDERING,
  ordering,
});

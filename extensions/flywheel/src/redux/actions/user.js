import { SET_USER, SET_ALL_USERS } from '../constants/ActionTypes';

export function setUser(user) {
  return {
    type: SET_USER,
    user,
  };
}

export function setAllUsers(users) {
  return {
    type: SET_ALL_USERS,
    users,
  };
}

import {
  SET_ACTIVE_SESSION,
  SET_ACTIVE_SESSIONS,
  SET_ALL_SESSIONS,
  SET_SESSION_ACQUISITIONS,
  SET_SESSIONS,
  SET_LAUNCHED_SESSION_ID,
  CLEAR_LAUNCHED_SESSION_ID,
} from '../constants/ActionTypes';

export function setActiveSession(
  session,
  clearExistingInfos = {
    clearNifti: true,
    clearMeta: true,
  }
) {
  return {
    type: SET_ACTIVE_SESSION,
    session,
    clearExistingInfos,
  };
}
export function setActiveSessions(
  sessions,
  clearExistingInfos = {
    clearNifti: true,
    clearMeta: true,
  }
) {
  return {
    type: SET_ACTIVE_SESSIONS,
    sessions,
    clearExistingInfos,
  };
}

export function setAllSessions(sessions) {
  return {
    type: SET_ALL_SESSIONS,
    sessions,
  };
}

export function setAcquisitions(acquisitions) {
  return {
    type: SET_SESSION_ACQUISITIONS,
    acquisitions,
  };
}

export function setSessions(sessions) {
  return {
    type: SET_SESSIONS,
    sessions,
  };
}

export function setLaunchedSessionId(launchedSessionId) {
  return {
    type: SET_LAUNCHED_SESSION_ID,
    launchedSessionId,
  };
}

export function clearLaunchedSessionId() {
  return {
    type: CLEAR_LAUNCHED_SESSION_ID,
    launchedSessionId: null,
  };
}

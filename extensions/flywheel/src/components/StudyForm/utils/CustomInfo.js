import store from '@ohif/viewer/src/store';
import { HTTP } from '@flywheel/extension-flywheel-common';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';

const { getSession, getAcquisition } = HTTP.services;
// Caching acquisition id and session id while sending an API request
// to prevent duplicate requests.
let cachedIds = {
  acquisitionIds: [],
  sessionIds: [],
};

const storeContainer = (containerType, ids) => {
  const state = store.getState();
  const { setAcquisitions, setActiveSession } = FlywheelRedux.actions;
  const { acquisitionId, sessionId } = ids;

  switch (containerType) {
    case 'Acquisition':
      const isAcquisitionRequested = cachedIds.acquisitionIds.includes(
        acquisitionId
      );
      const shouldStoreAcquisition =
        acquisitionId && acquisitionId !== 'None' && !isAcquisitionRequested;

      if (shouldStoreAcquisition) {
        cachedIds.acquisitionIds.push(acquisitionId);
        return getAcquisition(acquisitionId).then(response => {
          const { acquisitions } = state.flywheel;
          const acquisitionData = {
            ...acquisitions,
            [acquisitionId]: response,
          };
          store.dispatch(setAcquisitions(acquisitionData));
        });
      }

    case 'Session':
      const isSessionRequested = cachedIds.sessionIds.includes(sessionId);
      const shouldStoreSession = sessionId && !isSessionRequested;

      if (shouldStoreSession) {
        cachedIds.sessionIds.push(sessionId);
        return getSession(sessionId).then(response => {
          store.dispatch(
            setActiveSession(response, {
              clearNifti: false,
              clearMeta: false,
            })
          );
        });
      }
  }
};

const getInfoValue = (info, containers) => {
  if (!containers) {
    return '';
  }
  let container;

  Object.keys(containers).some(data => {
    if (data === info.containerType) {
      container = containers[data];
      return true;
    }
  });
  const path = info.field.split('.');
  const field = getDataFromPath(path, container);

  return field.toString();
};

const getDataFromPath = (path, location) => {
  if (!location) {
    return '';
  }
  let data;
  try {
    data = location;
    path.forEach(str => {
      data = data[str];
    });
  } catch (e) {
    return '';
  }
  const isValidFormat =
    typeof data === 'string' ||
    typeof data === 'number' ||
    typeof data === 'boolean';

  return isValidFormat ? data : '';
};

const clearRequestedIdCache = () => {
  cachedIds = {
    acquisitionIds: [],
    sessionIds: [],
  };
};
export { storeContainer, getInfoValue, clearRequestedIdCache };

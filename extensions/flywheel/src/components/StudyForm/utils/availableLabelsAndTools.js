import {
  getStudyFormActiveToolsForEnable,
  getStudyFormToolsForEnable,
  getAvailableTools,
  hasMeasurementTools,
} from './index.js';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { isEqual } from 'lodash';

import { ReaderTaskModes, getTaskMode } from './ReaderTaskModes.js';

const {
  selectActiveTool,
  measurementTools,
  scaleDisplayValidationTools,
} = FlywheelCommonRedux.selectors;
const {
  setActiveTool,
  setAvailableLabels,
  setActiveQuestion,
} = FlywheelCommonRedux.actions;

export const getAvailableLabelsForSet = async (
  studyFormState,
  props,
  state,
  toolsWithLabels,
  isFile,
  slice,
  isStudyFormContainsMeasurementTools,
  isPanelSwitched,
  prevProps
) => {
  const { currentSliceInfo, notes, readOnly } = studyFormState;
  const { projectConfig, sliceInfo, measurementLabels, measurements } = props;
  let sliceNumber = sliceInfo?.sliceNumber;
  const { bSlices, studyFormWorkflow, studyForm } = projectConfig;
  const sliceSettings = bSlices?.settings;
  const { subForm, infusions, multipleReaderTask, viewerAvatar } = state;
  const {
    currentSelectedQuestion,
    availableTools,
    subFormDialogData,
  } = infusions;
  const isFormBasedWorkFlow =
    studyFormWorkflow === 'Form' || !studyFormWorkflow;
  const isFormBasedWithOutTools =
    isFormBasedWorkFlow && !isStudyFormContainsMeasurementTools;
  const shouldEnableToolsInEditMode =
    !isFormBasedWorkFlow || isFormBasedWithOutTools;
  const taskMode = await getTaskMode();

  let labels;
  let tools;
  let tool;

  if (isPanelSwitched && subFormDialogData.sliceInfo?.sliceNumber) {
    sliceNumber = subFormDialogData.sliceInfo.sliceNumber;
  }

  let annotationTools = [...measurementTools];
  annotationTools.push(...scaleDisplayValidationTools);
  annotationTools = [...new Set(annotationTools)];

  if (readOnly) {
    labels = [];
    tools = [];
  } else if (taskMode === ReaderTaskModes.MultiTaskReview) {
    const toolList = _getToolsIncludingScaleDisplayTools([]);
    if (!isEqual(availableTools, toolList)) {
      labels = [];
      tools = [];
    }
  } else if (
    taskMode === ReaderTaskModes.MultiTaskEdit &&
    !isEqual(availableTools, annotationTools) &&
    shouldEnableToolsInEditMode
  ) {
    const allTools = getStudyFormToolsForEnable(props, toolsWithLabels);
    labels = [''];
    tools = allTools;
  } else if (!bSlices && !studyForm?.components) {
    labels = null;
    tools = null;
  } else if (
    // Initial loading time measurements need to fill in timepointmanager and
    // enable study form configured tool(limit doesn't reached) only in toolbar menu
    !isFormBasedWorkFlow &&
    Array.isArray(prevProps.measurements) &&
    !prevProps.measurements?.length &&
    Object.keys(measurements)?.length &&
    !sliceNumber &&
    !subForm.isSubFormPopupEnabled &&
    !currentSliceInfo &&
    currentSliceInfo?.imageId === sliceInfo?.imageId
  ) {
    const allTools = getStudyFormToolsForEnable(props, toolsWithLabels);
    labels = [''];
    tools = [...new Set(allTools)];
    await setAvailableLabelsToStore(labels, tools);
  } else if (
    !isFormBasedWorkFlow &&
    Array.isArray(prevProps.measurements) &&
    !prevProps.measurements?.length &&
    Object.keys(measurements)?.length &&
    sliceNumber > 0 &&
    !subForm.isSubFormPopupEnabled &&
    currentSliceInfo &&
    currentSliceInfo.imageId === sliceInfo?.imageId
  ) {
    const allTools = getStudyFormToolsForEnable(props, toolsWithLabels);
    labels = [''];
    tools = allTools;
  } else if (
    !isFormBasedWorkFlow &&
    sliceNumber > 0 &&
    !subForm.isSubFormPopupEnabled &&
    !isFile &&
    currentSliceInfo &&
    currentSliceInfo.imageId !== sliceInfo?.imageId
  ) {
    if (
      studyFormWorkflow === 'Mixed' &&
      currentSelectedQuestion?.isSubForm &&
      bSlices
    ) {
      tool = 'Wwwc';
      labels = [];
      tools = [];
      store.dispatch(setActiveQuestion(null));
    } else {
      let questionToolsList = getStudyFormActiveToolsForEnable(
        projectConfig,
        props,
        null,
        toolsWithLabels
      );

      if (
        (bSlices?.settings?.[sliceNumber] &&
          !questionToolsList?.length &&
          !isFile) ||
        (isFile && studyForm?.components?.length && !questionToolsList?.length)
      ) {
        labels = null;
        tools = null;
      } else if (
        (!bSlices &&
          studyForm?.components &&
          !state.infusions.currentSelectedQuestion?.isSubForm) ||
        (bSlices?.settings?.[sliceNumber] &&
          questionToolsList?.length &&
          !isFile) ||
        (isFile && studyForm?.components?.length && questionToolsList?.length)
      ) {
        questionToolsList = getAvailableTools(
          questionToolsList,
          projectConfig,
          toolsWithLabels,
          measurementLabels,
          slice
        );
        labels = [''];
        tools = questionToolsList;
      }
    }
  } else if (
    isFormBasedWorkFlow &&
    sliceNumber > 0 &&
    !isFile &&
    currentSliceInfo &&
    currentSliceInfo.imageId !== sliceInfo?.imageId &&
    sliceSettings
  ) {
    if (sliceSettings[sliceNumber] && currentSelectedQuestion !== null) {
      if (currentSelectedQuestion.isSubForm) {
        tool = 'Wwwc';
        labels = [];
        tools = [];
        store.dispatch(setActiveQuestion(null));
      } else {
        const toolAndLabelData = _retainToolAndLabelForCurrentQuestion(
          notes,
          currentSelectedQuestion,
          sliceSettings,
          sliceNumber
        );
        labels = toolAndLabelData.labels;
        tools = toolAndLabelData.tools;
      }
    }
  } else if (
    isFormBasedWorkFlow &&
    !currentSliceInfo &&
    sliceNumber > 0 &&
    !subForm.isSubFormPopupEnabled &&
    currentSelectedQuestion === null
  ) {
    labels = [];
    tools = [];
  }

  if (sliceNumber > 0 && (labels !== undefined || tools !== undefined)) {
    tools = [...new Set(tools)];
    await setAvailableLabelsToStore(labels, tools);
  }

  if (tool) {
    setActiveToolToStore(tool);
  }
};

export const setAvailableLabelsToStore = async (labels, tools) => {
  const state = store.getState();
  const infusions = state.infusions;
  const { availableLabels, availableTools } = infusions;
  const hasTool = hasMeasurementTools();
  const toolList = _getToolsIncludingScaleDisplayTools(tools);
  const taskMode = await getTaskMode();
  const isProtocolDeleted = state.flywheel.isProtocolDeleted;

  if (taskMode === ReaderTaskModes.MultiTaskReview) {
    if (!isProtocolDeleted || (!availableLabels && !availableTools)) {
      store.dispatch(setAvailableLabels([], []));
    }
    return;
  }

  const isMultiTaskMode = state.multipleReaderTask?.TaskIds?.length > 1;

  if (!hasTool && (state.viewerAvatar?.singleAvatar || !isMultiTaskMode)) {
    store.dispatch(setAvailableLabels([''], [...measurementTools]));
    return;
  }

  if (!isEqual(availableLabels, labels) || !isEqual(availableTools, toolList)) {
    store.dispatch(setAvailableLabels(labels, tools));
  }
};

export const setActiveToolToStore = tool => {
  const infusions = store.getState().infusions;
  const { activeTool } = infusions;
  if (activeTool !== tool) {
    store.dispatch(setActiveTool(tool));
  }
};

export const getActiveTool = (tools = [], studyFormState) => {
  let activeTool = selectActiveTool(store.getState());
  const state = studyFormState;
  if (
    (state.currentQuestion?.key !== state.previousQuestion?.key &&
      state.previousQuestion?.values?.some(x => x?.measurementTools)) ||
    (state.currentType && !tools?.includes(activeTool))
  ) {
    if (!tools.includes(activeTool) && tools[0]) {
      activeTool = tools[0];
    }
  }
  return activeTool;
};

const _retainToolAndLabelForCurrentQuestion = (
  notes,
  currentSelectedQuestion,
  sliceSettings,
  sliceNumber
) => {
  const toolAndLabel = {};
  let formNotes = notes;
  let answer =
    notes?.[currentSelectedQuestion.questionKey]?.value ||
    notes?.[currentSelectedQuestion.questionKey];

  if (sliceSettings[sliceNumber]) {
    formNotes = notes?.slices?.[sliceNumber];
    answer =
      formNotes?.[currentSelectedQuestion.questionKey]?.value ||
      formNotes?.[currentSelectedQuestion.questionKey];
  }

  const selectedOption = currentSelectedQuestion.values?.find(
    value => value.value === answer
  );
  const selectedOptionTools = selectedOption?.measurementTools || [];
  toolAndLabel.labels = selectedOption?.requireMeasurements || [];
  toolAndLabel.tools = selectedOptionTools;
  return toolAndLabel;
};

const _getToolsIncludingScaleDisplayTools = tools => {
  const state = store.getState();
  const isScaleDisplayed = state.infusions.isScaleDisplayed;
  let toolList = [];

  if (!isScaleDisplayed) {
    toolList.push(...tools, ...scaleDisplayValidationTools);
    toolList = [...new Set(toolList)];
  }

  return toolList;
};

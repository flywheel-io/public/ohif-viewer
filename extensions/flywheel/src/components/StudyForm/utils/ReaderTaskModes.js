import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import store from '@ohif/viewer/src/store';

const { hasMultipleReaderTask } = FlywheelCommonUtils;

const ReaderTaskModes = {
  OwnTask: 1,
  SingleTaskReview: 2,
  MultiTaskReview: 4,
  MultiTaskEdit: 8,
};

const getTaskMode = async () => {
  const state = store.getState();
  const isMultipleReaderTask = hasMultipleReaderTask(state);

  if (isMultipleReaderTask) {
    return state.viewerAvatar.singleAvatar
      ? ReaderTaskModes.MultiTaskEdit
      : ReaderTaskModes.MultiTaskReview;
  } else {
    const {
      selectReaderTask,
      selectUser,
      hasPermission,
    } = FlywheelRedux.selectors;
    const readerTask = await selectReaderTask(state);

    if (!readerTask) {
      return;
    }

    const user = selectUser(state);
    const isSiteAdmin = user.roles.includes('site_admin');
    const hasEditFormOthersPermission = await hasPermission(
      state,
      'form_responses_edit_others'
    );

    if (readerTask.assignee === user._id) {
      return ReaderTaskModes.OwnTask;
    }
    // TODO: Verify all SingleTaskReview mode scenarios
    if (
      readerTask.assignee !== user._id &&
      (hasEditFormOthersPermission || isSiteAdmin)
    ) {
      return ReaderTaskModes.SingleTaskReview;
    }
  }
};
export { ReaderTaskModes, getTaskMode };

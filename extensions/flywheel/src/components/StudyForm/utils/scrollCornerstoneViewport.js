import cornerstoneTools from 'cornerstone-tools';
import cornerstone from 'cornerstone-core';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');
const { cornerstoneUtils } = FlywheelCommonUtils;

export const scrollCornerstoneViewport = sliceInfo => {
  if (sliceInfo?.sliceNumber) {
    const element = cornerstoneUtils.getEnabledElement(sliceInfo.viewportIndex);
    if (element) {
      const enabledElement = cornerstone.getEnabledElement(element);
      if (enabledElement?.image) {
        const scrollIndex = sliceInfo.sliceNumber - 1;
        scrollToIndex(enabledElement.element, scrollIndex);
      }
    }
  }
};

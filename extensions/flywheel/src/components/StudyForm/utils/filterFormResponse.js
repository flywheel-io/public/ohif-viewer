import store from '@ohif/viewer/src/store';
import cloneDeep from 'lodash.clonedeep';
import { Utils } from '@flywheel/extension-flywheel-common';

const filterFormResponse = formResponse => {
  const state = store.getState();
  const measurements = state.timepointManager.measurements;
  let subFormAnnotations = {};
  let studyFormQuestions = [];
  const filteredFormResponse = cloneDeep(formResponse);
  const isMultiReaderTask = Utils.hasMultipleReaderTask(state);

  if (Utils.isCurrentFile(state)) {
    Object.keys(measurements).forEach(toolType => {
      measurements[toolType].forEach(annotation => {
        const isAnnotationForCurrentTask = isMultiReaderTask
          ? state.viewerAvatar.taskId === annotation.task_id
          : true;
        if (
          annotation.isSubForm &&
          !subFormAnnotations[annotation.subFormAnnotationId] &&
          isAnnotationForCurrentTask
        ) {
          studyFormQuestions.push(annotation.question);
          subFormAnnotations[annotation.subFormAnnotationId] = true;
        }
      });
    });

    studyFormQuestions.forEach(question => {
      Object.keys(formResponse[question]).forEach(item => {
        if (Array.isArray(formResponse[question][item])) {
          const filtered = formResponse[question][item].filter(x => {
            return subFormAnnotations[x.annotationId];
          });
          filteredFormResponse[question][item] = filtered;
        }
      });
    });

    Object.keys(formResponse).forEach(questionKey => {
      if (typeof formResponse[questionKey] === 'object') {
        if (!studyFormQuestions.includes(questionKey)) {
          filteredFormResponse[questionKey] =
            filteredFormResponse[questionKey]?.value ||
            filteredFormResponse[questionKey];
        }
      }
    });
  }

  return filteredFormResponse;
};

export { filterFormResponse };

import React, { Fragment } from 'react';
import { getInfoValue } from '../../utils/CustomInfo';

const SingleInfo = props => {
  const { info, containers } = props;
  const data = getInfoValue(info, containers);

  return (
    <Fragment>
      <div className="single-info">
        <div title={info.label}>{info.label}</div>
        <div title={data}>{data}</div>
      </div>
    </Fragment>
  );
};

export default SingleInfo;

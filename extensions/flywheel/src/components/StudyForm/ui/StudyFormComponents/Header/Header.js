import React from 'react';
import { getAvatarStyle } from '@ohif/ui/src/components/measurementTable/Utils.js';
import store from '@ohif/viewer/src/store';
import {
  avatarClick,
  onAvatarMouseEnter,
  onAvatarMouseLeave,
} from '../../../../../utils/index.js';
import './Header.styl';

export default function Header(props) {
  const { title, slice } = props;

  const state = store.getState();
  const isProtocolDeleted = state.flywheel.isProtocolDeleted;
  const allUsers = state.flywheel.allUsers;
  const users = [];
  state.multipleReaderTask?.multipleTaskResponse?.forEach(userData => {
    if (allUsers?.[userData.assignee]) {
      let user = { ...allUsers[userData.assignee] };
      user.task_id = userData._id;
      users.push(user);
    }
  });

  let dispatchActions = null;
  let userAvatarClickedDetails = [];

  return (
    <h5 className="study-form-header-section">
      {title}
      <div className="user-avatar-container">
        {users?.map(user => {
          const { avatarStyle, initials, avatar } = getAvatarStyle(user);
          avatarStyle.color = 'black';
          avatarStyle.borderWidth = '1px';
          avatarStyle.borderStyle = 'solid';
          avatarStyle.pointerEvents = isProtocolDeleted ? 'none' : 'auto';
          avatarStyle.opacity = isProtocolDeleted ? 0.3 : 1;

          if (
            !userAvatarClickedDetails.find(
              task =>
                task[initials + '-' + user.task_id] === true ||
                task[initials + '-' + user.task_id] === false
            )
          ) {
            userAvatarClickedDetails.push({
              [initials + '-' + user.task_id]: false,
              initials: initials,
              taskId: user.task_id,
              slice: slice,
              visible: false,
            });
          }

          const viewerAvatar = state.viewerAvatar.viewerAvatar;

          if (viewerAvatar.length) {
            viewerAvatar.forEach(avatar => {
              const data = avatar.initials + '-' + avatar.taskId;
              const currentAvatarLength = document.getElementsByClassName(data)
                .length;
              if (!avatar[data] && currentAvatarLength > 0) {
                for (let n = 0; n < currentAvatarLength; n++) {
                  document
                    .getElementsByClassName(data)
                    .item(n).style.opacity = 0.3;
                }
              } else if (currentAvatarLength > 0 && avatar[data]) {
                for (let n = 0; n < currentAvatarLength; n++) {
                  document
                    .getElementsByClassName(data)
                    .item(n).style.opacity = 1;
                }
              }
            });
          }

          return (
            <div className="avatar-wrapper" key={user.task_id}>
              <div
                className={`avatar viewerFormAvatar ${initials +
                  '-' +
                  user.task_id}`}
                style={avatarStyle}
                onClick={e =>
                  avatarClick(
                    userAvatarClickedDetails,
                    initials,
                    user.task_id,
                    e,
                    dispatchActions,
                    props.measurements,
                    user
                  )
                }
                onMouseEnter={e =>
                  onAvatarMouseEnter(
                    e,
                    dispatchActions,
                    props.measurements,
                    user,
                    null,
                    user.task_id,
                    initials
                  )
                }
                onMouseLeave={e =>
                  onAvatarMouseLeave(
                    e,
                    dispatchActions,
                    props.measurements,
                    user.task_id,
                    initials
                  )
                }
              >
                {avatar ? '' : initials}
              </div>
            </div>
          );
        })}
      </div>
    </h5>
  );
}

import React, { Component } from 'react';
import { percentColorsConfig } from './CircleChartUtils.js';
import {
  getHighestPercentage,
  getPercentage,
  getOptions,
} from '../StudyForm/utils/AgreementIndicatorUtils.js';

const RADIUS = 100;
const PERIMETER = Math.round(Math.PI * RADIUS * 2);
const DEFAULT_BG_COLOR = 'var(--circle-chart-background)';
const DEFAULT_FONT_COLOR = 'var(--hover-color)';
const DEFAULT_CIRCLE_STROKE_COLOR = '#111112';
const DEFAULT_START_ANGLE = -90;

export default class CircleChart extends Component {
  static defaultProps = {
    options: [],
    animate: true,
    animationDuration: '1s',
    showPercentage: true,
    showPercentageSymbol: true,
    textColor: DEFAULT_FONT_COLOR,
    size: '70',
    bgColor: DEFAULT_CIRCLE_STROKE_COLOR,
    lineWidth: '25',
    percentSpacing: 1,
    textStyle: { font: '3.3rem Helvetica, Arial, sans-serif' },
    invert: false,
    percentColors: percentColorsConfig,
    percent: 100,
  };

  get text() {
    const { taskCount: total, options } = this.props;
    const progress = getHighestPercentage(total, options);
    const {
      showPercentage,
      textColor,
      textStyle,
      percentSpacing,
      showPercentageSymbol,
      invert,
    } = this.props;
    if (!showPercentage) return;

    return (
      <text
        style={textStyle}
        fill={invert ? this.getBackgroundColor() : textColor}
        x={RADIUS}
        y={RADIUS}
        textAnchor="middle"
        dominantBaseline="central"
      >
        {progress}
        {showPercentageSymbol && <tspan dx={percentSpacing}>%</tspan>}
      </text>
    );
  }

  getOffset = (val = 0) => {
    return Math.round(((100 - Math.min(val, 100)) / 100) * PERIMETER);
  };

  getFillColor() {
    const { invert } = this.props;

    if (invert) {
      return this.getForegroundColor();
    }
    return this.getBackgroundColor();
  }

  getForegroundColor() {
    return DEFAULT_FONT_COLOR;
  }

  getBackgroundColor() {
    return DEFAULT_BG_COLOR;
  }

  getVectorDetails(option, index, options, total) {
    let angle = DEFAULT_START_ANGLE;
    const percent = getPercentage(option, total);
    const strokeDashoffset = this.getOffset(percent);
    if (!percent) {
      return { angle, strokeDashoffset: 0 };
    }
    if (index) {
      let sum = 0;
      options.forEach((x, xIndex) => {
        if (xIndex < index) {
          sum += x.count;
        }
      });
      angle = Math.round((sum / total) * 360) + angle;
    }
    return { angle, strokeDashoffset };
  }

  getPercentColor(options, index, total) {
    const { percentColors, bgColor } = this.props;
    const percent = getPercentage(options[index], total);
    const percentColor = percentColors.find(
      x => percent >= x.from && percent <= x.to
    );
    if (percentColor?.color) {
      return percentColor.color;
    }
    return bgColor;
  }

  render() {
    const {
      size,
      lineWidth,
      animate,
      animationDuration,
      roundedStroke,
      responsive,
      onAnimationEnd,
      bgColor,
      taskCount: total,
      options,
    } = this.props;
    const transition = animate
      ? `stroke-dashoffset ${animationDuration} ease-out`
      : undefined;
    const strokeLinecap = roundedStroke ? 'round' : 'butt';
    const svgSize = responsive ? '100%' : size;
    const allOptions = getOptions(options);
    const fillColor = this.getFillColor();
    const strokeOffset = Number(lineWidth) / 2;
    const viewSize = RADIUS * 2 + Number(lineWidth);
    const strokeDasharray = this.getOffset();
    const firstOption = [allOptions[0]];
    return (
      <svg
        width={svgSize}
        height={svgSize}
        viewBox={`-${strokeOffset} -${strokeOffset} ${viewSize} ${viewSize}`}
      >
        <circle
          fill={fillColor}
          cx={RADIUS}
          cy={RADIUS}
          r={RADIUS}
          strokeWidth={lineWidth}
        />
        <circle
          stroke={bgColor}
          transform={`rotate(${DEFAULT_START_ANGLE} ${RADIUS} ${RADIUS})`}
          cx={RADIUS}
          cy={RADIUS}
          r={RADIUS}
          strokeDasharray={strokeDasharray}
          strokeWidth={lineWidth}
          strokeDashoffset={0}
          strokeLinecap={strokeLinecap}
          fill="none"
          style={{ strokeDasharray: 0, transition }}
          onTransitionEnd={onAnimationEnd}
        />
        {firstOption.map((option, index) => {
          const { angle, strokeDashoffset } = this.getVectorDetails(
            option,
            index,
            options,
            total
          );
          const color = this.getPercentColor(options, index, total);

          return (
            <circle
              key={index}
              stroke={color}
              transform={`rotate(${angle} ${RADIUS} ${RADIUS})`}
              cx={RADIUS}
              cy={RADIUS}
              r={RADIUS}
              strokeDasharray={strokeDasharray}
              strokeWidth={lineWidth}
              strokeDashoffset={strokeDasharray}
              strokeLinecap={strokeLinecap}
              fill="none"
              style={{ strokeDashoffset, transition }}
              onTransitionEnd={onAnimationEnd}
            >
              {option?.value && <title>{option.value}</title>}
            </circle>
          );
        })}
        {this.text}
      </svg>
    );
  }
}

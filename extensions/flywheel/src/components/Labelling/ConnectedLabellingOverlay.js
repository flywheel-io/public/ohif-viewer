import { connect } from 'react-redux';
import LabellingOverlay from './LabellingOverlay';

import Redux from '../../redux';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import filterLabelsForCurrentAnswer from '../StudyForm/utils/filterLabelsForCurrentAnswer';

const {
  selectAvailableLabels,
  selectedLabels,
  selectedQuestion,
} = FlywheelCommonRedux.selectors;
const { selectProjectConfig } = Redux.selectors;

const mapStateToProps = state => {
  if (!state.ui || !state.ui.labelling) {
    return {
      visible: false,
    };
  }

  const labellingFlowData = state.ui.labelling;

  const availableLabels = filterLabelsForCurrentAnswer(state);
  return {
    availableLabels,
    selectedLabels: selectedLabels(state),
    projectConfig: selectProjectConfig(state),
    selectedQuestion: selectedQuestion(state),
    visible: false,
    ...labellingFlowData,
  };
};

const ConnectedLabellingOverlay = connect(
  mapStateToProps,
  null
)(LabellingOverlay);

export default ConnectedLabellingOverlay;

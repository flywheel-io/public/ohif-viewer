import React, { Component } from 'react';
import cloneDeep from 'lodash.clonedeep';
import PropTypes from 'prop-types';

import { SimpleDialog } from '@ohif/ui';
import store from '@ohif/viewer/src/store';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import { getDialogStyle } from '@ohif/viewer/src/components/Labelling/labellingPositionUtils.js';
import OHIFLabellingData from '@ohif/viewer/src/components/Labelling/OHIFLabellingData';
import { Select } from '@ohif/ui';
import { getMeasurementLabels } from '../StudyForm/validation/ValidationEngine.js';

import './EditLabelDescriptionDialog.css';

const { selectedQuestion } = FlywheelCommonRedux.selectors;
const {
  hasLabelsLimitReached,
  getActiveSliceInfo,
  getBSliceSettings,
  isCurrentFile,
} = FlywheelCommonUtils;
const { setAvailableLabels, setActiveQuestion } = FlywheelCommonRedux.actions;

const DEFAULT_LABEL = '--- Select Label ---';
const DEFAULT_COLOR = 'rgba(255, 255, 0, 0.2)';
export default class EditLabelDescriptionDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    measurementData: PropTypes.object.isRequired,
    onCancel: PropTypes.func.isRequired,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
    onUpdate: PropTypes.func.isRequired,
    updateLabelling: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    const label = props.measurementData.location || '';
    this.state = {
      description: props.measurementData.description || '',
      color: props.measurementData.color || DEFAULT_COLOR,
      label: this.getLabel(label),
      currentItems: this.loadLabels(props),
    };

    this.bSliceSettings = getBSliceSettings();
    this.mainElement = React.createRef();
  }

  componentDidMount = () => {
    bounding(this.mainElement);
  };

  componentDidUpdate(prevProps) {
    const label = this.props?.measurementData?.location || '';
    const previousLabel = prevProps?.measurementData?.location || '';
    const description =
      this.props?.measurementData?.description || this.props?.description || '';
    if (label !== previousLabel) {
      this.setState({
        description: description,
        currentItems: this.loadLabels(),
        label: this.getLabel(label),
      });
    }
  }

  getLabel(label = this.state.label, hasValue = false) {
    const labels = this.loadLabels();
    const labelObj = labels.find(x => x.item === label || x.label === label);
    if (labelObj) {
      if (hasValue) {
        return labelObj.item;
      }
      return labelObj.label;
    }
    return '';
  }

  loadLabels(props = this.props) {
    let initialItems = OHIFLabellingData;
    const projectConfig = props.projectConfig;
    if (projectConfig?.labels) {
      initialItems = projectConfig.labels;
    }
    const { measurementData } = props;
    const location = measurementData.location;
    if (projectConfig?.studyForm) {
      const state = store.getState();
      const activeViewportIndex = state.viewports.activeViewportIndex;
      const currentSelectedQuestion = this.getQuestion();
      const currentSlice = getActiveSliceInfo(activeViewportIndex);
      const sliceNumber = currentSlice?.sliceNumber;

      let requireMeasurements = [];
      const { answer = '', values = [] } = currentSelectedQuestion || {};
      let selectedOptionKey = answer?.value || answer;
      let selectedOption = values.find(x => x.value === selectedOptionKey);
      if (selectedOption?.instructionSet?.length) {
        this.addInstructionSetLabels(selectedOption, requireMeasurements);
      } else {
        requireMeasurements = selectedOption?.requireMeasurements || [];
      }

      if (
        currentSelectedQuestion?.questionKey &&
        measurementData?.questionKey !== currentSelectedQuestion?.questionKey
      ) {
        const {
          subFormName = '',
          questionKey = '',
          isSubForm = false,
        } = measurementData;
        selectedOptionKey =
          measurementData?.answer?.value || measurementData?.answer;
        let questions = projectConfig.studyForm?.components || [];
        if (isSubForm) {
          questions =
            projectConfig.studyForm?.subForms?.[subFormName]?.components || [];
        }
        const selectedFormQuestion = questions.find(x => x.key === questionKey);
        const selectedFormOption = (selectedFormQuestion?.values || []).find(
          x => x.value === selectedOptionKey
        );

        if (selectedFormOption?.instructionSet?.length) {
          this.addInstructionSetLabels(selectedFormOption, requireMeasurements);
        } else if (selectedFormOption?.requireMeasurements) {
          requireMeasurements = [
            ...requireMeasurements,
            ...selectedFormOption?.requireMeasurements,
          ];
        }
      }

      const filterLabels = ({ value }) => {
        const hasLabel =
          requireMeasurements.includes(value) &&
          !hasLabelsLimitReached(
            [value],
            currentSelectedQuestion,
            selectedOption,
            sliceNumber,
            this.bSliceSettings,
            ''
          );
        return (
          props.availableLabels.includes(value) ||
          value === location ||
          hasLabel
        );
      };
      initialItems = initialItems.filter(filterLabels);
    }

    if (
      location &&
      !initialItems.find(x => x.label === location || x.value === location)
    ) {
      initialItems.push({
        key: location,
        item: location,
        label: location,
        value: location,
      });
    }
    initialItems = initialItems.reduce((accumulator, labelObj) => {
      accumulator.push({
        ...labelObj,
        key: labelObj.label,
        item: labelObj.value,
        value: labelObj.label,
      });
      return accumulator;
    }, []);
    initialItems = cloneDeep(initialItems);
    initialItems.unshift({
      key: DEFAULT_LABEL,
      label: DEFAULT_LABEL,
      value: DEFAULT_LABEL,
    });
    return initialItems;
  }

  getLabelCount(location) {
    const state = store.getState();
    const measurements = state.timepointManager.measurements;
    const measureList = [];
    Object.keys(measurements).map(toolType => {
      measurements[toolType].map(x => {
        if (x.location === location) {
          measureList.push(x);
        }
      });
    });
    return measureList;
  }

  addInstructionSetLabels(selectedOption, requireMeasurements) {
    selectedOption.instructionSet.forEach(x => {
      if (x.requireMeasurements?.length) {
        requireMeasurements.push(...x.requireMeasurements);
      }
    });
  }

  getQuestion() {
    const state = store.getState();
    if (state.infusions.currentSelectedQuestion) {
      return state.infusions.currentSelectedQuestion;
    }

    const projectConfig = state.flywheel.projectConfig;
    if (!projectConfig?.studyForm) {
      return null;
    }

    const {
      questionKey = '',
      question = '',
      answer = '',
      isSubForm = false,
      subFormName,
    } = this.props.measurementData;
    let questions = projectConfig.studyForm.components;
    if (isSubForm && projectConfig?.studyForm?.subForms) {
      questions =
        projectConfig.studyForm.subForms?.[subFormName]?.components || [];
    }
    const selectedFormQuestion = questions.find(x => x.key === questionKey);
    if (selectedFormQuestion) {
      return {
        ...selectedFormQuestion,
        questionKey,
        answer,
        question,
        isSubForm,
        subFormName,
      };
    }

    return null;
  }

  render() {
    const style = getDialogStyle(this.props.componentStyle);
    const value = this.state.label || '';
    const label = this.getLabel(value);
    return (
      <SimpleDialog
        headerTitle="Edit Label"
        onClose={this.props.onCancel}
        onConfirm={this.onConfirm}
        rootClass="editLabelDescriptionDialog inner-header inner-content"
        componentRef={this.mainElement}
        componentStyle={style}
        showFooterButtons={false}
      >
        <div>
          <div className="inner-container">
            <div className="fs-12 mb-5">Label</div>
            <div>
              <Select
                style={{
                  width: '100%',
                  margin: '0px 0px 15px 0px',
                  backgroundColor: '#1c232e',
                  boxShadow: 'none',
                }}
                value={label}
                onChange={this.onChange}
                options={this.state.currentItems}
              />
            </div>
            <div className="fs-12">Description</div>
            <div>
              <textarea
                value={this.state.description}
                className="text-area-style"
                id="description"
                autoComplete="off"
                autoFocus
                onChange={this.handleChange}
              />
            </div>
          </div>

          <div className="footer-container">
            <div className="cancel-button" onClick={this.props.onCancel}>
              Cancel
            </div>
            <div className="save-button" onClick={this.onConfirm}>
              Save Changes
            </div>
          </div>
        </div>
      </SimpleDialog>
    );
  }

  onConfirm = e => {
    e.preventDefault();
    this.props.onUpdate(this.state.description);
    const label = this.getLabel(this.state.label, true);
    const labelDetails = this.loadLabels()?.find(
      x => x.item === label || x.label === label
    );
    const color = labelDetails?.color || this.state.color;
    const question = selectedQuestion(store.getState());
    let questionKey,
      answer = '';
    if (question?.questionKey && question?.answer) {
      questionKey =
        this.props.measurementData?.questionKey || question.questionKey;
      answer = this.props.measurementData?.answer || question.answer;
    }

    if (question?.isSubForm) {
      this.props.updateLabelling({
        location: label,
        description: this.state.description,
        color,
        questionKey: questionKey,
        answer: answer,
        isSubForm: question.isSubForm,
        subFormName: question.subFormName,
        subFormAnnotationId: question.subFormAnnotationId,
        question: question.question,
      });
    } else {
      this.props.updateLabelling({
        location: label,
        description: this.state.description,
        color,
        questionKey: questionKey,
        answer: answer,
      });
    }

    const state = store.getState();
    const allMeasurements = state.timepointManager.measurements;
    const projectConfig = state.flywheel.projectConfig;
    const sliceNo = this.props.measurementData?.sliceNumber;
    const isSubForm = this.props.measurementData?.isSubForm;
    const isFile = isCurrentFile(state);
    const allMeasurementLabels = getMeasurementLabels(
      allMeasurements,
      projectConfig,
      sliceNo
    );
    const subFormName = this.props.measurementData?.subFormName;
    const subFormAnnotationId = this.props.measurementData?.subFormAnnotationId;
    const components = !isSubForm
      ? !isFile && projectConfig?.bSlices
        ? allMeasurementLabels.slices?.[sliceNo]?.component
        : allMeasurementLabels.component
      : !isFile && projectConfig?.bSlices
      ? allMeasurementLabels.slices?.[sliceNo]?.[question]?.[subFormName]?.[
          subFormAnnotationId
        ]
      : allMeasurementLabels?.[question]?.[subFormName]?.[subFormAnnotationId];
    const labelLength = components.filter(data => data === label).length;
    if (labelLength >= labelDetails?.limit && labelDetails?.limit) {
      store.dispatch(setAvailableLabels([''], state.infusions.availableTools));
      store.dispatch(setActiveQuestion(null));
    }
  };

  handleChange = event => {
    this.setState({ description: event.target.value });
  };

  onChange = event => {
    this.setState({ label: event.target.value });
  };
}

import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Icon } from '@ohif/ui';
import PropTypes from 'prop-types';
import ProgressLoader from './ProgressLoader';
import MultipleErrorResponse from './MultipleErrorResponse';
import SingleErrorResponse from './SingleErrorResponse';
import ExternalLinks from './ExternalLinks';
import ReaderTaskResponse from './ReaderTaskResponse';
import WarningIcon from './WarningIcon';
import { getErrorTypeDisplay } from '../../utils';
import './ReIndexing.css';

function Title({ title, subtext }) {
  return (
    <div className="title-wrapper">
      <h2>{title}</h2>
      <p>{subtext}</p>
    </div>
  );
}

function ContinueButton({ onClick }) {
  return (
    <>
      <div className="button-wrapper">
        <button className="button-dark" onClick={onClick}>
          Continue to Viewer &nbsp;
          <Icon name="right-arrow" />
        </button>
      </div>
    </>
  );
}

export default function ReIndexingSession({
  files,
  error,
  hasResponseFinished,
  isReaderTask,
  errorLinks,
}) {
  const history = useHistory();

  const isIndexed =
    files?.length > 0 ? files?.every(file => file.indexed) : false;
  const isAnyFileIndexed = files?.some(file => file.indexed);
  const relaunchTask = isReaderTask && isAnyFileIndexed;

  const goBackInHistory = () => {
    history.goBack();
  };

  useEffect(() => {
    if (isIndexed || relaunchTask) {
      goBackInHistory();
    }
  }, [files, isIndexed, relaunchTask]);

  const pendingNoFiles = !isAnyFileIndexed && !hasResponseFinished;
  const pendingWithFiles = isAnyFileIndexed && !hasResponseFinished;

  if (isReaderTask) {
    return (
      <ReaderTaskResponse
        files={files}
        error={error}
        hasResponseFinished={hasResponseFinished}
        isAnyFileIndexed={isAnyFileIndexed}
      />
    );
  } else if (!error) {
    return (
      <>
        <div className="reindexing-wrapper">
          {pendingNoFiles ? <ProgressLoader /> : <WarningIcon />}
          <Title
            title={
              pendingNoFiles
                ? 'Attempting to index or re-index file...'
                : 'Problem indexing file.'
            }
            subtext={
              pendingNoFiles
                ? 'In progress...'
                : 'Indexing completed but encountered an error.  Please see below for more information.'
            }
          />
          {files && (
            <div className="error-messages-container">
              <MultipleErrorResponse
                responses={files}
                hasResponseFinished={hasResponseFinished}
              />
            </div>
          )}
          {errorLinks?.length > 0 && <ExternalLinks data={errorLinks} />}
        </div>
        {hasResponseFinished && isAnyFileIndexed && (
          <ContinueButton onClick={goBackInHistory} />
        )}
      </>
    );
  } else {
    return (
      <>
        <div className="reindexing-wrapper">
          <div>
            <WarningIcon />
            <Title
              title={getErrorTypeDisplay(error.type)}
              subtext={error.message}
            />
            {error.detail && <SingleErrorResponse response={error.detail} />}
            {error.links && <ExternalLinks data={error.links} />}
          </div>
        </div>
        {hasResponseFinished && isAnyFileIndexed && (
          <ContinueButton onClick={goBackInHistory} />
        )}
      </>
    );
  }
}

const IndexingErrorPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  detail: PropTypes.string,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string,
      message: PropTypes.string,
      title: PropTypes.string,
    })
  ),
});

ReIndexingSession.propTypes = {
  files: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      indexed: PropTypes.bool.isRequired,
      error: IndexingErrorPropType,
    })
  ),
  error: IndexingErrorPropType,
  hasResponseFinished: PropTypes.bool,
};

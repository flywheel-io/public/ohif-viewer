import React, { useEffect, useRef } from 'react';
import { getErrorTypeDisplay } from '../../utils';
import './ReIndexing.css';

export default function MultipleErrorResponse({
  responses,
  hasResponseFinished,
}) {
  const containerRef = useRef(null);
  useEffect(() => {
    const container = containerRef.current;
    if (container) {
      container.style.height = `${Math.min(container.scrollHeight, 250)}px`;
    }
  }, [responses]);

  return (
    <div className="response-box-session">
      <div className="error-messages">
        {!hasResponseFinished && (
          <span className="blue">
            indexing files = <span className="offWhite ">in progress...</span>
          </span>
        )}
        {responses?.map((response, index) =>
          response.error ? (
            <div key={index} className="error-message" ref={containerRef}>
              <span className="fileId blue">
                {response?.error?.id || response.id}{' '}
              </span>
              {response?.error && <span className="red">Error</span>}

              <br />
              <br />
              <span className="red">
                {getErrorTypeDisplay(response?.error?.type)}
              </span>
              <br />
              <span className="offWhite message">
                {response?.error?.message}
              </span>
              <span className="offWhite message">
                {response?.error?.detail}
              </span>
            </div>
          ) : (
            <div key={index} className="error-message">
              <span className="fileId blue">{response.id}</span>
              {response?.indexed ? (
                <span className="green">Indexed Succesfully</span>
              ) : (
                <span className="offWhite">Indexing...</span>
              )}
            </div>
          )
        )}
        {hasResponseFinished && (
          <span className="blue">
            indexing files = <span className="green">complete</span>
          </span>
        )}
      </div>
    </div>
  );
}

import React from 'react';
import './ReIndexing.css';

export default function ProgressLoader() {
  return (
    <div className="progressWrapper">
      <div className="loader-2">
        <div className="spin"></div>
      </div>
    </div>
  );
}

import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import ProgressLoader from './ProgressLoader';
import ExternalLinks from './ExternalLinks';
import './ReIndexing.css';
import SingleErrorResponse from './SingleErrorResponse';
import { getErrorTypeDisplay } from '../../utils';

export default function ReIndexingFile({
  files,
  error,
  containerId,
  hasResponseFinished,
  isReaderTask = true,
}) {
  const history = useHistory();

  const isIndexed = files && files[0]?.indexed;

  const goBackInHistory = () => {
    history.goBack();
  };

  useEffect(() => {
    if (isIndexed && !error) {
      goBackInHistory();
    }
  }, [isIndexed]);

  const file = files?.length > 0 ? files[0] : null;

  return (
    <div className="reindexing-wrapper">
      {!error ? (
        <div>
          {!hasResponseFinished && <ProgressLoader />}
          <div className="title-wrapper">
            <h2>
              {file && file?.error
                ? getErrorTypeDisplay(file.error.type)
                : 'Attempting to index or re-index file...'}
            </h2>
            <p>{file?.error ? file?.error?.message : 'In progress...'}</p>
            {file?.error?.detail && <p>{file.error.detail}</p>}
          </div>

          {!file?.error && (
            <SingleErrorResponse response={`${containerId} Indexing...`} />
          )}
          {isReaderTask && hasResponseFinished && (
            <SingleErrorResponse response={`Error ID ${file?.error?.id}`} />
          )}
          {file?.error?.links && !isReaderTask && (
            <ExternalLinks data={file.error.links} />
          )}
        </div>
      ) : (
        <div>
          <div className="title-wrapper">
            <h2>{getErrorTypeDisplay(error?.type)}</h2>
            <p>{error?.message}</p>
          </div>
          {error?.detail && <SingleErrorResponse response={error?.detail} />}
          {error?.links && <ExternalLinks data={error?.links} />}
        </div>
      )}
    </div>
  );
}

const IndexingErrorPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  message: PropTypes.string,
  detail: PropTypes.string || PropTypes.object,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string.isRequired,
      message: PropTypes.string,
      title: PropTypes.string,
    })
  ),
});

ReIndexingFile.propTypes = {
  files: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      indexed: PropTypes.bool.isRequired,
      error: IndexingErrorPropType,
    })
  ),
  error: IndexingErrorPropType,
};

import React, { useState, useEffect, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import {
  Utils as FlywheelCommonUtils,
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';
import ReIndexingFile from './ReIndexingFile';
import ReIndexingSession from './ReIndexingSession';

const { replacePlaceholders } = FlywheelCommonUtils;
const { indexContainer } = FlywheelCommonHTTP.services;

export default function ReIndexingWrapper() {
  const [apiResponse, setApiResponse] = useState({});
  const [errorLinks, setErrorLinks] = useState([]);
  const { state } = useLocation();
  const { containerId = null, isSession = true, isReaderTask = false } =
    state || {};

  const updateErrorMessagesAndDetails = data => {
    const replaceErrorFields = (error, links) => {
      if (error?.message) {
        const result = replacePlaceholders(error.message, { links });
        error.message = result.content;
        error.links = result.updatedLinks;
      }
      if (error?.detail) {
        const result = replacePlaceholders(error.detail, { links });
        error.detail = result.content;
        error.links = result.updatedLinks;
      }
    };

    if (data?.error) {
      replaceErrorFields(data.error, data.error.links);
    }

    if (data?.files) {
      data.files.forEach(file => {
        if (file?.error) {
          replaceErrorFields(file.error, file.error.links);
        }
      });
    }
  };

  useEffect(() => {
    if (containerId) {
      const containerType = isSession ? 'session' : 'file';
      indexContainer(containerId, setApiResponse, isReaderTask, containerType);
    }
  }, [containerId]);

  const processedResponse = useMemo(() => {
    const updatedResponse = { ...apiResponse };
    updateErrorMessagesAndDetails(updatedResponse);
    return updatedResponse;
  }, [apiResponse]);

  useEffect(() => {
    if (!isSession) {
      return;
    }
    const collectedLinks = [];
    processedResponse?.files?.forEach(file => {
      if (file.error && file.error.links) {
        file.error.links.forEach(link => {
          const linkExists = collectedLinks.some(
            existingLink => existingLink.url === link.url
          );
          if (!linkExists) {
            collectedLinks.push(link);
          }
        });
      }
    });
    setErrorLinks(collectedLinks);
  }, [processedResponse]);

  return isSession ? (
    <ReIndexingSession
      files={processedResponse.files}
      error={processedResponse.error}
      hasResponseFinished={processedResponse.done}
      isReaderTask={isReaderTask}
      errorLinks={errorLinks}
    />
  ) : (
    <ReIndexingFile
      files={processedResponse.files}
      error={processedResponse.error}
      containerId={containerId}
      hasResponseFinished={processedResponse.done}
      isReaderTask={isReaderTask}
    />
  );
}

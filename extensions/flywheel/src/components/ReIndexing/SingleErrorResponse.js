import React, { useEffect, useRef } from 'react';
import './ReIndexing.css';

export default function SingleErrorResponse({ response }) {
  const containerRef = useRef(null);
  useEffect(() => {
    if (containerRef.current) {
      containerRef.current.style.height = 'auto';
      containerRef.current.style.height = `${Math.min(
        containerRef.current.scrollHeight,
        250
      )}px`;
    }
  }, [response]);
  return (
    <div className="response-box-session singleError" ref={containerRef}>
      <div className="error-messages">
        <div className="error-message">{response}</div>
      </div>
    </div>
  );
}

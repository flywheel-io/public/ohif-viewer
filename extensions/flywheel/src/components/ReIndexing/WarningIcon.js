import React from 'react';

const WarningIcon = () => (
  <svg
    width="32"
    height="33"
    viewBox="0 0 32 33"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M16 5.5C9.92428 5.5 5 10.4243 5 16.5C5 22.5757 9.92428 27.5 16 27.5C22.0757 27.5 27 22.5757 27 16.5C27 10.4243 22.0757 5.5 16 5.5ZM16 29.5C8.81972 29.5 3 23.6803 3 16.5C3 9.31972 8.81972 3.5 16 3.5C23.1803 3.5 29 9.31972 29 16.5C29 23.6803 23.1803 29.5 16 29.5Z"
      fill="#F15757"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M16 9.5C16.5523 9.5 17 9.94772 17 10.5V17.1667C17 17.719 16.5523 18.1667 16 18.1667C15.4477 18.1667 15 17.719 15 17.1667V10.5C15 9.94772 15.4477 9.5 16 9.5Z"
      fill="#F15757"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M14.6667 22.1703C14.6633 21.4286 15.265 20.8333 15.9987 20.8333C16.7337 20.8333 17.3333 21.4278 17.3333 22.1667C17.3333 22.903 16.7363 23.5 16 23.5C15.2649 23.5 14.6686 22.9049 14.6667 22.1703Z"
      fill="#F15757"
    />
  </svg>
);

export default WarningIcon;

import React from 'react';
import { Icon } from '@ohif/ui';
import ProgressLoader from './ProgressLoader';
import SingleErrorResponse from './SingleErrorResponse';
import ExternalLinks from './ExternalLinks';
import { getErrorTypeDisplay } from '../../utils';

const ReaderTaskResponse = ({
  files,
  error,
  hasResponseFinished,
  isAnyFileIndexed,
}) => {
  return (
    <div className="reindexing-wrapper">
      {!error ? (
        <>
          {!isAnyFileIndexed && !hasResponseFinished && <ProgressLoader />}

          <div className="title-wrapper">
            <h2>
              {!isAnyFileIndexed && !hasResponseFinished
                ? 'Attempting to index or re-index file...'
                : getErrorTypeDisplay(files && files[0]?.error?.type) ||
                  (files && files[0]?.error?.type)}
            </h2>
            <p>
              {!isAnyFileIndexed && !hasResponseFinished
                ? 'In progress...'
                : 'DICOM image could not be indexed. Please contact your site administrator.'}
            </p>
          </div>
          {files && files[0]?.error?.id && (
            <div className="error-messages-container">
              {hasResponseFinished && (
                <SingleErrorResponse
                  response={`Error ID ${files[0]?.error?.id}`}
                />
              )}
            </div>
          )}
        </>
      ) : (
        <>
          <div>
            <div className="title-wrapper">
              <h2>{error.type}</h2>
              <p>{error.message}</p>
            </div>
            {error.detail && <SingleErrorResponse response={error.detail} />}
            {error.links && <ExternalLinks data={error.links} />}
          </div>
        </>
      )}
    </div>
  );
};

export default ReaderTaskResponse;

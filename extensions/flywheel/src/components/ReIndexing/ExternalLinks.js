import React from 'react';
import './ReIndexing.css';

import { Icon } from '@ohif/ui';

export default function ExternalLinks({ data }) {
  if (!data) {
    return null;
  }
  return (
    <div className="">
      {data.map((link, index) => (
        <a
          href={link.url}
          target="_blank"
          rel="noopener noreferrer"
          key={index}
        >
          <div className="external-links">
            <div className="icon">
              <Icon
                name="open-new-tab"
                style={{ color: 'rgba(118, 164, 252, 1) ' }}
              />
            </div>
            <div className="text">
              <h3 className="white">{link.title}</h3>
              <p className="offWhite">{link.message}</p>
            </div>
          </div>
        </a>
      ))}
    </div>
  );
}

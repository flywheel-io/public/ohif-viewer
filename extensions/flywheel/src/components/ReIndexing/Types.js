const ErrorTypes = {
  AUTHORIZATION_ERROR: 'Authorization Error',
  DICOM_PARSE_ERROR: 'DICOM Read Error',
  IN_PROGRESS: 'In Progress',
  INTERNAL_SERVER_ERROR: 'Internal Server Error',
  UNSUPPORTED_CONTAINER_TYPE: 'Unsupported Container Type',
  UNSUPPORTED_MODALITY: 'Unsupported Modality',
  UID_CONFLICT: 'UID Conflict',
  READER_TASK_INDEX_ERROR: 'DICOM image could not be indexed',
  NO_DICOM_FILES: 'No DICOM files found',
};

export { ErrorTypes };

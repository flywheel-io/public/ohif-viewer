import cornerstoneEvents from './cornerstoneEvents.js';
import { Utils as FlywheelCommonUtils, HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';

const {
  getFileData,
} = FlywheelCommonHTTP.services;

const {
  getParamsFromContainerFileUrl,
} = FlywheelCommonUtils;

export default class FileStreamer {
  constructor(options) {
    this.options = {
      headers: options && options.headers || {}
    };
  }

  stream(imageIdObject, handleChunk) {
    const eventParams = {
      url: imageIdObject.filePath,
      imageId: imageIdObject.url
    };
    const { fileContainerId, fileName } = getParamsFromContainerFileUrl(imageIdObject.filePath);
    const fileStreamPromise = getFileData(fileContainerId, fileName)
      .then(response => {
        if (response.ok) {
          const reader = response.body.getReader();

          const contentLength = response.headers.get('Content-Length') || response.headers.get('content-length') || 0;
          const progressCallback = progress(imageIdObject.filePath, imageIdObject.url, this.options, eventParams);
          let bytesRead = 0;

          reader.read().then(function processData({ done, value }) {
            if (done) {
              return;
            }
            bytesRead += value.length;

            progressCallback({
              detail: {
                loaded: bytesRead,
                total: contentLength
              }
            });
            handleChunk(value, imageIdObject, contentLength);

            return reader.read().then(processData);
          }).catch(error => {
            handleChunk(null, imageIdObject, contentLength, error);
            const errorDescription = `Could not fetch the file '${imageIdObject.filePath}'.`;

            throw new Error(errorDescription);
          });
        } else {
          throw new Error(response.statusText);
        }
      }).catch(error => {
        handleChunk(null, imageIdObject);
        const errorDescription = `Could not fetch the file '${imageIdObject.filePath}'.`;

        throw new Error(errorDescription);
      });

    return fileStreamPromise;
  }
}

// Builds a function that is going to be called when there is progress on the
// request response
function progress(url, imageId, options, params) {
  return function (e) {
    const loaded = e.detail.loaded;
    const total = e.detail.total;
    const percentComplete = Math.round((loaded / total) * 100);

    // Event
    const eventData = {
      url,
      imageId,
      loaded,
      total,
      percentComplete
    };

    cornerstoneEvents.imageLoadProgress(eventData);
  };
}

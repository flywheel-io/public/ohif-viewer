import {
  SET_EDIT_SUBFORM_ROI_LAYOUT,
  SET_SUBFORM_POPUP_VISIBILITY,
  SET_LABEL_FOR_QUESTION,
  SET_SUBFORM_QUESTION_ANSWERED,
  SET_STUDYFORM_ANSWER_IN_MIXED_MODE,
  SET_SUBFORM_POPUP_CLOSE,
} from '../constants/ActionTypes';

export function subFormRoiState(data) {
  return {
    type: SET_EDIT_SUBFORM_ROI_LAYOUT,
    data,
  };
}

export function subFormPopupVisible(data) {
  return {
    type: SET_SUBFORM_POPUP_VISIBILITY,
    data,
  };
}

export function setLabelForQuestion(data) {
  return {
    type: SET_LABEL_FOR_QUESTION,
    data,
  };
}

export function subFormQuestionAnswered(data) {
  return {
    type: SET_SUBFORM_QUESTION_ANSWERED,
    data,
  };
}

export function studyFormAnswerInMixedMode(data) {
  return {
    type: SET_STUDYFORM_ANSWER_IN_MIXED_MODE,
    data,
  };
}

export function setSubFormPopupClose(data) {
  return {
    type: SET_SUBFORM_POPUP_CLOSE,
    data,
  };
}

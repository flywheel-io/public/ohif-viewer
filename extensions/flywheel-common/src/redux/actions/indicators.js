import {
  SET_SCALE_INDICATOR_STATUS,
  SET_FUNDUS_RING,
  SET_IS_RESTORE_NEEDED,
} from '../constants/ActionTypes';

export function setScaleIndicatorStatus(isDisplayed) {
  return {
    type: SET_SCALE_INDICATOR_STATUS,
    isDisplayed,
  };
}
export function setFundusRingDisplay(ringOverlay) {
  return {
    type: SET_FUNDUS_RING,
    ringOverlay,
  };
}
export function setIsRingOverlayRestoreNeeded(isRestoreNeeded) {
  return {
    type: SET_IS_RESTORE_NEEDED,
    isRestoreNeeded,
  };
}

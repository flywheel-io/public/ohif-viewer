import * as labels from './labels';
import * as session from './session';
import * as tools from './tools';
import * as viewports from './viewports';
import * as loader from './loader';
import * as ranges from './ranges';
import * as fovGridRestoreInfo from './fovGridRestoreInfo';

const selectors = {
  ...labels,
  ...session,
  ...tools,
  ...viewports,
  ...loader,
  ...ranges,
  ...fovGridRestoreInfo,
};

export default selectors;

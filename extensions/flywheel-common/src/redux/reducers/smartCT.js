import _ from 'lodash';
import { addOrUpdateSmartCTRange } from '../../utils/addOrUpdateSmartCTRange';

const defaultState = {
  selectedRange: null,
  smartCTRanges: null,
};

export default function smartCT(state = defaultState, action) {
  switch (action.type) {
    case 'SET_SELECTED_RANGE': {
      return Object.assign({}, state, { selectedRange: action.smartCTRange });
    }
    case 'SET_AVAILABLE_RANGES': {
      return { ...state, smartCTRanges: action.smartCTRanges };
    }
    case 'ADD_AVAILABLE_RANGE': {
      const smartRange = action.smartCTRange;
      addOrUpdateSmartCTRange(state.smartCTRanges, smartRange);
      return { ...state };
    }
    default:
      return state;
  }
}

import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';
import VolumetricImageSegmentation from './VolumetricImageSegmentation';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import {
  getSmartCTRangeList,
  isCurrentWebImage,
  isCurrentMetaImage,
  isCurrentNifti,
  isSliceOrderSame,
  cornerstoneUtils,
} from '../../utils';
import * as segmentationUtils from '../../utils/labelMapSegmentationUtils';
import { setSegmentToolActive } from '@flywheel/extension-cornerstone-infusions/src/utils/segmentationUtils';
import { utils } from '@ohif/core';

const { setters } = cornerstoneTools.getModule('segmentation');
const { setSegmentationData } = FlywheelCommonRedux.actions;
const { setSmartCTRange } = FlywheelCommonRedux.actions;
const { studyMetadataManager } = utils;

const { rePositionSegment } = segmentationUtils;

// TODO segmentation docs and evaluate to move it from here
async function loadSegmentation(
  segDisplaySet,
  referencedDisplaySet,
  studies,
  segmentationLoaderFn,
  segmentationIndex = 1
) {
  const { StudyInstanceUID } = referencedDisplaySet;

  const imageIds = segmentationUtils.getImageIdsForDisplaySet(
    studies,
    StudyInstanceUID,
    referencedDisplaySet.SeriesInstanceUID
  );

  const referencedImageIds = segmentationUtils.getImageIdsForDisplaySet(
    studies,
    StudyInstanceUID,
    segDisplaySet.SeriesInstanceUID,
    'derivedDisplaySets'
  );

  const {
    imageOrientationPatient: refImageOrientation,
  } = cornerstone.metaData.get('imagePlaneModule', imageIds[0]);

  // Set here is loading is asynchronous.
  // If this function throws its set back to false.
  segDisplaySet.isLoaded = true;
  const dataSegments = await Promise.all(
    referencedImageIds.map(imageId =>
      segmentationLoaderFn(imageId, refImageOrientation)
    )
  ).catch(error => {
    segDisplaySet.isLoaded = false;
    segDisplaySet.loadError = true;
    segDisplaySet.segmentationIndexes = [segmentationIndex]; // For removal from store
    return Promise.reject({
      segInfo: {
        ...segDisplaySet,
        ...{
          referencedDisplaySetUID: referencedDisplaySet.StudyInstanceUID,
        },
      },
      message: 'Failed to load the segmentation file ' + segDisplaySet.imageId,
    });
  });
  if (!dataSegments || !dataSegments[0]) {
    segDisplaySet.isLoaded = false;
    segDisplaySet.loadError = true;
    segDisplaySet.segmentationIndexes = [segmentationIndex]; // For removal from store
    return Promise.reject({
      segInfo: {
        ...segDisplaySet,
        ...{
          referencedDisplaySetUID: referencedDisplaySet.StudyInstanceUID,
        },
      },
      message: 'Failed to load the segmentation for ' + segDisplaySet.imageId,
    });
  }

  const projectConfig = store.getState().flywheel?.projectConfig;

  // If the slice ordering in base dicom is different from that of nifti overlay,
  // update referenceSOPInstanceUID in overlay to point to correct base image.
  if (
    referencedDisplaySet.dataProtocol !== 'nifti' &&
    projectConfig?.useRadiologyOrientationForNiftiOverDicomOverlay &&
    dataSegments.length > 1 &&
    !isSliceOrderSame(imageIds, [
      dataSegments[0].imageId,
      dataSegments[1].imageId,
    ])
  ) {
    const isMultiFrame =
      referencedDisplaySet.isMultiFrame &&
      referencedDisplaySet.numImageFrames > 1;
    if (isMultiFrame) {
      dataSegments.reverse();
    } else {
      const refImages = studies
        .find(
          study =>
            study.StudyInstanceUID === referencedDisplaySet.StudyInstanceUID
        )
        .series.find(
          series =>
            series.SeriesInstanceUID === referencedDisplaySet.SeriesInstanceUID
        ).instances;
      const length = refImages.length;

      if (refImages.length !== dataSegments.length) {
        console.warn(
          'No of slices is different for base data and segmentation data.'
        );
      }

      dataSegments.forEach((segment, index) => {
        const imageId = segment.imageId;
        const instanceData = cornerstone.metaData.get('instance', imageId);
        instanceData.ReferencedSOPInstanceUID =
          refImages[length - 1 - index]?.metadata?.SOPInstanceUID;
      });
    }
  }

  const segmentation = new VolumetricImageSegmentation(
    imageIds,
    dataSegments,
    cornerstone.metaData,
    segmentationIndex,
    referencedDisplaySet.isMultiFrame && referencedDisplaySet.numImageFrames > 1
  );
  return new Promise((resolve, reject) => {
    let results;

    try {
      results = segmentation.generateToolState(
        dataSegments,
        cornerstone.metaData
      );
    } catch (error) {
      segDisplaySet.isLoaded = false;
      segDisplaySet.loadError = true;
      segDisplaySet.segmentationIndexes = [segmentationIndex]; // For removal from store
      reject({
        ...error,
        ...{
          segInfo: {
            ...segDisplaySet,
            ...{
              referencedDisplaySetUID: referencedDisplaySet.StudyInstanceUID,
            },
          },
        },
      });
    }
    setDisplaySetInstanceUID(segDisplaySet, segmentationIndex);
    const {
      labelmapBuffer,
      labelmapBufferArray,
      segMetadata,
      segmentsOnFrame,
    } = results;

    // TODO: Could define a color LUT based on colors in the SEG.
    const labelmapIndex = segmentationUtils.getNextLabelmapIndex(imageIds[0]);
    const colorLUTIndex = 0;

    // TODO segmentation check weather using labelmapBufferArray or labelmapBuffer
    let currentLabelmapIndex = 0;

    setters.labelmap3DByFirstImageId(
      imageIds[0],
      labelmapBufferArray[0] || labelmapBuffer,
      labelmapIndex,
      segMetadata,
      imageIds.length,
      segmentsOnFrame,
      colorLUTIndex
    );
    const { state } = cornerstoneTools.getModule('segmentation');
    const brushStackState = state.series[imageIds[0]];
    brushStackState.labelmaps3D[
      labelmapIndex
    ].activeSegmentIndex = segmentationIndex;
    segDisplaySet.labelmapIndexes = [labelmapIndex];
    segDisplaySet.segmentationIndexes = [segmentationIndex];
    if (labelmapIndex > 1) {
      // Push the new segment to front to manage the rendering order.
      rePositionSegment(segDisplaySet, referencedDisplaySet, studies, 1);
      segDisplaySet.labelmapIndexes = [1];
    }
    const storeState = store.getState();
    const config = storeState?.flywheel?.projectConfig;
    let loadMaskAsEditable = !!config?.loadMaskAsEditable;
    if (
      loadMaskAsEditable &&
      !isCurrentWebImage() &&
      !isCurrentMetaImage(storeState)
    ) {
      // In order to enable edit mode for the newly loaded segmentation mask
      currentLabelmapIndex = 1;
      setSegmentToolActive();
    }

    setters.activeLabelmapIndexOffline(imageIds, currentLabelmapIndex);
    setActiveLabelMapForNewlyAddedSegment(segDisplaySet);

    if (
      loadMaskAsEditable &&
      !isCurrentWebImage() &&
      !isCurrentMetaImage(storeState)
    ) {
      updateSegmentationData(false, loadMaskAsEditable, segDisplaySet);
      updateCTRange(segDisplaySet.segmentationIndexes[0]);
    }

    console.log('Segmentation loaded.');
    const event = new CustomEvent('extensiondicomsegmentationsegloaded');
    document.dispatchEvent(event);

    resolve(segDisplaySet.labelmapIndexes);
  });
}

function setActiveLabelMapForNewlyAddedSegment(segDisplaySet) {
  const state = store.getState();
  const segmentationData = getCurrentSegmentationDataIndex(segDisplaySet);
  const allRanges = getSmartCTRangeList();
  const selectedRange = state?.smartCT?.selectedRange;
  const segmentIndex = allRanges.findIndex(
    r => r.label === selectedRange?.label
  );
  if (segmentationData) {
    const element = cornerstoneUtils.getEnabledElement(
      state.viewports.activeViewportIndex
    );
    if (
      segmentationData.modality === 'SEG' &&
      segmentationData.containerId === '' &&
      segmentationData?.displaySetInstanceUIDs.includes(
        segDisplaySet.displaySetInstanceUID
      )
    ) {
      setters.activeLabelmapIndex(element, segDisplaySet.labelmapIndexes[0]);
      setters.activeSegmentIndex(element, segDisplaySet.segmentationIndexes[0]);
      segDisplaySet.segmentationIndexes = [segmentIndex + 1];
      updateSegmentationData(true, true, segDisplaySet);
      setSegmentToolActive();
    }
  }
}

function getCurrentSegmentationDataIndex(segDisplaySet) {
  const activeReferenceUID = getActiveReferenceUID();
  const segmentationData = store.getState().segmentation.segmentationData[
    activeReferenceUID
  ];
  const index = segmentationData.findIndex(data =>
    data.displaySetInstanceUIDs.includes(segDisplaySet.displaySetInstanceUID)
  );
  return segmentationData[index];
}

function updateSegmentationData(
  isUpdatingNewSegment,
  loadMaskAsEditable,
  segDisplaySet
) {
  const activeReferenceUID = getActiveReferenceUID();
  const segmentationData = store.getState().segmentation.segmentationData[
    activeReferenceUID
  ];
  const index = segmentationData.findIndex(data =>
    data.displaySetInstanceUIDs.includes(segDisplaySet.displaySetInstanceUID)
  );
  if (index > -1) {
    const newSegmentData = segmentationData[index];
    let editedSegmentData = { ...newSegmentData };
    editedSegmentData.isEditing = loadMaskAsEditable;
    if (isUpdatingNewSegment) {
      const fileName = cornerstoneUtils.getFileName(segDisplaySet.imageId);
      editedSegmentData.name = fileName;
      editedSegmentData.studyInstanceUID = fileName;
    }
    let newMaskArray = [...segmentationData];
    newMaskArray[index] = editedSegmentData;

    if (newSegmentData?.modality !== 'RTSTRUCT') {
      store.dispatch(
        setSegmentationData([...newMaskArray], activeReferenceUID)
      );
    }
  }
}

function updateCTRange(activeSegmentIndex) {
  const smartCTRangeList = getSmartCTRangeList();
  const selectedSegmentationColor = smartCTRangeList?.[activeSegmentIndex - 1];

  if (selectedSegmentationColor) {
    store.dispatch(setSmartCTRange(selectedSegmentationColor));
  }
}

function setDisplaySetInstanceUID(segDisplaySet, segmentationIndex) {
  const activeReferenceUID = getActiveReferenceUID();
  const segmentationData = store.getState().segmentation.segmentationData[
    activeReferenceUID
  ];
  const index = segmentationData.findIndex(
    data =>
      data.originalId === segmentationIndex &&
      data.modality === segDisplaySet.Modality
  );
  if (index > -1) {
    const newSegmentData = segmentationData[index];
    const editedSegmentData = { ...newSegmentData };
    editedSegmentData.displaySetInstanceUIDs =
      editedSegmentData?.displaySetInstanceUIDs || [];
    editedSegmentData.displaySetInstanceUIDs.push(
      segDisplaySet.displaySetInstanceUID
    );
    const newMaskArray = [...segmentationData];
    newMaskArray[index] = editedSegmentData;

    store.dispatch(setSegmentationData([...newMaskArray], activeReferenceUID));
  }
}

function getActiveReferenceUID() {
  const state = store.getState();
  let referenceUID = null;
  const currentVolumetricImage =
    state.flywheel?.currentNiftis[0] || state.flywheel?.currentMetaImage;
  const { viewportSpecificData, activeViewportIndex } = state.viewports;
  if (viewportSpecificData[activeViewportIndex]) {
    referenceUID = currentVolumetricImage
      ? viewportSpecificData[activeViewportIndex].StudyInstanceUID
      : viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
  }
  return referenceUID;
}

export { loadSegmentation };

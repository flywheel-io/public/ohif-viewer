import * as utils from './utils';
import configureClient from './configureClient';

const { COMMON_UTILS } = utils;
const clientFetchInstance = configureClient();

/**
 * Structure to encapsulate client usage.
 */
const ApiClient = {
  get: (urlOptions = {}, config = {}) => {
    return clientFetchInstance(urlOptions, COMMON_UTILS.METHODS.GET, config);
  },
  post: (urlOptions = {}, data = {}, config = {}) => {
    return clientFetchInstance(
      urlOptions,
      COMMON_UTILS.METHODS.POST,
      config,
      data
    );
  },
  put: (urlOptions = {}, data = {}, config = {}) => {
    return clientFetchInstance(
      urlOptions,
      COMMON_UTILS.METHODS.PUT,
      config,
      data
    );
  },

  delete: (urlOptions = {}, config = {}) => {
    return clientFetchInstance(urlOptions, COMMON_UTILS.METHODS.DELETE, config);
  },
};

export default ApiClient;
export { utils };

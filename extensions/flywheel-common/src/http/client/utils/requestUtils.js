import queryString from 'query-string';
import moment from 'moment';
import _ from 'lodash';
import { log } from '@ohif/core';
import { METHODS, replacePathParams, encodeParamKey } from './commonUtils';
import { getAuthToken } from './authUtils';
import { API_ROOT } from '../../urls';

/**
 * It parse QueryString to be api format.
 * It encode query param keys to be snack cased and format query param values
 * @param {object} queryParams Object containing query params
 * @return {object} query params formatted
 */
const parseQueryString = queryParams => {
  const parsedKeys = _.mapKeys(queryParams, (value, key) => {
    const keySnakeCase = encodeParamKey(key);
    return keySnakeCase;
  });

  const parsed = _.mapValues(parsedKeys, value => {
    if (value instanceof moment || value instanceof Date) {
      return value.toISOString();
    }
    return value;
  });

  return parsed;
};

const normalizeURL = url => {
  return url.replace(/^\//, '');
};

/**
 * Return full api url (combined, parsed, encoded) containing queryString params or/and pathParams (replaced)
 * @param {string} url string url
 * @param {object} urlParams Object with queryParams Object or/and pathParams
 * @param {object} config Object with some usefull configuration
 * @return {string}
 */
export const getRequestURL = (url, urlParams = {}, config = {}) => {
  let { queryParams, pathParams } = urlParams;
  let _url = url;

  try {
    // normalize might be required by default soon, but for now only colormap.json use it
    const { prefixRoot = true, normalize = false } = config;

    if (pathParams) {
      _url = replacePathParams(_url, pathParams);
    }

    if (prefixRoot) {
      const apiRoot = window.config.apiRoot || API_ROOT;
      _url = `${apiRoot}${_url}`;
    }

    if (normalize) {
      _url = normalizeURL(_url);
    }

    // inject some query params
    if (config.isAdmin) {
      queryParams = queryParams || {};
      queryParams.exhaustive = true;
    }
    if (queryParams) {
      const adaptedQueryParams = parseQueryString(queryParams);
      const qsStr = queryString.stringify(adaptedQueryParams);

      if (qsStr) {
        return `${_url}?${qsStr}`;
      }
    }

    return _url;
  } catch (e) {
    log.warn('Api fetch:: Error while getting request url');
    return;
  }
};

/** Headers methods */
export const getRequestHeaders = (
  config,
  methodType,
  useBody,
  isFormData = false
) => {
  const headers = new Headers();
  const { useAuth = true, headersMap = {}, queryAll = false } = config;

  // reserved header keys
  if (useAuth) {
    headers.append('Authorization', getAuthToken());
  }
  // by default in case there is a body content type must be application/json
  if (useBody && !isFormData) {
    headers.append('Content-type', 'application/json;charset=utf-8');
  }
  if (queryAll) {
    headers.append('X-Accept-Feature', 'pagination');
  }

  _.forOwn(headersMap, (value, key) => {
    headers.append(key, value);
  });

  return headers;
};

/** Body methods */
export const useBody = methodType => {
  return methodType === METHODS.POST || methodType === METHODS.PUT;
};

export const getRequestBody = (data, config, methodType) => {
  let bodyData = {};

  try {
    // by default data must be stringified and header must content content type as application/json
    bodyData = data instanceof FormData ? data : JSON.stringify(data);
  } catch (e) {
    log.warn('Api fetch:: Error while parsing data');
  } finally {
    return bodyData;
  }
};

export function customInfoKey(key = '') {
  return key.replace(/\./g, '_');
}

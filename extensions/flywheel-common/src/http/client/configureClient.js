import { log } from '@ohif/core';

import { REQUEST_UTILS, RESPONSE_UTILS } from './utils';
import { METHODS } from './utils/commonUtils';

const RESPONSE_TEXT = {
  '400': 'Bad Request',
  '401': 'Unauthorized',
  '403': 'Forbidden',
  '404': 'Not Found',
  '408': 'Request Timeout',
  '422': 'Unprocessable Entity',
  '500': 'Internal Server Error',
  '502': 'Bad Gateway',
  '503': 'Service Unavailable',
  '504': 'Gateway Timeout',
};

function request(fetch, ...args) {
  return fetch(...args).then(response => {
    if (response.ok) {
      return response;
    }
    // Adding fallback since responses over an HTTP/2 connection will always have
    // an empty string as status message as HTTP/2 does not support them
    const error = new Error(
      response.statusText || RESPONSE_TEXT[`${response.status}`] || ''
    );
    error.code = response.status;
    if (response.json) {
      error.messagePromise = response.json();
    }
    throw error;
  });
}

// a very primitive fetch (http client call) decorator to add response interceptors
function interceptor(fetch, interceptors = [], ...args) {
  let promise = request(fetch, ...args);

  // register response interceptors
  interceptors.forEach(({ responseSuccess, responseError }) => {
    if (responseSuccess) {
      promise = promise.then(responseSuccess, responseError);
    }
  });

  return promise;
}

export default function configureClient() {
  return (urlOptions, methodType, config, data) => {
    if (!methodType) {
      log.warn('Api fetch:: Missing method type');
      return;
    }
    const { url, urlParams = {} } = urlOptions;
    const requestURL = REQUEST_UTILS.getRequestURL(url, urlParams, config);
    const useBody = REQUEST_UTILS.useBody(methodType);
    const requestHeaders = REQUEST_UTILS.getRequestHeaders(
      config,
      methodType,
      useBody,
      data instanceof FormData
    );
    const responseInterceptors = RESPONSE_UTILS.getResponseInterceptors(config);

    const requestObject = {
      method: methodType,
      headers: requestHeaders,
    };

    if (useBody) {
      const requestBody = REQUEST_UTILS.getRequestBody(
        data,
        config,
        methodType
      );

      requestObject.body = requestBody;
    }

    // return a fetch promise enhanced by interceptors
    return interceptor(
      fetch,
      responseInterceptors,
      requestURL,
      requestObject
    ).then(response => {
      if (config.queryAll && methodType === METHODS.GET) {
        const paginated = Array.isArray(response)
          ? {
              results: response,
              count: response.length,
              total: response.length,
            }
          : response;
        const queryAllLimit = config.queryAllLimit || 5000;
        if (
          paginated.total > paginated.count &&
          paginated.total < queryAllLimit
        ) {
          const queryParams = {
            ...urlParams.queryParams,
            limit: paginated.total,
          };
          const requestURL = REQUEST_UTILS.getRequestURL(
            url,
            { ...urlParams, queryParams },
            config
          );
          return interceptor(
            fetch,
            responseInterceptors,
            requestURL,
            requestObject
          ).then(response => response.results);
        }
        return response.results;
      }
      return response;
    });
  };
}

import * as container from './container';
import * as files from './files';
import * as project from './project';
import * as session from './session';
import * as study from './study';
import * as user from './user';
import * as acquisition from './acquisition';
import * as subject from './subject';
import * as config from './config';
import * as readerTask from './readerTask';
import { indexContainer } from './indexContainer';

const services = {
  ...container,
  ...files,
  ...project,
  ...session,
  ...study,
  ...user,
  ...acquisition,
  ...subject,
  ...config,
  ...readerTask,
  indexContainer,
};

export default services;

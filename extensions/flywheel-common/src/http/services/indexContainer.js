import { INDEX_FILE } from '../urls';
import { getAuthToken } from '../client/utils/authUtils';

const baseURl = window.location.origin;

export async function indexContainer(
  containerId,
  setApiResponse,
  readerTask = false,
  type = 'file'
) {
  let accumulatedResults = null;
  const url = `${baseURl}${INDEX_FILE}`;

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'text/event-stream',
      'Content-Type': 'application/json',
      Authorization: getAuthToken(),
    },
    body: JSON.stringify({
      id: containerId,
      type,
      readerTask,
    }),
  });

  const reader = response.body.pipeThrough(new TextDecoderStream()).getReader();
  while (true) {
    const { value, done } = await reader.read();

    if (done) {
      accumulatedResults.done = true;
      setApiResponse(prev => ({ ...prev, ...accumulatedResults }));
      break;
    }

    const parsedResult = JSON.parse(value);
    accumulatedResults = accumulatedResults
      ? updateResults(accumulatedResults, parsedResult)
      : parsedResult;

    setApiResponse(prev => ({ ...prev, ...accumulatedResults }));
  }
}

function updateResults(currentResults, newResult) {
  const fileIndex = currentResults.files.findIndex(
    file => file.id === newResult.id
  );
  if (fileIndex !== -1) {
    currentResults.files[fileIndex] = newResult;
  }

  return currentResults;
}

import ApiClient from '../client';
import { GET_USERS, GET_USER } from '../urls';
import Reactive from '../../reactive';

export function getUser() {
  return (
    Reactive.user.observables.userObservable$ ||
    Reactive.user.createObservable(requestUser)
  ) // in case somehow is not initialized yet
    .toPromise();
}

export function requestUser() {
  const urlOptions = {
    url: GET_USER,
  };

  return ApiClient.get(urlOptions).then(response => {
    if (response.status === 401) {
      if (window.self === window.top && window.location.pathname !== '/') {
        // redirect to login page
        window.location.href = window.location.origin;
      }
      throw new Error('Flywheel session token is invalid');
    } else if (response.status >= 400) {
      throw new Error('Error getting Flywheel user');
    }
    return response.json();
  });
}

export function getUsers() {
  const urlOptions = {
    url: GET_USERS,
    urlParams: {
      queryParams: {
        joinAvatars: true, // append ?join_avatars=true query param
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config);
}

import ApiClient from '../client';
import { GET_CONFIG } from '../urls';

export function getConfig() {
  const urlOptions = {
    url: GET_CONFIG,
    urlParams: {
      queryParams: {
        joinAvatars: true, // append ?join_avatars=true query param
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config);
}

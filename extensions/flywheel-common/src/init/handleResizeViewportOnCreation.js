import cornerstone from 'cornerstone-core';

const handleResizeViewportOnCreation = () => {
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    event => {
      const element = event.detail.element;
      element.addEventListener(
        cornerstone.EVENTS.IMAGE_RENDERED,
        onImageRendered
      );
    }
  );

  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_DISABLED,
    event => {
      const element = event.detail.element;
      element.removeEventListener(
        cornerstone.EVENTS.IMAGE_RENDERED,
        onImageRendered
      );
    }
  );
};

const onImageRendered = event => {
  const element = event.detail.element;
  setTimeout(() => {
    const enabledElements = cornerstone.getEnabledElements();
    enabledElements.forEach(enabledElement =>
      cornerstone.resize(enabledElement.element)
    );
  }, 0);
  element.removeEventListener(
    cornerstone.EVENTS.IMAGE_RENDERED,
    onImageRendered
  );
};

export default handleResizeViewportOnCreation;

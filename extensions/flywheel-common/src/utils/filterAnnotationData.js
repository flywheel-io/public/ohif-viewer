export default function filterData(data) {
  const keysToExclude = [
    'invalidated',
    'active',
    'forceInvalidated',
    'invalidHandlePlacement',
    'highlight',
    'moving',
  ];
  return Object.keys(data).reduce((filteredData, key) => {
    if (keysToExclude.includes(key)) {
      return filteredData;
    }
    if (typeof data[key] === 'object' && data[key]) {
      filteredData[key] = filterData(data[key]);
    } else {
      filteredData[key] = data[key];
    }
    return filteredData;
  }, {});
}

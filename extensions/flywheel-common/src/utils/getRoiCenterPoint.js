export const getRoiCenterPoint = annotation => {
  let centerPoint;
  if (
    annotation.toolType === 'FreehandRoi' ||
    annotation.toolType === 'ContourRoi'
  ) {
    centerPoint = {};
    let xPoint = 0;
    let yPoint = 0;
    annotation.handles.points.forEach(point => {
      xPoint += point.x;
      yPoint += point.y;
    });
    centerPoint = {
      x: xPoint / annotation.handles.points.length,
      y: yPoint / annotation.handles.points.length,
    };
  } else if (
    annotation.toolType === 'RectangleRoi' ||
    annotation.toolType === 'EllipticalRoi'
  ) {
    centerPoint = {};
    centerPoint.x = (annotation.handles.start.x + annotation.handles.end.x) / 2;
    centerPoint.y = (annotation.handles.start.y + annotation.handles.end.y) / 2;
  } else {
    centerPoint = annotation.handles.start;
  }
  return centerPoint;
};

import store from '@ohif/viewer/src/store';
import { getBSliceSettings } from './hasToolLimitReached';
import { isCurrentFile } from './isCurrentFile';
import { setActiveQuestion } from '../redux/actions/labels';

function clearSubFormQuestion() {
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const bSliceSettings = getBSliceSettings();
  const currentSelectedQuestion = state.infusions.currentSelectedQuestion;
  if (
    bSliceSettings &&
    currentSelectedQuestion?.isSubForm &&
    !isFile &&
    state.subForm.isSubFormPopupEnabled
  ) {
    store.dispatch(setActiveQuestion(null));
  }
}

export default clearSubFormQuestion;

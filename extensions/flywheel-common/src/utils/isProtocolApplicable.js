import { utils } from '@ohif/core';
import isSingleFileMode from './isSingleFileMode';
import { isCurrentVolumetricImage, isCurrentWebImage } from './isCurrentFile';
import { isHpForWebImages, isImageOpenedFromSession } from './webImageUtils';

const { ALL_IMAGE_TYPE } = utils.regularExpressions;

/**
 * It checks if hanging protocol should apply or not, based on store state or url path
 * @param {Object} state app redux store
 * @param {Object} location application location
 * @param {boolean} skipForTaskMode whether to skip application URL checking for task mode
 * @return {boolean} true in case its a single file mode
 */
const isProtocolApplicable = (state, location, skipForTaskMode = false) => {
  const config = state.flywheel.projectConfig;
  if (isCurrentVolumetricImage(state) && !isHpForWebImages(config?.layouts)) {
    return true;
  } else if (isCurrentWebImage(state)) {
    // Now, Hanging protocol is only supported for files opened from session
    return isImageOpenedFromSession(location.pathname);
  } else {
    return !isSingleFileMode(state, location, skipForTaskMode);
  }
};

export default isProtocolApplicable;

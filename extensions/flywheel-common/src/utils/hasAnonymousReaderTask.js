import getRouteParams from './getRouteParams';

const regExPattern = /^true$/i;

function hasAnonymousReaderTask() {
  const { blind } = getRouteParams();
  const isAnonymous = regExPattern.test(blind);

  return isAnonymous;
}

export default hasAnonymousReaderTask;

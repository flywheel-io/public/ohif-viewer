import isBSliceApplicable from './isBSliceApplicable';

/**
 * It gives a concise version of project config,
 * after cleaning up all invalid config values.
 * @return {object} projectConfig
 */

const getCondensedProjectConfig = (currentProjectConfig = null) => {
  const state = store.getState();
  let projectConfig = currentProjectConfig || state.flywheel?.projectConfig;

  // Special handling to restrict the b slice configurations only for single series
  // Todo - Need to revisit once this restriction can be relaxed

  if (!isBSliceApplicable(state)) {
    projectConfig = {
      ...projectConfig,
      ...{ bSlices: null },
    };
  }
  return projectConfig;
};

export default getCondensedProjectConfig;

import csTools from 'cornerstone-tools';
const segmentationModule = csTools.getModule('segmentation');

function addOrUpdateSmartCTRange(smartCTRanges, entry) {
  let existingEntry = smartCTRanges.find(x => x.label === entry.label);
  if (existingEntry) {
    existingEntry.range.min = Math.min(
      entry.range.min,
      existingEntry.range.min
    );
    existingEntry.range.max = Math.max(
      entry.range.max,
      existingEntry.range.max
    );
    existingEntry.color = entry.color;
  } else {
    smartCTRanges.push(entry);
    segmentationModule.state.colorLutTables[0][smartCTRanges.length] = [
      ...entry.color.slice(0, 3),
      entry.color[3] * 255,
    ];
  }
}

export { addOrUpdateSmartCTRange };

import store from '@ohif/viewer/src/store';

export default function hasPermissionForFileDownload() {
  const state = store.getState();
  const user = state.flywheel.user;
  const userId = user?._id;
  const userPermissions = state.flywheel.project?.permissions;
  const userRoleIds = userPermissions?.find(
    permission => permission._id === userId
  )?.role_ids;
  const roles = state.flywheel.roles;
  const isSiteAdmin = user?.roles?.includes('site_admin');
  let isFileDownloadPermission = false;
  userRoleIds?.forEach(roleId => {
    const role = roles?.find(r => r._id === roleId);
    if (role?.actions?.includes('files_download')) {
      isFileDownloadPermission = true;
    }
  });
  const hasPermission = !isSiteAdmin && !isFileDownloadPermission;
  return !hasPermission;
}

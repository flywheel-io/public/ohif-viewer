const showIndexingPage = (history, containerId, isReaderTask, isSession) => {
  history.push({
    pathname: '/indexing',
    state: { containerId, isReaderTask, isSession },
  });
};

export default showIndexingPage;

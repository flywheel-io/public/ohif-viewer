import React from 'react';

const replacePlaceholders = (input, data) => {
  if (typeof input !== 'string') {
    return { content: input, updatedLinks: data.links };
  }

  const templateExp = /\{\{\s*(\w+)\[?\s*(\d*)\s*\]?\s*\}\}/gi;

  const parts = [];
  let lastIndex = 0;
  let hasReplacements = false;
  const updatedLinks = [...data.links];

  input.replace(templateExp, (match, key, index, offset) => {
    parts.push(input.slice(lastIndex, offset));

    let replaceVal = data[key];
    if (index !== '') {
      replaceVal = replaceVal[parseInt(index)];
    }

    if (replaceVal && replaceVal.url && replaceVal.message) {
      parts.push(
        <a
          href={replaceVal.url}
          key={key}
          target="_blank"
          rel="noopener noreferrer"
        >
          {replaceVal.message}
        </a>
      );
      hasReplacements = true;
      updatedLinks.splice(parseInt(index), 1);
    } else {
      parts.push(match);
    }

    lastIndex = offset + match.length;
  });

  parts.push(input.slice(lastIndex));

  return {
    content: hasReplacements ? <>{parts}</> : input,
    updatedLinks,
  };
};

export default replacePlaceholders;

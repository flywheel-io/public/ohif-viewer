/**
 * Is the study is microscopy data or not.
 * @param {Array} studies studies from viewer props
 * @return {boolean} true in case its a microscopy data
 */

const isMicroscopyData = studies => {
  return studies?.[0]?.series?.[0]?.Modality === 'SM';
};

export default isMicroscopyData;

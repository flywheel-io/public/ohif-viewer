import store from '@ohif/viewer/src/store';
import { isCurrentFile } from './isCurrentFile';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const { setLimitReachedDialog } = FlywheelCommonRedux.actions;

function closeLimitReachedDialog(servicesManager) {
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const { projectConfig } = state.flywheel;

  if (
    servicesManager?.services &&
    state.infusions?.limitReachedDialog?.limitReachedDialogId &&
    !isFile &&
    projectConfig.bSlices
  ) {
    const { UIDialogService } = servicesManager.services;
    UIDialogService.dismiss({
      id: state.infusions?.limitReachedDialog?.limitReachedDialogId,
    });
    store.dispatch(setLimitReachedDialog(null));
  }
}

export default closeLimitReachedDialog;

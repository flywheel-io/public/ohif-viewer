import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import BrushTool from './BrushTool';
import store from '@ohif/viewer/src/store';
import commonToolUtils from './commonUtils';
import { getDefaultSmartCTRangeList, getSmartCTRangeList } from '../utils';
const { segmentationRangeManager } = commonToolUtils;
const { drawBrushPixels, getCircle } = csTools.importInternal(
  'util/segmentationUtils'
);
const segmentationModule = csTools.getModule('segmentation');

/**
 * @public
 * @class SmartBrushTool
 * @memberof Tools.Brush
 * @classdesc Tool for drawing segmentations on an image.
 * @extends BrushTool
 */
export default class SmartBrushTool extends BrushTool {
  static toolName = 'SmartBrushTool';

  constructor(props = {}) {
    super(props);
    this.name = 'SmartBrush';
  }

  /**
   * Paints the data to the labelmap.
   *
   * @protected
   * @param  {Object} evt The data object associated with the event.
   * @returns {void}
   */
  _paint(evt) {
    const { configuration } = segmentationModule;
    const eventData = evt.detail;
    const element = eventData.element;
    const { rows, columns } = eventData.image;
    const brushCenter = {
      x: Math.floor(eventData.currentPoints.image.x),
      y: Math.floor(eventData.currentPoints.image.y),
    };
    const isBrushCenterWithinImage =
      brushCenter.x < 0 ||
      brushCenter.x > columns ||
      brushCenter.y < 0 ||
      brushCenter.y > rows;

    if (isBrushCenterWithinImage) {
      return;
    }

    const radius = configuration.radius;
    let pointerArray = getCircle(
      radius,
      rows,
      columns,
      brushCenter.x,
      brushCenter.y
    );

    const { labelmap2D, labelmap3D, shouldErase } = this.paintEventData;

    if (!shouldErase) {
      pointerArray = this.filterByRange(
        eventData.image,
        element,
        pointerArray,
        brushCenter,
        radius,
        this.getSelectedRange()
      );
    }

    // Draw / Erase the active color.
    drawBrushPixels(
      pointerArray,
      labelmap2D.pixelData,
      labelmap3D.activeSegmentIndex,
      columns,
      shouldErase
    );

    cornerstone.updateImage(element);
  }

  getSelectedRange() {
    const state = store.getState();
    const smartCT = state.smartCT;
    return smartCT?.selectedRange;
  }

  isRangeWithinSelectedRange(range) {
    const selectedRange = this.getSelectedRange();
    return (
      !selectedRange ||
      (selectedRange?.range?.min <= range.min &&
        selectedRange?.range?.max >= range.max)
    );
  }
  _startPainting(evt) {
    const { configuration, state } = segmentationModule;
    const radius = configuration.radius;
    const eventData = evt.detail;
    const { rows, columns } = eventData.image;
    const brushCenter = {
      x: Math.floor(eventData.currentPoints.image.x),
      y: Math.floor(eventData.currentPoints.image.y),
    };

    let pointerArray = getCircle(
      radius,
      rows,
      columns,
      brushCenter.x,
      brushCenter.y
    );
    const smartCTRange = this.getWindowRange(
      evt,
      pointerArray,
      brushCenter,
      radius
    );
    if (this.isRangeWithinSelectedRange(smartCTRange.range)) {
      const allRanges = getSmartCTRangeList();
      const segementationIndex = allRanges.findIndex(
        r => r.label === smartCTRange.label
      );

      this.initializeBrushLabelMap(eventData.element);

      if (segementationIndex !== -1) {
        segmentationModule.setters.activeSegmentIndex(
          eventData.element,
          segementationIndex + 1
        );
      } else {
        let maxSegementationColorIndex = this.getMaxSegementationColorIndex(
          eventData.element
        );
        if (maxSegementationColorIndex < allRanges.length) {
          maxSegementationColorIndex = allRanges.length;
        }
        segmentationModule.setters.activeSegmentIndex(
          eventData.element,
          maxSegementationColorIndex + 1
        );
      }
      segmentationRangeManager.setSelectedRange(smartCTRange);
    }
    super._startPainting(evt);
  }

  getMaxSegementationColorIndex(element) {
    const { labelmap2D } = segmentationModule.getters.labelmap2D(element);
    const pixelData = labelmap2D.pixelData;
    let max = 0;
    for (let i = 0, j = pixelData.length - 1; i <= j; i++, j--) {
      if (pixelData[i] > max) {
        max = pixelData[i];
      }
      if (pixelData[j] > max) {
        max = pixelData[j];
      }
    }
    return max;
  }

  getWindowRange(evt, pointerArray, data, radius) {
    const selectedRange = this.getSelectedRange();
    if (selectedRange) {
      return selectedRange;
    }
    const defaultRange = getDefaultSmartCTRangeList();
    segmentationRangeManager.setSelectedRange(defaultRange);
    return defaultRange;
  }

  _getTolerance() {
    const state = store.getState();
    return state.segmentation?.sliderRange || 0;
  }

  filterByRange(
    image,
    element,
    pointerArray,
    brushCenter,
    radius,
    windowRange
  ) {
    if (!image.color) {
      const brushDia = radius * 2;
      const storedPixels = cornerstone.getStoredPixels(
        element,
        brushCenter.x - radius,
        brushCenter.y - radius,
        brushDia,
        brushDia
      );
      const tolerance = this._getTolerance();

      let minRangeInPixelValue =
        (windowRange.range.min - tolerance - image.intercept) / image.slope;
      let maxRangeInPixelValue =
        (windowRange.range.max + tolerance - image.intercept) / image.slope;

      pointerArray = pointerArray.filter(coords => {
        const relativePoint = {
          x: coords[0] + radius - brushCenter.x,
          y: coords[1] + radius - brushCenter.y,
        };
        const pointIndex = relativePoint.y * brushDia + relativePoint.x;
        const pixelValue = storedPixels[pointIndex];

        return (
          minRangeInPixelValue <= pixelValue &&
          pixelValue < maxRangeInPixelValue
        );
      });
    } else {
      // smart ct is not supported in color images.
    }

    return pointerArray;
  }

  _endPainting(evt) {
    super._endPainting(evt);
  }
}

import csTools from 'cornerstone-tools';

// State
const getToolState = csTools.getToolState;

export default function findToolIndex(element, toolName, handle) {
  const toolState = getToolState(element, toolName);
  let currentTool = -1;
  if (handle) {
    // Iterating over handles of all toolData instances to find the indices of the selected handle
    for (let toolIndex = 0; toolIndex < toolState.data.length; toolIndex++) {
      const handles = toolState.data[toolIndex].handles;
      if (handle === handles.start || handle === handles.end) {
        currentTool = toolIndex;
      }
    }
  }
  return currentTool;
}

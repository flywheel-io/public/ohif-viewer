import cornerstoneTools from 'cornerstone-tools';

const { state, configuration } = cornerstoneTools.getModule('segmentation');
const { setters, getters } = cornerstoneTools.getModule('segmentation');
const ARRAY_TYPES = {
  UINT_16_ARRAY: 0,
  FLOAT_32_ARRAY: 1,
};

/**
 * Returns the `activeSegmentIndex` for the active `Labelmap3D` for the `BrushStackState` displayed on the element.
 *
 * @param  {HTMLElement|string} elementOrEnabledElementUID   The cornerstone enabled
 *                                                    element or its UUID.
 * @param  {number} [labelmapIndex] The labelmap index, defaults to the active labelmap index.
 * @returns {number}                                  The active segment index.
 */
function getActiveSegmentIndexOffline(firstImageId, labelmapIndex) {
  if (!firstImageId) {
    return;
  }

  const brushStackState = state.series[firstImageId];

  if (brushStackState) {
    labelmapIndex =
      labelmapIndex === undefined
        ? brushStackState.activeLabelmapIndex
        : labelmapIndex;

    const labelmap3D = brushStackState.labelmaps3D[labelmapIndex];

    if (labelmap3D) {
      return labelmap3D.activeSegmentIndex;
    }
  }

  return 1;
}

/**
 * Sets the `activeSegmentIndex` for the active `Labelmap3D` on the element.
 *
 * @param  {HTMLElement|string} elementOrEnabledElementUID   The cornerstone enabled
 *                                                    element or its UUID.
 * @param {number}  segmentIndex The segmentIndex to set active.
 * @returns {null}
 */
function setActiveSegmentIndexOffline(firstImageId, segmentIndex) {
  if (!firstImageId) {
    return;
  }

  const brushStackState = state.series[firstImageId];

  if (!brushStackState) {
    return;
  }

  const activeLabelmapIndex = brushStackState.activeLabelmapIndex;
  const labelmap3D = brushStackState.labelmaps3D[activeLabelmapIndex];

  labelmap3D.activeSegmentIndex = segmentIndex;
}

function getActiveLabelmapIndexOffline(imageIds) {
  if (!imageIds || !imageIds.length) {
    return;
  }

  const firstImageId = imageIds[0];
  const brushStackState = state.series[firstImageId];

  if (!brushStackState) {
    return;
  }

  return brushStackState.activeLabelmapIndex;
}

/**
 * Sets the active `labelmapIndex` for the `BrushStackState` displayed on this
 * element. Creates the corresponding `Labelmap3D` if it doesn't exist.
 *
 * @param  {HTMLElement|string} elementOrEnabledElementUID   The cornerstone enabled
 *                                                    element or its UUID.
 * @param  {number} labelmapIndex = 0 The index of the labelmap.
 * @returns {null}
 */
function setActiveLabelmapIndexOffline(imageIds, labelmapIndex = 0) {
  if (!imageIds || !imageIds.length) {
    return;
  }

  const firstImageId = imageIds[0];

  const firstImagePlane = cornerstone.metaData.get(
    'imagePlaneModule',
    firstImageId
  );
  const { rows, columns } = firstImagePlane;
  const numberOfFrames = imageIds.length;
  const size = rows * columns * numberOfFrames;

  let brushStackState = state.series[firstImageId];

  if (brushStackState) {
    brushStackState.activeLabelmapIndex = labelmapIndex;

    if (!brushStackState.labelmaps3D[labelmapIndex]) {
      addLabelmap3D(brushStackState, labelmapIndex, size);
    }
  } else {
    state.series[firstImageId] = {
      activeLabelmapIndex: labelmapIndex,
      labelmaps3D: [],
    };

    brushStackState = state.series[firstImageId];

    addLabelmap3D(brushStackState, labelmapIndex, size);
  }
}

function addLabelmap3D(brushStackState, labelmapIndex, size) {
  let bytesPerVoxel;

  switch (configuration.arrayType) {
    case ARRAY_TYPES.UINT_16_ARRAY:
      bytesPerVoxel = 2;

      break;

    case ARRAY_TYPES.FLOAT_32_ARRAY:
      bytesPerVoxel = 4;
      break;

    default:
      throw new Error(`Unsupported Array Type ${configuration.arrayType}`);
  }

  // Buffer size is multiplied by bytesPerVoxel to allocate enough space.
  brushStackState.labelmaps3D[labelmapIndex] = {
    buffer: new ArrayBuffer(size * bytesPerVoxel),
    labelmaps2D: [],
    metadata: [],
    activeSegmentIndex: 1,
    colorLUTIndex: 0,
    segmentsHidden: [],
    undo: [],
    redo: [],
  };
}

/**
 * Returns if a segment is visible.
 *
 * @param  {HTMLElement|string} elementOrEnabledElementUID   The cornerstone enabled
 *                                                    element or its UUID.
 * @param  {number} segmentIndex     The segment index.
 * @param  {number} [labelmapIndex]    If undefined, defaults to the active
 *                                     labelmap index.
 * @returns {boolean} True if the segment is visible.
 */
function isSegmentVisibleOffline(firstImageId, segmentIndex, labelmapIndex) {
  if (!segmentIndex) {
    return;
  }

  if (!firstImageId) {
    return;
  }

  const brushStackState = state.series[firstImageId];

  if (!brushStackState) {
    console.warn(`brushStackState is undefined`);

    return;
  }

  labelmapIndex =
    labelmapIndex === undefined
      ? brushStackState.activeLabelmapIndex
      : labelmapIndex;

  if (!brushStackState.labelmaps3D[labelmapIndex]) {
    console.warn(`No labelmap3D of labelmap index ${labelmapIndex} on stack.`);

    return;
  }

  const labelmap3D = brushStackState.labelmaps3D[labelmapIndex];
  const visible = !labelmap3D.segmentsHidden[segmentIndex];

  return visible;
}

/**
 * Toggles the visability of a segment.
 *
 * @param  {HTMLElement|string} elementOrEnabledElementUID   The cornerstone enabled
 *                                                    element or its UUID.
 * @param  {number} segmentIndex     The segment index.
 * @param  {number} [labelmapIndex]    If undefined, defaults to the active
 *                                     labelmap index.
 * @returns {boolean} True if the segment is now visible.
 */
function toggleSegmentVisibilityOffline(
  firstImageId,
  segmentIndex,
  labelmapIndex
) {
  if (!segmentIndex) {
    return;
  }

  if (!firstImageId) {
    return;
  }

  const brushStackState = state.series[firstImageId];

  if (!brushStackState) {
    console.warn(`brushStackState is undefined`);

    return;
  }

  labelmapIndex =
    labelmapIndex === undefined
      ? brushStackState.activeLabelmapIndex
      : labelmapIndex;

  if (!brushStackState.labelmaps3D[labelmapIndex]) {
    console.warn(`No labelmap3D of labelmap index ${labelmapIndex} on stack.`);

    return;
  }

  const labelmap3D = brushStackState.labelmaps3D[labelmapIndex];
  const segmentsHidden = labelmap3D.segmentsHidden;

  segmentsHidden[segmentIndex] = !segmentsHidden[segmentIndex];

  return !segmentsHidden[segmentIndex];
}

setters.activeLabelmapIndexOffline = setActiveLabelmapIndexOffline;
getters.activeLabelmapIndexOffline = getActiveLabelmapIndexOffline;
setters.activeSegmentIndexOffline = setActiveSegmentIndexOffline;
getters.activeSegmentIndexOffline = getActiveSegmentIndexOffline;
setters.toggleSegmentVisibilityOffline = toggleSegmentVisibilityOffline;
getters.isSegmentVisibleOffline = isSegmentVisibleOffline;

export default {};

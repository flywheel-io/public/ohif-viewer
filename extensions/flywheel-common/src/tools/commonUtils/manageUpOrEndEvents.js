import csTools from 'cornerstone-tools';
const { EVENTS } = csTools;

const upOrEndEvents = {
  mouse: [EVENTS.MOUSE_UP, EVENTS.MOUSE_CLICK],
  touch: [
    EVENTS.TOUCH_END,
    EVENTS.TOUCH_DRAG_END,
    EVENTS.TOUCH_PINCH,
    EVENTS.TOUCH_PRESS,
    EVENTS.TAP,
  ],
};

/**
 * Adds modify loop event listeners.
 *
 * @param {Object} element - The viewport element to add event listeners to.
 * @param  {string} [interactionType=mouse]
 * @param {function} handler
 * @returns {void}
 */
const registerHandler = (element, interactionType, handler) => {
  (upOrEndEvents[interactionType] || []).forEach(eventType => {
    element.addEventListener(eventType, handler);
  });
};

/**
 * Removes modify loop event listeners.
 *
 * @param {Object} element - The viewport element to add event listeners to.
 * @param  {string} [interactionType=mouse]
 * @param {function} handler
 * @returns {void}
 */
const removeHandler = (element, interactionType, handler) => {
  (upOrEndEvents[interactionType] || []).forEach(eventType => {
    element.removeEventListener(eventType, handler);
  });
};

/**
 * Get the interaction type from event.
 *
 * @param {Object} evt - The event.
 * @returns {string}- interactionType
 */
const getCurrentInteractionType = evt => {
  const interactionType = (Object.keys(upOrEndEvents) || []).find(
    key => !!upOrEndEvents[key].find(eventType => eventType === evt.type)
  );
  return interactionType || 'mouse';
};

const upOrEndEventsManager = {
  registerHandler,
  removeHandler,
  getCurrentInteractionType,
};

export default upOrEndEventsManager;

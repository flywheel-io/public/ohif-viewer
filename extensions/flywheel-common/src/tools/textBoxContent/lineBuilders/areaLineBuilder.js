import { getLineItem } from '../common';
import * as unitUtils from '../../unit';

/**
 * It returns a method builder for given line type.
 * @param {number} area Area value
 * @param {boolean} hasPixelSpacing if given study has pixel spacing values
 * @return {function} Area line builder
 */
const getAreaBuilder = (area, hasPixelSpacing) => {
  const mmToMicronConversionFactor = 1000;
  const convertedArea = unitUtils.shouldConvertToMicrometer(area)
    ? area * mmToMicronConversionFactor * mmToMicronConversionFactor
    : area;
  return () =>
    getLineItem(
      convertedArea,
      'Area:',
      unitUtils.squareUnit(unitUtils.getLengthUnit(hasPixelSpacing, area))
    );
};

export default getAreaBuilder;

export default function getDefaultBoundingBox(textBox) {
  const BOX_WIDTH = 150;
  const BOX_HEIGHT = 50;
  return {
    top: textBox.x,
    left: textBox.y,
    height: BOX_HEIGHT,
    width: BOX_WIDTH,
  };
}

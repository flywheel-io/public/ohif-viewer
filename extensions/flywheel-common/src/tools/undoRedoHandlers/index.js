import handleUndoRedoOperation from './handleUndoRedoOperation';
import * as handleSegmentUndoRedoOperation from './handleSegmentUndoRedoOperation';

const undoRedoHandlers = {
  handleUndoRedoOperation,
  handleSegmentUndoRedoOperation,
};

export default undoRedoHandlers;

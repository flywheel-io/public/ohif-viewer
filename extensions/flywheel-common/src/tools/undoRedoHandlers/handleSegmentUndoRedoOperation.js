import csTools from 'cornerstone-tools';
import cornerstone from 'cornerstone-core';
import commonToolUtils from '../commonUtils';
const segmentationModule = csTools.getModule('segmentation');
const {
  syncInCrossSectional2DViewports,
} = commonToolUtils.syncInCrossSectional2DViewports;

export function handleUndoOperation(lastAction, element) {
  const enabledElement = cornerstone.getEnabledElement(element);
  segmentationModule.setters.undo(element, lastAction.data.labelmapIndex);
  syncInCrossSectional2DViewports(
    lastAction.data.toolType,
    enabledElement,
    segmentationModule.getters.labelmap2D(element)
  );
}

export function handleRedoOperation(nextAction, element) {
  const enabledElement = cornerstone.getEnabledElement(element);
  segmentationModule.setters.redo(element, nextAction.data.labelmapIndex);
  syncInCrossSectional2DViewports(
    nextAction.data.toolType,
    enabledElement,
    segmentationModule.getters.labelmap2D(element)
  );
}

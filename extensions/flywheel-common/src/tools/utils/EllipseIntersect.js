import csTools from 'cornerstone-tools';
import cornerstoneMath from 'cornerstone-math';
import rotatePoint from './rotatePoint.js';
const { Vector3 } = cornerstoneMath;

const ellipseUtils = csTools.importInternal('util/ellipseUtils');

const { pointInEllipse } = ellipseUtils;

/**
 * Is point is inside the ellipse
 * @param {[Object]} points ellipse measurement bounding box points
 * @param {Object} point point to check
 * @param {Object} imageDimension image dimension on which ellipse drawn
 * @param {number} rotation initial image rotation at the time of drawing ellipse
 * @return {boolean} true if inside else false
 */
export function isPointInsideEllipse(points, point, imageDimension, rotation) {
  if (points.length < 4) {
    return false;
  }
  const centerPoint = {
    x: imageDimension.x / 2,
    y: imageDimension.y / 2,
  };
  let corner1 = points[0];
  let corner3 = points[2];
  let srcPoint = point;

  // Rotate the points with initial drawn rotation to make the ellipse non-rotated
  if (Math.abs(rotation) > 0.05) {
    corner1 = rotatePoint(corner1, centerPoint, rotation);
    corner3 = rotatePoint(corner3, centerPoint, rotation);
    srcPoint = rotatePoint(srcPoint, centerPoint, rotation);
  }
  const topLeft = {
    x: Math.min(corner1.x, corner3.x),
    y: Math.min(corner1.y, corner3.y),
  };
  const bottomRight = {
    x: Math.max(corner1.x, corner3.x),
    y: Math.max(corner1.y, corner3.y),
  };
  corner1 = topLeft;
  corner3 = bottomRight;

  const rect = {
    left: corner1.x,
    top: corner1.y,
    width: corner3.x - corner1.x,
    height: corner3.y - corner1.y,
  };
  return pointInEllipse(rect, srcPoint);
}

/**
 * To retrieve the ellipse data
 * @param {[Object]} start ellipse start point
 * @param {Object} end ellipse end point
 * @param {Object} imageDimension image dimension on which ellipse drawn
 * @param {number} rotation initial image rotation at the time of drawing ellipse
 * @return {Object} ellipse points, corner points and the line points through the ellipse curve
 */
export function getEllipseData(
  start,
  end,
  imageDimension,
  rotation,
  resolution = 3
) {
  const centerPoint = {
    x: imageDimension.x / 2,
    y: imageDimension.y / 2,
  };
  let corner1 = start;
  let corner3 = end;

  // Rotate the points with initial drawn rotation to make the ellipse non-rotated
  if (Math.abs(rotation) > 0.05) {
    corner1 = rotatePoint(start, centerPoint, rotation);
    corner3 = rotatePoint(end, centerPoint, rotation);
  }

  // All major and minor axis points and the center point
  const pts = [
    { x: (corner1.x + corner3.x) / 2, y: corner1.y },
    { x: corner3.x, y: (corner1.y + corner3.y) / 2 },
    { x: (corner1.x + corner3.x) / 2, y: corner3.y },
    { x: corner1.x, y: (corner1.y + corner3.y) / 2 },
    { x: (corner1.x + corner3.x) / 2, y: (corner1.y + corner3.y) / 2 },
  ];

  // Corner points of the ellipse bounding box
  const cornerPoints = [
    { x: corner1.x, y: corner1.y },
    { x: corner3.x, y: corner1.y },
    { x: corner3.x, y: corner3.y },
    { x: corner1.x, y: corner3.y },
  ];

  // Ellipse curve points which forms the a set of joined and also connects through major-minor axis end points.
  const linePoints = getCurveLinePoints(cornerPoints, resolution);

  // Rotate back the points to the image pixel co-ordinate.
  if (Math.abs(rotation) > 0.05) {
    pts.forEach((pt, index) => {
      pts[index] = rotatePoint(pt, centerPoint, -rotation);
    });
    linePoints.forEach((pt, index) => {
      linePoints[index] = rotatePoint(pt, centerPoint, -rotation);
    });
    cornerPoints.forEach((pt, index) => {
      cornerPoints[index] = rotatePoint(pt, centerPoint, -rotation);
    });
  }

  return {
    points: pts,
    linePoints: linePoints,
    cornerPoints: cornerPoints,
  };
}

/**
 * To get the line points through the ellipse curve
 * @param {[Object]} cornerPoints ellipse corner points
 * @return {Object} line points through the ellipse curve
 */
function getCurveLinePoints1(cornerPoints, resolution = 3) {
  if (cornerPoints.length < 4) {
    return [];
  }
  const corner1 = cornerPoints[0];
  const corner3 = cornerPoints[2];
  const linePoints = [{ x: (corner1.x + corner3.x) / 2, y: corner1.y }];

  let axisVector1 = new Vector3(0, corner3.y - corner1.y, 0);
  let axisVector2 = new Vector3(corner3.x - corner1.x, 0, 0);
  const length1 = axisVector1.clone().length();
  const length2 = axisVector2.clone().length();
  axisVector1 = axisVector1.normalize();
  axisVector2 = axisVector2.normalize();
  const avgLength = (length1 + length2) / 2;
  const initialVec = new Vector3(
    (corner1.x + corner3.x) / 2,
    (corner1.y + corner3.y) / 2,
    0
  );

  const rect = {
    left: Math.min(cornerPoints[0].x, cornerPoints[2].x),
    top: Math.min(cornerPoints[0].y, cornerPoints[2].y),
    width: Math.abs(cornerPoints[2].x - cornerPoints[0].x),
    height: Math.abs(cornerPoints[2].y - cornerPoints[0].y),
  };

  // Ellipse curve points in the right top quadrant
  axisVector1 = axisVector1.multiplyScalar(-1);

  const splitByAngle = 90 / resolution;

  for (let i = splitByAngle; i < 90; i += splitByAngle) {
    let curvePt = findOptimalCurvePointInDirection(
      avgLength / 2,
      initialVec,
      axisVector1,
      i,
      rect
    );
    linePoints.push(curvePt);
  }
  linePoints.push({ x: corner3.x, y: (corner1.y + corner3.y) / 2 });

  // Ellipse curve points in the right bottom quadrant
  for (let i = splitByAngle; i < 90; i += splitByAngle) {
    let curvePt = findOptimalCurvePointInDirection(
      avgLength / 2,
      initialVec,
      axisVector2,
      i,
      rect
    );
    linePoints.push(curvePt);
  }
  linePoints.push({ x: (corner1.x + corner3.x) / 2, y: corner3.y });

  // Ellipse curve points in the left bottom quadrant
  axisVector1 = axisVector1.multiplyScalar(-1);
  for (let i = splitByAngle; i < 90; i += splitByAngle) {
    let curvePt = findOptimalCurvePointInDirection(
      avgLength / 2,
      initialVec,
      axisVector1,
      i,
      rect
    );
    linePoints.push(curvePt);
  }
  linePoints.push({ x: corner1.x, y: (corner1.y + corner3.y) / 2 });

  // Ellipse curve points in the left top quadrant
  axisVector2 = axisVector2.multiplyScalar(-1);
  for (let i = splitByAngle; i < 90; i += splitByAngle) {
    let curvePt = findOptimalCurvePointInDirection(
      avgLength / 2,
      initialVec,
      axisVector2,
      i,
      rect
    );
    linePoints.push(curvePt);
  }
  linePoints.push({ x: (corner1.x + corner3.x) / 2, y: corner1.y });

  return linePoints;
}

/**
 * To get the line points through the ellipse curve
 * @param {[Object]} cornerPoints ellipse corner points
 * @return {Object} line points through the ellipse curve
 */
function getCurveLinePoints(cornerPoints, resolution = 3) {
  if (cornerPoints.length < 4) {
    return [];
  }
  const corner1 = cornerPoints[0];
  const corner2 = cornerPoints[1];
  const corner3 = cornerPoints[2];
  const corner4 = cornerPoints[3];
  const linePoints = [];

  const majorAxis = Math.abs(corner3.x - corner1.x) / 2;
  const minorAxis = Math.abs(corner3.y - corner1.y) / 2;

  const centerPoint = {
    x: (corner1.x + corner3.x) / 2,
    y: (corner2.y + corner4.y) / 2,
  };

  const splitByAngle = 90 / resolution;

  for (let i = 0; i < 360; i += splitByAngle) {
    const theta = (i * Math.PI) / 180;
    linePoints.push({
      x: majorAxis * Math.cos(theta) + centerPoint.x,
      y: minorAxis * Math.sin(theta) + centerPoint.y,
    });
  }
  return linePoints;
}

/**
 * Find the optimal point on the curve in the direction from middle of ellipse
 * @param {number} startLength give length to find the optimal curve point
 * @param {Object} startVec center point
 * @param {Object} axisDir direction of axis from center
 * @param {number} angle angle to rotate the axis point
 * @param {Object} ellipseRect ellipse bounding box rect
 * @return {Object} optimal point on the curve
 */
function findOptimalCurvePointInDirection(
  startLength,
  startVec,
  axisDir,
  angle,
  ellipseRect
) {
  if (startLength < 2 || ellipseRect.width < 5 || ellipseRect.height < 5) {
    return startVec.clone();
  }
  let appliedLength = startLength;
  let result = startVec
    .clone()
    .add(axisDir.clone().multiplyScalar(appliedLength));
  let curvePt = rotatePoint(result, startVec, angle);
  let isOptimalLength = false;
  // Iterate to find the optimal ellipse curve point from center of the ellipse
  while (!isOptimalLength) {
    if (pointInEllipse(ellipseRect, curvePt)) {
      appliedLength++;
      result = startVec
        .clone()
        .add(axisDir.clone().multiplyScalar(appliedLength));
      curvePt = rotatePoint(result, startVec, angle);
    } else {
      appliedLength--;
      result = startVec
        .clone()
        .add(axisDir.clone().multiplyScalar(appliedLength));
      curvePt = rotatePoint(result, startVec, angle);
      isOptimalLength = pointInEllipse(ellipseRect, curvePt);
    }
  }
  return curvePt;
}

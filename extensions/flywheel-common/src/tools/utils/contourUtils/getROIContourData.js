import { globalImageIdSpecificToolStateManager } from 'cornerstone-tools';
import TOOL_NAMES from './toolNames';

const { CONTOUR_ROI_TOOL } = TOOL_NAMES;
const globalToolStateManager = globalImageIdSpecificToolStateManager;

/**
 * getROIContourData - Generates a list of the slice locations of the 2D
 * polygons that make up the ROIContour.
 *
 * @param  {String[]} imageIds  An array of Image Ids.
 * @param  {Object} filterParams {key: propertyName to compare, value: value of property}.
 * @param  {boolean} onlyContourImage if true include contour existing images only.
 * @return {object[]}           The list of contour locations in the stack.
 */

export default function getROIContourData(
  imageIds,
  filterParams,
  onlyContourImage = false
) {
  const ROIContourData = [];
  const key = filterParams.key || 'ROIContourUid';
  const toolStateManager = globalToolStateManager.saveToolState();

  for (let i = 0; i < imageIds.length; i++) {
    const imageId = imageIds[i];
    const imageToolState = toolStateManager[imageId];

    if (!imageToolState || !imageToolState[CONTOUR_ROI_TOOL]) {
      if (!onlyContourImage) {
        ROIContourData.push({
          imageId,
        });
      }
    } else {
      const contours = imageToolState[CONTOUR_ROI_TOOL].data.filter(contour => {
        return contour[key] === filterParams.value;
      });

      const contoursOnSlice = {
        imageId,
      };

      if (contours.length) {
        contoursOnSlice.contours = contours;
      }

      ROIContourData.push(contoursOnSlice);
    }
  }

  return ROIContourData;
}

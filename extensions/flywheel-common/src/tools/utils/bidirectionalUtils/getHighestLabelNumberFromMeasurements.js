import store from '@ohif/viewer/src/store';
import { hasMultipleReaderTask } from '../../../utils';
import ToolTypes from '../../constants/ToolTypes';

function getHighestLabelNumberFromMeasurements(annotations) {
  const state = store.getState();
  const measurements = getAnnotations(state, annotations);
  const max = findHighestLabelNumberFromMeasurements(measurements);
  return max + 1;
}

function findHighestLabelNumberFromMeasurements(measurements) {
  const maxValue = measurements.reduce((highest, item) => {
    const labelNumber = item.labelNumber || 0;
    return Math.max(highest, labelNumber);
  }, 0);
  return maxValue;
}

function getAnnotations(state, annotations = null) {
  const hasMultipleTask = hasMultipleReaderTask(state);
  const measurements =
    annotations || state.timepointManager.measurements[ToolTypes.Bidirectional];

  if (hasMultipleTask && state.viewerAvatar.singleAvatar) {
    const annotationsList = [];
    measurements.forEach(x => {
      if (state.viewerAvatar.taskId === x.task_id) {
        annotationsList.push(x);
      }
    });
    return annotationsList;
  }

  return measurements;
}

export default getHighestLabelNumberFromMeasurements;

import csTools from 'cornerstone-tools';
import cornerstoneMath from 'cornerstone-math';
import cornerstone from 'cornerstone-core';
import FreehandRoiTool from './FreehandRoiTool';
import openFreehandIntersect from './utils/openFreehandIntersect';

import {
  setToolCursor,
  hideToolCursor,
  isCursorExist,
} from './utils/setToolCursor';

import commonToolUtils from './commonUtils';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

const { saveActionState } = commonToolUtils;

const EVENTS = csTools.EVENTS;

// Util
const pointInsideBoundingBox = csTools.importInternal(
  'util/pointInsideBoundingBox'
);
const moveHandleNearImagePoint = csTools.importInternal(
  'manipulators/moveHandleNearImagePoint'
);

const triggerEvent = csTools.importInternal('util/triggerEvent');
const freehandUtils = csTools.importInternal('util/freehandUtils');
const drawJoinedLines = csTools.importInternal('drawing/drawJoinedLines');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');
const {
  freehandArea,
  calculateFreehandStatistics,
  freehandIntersect,
  FreehandHandleData,
} = freehandUtils;

// State
const { state } = csTools.store;
const getToolState = csTools.getToolState;

/**
 * Improvement on csTool ROI tool to use FLYW custom textBox implementation.
 * renderToolData method is similar to its original version (csTools)
 *
 * @public
 * @class OpenFreehandRoiTool
 * @memberof Tools.Annotation
 * @classdesc Tool for selecting a region of an image to crop and create a new one
 * @extends FreehandRoiTool
 */
export default class OpenFreehandRoiTool extends FreehandRoiTool {
  static toolName = 'OpenFreehandRoiTool';
  static enableFragment = false;

  constructor(props = {}) {
    super(props);
    this.name = 'OpenFreehandRoi';
    this.drawJoinedLines = this.drawMultipleJoinedLines;
    this.drawHandles = this.drawMultipleHandles;
    this.drawLinkedTextBox = this.drawMultipleLinkedTextBox;
  }

  /**
   * To unbind the special hotkeys used in Open Freehand support
   * @returns
   */
  unBindKeys() {
    const defaultKey = 'ctrl';
    const enterKey = 'enter';
    const hotKeyConfigFn = this.configuration?.getHotKeyConfigForCommand;
    const hotKeys = hotKeyConfigFn
      ? hotKeyConfigFn('fragmenterKey')?.keys || defaultKey
      : defaultKey;
    Mousetrap.unbind(defaultKey, 'keyup');
    Mousetrap.unbind(defaultKey, 'keydown');
    Mousetrap.unbind(enterKey, 'keyup');
    if (hotKeys !== defaultKey) {
      Mousetrap.unbind(hotKeys, 'keyup');
      Mousetrap.unbind(hotKeys, 'keydown');
    }
  }

  /**
   * To register special hotkeys for Open Freehand support
   * @returns
   */
  bindKeys() {
    const defaultKey = 'ctrl';
    const enterKey = 'enter';
    const hotKeyConfigFn = this.configuration?.getHotKeyConfigForCommand;
    const hotKeys = hotKeyConfigFn
      ? hotKeyConfigFn('fragmenterKey')?.keys || defaultKey
      : defaultKey;
    Mousetrap.bind(
      hotKeys,
      e => {
        OpenFreehandRoiTool.enableFragment = true;
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.image) {
            cornerstone.updateImage(enabledElement.element);
          }
        });
      },
      'keydown'
    );

    Mousetrap.bind(
      hotKeys,
      e => {
        OpenFreehandRoiTool.enableFragment = false;
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.image) {
            cornerstone.updateImage(enabledElement.element);
          }
        });
      },
      'keyup'
    );

    Mousetrap.bind(
      enterKey,
      e => {
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.image) {
            cornerstone.updateImage(enabledElement.element);
            this._drawingKeyUpCallback(enabledElement.element, e);
          }
        });
      },
      'keyup'
    );
  }

  /**
   * to deactivate the contour.
   * @param {Element} element
   * @param {Object} evt
   */
  _drawingKeyUpCallback(element, evt) {
    // to trigger the mouse double click event.
    triggerEvent(element, EVENTS.MOUSE_DOUBLE_CLICK, { element });
  }

  /**
   * To create measurement
   * @param {Object} eventData
   * @returns
   */
  createNewMeasurement(eventData) {
    const measurementData = super.createNewMeasurement(eventData);
    measurementData.handles.points.push([]);
    return measurementData;
  }

  /**
   * @param {*} element
   * @param {*} data
   * @param {*} coords
   * @returns {number} the distance in px from the provided coordinates to the
   * closest rendered portion of the annotation. -1 if the distance cannot be
   * calculated.
   */
  distanceFromPoint(element, data, coords) {
    let distance = Infinity;
    const lines = data.handles.points;
    for (let i = 0; i < lines.length; i++) {
      const points = lines[i];
      for (let j = 0; j < points.length; j++) {
        const distanceI = cornerstoneMath.point.distance(points[j], coords);

        distance = Math.min(distance, distanceI);
      }
    }

    // If an error caused distance not to be calculated, return -1.
    if (distance === Infinity) {
      return -1;
    }

    return distance;
  }

  /**
   * @param {*} element
   * @param {*} data
   * @param {*} coords
   * @returns {number} the distance in canvas units from the provided coordinates to the
   * closest rendered portion of the annotation. -1 if the distance cannot be
   * calculated.
   */
  distanceFromPointCanvas(element, data, coords) {
    let distance = Infinity;

    if (!data) {
      return -1;
    }

    const canvasCoords = cornerstone.pixelToCanvas(element, coords);

    const lines = data.handles.points;

    for (let l = 0; l < lines.length; l++) {
      const points = lines[l];
      for (let i = 0; i < points.length; i++) {
        const handleCanvas = cornerstone.pixelToCanvas(element, points[i]);

        const distanceI = cornerstoneMath.point.distance(
          handleCanvas,
          canvasCoords
        );

        distance = Math.min(distance, distanceI);
      }
    }

    // If an error caused distance not to be calculated, return -1.
    if (distance === Infinity) {
      return -1;
    }

    return distance;
  }

  /**
   * @param {Object} image image
   * @param {Object} element element
   * @param {Object} data data
   *
   * @returns {void}  void
   */
  updateCachedStats(image, element, data) {
    const lines = data.handles.points;
    if (!lines) {
      return;
    }
    // If the data has been invalidated, and the tool is not currently active,
    // We need to calculate it again.

    let polyBoundingBox;

    // Retrieve the bounds of the ROI in image coordinates
    const bounds = {
      left: lines[0][0].x,
      right: lines[0][0].x,
      bottom: lines[0][0].y,
      top: lines[0][0].y,
    };

    for (let l = 0; l < lines.length; l++) {
      const points = lines[l];
      if (!points.length) {
        return;
      }

      for (let i = 0; i < points.length; i++) {
        bounds.left = Math.min(bounds.left, points[i].x);
        bounds.right = Math.max(bounds.right, points[i].x);
        bounds.bottom = Math.min(bounds.bottom, points[i].y);
        bounds.top = Math.max(bounds.top, points[i].y);
      }

      polyBoundingBox = {
        left: bounds.left,
        top: bounds.bottom,
        width: Math.abs(bounds.right - bounds.left),
        height: Math.abs(bounds.top - bounds.bottom),
      };
    }

    // Store the bounding box information for the text box
    data.polyBoundingBox = polyBoundingBox;

    // Set the invalidated flag to false so that this data won't automatically be recalculated
    data.invalidated = false;
  }

  /**
   * To draw joining multiple lines.
   * @param {CanvasRenderingContext2D} context - Target context
   * @param {HTMLElement} element - The DOM Element to draw on
   * @param {Object} firstLine - first line with points.
   * @param {Object[]} lines - `[[{ x, y }]]` An array of line with each line is an array of points in either pixel or canvas coordinates.
   * @param {Object} options
   * @param {Boolean} isStartPoint - is starting line or not
   */
  drawMultipleJoinedLines(
    context,
    element,
    firstLine,
    lines,
    options,
    isStartPoint = false
  ) {
    if (isStartPoint) {
      for (let i = 0; i < lines.length; i++) {
        if (lines[i].length) {
          drawJoinedLines(context, element, firstLine, lines[i], options);
        }
      }
    } else {
      drawJoinedLines(context, element, firstLine, lines, options);
    }
  }

  /**
   * To draw handles for each lines.
   * @param {CanvasRenderingContext2D} context - Target context
   * @param {Object} eventData
   * @param {Object[]} lines - `[[{ x, y }]]` An array of line with each line is an array of points in either pixel or canvas coordinates.
   * @param {Object} options
   * @param {Boolean} type
   */
  drawMultipleHandles(context, eventData, lines, options, type) {
    if (!this.configuration.drawHandles) {
      return;
    }
    if (type === 'POINTS') {
      for (let i = 0; i < lines.length; i++) {
        if (lines[i].length) {
          drawHandles(context, eventData, lines[i], options);
        }
      }
    } else {
      drawHandles(context, eventData, lines, options);
    }
  }

  /**
   * To draw linked text-box
   * @param {Object} context - The canvas context.
   * @param {HTMLElement} element - The element on which to draw the link.
   * @param {Object} textBox - The textBox to link.
   * @param {Object} textBoxContent - The text to display in the textbox.
   * @param {Object[]} lines - `[[{ x, y }]]` An array of line with each line is an array of points.
   * @param {Object[]} handles - The handles of the annotation.
   * @param {string} color - The link color.
   * @param {number} lineWidth - The line width of the link.
   * @param {number} xOffset - The x offset of the textbox.
   * @param {boolean} yCenter - Vertically centers the text if true.
   * @returns {undefined}
   */
  drawMultipleLinkedTextBox(
    context,
    element,
    textBox,
    textBoxContent,
    lines,
    handles,
    color,
    lineWidth,
    xOffset,
    yCenter
  ) {
    let allPoints = [];
    for (let i = 0; i < lines.length; i++) {
      allPoints = allPoints.concat(lines[i]);
    }
    if (!Array.isArray(textBoxContent)) {
      textBoxContent = [textBoxContent];
    }
    drawLinkedTextBox(
      context,
      element,
      textBox,
      textBoxContent,
      allPoints,
      handles,
      color,
      lineWidth,
      xOffset,
      yCenter
    );
  }

  /**
   * To draw close ends.
   * @param {Object} context
   * @param {Element} element
   * @param {Array} points
   * @param {Object} options
   */
  closeEndPoints(context, element, points, options) {
    // Skip closing the end points
  }

  /**
   * To modify selected handle
   * @param {Object} evt
   * @param {Object} toolData
   * @param {Object} handle
   * @param {String} interactionType
   * @returns
   */
  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    const { element } = evt.detail;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (toolData.readonly || !toolData.visible || !isAnnotationToolEnabled) {
      return;
    }
    const toolState = getToolState(element, this.name);

    if (handle.hasBoundingBox) {
      // Use default move handler.
      moveHandleNearImagePoint(evt, this, toolData, handle, interactionType);
      return;
    }
    const config = this.configuration;

    config.dragOrigin = {
      x: handle.x,
      y: handle.y,
    };

    let currentHandle = undefined;
    let currentTool = undefined;

    // Iterating over handles of all toolData instances to find the indices of the selected handle
    for (
      let toolIndex = 0;
      toolIndex < toolState.data.length && currentTool === undefined;
      toolIndex++
    ) {
      const lines = toolState.data[toolIndex].handles.points;
      let indexOffsetForLines = 0;
      for (let l = 0; l < lines.length && currentTool === undefined; l++) {
        const points = lines[l];
        for (let p = 0; p < points.length; p++) {
          if (points[p] === handle) {
            currentHandle = indexOffsetForLines + p;
            currentTool = toolIndex;
            break;
          }
        }
        indexOffsetForLines += points.length;
      }
    }

    if (currentTool !== undefined) {
      config.currentHandle = currentHandle;
      config.currentTool = currentTool;
    }

    this._modifying = true;

    this._activateModify(element);

    saveActionState(toolData, 'measurements', 'Modify', true, false);

    // Interupt eventDispatchers
    preventPropagation(evt);
  }

  /**
   * Event handler for MOUSE_MOVE during drawing event loop.
   *
   * @event
   * @param {Object} evt - The event.
   * @returns {undefined}
   */
  _drawingMouseMoveCallback(evt) {
    const eventData = evt.detail;
    const { currentPoints, element } = eventData;
    const toolState = getToolState(element, this.name);

    const config = this.configuration;
    const currentTool = config.currentTool;

    const data = toolState.data[currentTool];
    if (!data) {
      return;
    }
    if (OpenFreehandRoiTool.enableFragment) {
      if (data.handles.points[data.handles.points.length - 1].length) {
        data.handles.points.push([]);
      }
      if (!isCursorExist()) {
        setToolCursor(this.element, this.svgCursor);
      }
      return;
    }
    const coords = currentPoints.canvas;

    // Set the mouseLocation handle
    this._getMouseLocation(eventData);
    this._checkInvalidHandleLocation(data, eventData);

    // Mouse move -> Polygon Mode
    const handleNearby = this._pointNearHandle(element, data, coords);
    const points = data.handles.points;
    // If there is a handle nearby to snap to
    // (and it's not the actual mouse handle)

    if (
      handleNearby !== undefined &&
      !handleNearby.hasBoundingBox &&
      handleNearby < points.length - 1
    ) {
      config.mouseLocation.handles.start.x = points[handleNearby].x;
      config.mouseLocation.handles.start.y = points[handleNearby].y;
    }

    // Force onImageRendered
    cornerstone.updateImage(element);
  }

  /**
   * Event handler for MOUSE_DRAG during drawing event loop.
   *
   * @event
   * @param {Object} evt - The event.
   * @returns {undefined}
   */
  _drawingMouseDragCallback(evt) {
    super._drawingMouseDragCallback(evt);
    if (OpenFreehandRoiTool.enableFragment) {
      if (!isCursorExist()) {
        setToolCursor(this.element, this.svgCursor);
      }
    }
  }

  /**
   * To draw line
   * @param {Object} evt
   * @returns
   */
  _drawingDrag(evt) {
    const eventData = evt.detail;
    const { element } = eventData;
    const toolState = getToolState(element, this.name);
    const config = this.configuration;
    const currentTool = config.currentTool;
    const data = toolState.data[currentTool];

    if (!data) {
      return;
    }

    // Set the mouseLocation handle
    this._getMouseLocation(eventData);
    this._checkInvalidHandleLocation(data, eventData);
    const lines = data.handles.points;
    this._addPointPencilMode(eventData, lines);
    this._dragging = true;

    // Force onImageRendered
    cornerstone.updateImage(element);
  }

  /**
   * Event handler for MOUSE_UP during drawing event loop.
   *
   * @event
   * @param {Object} evt - The event.
   * @returns {undefined}
   */
  _drawingMouseUpCallback(evt) {
    if (!this._dragging) {
      return;
    }
    this._dragging = false;
    preventPropagation(evt);
    return;
  }

  /**
   * Event handler for MOUSE_DOWN during drawing event loop.
   *
   * @event
   * @param {Object} evt - The event.
   * @returns {undefined}
   */
  _drawingMouseDownCallback(evt) {
    const eventData = evt.detail;
    const { buttons, currentPoints, element } = eventData;

    if (!this.options.mouseButtonMask.includes(buttons)) {
      return;
    }

    const coords = currentPoints.canvas;
    const config = this.configuration;
    const currentTool = config.currentTool;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[currentTool];

    if (!data) {
      return;
    }

    const handleNearby = this._pointNearHandle(element, data, coords);

    if (handleNearby === undefined) {
      this._addPoint(eventData);
    }

    preventPropagation(evt);

    return;
  }

  /**
   * Event handler for TOUCH_START during drawing event loop.
   *
   * @event
   * @param {Object} evt - The event.
   * @returns {undefined}
   */
  _drawingTouchStartCallback(evt) {
    const eventData = evt.detail;
    const { currentPoints, element } = eventData;
    const coords = currentPoints.canvas;
    const config = this.configuration;
    const currentTool = config.currentTool;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[currentTool];

    if (!data) {
      return;
    }

    const handleNearby = this._pointNearHandle(element, data, coords);

    if (handleNearby === undefined) {
      this._addPoint(eventData);
    }

    preventPropagation(evt);

    return;
  }

  /** Ends the active drawing loop and completes the polygon.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  completeDrawing(element) {
    OpenFreehandRoiTool.enableFragment = false;
    if (!this._drawing) {
      return;
    }

    const toolState = getToolState(element, this.name);
    const config = this.configuration;
    let data = toolState.data[config.currentTool];

    if (!data) {
      return;
    }

    const lastHandlePlaced = config.currentHandle;
    data.polyBoundingBox = {};
    this._endDrawing(element, lastHandlePlaced);
  }

  /**
   * Event handler for MOUSE_DRAG during handle drag event loop.
   *
   * @event
   * @param {Object} evt - The event.
   * @returns {undefined}
   */
  _editMouseDragCallback(evt) {
    const eventData = evt.detail;
    const { element, buttons } = eventData;

    if (!this.options.mouseButtonMask.includes(buttons)) {
      return;
    }

    const toolState = getToolState(element, this.name);

    const config = this.configuration;
    const data = toolState.data[config.currentTool];
    if (!data) {
      return;
    }
    let handleIndex = -1;
    const { points, currentHandle } = this.getLineAndHandleIndex(
      data.handles.points,
      config
    );

    // Set the mouseLocation handle
    this._getMouseLocation(eventData);

    data.handles.invalidHandlePlacement = openFreehandIntersect.modify(
      points,
      currentHandle
    );
    data.active = true;
    data.highlight = true;
    points[currentHandle].x = config.mouseLocation.handles.start.x;
    points[currentHandle].y = config.mouseLocation.handles.start.y;

    handleIndex = this._getPrevHandleIndex(currentHandle, points);

    if (currentHandle >= 0) {
      const lastLineIndex = points[handleIndex].lines.length - 1;
      if (lastLineIndex > -1) {
        const lastLine = points[handleIndex].lines[lastLineIndex];

        lastLine.x = config.mouseLocation.handles.start.x;
        lastLine.y = config.mouseLocation.handles.start.y;
      }
    }

    // Update the image
    cornerstone.updateImage(element);
  }

  /**
   * Event handler for TOUCH_DRAG during handle drag event loop.
   *
   * @event
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _editTouchDragCallback(evt) {
    const eventData = evt.detail;
    const { element } = eventData;

    const toolState = getToolState(element, this.name);

    const config = this.configuration;
    const data = toolState.data[config.currentTool];
    if (!data) {
      return;
    }
    let handleIndex = -1;
    const { points, currentHandle } = this.getLineAndHandleIndex(
      data.handles.points,
      config
    );

    // Set the mouseLocation handle
    this._getMouseLocation(eventData);

    data.handles.invalidHandlePlacement = openFreehandIntersect.modify(
      points,
      currentHandle
    );
    data.active = true;
    data.highlight = true;
    points[currentHandle].x = config.mouseLocation.handles.start.x;
    points[currentHandle].y = config.mouseLocation.handles.start.y;

    handleIndex = this._getPrevHandleIndex(currentHandle, points);

    if (currentHandle >= 0) {
      const lastLineIndex = points[handleIndex].lines.length - 1;
      const lastLine = points[handleIndex].lines[lastLineIndex];

      lastLine.x = config.mouseLocation.handles.start.x;
      lastLine.y = config.mouseLocation.handles.start.y;
    }

    // Update the image
    cornerstone.updateImage(element);
  }

  /**
   * Places a handle of the freehand tool if the new location is valid.
   * If the new location is invalid the handle snaps back to its previous position.
   *
   * @private
   * @param {Object} eventData - Data object associated with the event.
   * @param {Object} toolState - The data associated with the freehand tool.
   * @modifies {toolState}
   * @returns {undefined}
   */
  _dropHandle(eventData, toolState) {
    const config = this.configuration;
    const currentTool = config.currentTool;
    const handles = toolState.data[currentTool].handles;

    const { points, currentHandle } = this.getLineAndHandleIndex(
      handles.points,
      config
    );

    // Don't allow the line being modified to intersect other lines
    if (handles.invalidHandlePlacement) {
      const currentHandleData = points[currentHandle];
      let previousHandleData;

      if (currentHandle === 0) {
        const lastHandleID = points.length - 1;

        previousHandleData = points[lastHandleID];
      } else {
        previousHandleData = points[currentHandle - 1];
      }

      // Snap back to previous position
      currentHandleData.x = config.dragOrigin.x;
      currentHandleData.y = config.dragOrigin.y;
      previousHandleData.lines[0] = currentHandleData;

      handles.invalidHandlePlacement = false;
    }
  }

  /**
   * Begining of drawing loop when tool is active and a click event happens far
   * from existing handles.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {undefined}
   */
  _startDrawing(evt) {
    super._startDrawing(evt);
    this.bindKeys();
  }

  /**
   * Adds a point on mouse click in polygon mode.
   *
   * @private
   * @param {Object} eventData - data object associated with an event.
   * @returns {undefined}
   */
  _addPoint(eventData) {
    const { currentPoints, element } = eventData;
    const toolState = getToolState(element, this.name);

    // Get the toolState from the last-drawn polygon
    const config = this.configuration;
    const data = toolState.data[config.currentTool];

    if (data.handles.invalidHandlePlacement) {
      return;
    }

    if (OpenFreehandRoiTool.enableFragment) {
      if (data.handles.points[data.handles.points.length - 1].length) {
        data.handles.points.push([]);
      }
      return;
    }

    const { points, currentHandle } = this.getLineAndHandleIndex(
      data.handles.points,
      config
    );

    const newHandleData = new FreehandHandleData(currentPoints.image);

    // If this is not the first handle
    if (points.length && currentHandle > 0) {
      // Add the line from the current handle to the new handle
      points[currentHandle - 1].lines.push(currentPoints.image);
    }

    // Add the new handle
    points.push(newHandleData);
    if (!OpenFreehandRoiTool.enableFragment && isCursorExist()) {
      hideToolCursor(this.element);
    }

    // Increment the current handle value
    config.currentHandle += 1;

    // Force onImageRendered to fire
    cornerstone.updateImage(element);
    this.fireModifiedEvent(element, data);
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} element - The element on which the roi is being drawn.
   * @param {Object} handleNearby - the handle nearest to the mouse cursor.
   * @returns {undefined}
   */
  _endDrawing(element, handleNearby) {
    const toolState = getToolState(element, this.name);
    const config = this.configuration;
    const data = toolState.data[config.currentTool];

    if (!data) {
      return;
    }

    data.active = false;
    data.highlight = false;
    data.handles.invalidHandlePlacement = false;

    if (this._modifying) {
      this._modifying = false;
      data.invalidated = true;
      saveActionState(data, 'measurements', 'Modify', false, true);
    }

    // Reset the current handle
    config.currentHandle = 0;
    config.currentTool = -1;
    data.canComplete = false;

    if (this._drawing) {
      this._deactivateDraw(element);
    }

    cornerstone.updateImage(element);

    this.fireModifiedEvent(element, data);
    this.fireCompletedEvent(element, data);
  }

  /**
   * Returns a handle of a particular tool if it is close to the mouse cursor
   *
   * @private
   * @param {Object} element - The element on which the roi is being drawn.
   * @param {Object} data      Data object associated with the tool.
   * @param {*} coords
   * @returns {Number|Object|Boolean}
   */
  _pointNearHandle(element, data, coords) {
    if (data.handles === undefined || data.handles.points === undefined) {
      return;
    }

    if (data.visible === false) {
      return;
    }

    for (let l = 0; l < data.handles.points.length; l++) {
      const line = data.handles.points[l];
      for (let i = 0; i < line.length; i++) {
        const handleCanvas = cornerstone.pixelToCanvas(element, line[i]);

        if (cornerstoneMath.point.distance(handleCanvas, coords) < 6) {
          return i;
        }
      }
    }

    // Check to see if mouse in bounding box of textbox
    if (data.handles.textBox) {
      if (pointInsideBoundingBox(data.handles.textBox, coords)) {
        return data.handles.textBox;
      }
    }
  }

  /**
   * Returns true if the proposed location of a new handle is invalid.
   *
   * @private
   * @param {Object} data      Data object associated with the tool.
   * @param {Object} eventData The data assoicated with the event.
   * @returns {Boolean}
   */
  _checkInvalidHandleLocation(data, eventData) {
    const { points } = this.getLineAndHandleIndex(
      data.handles.points,
      this.configuration
    );
    if (points.length < 2) {
      return true;
    }

    let invalidHandlePlacement;

    if (this._dragging) {
      invalidHandlePlacement = this._checkHandlesPencilMode(data, eventData);
    } else {
      invalidHandlePlacement = this._checkHandlesPolygonMode(data, eventData);
    }

    data.handles.invalidHandlePlacement = invalidHandlePlacement;
  }

  /**
   * Returns true if the proposed location of a new handle is invalid (in polygon mode).
   *
   * @private
   *
   * @param {Object} data - data object associated with the tool.
   * @param {Object} eventData The data assoicated with the event.
   * @returns {Boolean}
   */
  _checkHandlesPolygonMode(data, eventData) {
    const config = this.configuration;
    const { element } = eventData;
    const mousePoint = config.mouseLocation.handles.start;
    let invalidHandlePlacement = false;
    const { points } = this.getLineAndHandleIndex(data.handles.points, config);

    data.canComplete = false;

    const mouseAtOriginHandle = this._isDistanceSmallerThanCompleteSpacingCanvas(
      element,
      data.handles.points[0][0],
      mousePoint
    );

    if (
      mouseAtOriginHandle &&
      !freehandIntersect.end(points) &&
      points.length > 2
    ) {
      data.canComplete = true;
      invalidHandlePlacement = false;
    } else {
      invalidHandlePlacement = freehandIntersect.newHandle(mousePoint, points);
    }

    return invalidHandlePlacement;
  }

  /**
   * Returns true if the proposed location of a new handle is invalid (in pencilMode).
   *
   * @private
   * @param {Object} data - data object associated with the tool.
   * @param {Object} eventData The data associated with the event.
   * @returns {Boolean}
   */
  _checkHandlesPencilMode(data, eventData) {
    const config = this.configuration;
    const mousePoint = config.mouseLocation.handles.start;
    const lines = data.handles.points;
    const lastLine = data.handles.points[data.handles.points.length - 1];
    let invalidHandlePlacement = false;
    const previousHandle = lastLine[lastLine.length - 1];
    for (let i = 0; i < lines.length; i++) {
      invalidHandlePlacement = this.checkNewHandle(
        mousePoint,
        previousHandle,
        lines[i],
        i === lines.length - 1 ? [lines[i].length - 1] : []
      );
      if (invalidHandlePlacement) {
        break;
      }
    }

    if (invalidHandlePlacement === false) {
      invalidHandlePlacement = this._invalidHandlePencilMode(data, eventData);
    }

    return invalidHandlePlacement;
  }

  /**
   * Returns true if the mouse position is far enough from previous points (in pencilMode).
   *
   * @private
   * @param {Object} data - data object associated with the tool.
   * @param {Object} eventData The data associated with the event.
   * @returns {Boolean}
   */
  _invalidHandlePencilMode(data, eventData) {
    const config = this.configuration;
    const { element } = eventData;
    const mousePoint = config.mouseLocation.handles.start;
    const lines = data.handles.points;

    const mouseAtOriginHandle = this._isDistanceSmallerThanCompleteSpacingCanvas(
      element,
      lines[0][0],
      mousePoint
    );

    if (mouseAtOriginHandle) {
      data.canComplete = true;

      return false;
    }

    data.canComplete = false;

    // Compare with all other handles appart from the last one
    for (let l = 1; l < lines.length - 1; l++) {
      const line = lines[l];
      for (let i = 1; i < line.length - 1; i++) {
        if (this._isDistanceSmallerThanSpacing(element, line[i], mousePoint)) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Removes drawing loop event listeners.
   *
   * @private
   * @param {Object} element - The viewport element to add event listeners to.
   * @modifies {element}
   * @returns {undefined}
   */
  _deactivateDraw(element) {
    super._deactivateDraw(element);
    this.unBindKeys();
  }

  /**
   * New image event handler.
   *
   * @public
   * @param  {Object} evt The event.
   * @returns {null}
   */
  newImageCallback(evt) {
    const config = this.configuration;

    if (!(this._drawing && this._activeDrawingToolReference)) {
      return;
    }

    // Actively drawing but scrolled to different image.

    const element = evt.detail.element;
    const data = this._activeDrawingToolReference;

    data.active = false;
    data.highlight = false;
    data.handles.invalidHandlePlacement = false;

    // Reset the current handle
    config.currentHandle = 0;
    config.currentTool = -1;
    data.canComplete = false;

    this._deactivateDraw(element);

    cornerstone.updateImage(element);
  }

  /**
   * If in pencilMode, check the mouse position is farther than the minimum
   * distance between points, then add a point.
   *
   * @private
   * @param {Object} eventData - Data object associated with an event.
   * @param {Object} lines - Data object associated with the tool.
   * @returns {undefined}
   */
  _addPointPencilMode(eventData, lines) {
    const config = this.configuration;
    const { element } = eventData;
    const mousePoint = config.mouseLocation.handles.start;

    const handleFurtherThanMinimumSpacing = handle =>
      this._isDistanceLargerThanSpacing(element, handle, mousePoint);

    if (lines[lines.length - 1].every(handleFurtherThanMinimumSpacing)) {
      this._addPoint(eventData);
    }
  }

  /**
   * To get the text content
   * @param {Object} context - The canvas context.
   * @param {Object} image
   * @param {Object} data - measurement data.
   * @param {Object} hasPixelSpacing
   * @returns String
   */
  getTextboxContent(context, image, data, modality, hasPixelSpacing) {
    return data.location ? data.location : '';
  }

  /**
   * To get the line and handle index from lines
   * @param {Array} lines
   * @param {Object} config
   * @returns
   */
  getLineAndHandleIndex(lines, config) {
    let points = undefined;
    let currentHandle = -1;
    let indexOffsetForLines = 0;
    for (let l = 0; l < lines.length; l++) {
      if (config.currentHandle < indexOffsetForLines + lines[l].length) {
        points = lines[l];
        currentHandle = config.currentHandle - indexOffsetForLines;
        break;
      }
      indexOffsetForLines += lines[l].length;
    }
    if (!points) {
      points = lines[lines.length - 1];
      if (currentHandle === -1) {
        currentHandle = points.length;
      }
    }
    return { points, currentHandle };
  }

  /**
   * Determines whether a new handle causes an intersection of the lines of the polygon.
   * @public
   * @function newHandle
   *
   * @param {Object} candidateHandle The new handle to check.
   * @param {Object} previousHandle previous handle.
   * @param {Object} dataHandles data object associated with the tool.
   * @param {Array} ignoredPoints
   * @returns {boolean} - Whether the new line intersects with any other lines of the polygon.
   */
  checkNewHandle(candidateHandle, previousHandle, dataHandles, ignoredPoints) {
    // Check if the proposed line will intersect any existent line
    const lastHandle = getCoords(previousHandle);
    const newHandle = getCoords(candidateHandle);

    return doesIntersectOtherLines(
      dataHandles,
      lastHandle,
      newHandle,
      ignoredPoints
    );
  }

  getTextInfo(measurementData, data = null) {
    return [];
  }
}

function preventPropagation(evt) {
  evt.stopImmediatePropagation();
  evt.stopPropagation();
  evt.preventDefault();
}

/**
 * Returns an object with two properties, x and y, from a heavier FreehandHandleData object.
 * @private
 * @function getCoords
 *
 * @param {Object} dataHandle Data object associated with a single handle in the freehand tool.
 * @returns {Object} An object containing position propeties x and y.
 */
function getCoords(dataHandle) {
  return {
    x: dataHandle.x,
    y: dataHandle.y,
  };
}

/**
 * Checks whether the line (p1,q1) intersects any of the other lines in the polygon.
 * @private
 * @function doesIntersectOtherLines
 *
 * @param {Object} dataHandles Data object associated with the tool.
 * @param {Object} p1 Coordinates of the start of the line.
 * @param {Object} q1 Coordinates of the end of the line.
 * @param {Object} ignoredHandleIds Ids of handles to ignore (i.e. lines that share a vertex with the line being tested).
 * @returns {boolean} Whether the line intersects any of the other lines in the polygon.
 */
function doesIntersectOtherLines(dataHandles, p1, q1, ignoredHandleIds) {
  let j = dataHandles.length - 1;

  for (let i = 0; i < dataHandles.length; i++) {
    if (
      ignoredHandleIds.indexOf(i) !== -1 ||
      ignoredHandleIds.indexOf(j) !== -1
    ) {
      j = i;
      continue;
    }

    const p2 = getCoords(dataHandles[j]);
    const q2 = getCoords(dataHandles[i]);

    if (doesIntersect(p1, q1, p2, q2)) {
      return true;
    }

    j = i;
  }

  return false;
}

/**
 * Checks whether the line (p1,q1) intersects the line (p2,q2) via an orientation algorithm.
 * @private
 * @function doesIntersect
 *
 * @param {Object} p1 Coordinates of the start of the line 1.
 * @param {Object} q1 Coordinates of the end of the line 1.
 * @param {Object} p2 Coordinates of the start of the line 2.
 * @param {Object} q2 Coordinates of the end of the line 2.
 * @returns {boolean} Whether lines (p1,q1) and (p2,q2) intersect.
 */
function doesIntersect(p1, q1, p2, q2) {
  let result = false;

  const orient = [
    orientation(p1, q1, p2),
    orientation(p1, q1, q2),
    orientation(p2, q2, p1),
    orientation(p2, q2, q1),
  ];

  // General Case
  if (orient[0] !== orient[1] && orient[2] !== orient[3]) {
    return true;
  }

  // Special Cases
  if (orient[0] === 0 && onSegment(p1, p2, q1)) {
    // If p1, q1 and p2 are colinear and p2 lies on segment p1q1
    result = true;
  } else if (orient[1] === 0 && onSegment(p1, q2, q1)) {
    // If p1, q1 and p2 are colinear and q2 lies on segment p1q1
    result = true;
  } else if (orient[2] === 0 && onSegment(p2, p1, q2)) {
    // If p2, q2 and p1 are colinear and p1 lies on segment p2q2
    result = true;
  } else if (orient[3] === 0 && onSegment(p2, q1, q2)) {
    // If p2, q2 and q1 are colinear and q1 lies on segment p2q2
    result = true;
  }

  return result;
}

/**
 * Checks if point q lines on the segment (p,r).
 * @private
 * @function onSegment
 *
 * @param {Object} p Point p.
 * @param {Object} q Point q.
 * @param {Object} r Point r.
 * @returns {boolean} - If q lies on line segment (p,r).
 */
function onSegment(p, q, r) {
  if (
    q.x <= Math.max(p.x, r.x) &&
    q.x >= Math.min(p.x, r.x) &&
    q.y <= Math.max(p.y, r.y) &&
    q.y >= Math.min(p.y, r.y)
  ) {
    return true;
  }

  return false;
}

/**
 * Checks the orientation of 3 points.
 * @private
 * @function orientation
 *
 * @param {Object} p First point.
 * @param {Object} q Second point.
 * @param {Object} r Third point.
 * @returns {number} - 0: Colinear, 1: Clockwise, 2: Anticlockwise
 */
function orientation(p, q, r) {
  const orientationValue =
    (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

  if (orientationValue === 0) {
    return 0; // Colinear
  }

  return orientationValue > 0 ? 1 : 2;
}

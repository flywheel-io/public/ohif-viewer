import * as Sentry from '@sentry/react';
import { Observable } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';

let userObservable$;

const createObservable = requestUserPromise => {
  userObservable$ = Observable.create(observer => {
    requestUserPromise()
      .then(user => {
        observer.next(user);
        observer.complete();
      })
      .catch(error => {
        observer.error(error);
      });
  }).pipe(
    tap(user => {
      Sentry.setUser({ id: user.user_id });
    }),
    shareReplay(1)
  );

  return userObservable$;
};

/** Reactive object structure */

// define package object
const reactive = { createObservable };
Object.defineProperty(reactive, 'observables', {
  get: () => {
    return {
      userObservable$,
    };
  },
  configurable: false,
  enumerable: false,
});

export default reactive;

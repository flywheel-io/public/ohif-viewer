import store from './store';
import user from './user';

const reactive = {
  store,
  user,
};

export default reactive;

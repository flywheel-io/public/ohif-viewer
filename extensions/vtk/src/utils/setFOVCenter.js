import { getFovealPointByAnnotation } from './Indicator/FovealROI.js';

export const setFOVCenter = (
  fovCenter, imagePlane, fovAnnotationPoint
) => {
  const fovealPoint = getFovealPointByAnnotation(imagePlane, fovAnnotationPoint);
  fovCenter[0] = fovealPoint.x;
  fovCenter[1] = fovealPoint.y;
  fovCenter[2] = fovealPoint.z;
};

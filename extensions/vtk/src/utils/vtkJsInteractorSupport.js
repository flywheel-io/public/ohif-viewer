import { vec2, vec3 } from 'gl-matrix';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import vtkMatrixBuilder from 'vtk.js/Sources/Common/Core/MatrixBuilder';
import vtkMath from 'vtk.js/Sources/Common/Core/Math';
import _ from 'lodash';
import {
  getVolumeRadius,
  validateScale,
  getMouseButtonMask,
  getToolName,
  getConfiguredMouseActionButtons,
  getConfiguredMouseWheelAction,
} from './vtkUtils';
import { Utils } from '@flywheel/extension-flywheel';

const { calculateMinMaxMean } = Utils.cornerstoneUtils;
const EPSILON = 0.00001; // Negligible postion or angle difference factor
const MIN_WINDOW_WIDTH = 1;

const interactorStyleDefinitions = [
  {
    type: 'stackscroll',
    options: {
      button: 2,
      scrollEnabled: true,
      dragEnabled: false,
    },
  }, // scroll on Mouse Wheel
  {
    type: 'zoom',
    options: {
      button: 3,
      dragEnabled: true,
    },
  }, // zoom on Right button drag
  {
    type: 'pan',
    options: {
      button: 2,
      dragEnabled: true,
      scrollEnabled: false,
    },
  }, // pan on Mouse Wheel drag
];

// TODO - reused the vtk.js logic for rotating the crosshairs of other acquisition planes
function rotateCrosshairsByAngle(apis, apiIndex, angle) {
  const thisApi = apis[apiIndex];

  // Axis is the opposite direction of the plane normal for this API.
  const sliceNormal = thisApi.getSliceNormal();
  const axis = [-sliceNormal[0], -sliceNormal[1], -sliceNormal[2]];

  // Rotate other active apis
  const activeApis = apis.filter(
    (api, index) =>
      index !== apiIndex &&
      api.svgWidgets.rotatableCrosshairsWidget.getViewportActive()
  );

  activeApis.forEach((api, index) => {
    const cameraForApi = api.genericRenderWindow
      .getRenderWindow()
      .getInteractor()
      .getCurrentRenderer()
      .getActiveCamera();

    const crosshairPointForApi = api.get('cachedCrosshairWorldPosition');
    const initialCrosshairPointForApi = api.get(
      'initialCachedCrosshairWorldPosition'
    );

    const center = [];
    vtkMath.subtract(crosshairPointForApi, initialCrosshairPointForApi, center);
    const translate = [];
    vtkMath.add(crosshairPointForApi, center, translate);

    const { matrix } = vtkMatrixBuilder
      .buildFromRadian()
      .translate(translate[0], translate[1], translate[2])
      .rotate(angle, axis)
      .translate(-translate[0], -translate[1], -translate[2]);

    cameraForApi.applyTransform(matrix);

    const sliceNormalForApi = api.getSliceNormal();
    const viewUpForApi = api.getViewUp();
    api.setOrientation(sliceNormalForApi, viewUpForApi);
  });

  updateCrosshairs(apis, apiIndex);

  /*
        After the rotations and update of the crosshairs, the focal point of the
        camera has a shift along the line of sight coordinate respect to the
        crosshair (i.e., the focal point is not on the same slice of the crosshair).
        We calculate the new focal point coordinates as the nearest point between
        the line of sight of the camera and the crosshair coordinates:

          p1 = cameraPositionForApi
          p2 = cameraFocalPointForApi
          q = crosshairPointForApi

          Vector3 u = p2 - p1;
          Vector3 pq = q - p1;
          Vector3 w2 = pq - vtkMath.multiplyScalar(u, vtkMath.dot(pq, u) / u2);

          Vector3 newFocalPoint = q - w2;
        */

  apis.forEach(api => {
    const cameraForApi = api.genericRenderWindow
      .getRenderWindow()
      .getInteractor()
      .getCurrentRenderer()
      .getActiveCamera();

    const crosshairPointForApi = api.get('cachedCrosshairWorldPosition');
    const cameraFocalPointForApi = cameraForApi.getFocalPoint();
    const cameraPositionForApi = cameraForApi.getPosition();

    const u = [];
    vtkMath.subtract(cameraFocalPointForApi, cameraPositionForApi, u);
    const pq = [];
    vtkMath.subtract(crosshairPointForApi, cameraPositionForApi, pq);
    const uLength2 = u[0] * u[0] + u[1] * u[1] + u[2] * u[2];
    vtkMath.multiplyScalar(u, vtkMath.dot(pq, u) / uLength2);
    const w2 = [];
    vtkMath.subtract(pq, u, w2);
    const newFocalPointForApi = [];
    vtkMath.subtract(crosshairPointForApi, w2, newFocalPointForApi);

    cameraForApi.setFocalPoint(
      newFocalPointForApi[0],
      newFocalPointForApi[1],
      newFocalPointForApi[2]
    );
  });
  return true;
}

function updateCrosshairs(apis, apiIndex) {
  const thisApi = apis[apiIndex];

  const { rotatableCrosshairsWidget } = thisApi.svgWidgets;

  const worldPos = thisApi.get('cachedCrosshairWorldPosition');

  rotatableCrosshairsWidget.moveCrosshairs(worldPos, apis, apiIndex);
}

function getWorldPosDelta(api) {
  const worldPos = api.get('cachedCrosshairWorldPosition');

  const initialWorldPos = api.get('initialCachedCrosshairWorldPosition');

  const delta = [
    worldPos[0] - initialWorldPos[0],
    worldPos[1] - initialWorldPos[1],
    worldPos[2] - initialWorldPos[2],
  ];
  return delta;
}

function getLineVector(referenceLine) {
  let linePosVec = [];
  vec2.subtract(
    linePosVec,
    [referenceLine[0].x, referenceLine[0].y],
    [referenceLine[1].x, referenceLine[1].y]
  );
  return linePosVec;
}

// Reused the logic inside the vtkjs interactor styles
export function distanceFromLine(line, point) {
  const [a, b] = line.points;
  const c = point;

  // Get area from all 3 points...
  const areaOfTriangle = Math.abs(
    (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2
  );

  // And area also equals 1/2 base * height, where height is the distance!
  // So:
  const base = vec2.distance([a.x, a.y], [b.x, b.y]);
  const height = (2.0 * areaOfTriangle) / base;

  // Note we don't have to worry about finite line length
  // As the lines go across the whole canvas.

  return height;
}

// Reused the logic inside the vtkjs interactor styles
export function getWorldPosFromDisplayPos(api, pos, renderer) {
  const dPos = vtkCoordinate.newInstance();
  dPos.setCoordinateSystemToDisplay();

  dPos.setValue(pos.x, pos.y, 0);
  let worldPos = dPos.getComputedWorldValue(renderer);

  const camera = renderer.getActiveCamera();
  const directionOfProjection = camera.getDirectionOfProjection();

  const halfSlabThickness = api.getSlabThickness() / 2;

  // Add half of the slab thickness to the world position, such that we select
  // The center of the slice.

  for (let i = 0; i < worldPos.length; i++) {
    worldPos[i] += halfSlabThickness * directionOfProjection[i];
  }
  return worldPos;
}

// Reused the logic inside the vtkjs interactor styles
export function moveCrosshairs(apis, apiIndex, pos, renderer) {
  const thisApi = apis[apiIndex];
  const worldPos = getWorldPosFromDisplayPos(thisApi, pos, renderer);

  thisApi.svgWidgets.rotatableCrosshairsWidget.moveCrosshairs(
    worldPos,
    apis,
    apiIndex
  );
}

// Add widgets and set default interactorStyle of each viewport.
export function syncTargetApis(
  mprApis,
  srcSeriesInstanceUID,
  srcApiIndex,
  initialReferenceLinePoints
) {
  const seriesInstanceUID = srcSeriesInstanceUID;
  const viewportIndex = srcApiIndex;
  const thisApi = mprApis[seriesInstanceUID][viewportIndex];
  const delta = getWorldPosDelta(thisApi);
  let angle = 0;
  let determinant;
  const srcRotation = [];
  let currentLinePoints = [];
  let initialPosition = [];
  let newPosition = [];
  //Get the angle of rotation for both crosshair lines.
  if (!!initialReferenceLinePoints.src) {
    thisApi.svgWidgets.rotatableCrosshairsWidget
      .getReferenceLines()
      .forEach((line, lineIndex) => {
        currentLinePoints = line.points;

        initialPosition = getLineVector(
          initialReferenceLinePoints.src[lineIndex]
        );
        newPosition = getLineVector(currentLinePoints);
        angle = vec2.angle(initialPosition, newPosition);
        determinant =
          newPosition[0] * initialPosition[1] -
          newPosition[1] * initialPosition[0];
        srcRotation[lineIndex] = { angle, determinant };
      });
  }
  Object.keys(mprApis).forEach(key => {
    if (seriesInstanceUID === key) {
      return;
    }
    const apis = mprApis[key];
    const thisTgtApi = apis[viewportIndex];
    const deltaTgt = getWorldPosDelta(thisTgtApi);
    // Find the position delta of the initial position and current position for both source and target API
    // and apply the difference in delta between source and target to the target API.
    if (
      Math.abs(deltaTgt[0] - delta[0]) > EPSILON ||
      Math.abs(deltaTgt[1] - delta[1]) > EPSILON ||
      Math.abs(deltaTgt[2] - delta[2]) > EPSILON
    ) {
      const initialWorldPosTgt = thisTgtApi.get(
        'initialCachedCrosshairWorldPosition'
      );
      const newWorldPos = [
        initialWorldPosTgt[0] + delta[0],
        initialWorldPosTgt[1] + delta[1],
        initialWorldPosTgt[2] + delta[2],
      ];
      thisTgtApi.svgWidgets.rotatableCrosshairsWidget.moveCrosshairs(
        newWorldPos,
        apis
      );
    }

    const { rotatableCrosshairsWidget } = thisTgtApi.svgWidgets;
    let curTargetReferenceLinePoints = [];
    if (!!initialReferenceLinePoints.tgt) {
      //Compare angle of rotation of target crosshairs with source crosshairs
      //and rotate if there is any difference.
      let rotated = false;
      rotatableCrosshairsWidget
        .getReferenceLines()
        .forEach((line, lineIndex) => {
          if (rotated) {
            return;
          }
          curTargetReferenceLinePoints = line.points;
          initialPosition = getLineVector(
            initialReferenceLinePoints.tgt[key][lineIndex]
          );
          newPosition = getLineVector(curTargetReferenceLinePoints);
          const angleTgt = vec2.angle(initialPosition, newPosition);

          if (Math.abs(srcRotation[lineIndex].angle - angleTgt) > EPSILON) {
            let deltaAngle = angleTgt - srcRotation[lineIndex].angle;
            if (srcRotation[lineIndex].determinant > 0) {
              deltaAngle *= -1;
            }

            rotateCrosshairsByAngle(apis, viewportIndex, deltaAngle);
            rotated = true;
          }
        });
    }
  });
  return;
}

export function getPosition(api, renderer) {
  const cachedCrosshairWorldPosition = api.get('cachedCrosshairWorldPosition');
  if (cachedCrosshairWorldPosition === undefined) {
    // Crosshairs not initilised.
    return;
  }
  const wPos = vtkCoordinate.newInstance();
  wPos.setCoordinateSystemToWorld();
  wPos.setValue(...cachedCrosshairWorldPosition);
  const doubleDisplayPosition = wPos.getComputedDoubleDisplayValue(renderer);
  const pos = { x: doubleDisplayPosition[0], y: doubleDisplayPosition[1] };
  return pos;
}

export function addScrollListener(
  istyle,
  api,
  apis,
  apiIndex,
  scrollIndex = 0
) {
  const setNewSlice = newSlice => {
    istyle.setSlice(newSlice);
    if (api.svgWidgets.rotatableCrosshairsWidget) {
      const renderer = api.genericRenderWindow.getRenderer();
      const pos = getPosition(api, renderer);
      moveCrosshairs(apis, apiIndex, pos, renderer);
    }
  };
  const manipulator = istyle.getMouseManipulators()?.[scrollIndex];
  const range = istyle.getSliceRange();
  if (manipulator.setScrollListener) {
    // Adjust the step value if the range difference is less than 10
    const step = Math.abs(range[1] - range[0]) < 10 ? 0.1 : 1;
    manipulator.setScrollListener(
      range[0],
      range[1],
      step,
      istyle.getSlice,
      setNewSlice
    );
  }
}

export function getCustomInteractorStyleDefinitions(
  button,
  type,
  scrollEnabled,
  dragEnabled
) {
  let styleDefinitions = [...interactorStyleDefinitions];

  const config = window.store.getState()?.flywheel?.projectConfig;
  const mouseActions = config?.mouseActions;
  if (config && mouseActions) {
    styleDefinitions = [];
    mouseActions.forEach(action => {
      if (action.button !== 'left') {
        if (action.button === 'wheel') {
          if (action.toolName === 'StackScrollMouseWheel') {
            styleDefinitions.push({
              type: 'range',
              options: {
                button: getMouseButtonMask(action.button),
                dragEnabled: false,
                scrollEnabled: true,
              },
            });
          } else if (action.toolName === 'ZoomMouseWheel') {
            styleDefinitions.push({
              type: 'zoom',
              options: {
                button: getMouseButtonMask(action.button),
                dragEnabled: false,
                scrollEnabled: true,
              },
            });
          }
        } else {
          styleDefinitions.push({
            type: getToolName(action.toolName, action.button),
            options: {
              button: getMouseButtonMask(action.button),
              dragEnabled: true,
              scrollEnabled: false,
            },
          });
        }
      }
    });
  }

  if (button && type) {
    // TO DO: Need to extend the interactor style definitions in case of scenarios for updating or removing the existing style definitions in future.
    styleDefinitions.push({
      type: type,
      options: {
        button: button,
        dragEnabled: dragEnabled !== undefined ? dragEnabled : false,
        scrollEnabled: scrollEnabled !== undefined ? scrollEnabled : false,
      },
    });
  }
  return styleDefinitions;
}

export function addVerticalListener(
  istyle,
  api,
  apis,
  apiIndex,
  dragIndex = 3
) {
  const setNewSliceForStackScroll = newSlice => {
    const oldSlice = istyle.getSlice();
    const delta = newSlice - oldSlice;
    const defaultRange = istyle.getSliceRange();
    let value = oldSlice - delta;
    // Epsilon factor for adjusting the new slice on edge cases
    const epsilonFactor = 0.001;
    if (value <= defaultRange[0]) {
      value = defaultRange[0] + epsilonFactor;
    } else if (value >= defaultRange[1]) {
      value = defaultRange[1] - epsilonFactor;
    }
    istyle.setSlice(value);
    if (api.svgWidgets.rotatableCrosshairsWidget) {
      const renderer = api.genericRenderWindow.getRenderer();
      const pos = getPosition(api, renderer);
      moveCrosshairs(apis, apiIndex, pos, renderer);
    }
  };
  const manipulator = istyle.getMouseManipulators()?.[3];
  const range = istyle.getSliceRange();
  if (manipulator.setVerticalListener) {
    // Adjust the step value if the range difference is less than 10
    const step = Math.abs(range[1] - range[0]) < 10 ? 0.1 : 1;
    manipulator.setVerticalListener(
      range[0],
      range[1],
      step,
      istyle.getSlice,
      setNewSliceForStackScroll
    );
  }
}

function panImage(sourceApi, targetApi, srcPrevCamPos) {
  const tgtRenderer = targetApi.genericRenderWindow.getRenderer();
  const tgtCam = tgtRenderer.getActiveCamera();
  const tgtCamPos = tgtCam.getPosition();
  const tgtFp = tgtCam.getFocalPoint();

  // calculate camera position difference of source Api
  const srcCam = sourceApi.genericRenderWindow.getRenderer().getActiveCamera();
  const srcCamPos = srcCam.getPosition();
  const posDiff = [
    srcCamPos[0] - srcPrevCamPos[0],
    srcCamPos[1] - srcPrevCamPos[1],
    srcCamPos[2] - srcPrevCamPos[2],
  ];

  if (tgtCam.getParallelProjection()) {
    tgtCam.orthogonalizeViewUp();

    const up = tgtCam.getViewUp();
    const vpn = tgtCam.getViewPlaneNormal();

    const right = [0, 0, 0];

    vtkMath.cross(vpn, up, right);

    // Inverted implementation of vtk.js (to calculate pos diff from dx and dy)
    // to get back dx and dy from pos diff.
    let sourcedx = 0;
    let sourcedy = 0;
    if (up[0] === 0 && up[1] === 0) {
      if (right[0] == 0 && right[2] == 0) {
        sourcedx =
          (posDiff[1] * up[2] + posDiff[2] * up[1]) /
          (right[1] * up[2] + right[2] * up[1]);
        sourcedy =
          (posDiff[1] * right[2] + posDiff[2] * right[1]) /
          (right[1] * up[2] + right[2] * up[1]);
      } else {
        sourcedx =
          (posDiff[0] * up[2] + posDiff[2] * up[0]) /
          (right[0] * up[2] + right[2] * up[0]);
        sourcedy =
          (posDiff[0] * right[2] + posDiff[2] * right[0]) /
          (right[0] * up[2] + right[2] * up[0]);
      }
    } else {
      sourcedx =
        (posDiff[0] * up[1] + posDiff[1] * up[0]) /
        (right[0] * up[1] + right[1] * up[0]);
      sourcedy =
        (posDiff[0] * right[1] + posDiff[1] * right[0]) /
        (right[0] * up[1] + right[1] * up[0]);
    }

    const sourceScale = srcCam.getParallelScale();
    const targetScale = tgtCam.getParallelScale();

    const targetdx = (sourcedx / sourceScale) * targetScale;
    const targetdy = (sourcedy / sourceScale) * targetScale;

    if (!isNaN(targetdx) && !isNaN(targetdy)) {
      let change = right[0] * targetdx + up[0] * targetdy;
      tgtCamPos[0] += change;
      tgtFp[0] += change;
      change = right[1] * targetdx + up[1] * targetdy;
      tgtCamPos[1] += change;
      tgtFp[1] += change;
      change = right[2] * targetdx + up[2] * targetdy;
      tgtCamPos[2] += change;
      tgtFp[2] += change;
    }
    tgtCam.setPosition(tgtCamPos[0], tgtCamPos[1], tgtCamPos[2]);
    tgtCam.setFocalPoint(tgtFp[0], tgtFp[1], tgtFp[2]);
  } else {
    const newCamPos = [
      tgtCamPos[0] + posDiff[0],
      tgtCamPos[1] + posDiff[1],
      tgtCamPos[2] + posDiff[2],
    ];

    const newFp = [
      tgtFp[0] + posDiff[0],
      tgtFp[1] + posDiff[1],
      tgtFp[2] + posDiff[2],
    ];

    tgtCam.setPosition(newCamPos[0], newCamPos[1], newCamPos[2]);
    tgtCam.setFocalPoint(newFp[0], newFp[1], newFp[2]);
  }
  tgtRenderer.resetCameraClippingRange();
  tgtRenderer.updateLightsGeometryToFollowCamera();
  tgtCam.setThicknessFromFocalPoint(targetApi.getSlabThickness());
}

export function registerSyncCallback(apiIndex, api, mprApis, leftBtnTool) {
  const zoomActionButtons = getConfiguredMouseActionButtons(
    'zoom',
    leftBtnTool
  );
  const panActionButtons = getConfiguredMouseActionButtons('pan', leftBtnTool);
  const mouseWheelAction = getConfiguredMouseWheelAction();
  let prevCameraPos = {};
  const istyle = api.genericRenderWindow.getInteractor().getInteractorStyle();
  istyle.onStartInteractionEvent(() => {
    prevCameraPos = api.genericRenderWindow
      .getRenderer()
      .getActiveCamera()
      .getPosition();
  });
  istyle.onInteractionEvent(() => {
    if (
      zoomActionButtons.includes(event.buttons) ||
      (event.type === 'wheel' && mouseWheelAction === 'zoommousewheel')
    ) {
      const radius = getVolumeRadius(api);
      const pScale = api.genericRenderWindow
        .getRenderer()
        .getActiveCamera()
        .getParallelScale();

      const scale = validateScale(radius, pScale);
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach(api => {
          api.genericRenderWindow
            .getRenderer()
            .getActiveCamera()
            .setParallelScale(scale);
          api.genericRenderWindow.getRenderWindow().render();
        });
      });
    }
    if (panActionButtons.includes(event.buttons)) {
      const apiCam = api.genericRenderWindow.getRenderer().getActiveCamera();
      const position = apiCam.getPosition();

      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        if (apis.findIndex(curApi => curApi === api) === -1) {
          const targetApi = apis.find(curApi =>
            _.isEqual(curApi.getSliceNormal(), api.getSliceNormal())
          );
          if (targetApi) {
            panImage(api, targetApi, prevCameraPos);

            if (targetApi.svgWidgets.crosshairsWidget) {
              targetApi.svgWidgets.crosshairsWidget.updateCrosshairForApi(
                targetApi
              );
            }
            if (targetApi.svgWidgets.rotatableCrosshairsWidget) {
              const worldPos = targetApi.get('cachedCrosshairWorldPosition');

              targetApi.svgWidgets.rotatableCrosshairsWidget.moveCrosshairs(
                worldPos,
                apis,
                apiIndex
              );
            }
            targetApi.genericRenderWindow.getRenderWindow().render();
          }
        }
      });

      prevCameraPos = position;
    }
  });
}

/**
 *
 * @param {Object} listener  contains max, min, getValue()
 * @param {*} delta
 * @returns
 */
export function getNewSlice(listener, delta) {
  const oldValue = listener.getValue();
  let value = oldValue + delta;

  const difference = value - listener.min;
  const stepsToDifference = Math.round(difference / listener.step);
  value = listener.min + listener.step * stepsToDifference;
  value = Math.max(value, listener.min);
  value = Math.min(value, listener.max);

  // Check if value will change or if we need to store
  // the delta to append at the next iteration
  if (value !== oldValue) {
    const delta = value - oldValue;
    let newSlice = oldValue - delta;
    // Epsilon factor for adjusting the new slice on edge cases
    const epsilonFactor = 0.001;
    if (newSlice <= listener.min) {
      newSlice = listener.min + epsilonFactor;
    } else if (newSlice >= listener.max) {
      newSlice = listener.max - epsilonFactor;
    }
    return newSlice;
  }
  return false;
}

/**
 *
 * @param {Object} activeApi
 * @returns
 */
export function setNewSlice(activeApi, dy) {
  const renderWindow = activeApi.genericRenderWindow.getRenderWindow();
  const istyle = renderWindow.getInteractor().getInteractorStyle();
  const range = istyle.getSliceRange();
  const step = Math.abs(range[1] - range[0]) < 10 ? 0.1 : 1;
  const listener = {
    min: range[0],
    max: range[1],
    step,
    getValue: istyle.getSlice,
  };
  const newSlice = getNewSlice(listener, dy);
  if (newSlice) {
    istyle.setSlice(newSlice);
  }
}
/**
 * Set given window and level to apis
 * @param {Array} apis
 * @param {Number} window
 * @param {Number} level
 */
export function setWindowLevel(apis, window, level) {
  apis.forEach((api, apiIndex) => {
    const lowHigh = toLowHighRange(Number(window), Number(level));
    const rgbTransferFunction = api.volumes[0]
      .getProperty()
      .getRGBTransferFunction(0);

    rgbTransferFunction.setRange(lowHigh.lower, lowHigh.upper);
    const renderWindow = api.genericRenderWindow.getRenderWindow();
    renderWindow.render();
    api.updateVOI(Number(window), Number(level));
  });
}

/**
 *
 * @param {Number} windowWidth
 * @param {Number} windowCenter
 * @returns
 */
export function toLowHighRange(windowWidth, windowCenter) {
  const lower = windowCenter - windowWidth / 2.0;
  const upper = windowCenter + windowWidth / 2.0;

  return { lower, upper };
}

/** Calculate and return full dynamic window width and window center value
 *
 * @param {Object} image
 * @param {Object} config
 * @returns
 */
export const getFullDynamicWWWC = (image, config = {}) => {
  const left = 0;
  const top = 0;
  const { width, height } = image;

  if (!width || !height) {
    return;
  }

  // Get luminance of given cs element
  const pixelLuminanceData = getLuminance(image, left, top, width, height);

  // Calculate the minimum and maximum pixel values
  const minMaxMean = calculateMinMaxMean(
    pixelLuminanceData,
    image.minPixelValue,
    image.maxPixelValue
  );

  if (config.minWindowWidth === undefined) {
    config.minWindowWidth = MIN_WINDOW_WIDTH;
  }

  // window width should be at minimum MIN_WINDOW_WIDTH (or what ever is on config)
  const windowWidth = Math.max(
    Math.abs(minMaxMean.max - minMaxMean.min),
    config.minWindowWidth
  );
  const windowCenter = minMaxMean.mean;

  return {
    windowWidth,
    windowCenter,
  };
};

/**
 *
 * @param {Object} image
 * @param {Number} y
 * @param {Number} x
 * @param {Number} width
 * @param {Number} height
 * @returns
 */
const getLuminance = (image, x, y, width, height) => {
  x = Math.round(x);
  y = Math.round(y);
  const luminance = [];
  let index = 0;
  const pixelData = image.getPixelData();
  let spIndex, row, column;

  if (image.color) {
    for (row = 0; row < height; row++) {
      for (column = 0; column < width; column++) {
        spIndex = ((row + y) * image.columns + (column + x)) * 4;
        const red = pixelData[spIndex];
        const green = pixelData[spIndex + 1];
        const blue = pixelData[spIndex + 2];

        luminance[index++] = 0.2126 * red + 0.7152 * green + 0.0722 * blue;
      }
    }
  } else {
    for (row = 0; row < height; row++) {
      for (column = 0; column < width; column++) {
        spIndex = (row + y) * image.columns + (column + x);
        luminance[index++] = pixelData[spIndex] * image.slope + image.intercept;
      }
    }
  }

  return luminance;
};

/** Calculate and return scale to fit image to viewport
 *
 * @param {Array} boundsToUse
 * @returns
 */
const getViewportFitScale = boundsToUse => {
  const center = [0, 0, 0];
  center[0] = (boundsToUse[0] + boundsToUse[1]) / 2.0;
  center[1] = (boundsToUse[2] + boundsToUse[3]) / 2.0;
  center[2] = (boundsToUse[4] + boundsToUse[5]) / 2.0;

  let w1 = boundsToUse[1] - boundsToUse[0];
  let w2 = boundsToUse[3] - boundsToUse[2];
  let w3 = boundsToUse[5] - boundsToUse[4];
  w1 *= w1;
  w2 *= w2;
  w3 *= w3;
  let radius = w1 + w2 + w3;

  // If we have just a single point, pick a radius of 1.0
  radius = radius === 0 ? 1.0 : radius;

  // compute the radius of the enclosing sphere
  radius = Math.sqrt(radius) * 0.5;
  const parallelScale = radius;
  return parallelScale;
};

/** Resize IMage to viewport
 *
 * @param {Object} api
 */
export function fitToViewport(api) {
  const genericRenderWindow = api.genericRenderWindow;
  const renderer = genericRenderWindow.getRenderer();
  const camera = renderer.getActiveCamera();

  const boundsToUse = renderer.computeVisiblePropBounds();
  const parallelScale = getViewportFitScale(boundsToUse);
  camera.setParallelScale(parallelScale);

  genericRenderWindow.getRenderWindow().render();
}

import setLayoutAndViewportData from './setLayoutAndViewportData.js';

export default function setMultiViewMPRLayout(
  displaySets,
  viewportPropsArray,
  numRows = 1,
  numColumns = 1,
  isFusionMode = false
) {
  return new Promise((resolve, reject) => {
    const viewports = [];
    const numViewports = numRows * numColumns;

    if (viewportPropsArray && viewportPropsArray.length !== numViewports) {
      reject(
        new Error(
          'viewportProps is supplied but its length is not equal to numViewports'
        )
      );
    }

    const viewportSpecificData = {};

    for (let i = 0; i < numViewports; i++) {
      viewports.push({});
      viewportSpecificData[i] = displaySets[Math.floor(i / numColumns)];
      viewportSpecificData[i].plugin = 'vtk';
    }

    const mprApis = {};
    viewports.forEach((viewport, index) => {
      const seriesInstanceUID = viewportSpecificData[index].SeriesInstanceUID;
      if (!mprApis[seriesInstanceUID]) {
        mprApis[seriesInstanceUID] = [];
      }
      mprApis[seriesInstanceUID][index % numColumns] = null;
      const viewportProps = viewportPropsArray[index];
      viewports[index] = Object.assign({}, viewports[index], {
        vtk: {
          mode: 'mpr',
          isFusionMode,
          afterCreation: api => {
            mprApis[seriesInstanceUID][index % numColumns] = api;
            let allApis = [];
            Object.keys(mprApis).forEach(key => {
              const apis = mprApis[key];
              allApis = allApis.concat(apis);
            });
            if (allApis.every(a => !!a)) {
              resolve(mprApis);
            }
          },
          ...viewportProps,
        },
      });
    });

    setLayoutAndViewportData(
      {
        numRows,
        numColumns,
        viewports,
      },
      viewportSpecificData
    );
  });
}

import { getPosition } from './vtkJsInteractorSupport';

/**
 * Get the display state of the 2D MPR viewports
 * @param {Object} mprApis
 * @param {Number} viewPortIndex
 * @param {Object} viewports
 * @returns display state object with layout information for each series(if multi-session)
 */
export function getCurrentDisplayState(mprApis, viewPortIndex, viewports) {
  const restoreLayoutData = [];
  Object.keys(mprApis).forEach(key => {
    const apis = mprApis[key];
    const layoutData = {
      isCrosshairEnabled: (
        apis[0].svgWidgets.rotatableCrosshairsWidget ||
        apis[0].svgWidgets.crosshairsWidget
      )?.getDisplay(),
      orientation: [],
      slice: {},
      parallelScale: 0,
      position: {},
      numberOfLayout: viewports.layout.viewports.length,
      slabThickness: 0,
      worldPos: [],
      activeCrossHairViewports: [],
      allCrossHairViewportsActive: true,
    };
    apis.forEach((api, apiIndex) => {
      const apiWidget =
        api.svgWidgets.rotatableCrosshairsWidget ||
        api.svgWidgets.crosshairsWidget;
      const renderWindow = api.genericRenderWindow.getRenderWindow();
      const renderer = api.genericRenderWindow.getRenderer();
      const currentIStyle = renderWindow.getInteractor().getInteractorStyle();
      layoutData.parallelScale = renderer.getActiveCamera().getParallelScale();
      if (viewPortIndex === apiIndex) {
        layoutData.slice = currentIStyle.getSlice();
        const position = getPosition(api, renderer);
        layoutData.position = position;
      }
      layoutData.orientation.push({
        sliceNormal: currentIStyle.getSliceNormal(),
        viewUp: currentIStyle.getViewUp(),
      });
      layoutData.slabThickness = api.getSlabThickness();
      layoutData.worldPos = api.get('cachedCrosshairWorldPosition');
      const isCrossHairViewportActive = apiWidget.getViewportActive?.();
      layoutData.activeCrossHairViewports.push(isCrossHairViewportActive);
      layoutData.allCrossHairViewportsActive = isCrossHairViewportActive
        ? layoutData.allCrossHairViewportsActive
        : false;
    });
    layoutData.activeViewportIndex = viewports.activeViewportIndex;
    const activeViewportSpecificData = [];
    for (let i = 0; i < viewports.layout.viewports.length; i++) {
      if (viewports.viewportSpecificData[i].SeriesInstanceUID === key) {
        activeViewportSpecificData.push(viewports.viewportSpecificData[i]);
      }
    }
    layoutData.activeDisplaySet = activeViewportSpecificData[0];
    restoreLayoutData.push(layoutData);
  });
  return restoreLayoutData;
}

export function applyDisplayState(restoreLayoutData) {
  // Need to implement if required
}

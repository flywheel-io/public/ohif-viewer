import macro from 'vtk.js/Sources/macro';
import Constants from 'vtk.js/Sources/Rendering/Core/InteractorStyle/Constants';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import vtkMatrixBuilder from 'vtk.js/Sources/Common/Core/MatrixBuilder';
import { vec2 } from 'gl-matrix';
import vtkMath from 'vtk.js/Sources/Common/Core/Math';
import { vtkInteractorStyleRotatableMPRCrosshairs } from 'react-vtkjs-viewport';
import { distanceFromLine } from './utils/vtkJsInteractorSupport';
import store from '@ohif/viewer/src/store';

const { States } = Constants;

const operations = {
  ROTATE_CROSSHAIRS: 0,
  MOVE_CROSSHAIRS: 1,
  MOVE_REFERENCE_LINE: 2,
  PAN: 3,
  ACTIVATE_VIEWPORT: 4,
};

// ----------------------------------------------------------------------------
// Global methods
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// vtkInteractorStyleDetachedRotatableMPRCrosshairs methods
// ----------------------------------------------------------------------------

function vtkInteractorStyleDetachedRotatableMPRCrosshairs(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyleDetachedRotatableMPRCrosshairs');

  // Gets crosshair widget line indexes in an api, which represents active viewport/s.
  function getActiveApiLines(curApiIndex, apis) {
    const activeApiLines = [];
    apis.forEach((api, index) => {
      if (
        index !== curApiIndex &&
        api.svgWidgets.rotatableCrosshairsWidget.getViewportActive()
      ) {
        switch (index) {
          case 0: {
            activeApiLines.push(0);
            break;
          }
          case 1: {
            if (curApiIndex === 0) {
              activeApiLines.push(0);
            } else {
              activeApiLines.push(1);
            }
            break;
          }
          case 2: {
            activeApiLines.push(1);
            break;
          }
        }
      }
    });
    return activeApiLines;
  }

  // Returns coefficients of line(ax+by+c = 0) passing through a point and parallel to another line.
  function getLineCoefficients(parallelLinePoints, pointOnLine) {
    const xDiff = parallelLinePoints[1].x - parallelLinePoints[0].x;
    const yDiff = parallelLinePoints[1].y - parallelLinePoints[0].y;
    let a = 0;
    let b = 0;
    let c = 0;

    if (xDiff === 0) {
      a = 1;
      b = 0;
      c = -pointOnLine.x;
    } else if (yDiff === 0) {
      a = 0;
      b = 1;
      c = -pointOnLine.y;
    } else {
      const slope = yDiff / xDiff;
      a = slope;
      b = -1;
      c = pointOnLine.y - slope * pointOnLine.x;
    }
    return { a, b, c };
  }

  function selectOpperation(callData) {
    const { apis, apiIndex, lineGrabDistance } = model;
    const thisApi = apis[apiIndex];
    let { position } = callData;

    setOtherApisInactive();

    const { rotatableCrosshairsWidget } = thisApi.svgWidgets;

    if (!rotatableCrosshairsWidget) {
      throw new Error(
        'Must use rotatable crosshair svg widget with this istyle.'
      );
    }

    const { svgWidgetManager } = thisApi;
    const size = svgWidgetManager.getSize();
    const scale = svgWidgetManager.getScale();
    const height = size[1];

    //If dissociate Crosshair is enabled and  the click is on the top right circle, activate that viewport.
    const projectConfig = store.getState()?.flywheel?.projectConfig;
    const dissociateCrosshairs =
      projectConfig?.dissociateCrosshairs === undefined
        ? true
        : projectConfig?.dissociateCrosshairs;

    if (dissociateCrosshairs) {
      const { center, radius } = rotatableCrosshairsWidget.getWidgetCircle();
      const distFromBtn = vec2.distance(
        [center.x, center.y],
        [position.x, height - position.y]
      );
      if (distFromBtn <= radius) {
        model.operation = { type: operations.ACTIVATE_VIEWPORT };
        return;
      }
    }

    const lines = rotatableCrosshairsWidget.getReferenceLines();
    const point = rotatableCrosshairsWidget.getPoint();
    const centerRadius = rotatableCrosshairsWidget.getCenterRadius();

    const distanceFromCenter = vec2.distance(point, [position.x, position.y]);

    if (distanceFromCenter < centerRadius) {
      // Click on center -> move the crosshairs.
      model.operation = { type: operations.MOVE_CROSSHAIRS };

      lines.forEach(line => {
        line.selected = true;
      });

      return;
    }

    // Map to the click point to the same coords as the SVG.
    const p = { x: position.x * scale, y: height - position.y * scale };
    const activeLines = getActiveApiLines(apiIndex, apis);

    // Check each rotation handle
    for (let lineIndex = 0; lineIndex < lines.length; lineIndex++) {
      const line = lines[lineIndex];
      const lineRotateHandles = line.rotateHandles;
      const { points } = lineRotateHandles;

      for (let i = 0; i < points.length; i++) {
        const distance = vec2.distance([points[i].x, points[i].y], [p.x, p.y]);

        if (distance < lineGrabDistance && activeLines.includes(lineIndex)) {
          model.operation = {
            type: operations.ROTATE_CROSSHAIRS,
            prevPosition: position,
          };

          lineRotateHandles.selected = true;

          // Set this line active.
          if (lineIndex === 0) {
            lines[0].active = true;
            lines[1].active = false;
          } else {
            lines[0].active = false;
            lines[1].active = true;
          }

          return;
        }
      }
    }

    const distanceFromFirstLine = distanceFromLine(lines[0], p); //position);
    const distanceFromSecondLine = distanceFromLine(lines[1], p); //;

    if (
      distanceFromFirstLine <= lineGrabDistance ||
      distanceFromSecondLine <= lineGrabDistance
    ) {
      // Click on line -> start a rotate of the other planes.

      const selectedLineIndex =
        distanceFromFirstLine < distanceFromSecondLine ? 0 : 1;

      lines[selectedLineIndex].selected = true;
      lines[selectedLineIndex].active = true;

      // Deactivate other line if active
      const otherLineIndex = selectedLineIndex === 0 ? 1 : 0;

      lines[otherLineIndex].active = false;

      // Set operation data.

      model.operation = {
        type: operations.MOVE_REFERENCE_LINE,
        snapToLineIndex: selectedLineIndex === 0 ? 1 : 0,
      };

      return;
    }

    lines.forEach(line => {
      line.active = false;
    });

    setOtherApisInactive();

    // What is the fallback? Pan? Do nothing for now.
    model.operation = { type: null };
  }

  function setOtherApisInactive() {
    // Set other apis inactive

    const { apis, apiIndex } = model;

    apis.forEach((api, index) => {
      if (index !== apiIndex) {
        const { rotatableCrosshairsWidget } = api.svgWidgets;

        if (!rotatableCrosshairsWidget) {
          throw new Error(
            'Must use rotatable crosshair svg widget with this istyle.'
          );
        }

        const lines = rotatableCrosshairsWidget.getReferenceLines();

        lines[0].active = false;
        lines[1].active = false;
      }
    });
  }

  function performOperation(callData) {
    const { operation } = model;
    const { type } = operation;

    let snapToLineIndex;
    let pos;

    switch (type) {
      case operations.MOVE_CROSSHAIRS:
        moveCrosshairs(callData.position, callData.pokedRenderer);
        break;
      case operations.MOVE_REFERENCE_LINE:
        snapToLineIndex = operation.snapToLineIndex;
        pos = snapPosToLine(callData.position, snapToLineIndex);

        moveCrosshairs(pos, callData.pokedRenderer);
        break;
      case operations.ROTATE_CROSSHAIRS:
        rotateCrosshairs(callData);
        break;
      case operations.ACTIVATE_VIEWPORT:
        activateViewport(callData);
        break;
    }
  }

  function activateViewport(callData) {
    const { apis, apiIndex } = model;
    apis.forEach((api, index) => {
      const isActive = apiIndex === index ? true : false;
      api.svgWidgets.rotatableCrosshairsWidget.setViewportActive(isActive);
      api.svgWidgetManager.render();
    });
  }

  function rotateCrosshairs(callData) {
    const { operation } = model;
    const { prevPosition } = operation;

    const newPosition = callData.position;

    if (newPosition.x === prevPosition.x && newPosition.y === prevPosition.y) {
      // No change, exit early.
      return;
    }

    const { apis, apiIndex } = model;
    const thisApi = apis[apiIndex];

    const { rotatableCrosshairsWidget } = thisApi.svgWidgets;

    const point = rotatableCrosshairsWidget.getPoint();

    let pointToPreviousPosition = [];
    let pointToNewPosition = [];

    // Get vector from center of crosshairs to previous position.
    vec2.subtract(
      pointToPreviousPosition,
      [prevPosition.x, prevPosition.y],
      point
    );

    // Get vector from center of crosshairs to new position.
    vec2.subtract(pointToNewPosition, [newPosition.x, newPosition.y], point);

    // Get angle of rotation from previous reference line position to the new position.
    let angle = vec2.angle(pointToPreviousPosition, pointToNewPosition);

    // Use the determinant to find the sign of the angle.
    const determinant =
      pointToNewPosition[0] * pointToPreviousPosition[1] -
      pointToNewPosition[1] * pointToPreviousPosition[0];

    if (determinant > 0) {
      angle *= -1;
    }

    // Axis is the opposite direction of the plane normal for this API.
    const sliceNormal = thisApi.getSliceNormal();
    const axis = [-sliceNormal[0], -sliceNormal[1], -sliceNormal[2]];

    //Rotate active Apis
    const activeApis = apis.filter(
      (api, index) =>
        index !== apiIndex &&
        api.svgWidgets.rotatableCrosshairsWidget.getViewportActive()
    );
    activeApis.forEach((api, index) => {
      const cameraForApi = api.genericRenderWindow
        .getRenderWindow()
        .getInteractor()
        .getCurrentRenderer()
        .getActiveCamera();

      const crosshairPointForApi = api.get('cachedCrosshairWorldPosition');
      const initialCrosshairPointForApi = api.get(
        'initialCachedCrosshairWorldPosition'
      );

      const center = [];
      vtkMath.subtract(
        crosshairPointForApi,
        initialCrosshairPointForApi,
        center
      );
      const translate = [];
      vtkMath.add(crosshairPointForApi, center, translate);

      const { matrix } = vtkMatrixBuilder
        .buildFromRadian()
        .translate(translate[0], translate[1], translate[2])
        .rotate(angle, axis)
        .translate(-translate[0], -translate[1], -translate[2]);

      cameraForApi.applyTransform(matrix);

      const sliceNormalForApi = api.getSliceNormal();
      const viewUpForApi = api.getViewUp();
      api.setOrientation(sliceNormalForApi, viewUpForApi);
    });

    updateCrosshairs(callData);

    /*
    After the rotations and update of the crosshairs, the focal point of the
    camera has a shift along the line of sight coordinate respect to the
    crosshair (i.e., the focal point is not on the same slice of the crosshair).
    We calculate the new focal point coordinates as the nearest point between
    the line of sight of the camera and the crosshair coordinates:

      p1 = cameraPositionForApi
      p2 = cameraFocalPointForApi
      q = crosshairPointForApi

      Vector3 u = p2 - p1;
      Vector3 pq = q - p1;
      Vector3 w2 = pq - vtkMath.multiplyScalar(u, vtkMath.dot(pq, u) / u2);

      Vector3 newFocalPoint = q - w2;
    */

    apis.forEach(api => {
      const cameraForApi = api.genericRenderWindow
        .getRenderWindow()
        .getInteractor()
        .getCurrentRenderer()
        .getActiveCamera();

      const crosshairPointForApi = api.get('cachedCrosshairWorldPosition');
      const cameraFocalPointForApi = cameraForApi.getFocalPoint();
      const cameraPositionForApi = cameraForApi.getPosition();

      const u = [];
      vtkMath.subtract(cameraFocalPointForApi, cameraPositionForApi, u);
      const pq = [];
      vtkMath.subtract(crosshairPointForApi, cameraPositionForApi, pq);
      const uLength2 = u[0] * u[0] + u[1] * u[1] + u[2] * u[2];
      vtkMath.multiplyScalar(u, vtkMath.dot(pq, u) / uLength2);
      const w2 = [];
      vtkMath.subtract(pq, u, w2);
      const newFocalPointForApi = [];
      vtkMath.subtract(crosshairPointForApi, w2, newFocalPointForApi);

      cameraForApi.setFocalPoint(
        newFocalPointForApi[0],
        newFocalPointForApi[1],
        newFocalPointForApi[2]
      );
    });

    operation.prevPosition = newPosition;
  }

  function updateCrosshairs(callData) {
    const { apis, apiIndex } = model;
    const thisApi = apis[apiIndex];

    const { rotatableCrosshairsWidget } = thisApi.svgWidgets;

    const worldPos = thisApi.get('cachedCrosshairWorldPosition');

    rotatableCrosshairsWidget.moveCrosshairs(worldPos, apis, apiIndex);
  }

  function snapPosToLine(position, lineIndex) {
    const { apis, apiIndex } = model;
    const thisApi = apis[apiIndex];
    const { svgWidgetManager } = thisApi;
    const size = svgWidgetManager.getSize();
    const scale = svgWidgetManager.getScale();
    const height = size[1];

    // Map to the click point to the same coords as the SVG.
    const p = { x: position.x * scale, y: height - position.y * scale };

    const { rotatableCrosshairsWidget } = thisApi.svgWidgets;
    const lines = rotatableCrosshairsWidget.getReferenceLines();

    // Get the point of intersection between dragged line through P and other line
    // to get new crosshair center.
    const index = lineIndex === 0 ? 1 : 0;
    const line1 = getLineCoefficients(lines[index].points, p);
    const line2 = getLineCoefficients(
      lines[lineIndex].points,
      lines[lineIndex].points[0]
    );

    const pOIx =
      (line1.b * line2.c - line2.b * line1.c) /
      (line1.a * line2.b - line2.a * line1.b);
    const pOIy =
      (line2.a * line1.c - line1.a * line2.c) /
      (line1.a * line2.b - line2.a * line1.b);
    return { x: pOIx / scale, y: (height - pOIy) / scale };
  }

  function moveCrosshairs(pos, renderer) {
    const { apis, apiIndex } = model;
    const thisApi = apis[apiIndex];

    const dPos = vtkCoordinate.newInstance();
    dPos.setCoordinateSystemToDisplay();

    dPos.setValue(pos.x, pos.y, 0);
    let worldPos = dPos.getComputedWorldValue(renderer);

    const camera = renderer.getActiveCamera();
    const directionOfProjection = camera.getDirectionOfProjection();

    const halfSlabThickness = thisApi.getSlabThickness() / 2;

    // Add half of the slab thickness to the world position, such that we select
    // The center of the slice.

    for (let i = 0; i < worldPos.length; i++) {
      worldPos[i] += halfSlabThickness * directionOfProjection[i];
    }

    thisApi.svgWidgets.rotatableCrosshairsWidget.moveCrosshairs(
      worldPos,
      apis,
      apiIndex
    );

    publicAPI.invokeInteractionEvent({ type: 'InteractionEvent' });
  }

  function handlePassiveMouseMove(callData) {
    const { apis, apiIndex, lineGrabDistance } = model;
    const thisApi = apis[apiIndex];
    let { position } = callData;

    // Note: If rotate selected, don't select line.
    const selectedRotateHandles = [false, false];

    const selectedLines = [false, false];

    const { rotatableCrosshairsWidget } = thisApi.svgWidgets;

    if (!rotatableCrosshairsWidget) {
      throw new Error(
        'Must use rotatable crosshair svg widget with this istyle.'
      );
    }

    let shouldUpdate;

    const lines = rotatableCrosshairsWidget.getReferenceLines();
    const point = rotatableCrosshairsWidget.getPoint();
    const centerRadius = rotatableCrosshairsWidget.getCenterRadius();

    const distanceFromCenter = vec2.distance(point, [position.x, position.y]);

    if (distanceFromCenter > centerRadius) {
      const { svgWidgetManager } = thisApi;
      const size = svgWidgetManager.getSize();
      const scale = svgWidgetManager.getScale();
      const height = size[1];

      // Map to the click point to the same coords as the SVG.
      const p = { x: position.x * scale, y: height - position.y * scale };

      let selectedRotationHandle = false;

      // Check each rotation handle
      for (let lineIndex = 0; lineIndex < lines.length; lineIndex++) {
        const line = lines[lineIndex];
        const lineRotateHandles = line.rotateHandles;
        const { points } = lineRotateHandles;

        for (let i = 0; i < points.length; i++) {
          const distance = vec2.distance(
            [points[i].x, points[i].y],
            [p.x, p.y]
          );

          if (distance < lineGrabDistance) {
            selectedRotateHandles[lineIndex] = true;
            selectedRotationHandle = true;
            // Don't need to check both points if one is found to be valid.
            break;
          }
        }
      }

      // If a rotation handle isn't selected, see if we should select lines.
      if (!selectedRotationHandle) {
        const distanceFromFirstLine = distanceFromLine(lines[0], p);
        const distanceFromSecondLine = distanceFromLine(lines[1], p);

        if (
          distanceFromFirstLine <= lineGrabDistance ||
          distanceFromSecondLine <= lineGrabDistance
        ) {
          const selectedLineIndex =
            distanceFromFirstLine < distanceFromSecondLine ? 0 : 1;

          selectedLines[selectedLineIndex] = true;
        }
      }
    } else {
      // Highlight both lines.
      selectedLines[0] = true;
      selectedLines[1] = true;
    }

    lines.forEach((line, index) => {
      const selected = selectedLines[index];
      const rotateSelected = selectedRotateHandles[index];
      const rotateHandles = line.rotateHandles;

      // If changed, update and flag should update.
      if (line.selected !== selected) {
        line.selected = selected;
        shouldUpdate = true;
      }

      if (rotateHandles.selected !== rotateSelected) {
        rotateHandles.selected = rotateSelected;
        shouldUpdate = true;
      }
    });

    if (shouldUpdate) {
      updateCrosshairs(callData);
    }
  }

  const superHandleMouseMove = publicAPI.handleMouseMove;
  publicAPI.handleMouseMove = callData => {
    if (model.operation.type !== operations.ACTIVATE_VIEWPORT) {
      if (model.state === States.IS_WINDOW_LEVEL) {
        performOperation(callData);
      }

      if (superHandleMouseMove) {
        superHandleMouseMove(callData);
      }
    }
  };

  //const superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = callData => {
    if (model.volumeActor) {
      selectOpperation(callData);
      performOperation(callData);

      if (model.operation.type !== operations.ACTIVATE_VIEWPORT) {
        publicAPI.startWindowLevel();
      }
    }
  };

  publicAPI.superHandleLeftButtonRelease = publicAPI.handleLeftButtonRelease;
  publicAPI.handleLeftButtonRelease = callData => {
    if (model.operation.type === operations.ACTIVATE_VIEWPORT) {
      return;
    }
    switch (model.state) {
      case States.IS_WINDOW_LEVEL:
        mouseUp(callData);
        break;

      default:
        publicAPI.superHandleLeftButtonRelease();
        break;
    }
  };

  function mouseUp(callData) {
    model.operation = { type: null };

    // Unselect lines
    const { apis, apiIndex } = model;
    const thisApi = apis[apiIndex];
    const { rotatableCrosshairsWidget } = thisApi.svgWidgets;

    if (!rotatableCrosshairsWidget) {
      throw new Error(
        'Must use rotatable crosshair svg widget with this istyle.'
      );
    }

    const lines = rotatableCrosshairsWidget.getReferenceLines();

    lines.forEach(line => {
      line.selected = false;
      line.rotateHandles.selected = false;
    });

    updateCrosshairs(callData);

    publicAPI.endWindowLevel();
  }
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {
  operation: { type: null },
  lineGrabDistance: 20,
  disableNormalMPRScroll: false,
};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleRotatableMPRCrosshairs.extend(
    publicAPI,
    model,
    initialValues
  );

  // Object specific methods
  vtkInteractorStyleDetachedRotatableMPRCrosshairs(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyleDetachedRotatableMPRCrosshairs'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });

import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { microscopyEventRegister } from './viewportInstance';
import { toolProp, emptyStyle } from './utils/toolUtils';
import undoRedoHandlers from './utils/undoRedoHandlers';
import { onDrawStarted, onDrawAborted } from './utils/handleMeasurementEvents';
import { getAllViewports } from './activeMicroscopyViewports';
import { globalDicomMicroscopyToolStateManager } from './toolStateManager';

const { getUuid } = FlywheelCommonUtils;

const deactivateInteraction = (viewportInstance, type) => {
  viewportInstance.deactivateDrawInteraction();
  if (type === 'pan') {
    viewportInstance.deactivateDragZoomInteraction();
  } else if (type === 'zoom') {
    viewportInstance.deactivateDragPanInteraction();
  } else {
    viewportInstance.deactivateDragZoomInteraction();
    viewportInstance.deactivateDragPanInteraction();
  }
};

const activateDrawInteraction = (viewportInstance, options) => {
  options = { ...options, getUid: getUuid };
  viewportInstance.activateDrawInteraction(options);
  const viewerSymbols = Object.getOwnPropertySymbols(viewportInstance);
  const interactionsSymbol = viewerSymbols?.find(
    x => String(x) === 'Symbol(interactions)'
  );
  const interactions = viewportInstance[interactionsSymbol];

  interactions?.draw?.on('drawstart', onDrawStarted);
  interactions?.draw?.on('drawabort', onDrawAborted);
};

const commandsModule = ({ servicesManager }) => {
  const actions = {
    setZoomActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }
      const options = {
        bindings: {
          mouseButtons: ['left'],
        },
      };
      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'zoom');
        viewport.viewerInstance.activateDragZoomInteraction(options);
      });
    },

    setPanActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }
      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'pan');
        viewport.viewerInstance.activateDragPanInteraction();
      });
    },

    setEllipticalActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }

      let options = toolProp();
      options.geometryType = 'ellipse';
      options.markup = 'measurement';
      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'tool');
        activateDrawInteraction(viewport.viewerInstance, options);
      });
    },

    setRectangleActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }

      let options = toolProp();
      options.geometryType = 'rectangle';
      options.markup = 'measurement';
      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'tool');
        activateDrawInteraction(viewport.viewerInstance, options);
      });
    },
    setCircleActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }

      let options = toolProp();
      options.geometryType = 'circle';
      options.markup = 'measurement';
      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'tool');
        activateDrawInteraction(viewport.viewerInstance, options);
      });
    },

    setFreehandActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }

      let options = toolProp();
      options.geometryType = 'freehandpolygon';
      options.markup = 'measurement';
      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'tool');
        activateDrawInteraction(viewport.viewerInstance, options);
      });
    },

    setLengthActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }

      let options = toolProp();
      options.geometryType = 'line';
      options.markup = 'measurement';
      options.maxPoints = 1;
      options.minPoints = 1;
      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'tool');
        activateDrawInteraction(viewport.viewerInstance, options);
      });
    },

    setAnnotateActive: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setToolActive command');
      }

      const geometryType = 'length';
      const toolToGeometryType = {
        /** Annotations */
        length: 'line',
      };

      const toolToDrawOptions = {
        length: {
          marker: 'arrow',
          markup: '',
          maxPoints: 1,
          minPoints: 1,
          vertexEnabled: false,
        },
      };
      const drawOptions = toolToDrawOptions[geometryType] || {};
      const options = emptyStyle;

      getAllViewports().forEach(viewport => {
        deactivateInteraction(viewport.viewerInstance, 'tool');
        activateDrawInteraction(viewport.viewerInstance, {
          geometryType: toolToGeometryType[geometryType],
          styleOptions: options,
          ...drawOptions,
        });
      });
    },

    performUndo: ({ viewports }) => {
      // Will be supported as part of FLYW 10884
      return;
      // Unbind event handlers for updates as part of undo to avoid unwanted event handling
      microscopyEventRegister.unBindEvents();
      undoRedoHandlers.handleUndoRedoOperation(true, viewports);
      // Bind the events after the undo
      microscopyEventRegister.bindEvents();
    },
    performRedo: ({ viewports }) => {
      // Will be supported as part of FLYW 10884
      return;
      // Unbind event handlers for updates as part of redo to avoid unwanted event handling
      microscopyEventRegister.unBindEvents();
      undoRedoHandlers.handleUndoRedoOperation(false, viewports);
      // Bind the events after the redo
      microscopyEventRegister.bindEvents();
    },
    clearMicroscopyToolState: () => {
      globalDicomMicroscopyToolStateManager.clearToolState();
    },
  };

  const definitions = {
    setZoomActive: {
      commandFn: actions.setZoomActive,
      storeContexts: [],
      options: {},
    },
    setPanActive: {
      commandFn: actions.setPanActive,
      storeContexts: [],
      options: {},
    },
    setEllipticalActive: {
      commandFn: actions.setEllipticalActive,
      storeContexts: [],
      options: {},
    },
    setCircleActive: {
      commandFn: actions.setCircleActive,
      storeContexts: [],
      options: {},
    },
    setRectangleActive: {
      commandFn: actions.setRectangleActive,
      storeContexts: [],
      options: {},
    },
    setFreehandActive: {
      commandFn: actions.setFreehandActive,
      storeContexts: [],
      options: {},
    },
    setLengthActive: {
      commandFn: actions.setLengthActive,
      storeContexts: [],
      options: {},
    },
    setAnnotateActive: {
      commandFn: actions.setAnnotateActive,
      storeContexts: [],
      options: {},
    },
    performUndo: {
      commandFn: actions.performUndo,
      storeContexts: ['viewports'],
      options: {},
    },
    performRedo: {
      commandFn: actions.performRedo,
      storeContexts: ['viewports'],
      options: {},
    },
    clearMicroscopyToolState: {
      commandFn: actions.clearMicroscopyToolState,
      storeContexts: [],
      options: {},
    },
  };

  return {
    actions,
    definitions,
    defaultContext: 'ACTIVE_VIEWPORT::MICROSCOPY',
  };
};

export default commandsModule;

import actions from './actions';
import reducers from './reducers';

const redux = {
  actions,
  reducers,
};

export default redux;

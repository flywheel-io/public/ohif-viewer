import _MarkupManager from 'dicom-microscopy-viewer/src/annotations/markups/_MarkupManager'
import { ToolsUtils } from '@flywheel/extension-flywheel-common'
import { getFWToolType } from '../tools/utils';
import { isRoiTool, isMeasurementTool } from './toolUtils';

const { isRoiOutputHidden, isMeasurementOutputHidden } = ToolsUtils.unitUtils;

export class MarkupManager extends _MarkupManager {

  constructor(props = {}) {
    super(props);
  }

  create({
    feature,
    target,
    value = "",
    isLinkable = false,
    isDraggable = true,
  }) {
    const toolType = getFWToolType(feature);
    const isOutputHidden = (isRoiTool(toolType) && isRoiOutputHidden())
      || (isMeasurementTool(toolType) && isMeasurementOutputHidden())
    if (isOutputHidden) {
      return;
    }

    super.create({
      feature,
      target,
      value,
      isLinkable,
      isDraggable
    });
  }
}

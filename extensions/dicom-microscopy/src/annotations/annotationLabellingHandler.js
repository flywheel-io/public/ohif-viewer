import store from '@ohif/viewer/src/store';
import {
  getToolLabellingFlowCallback
} from '@ohif/viewer/src/appExtensions/MeasurementsPanel/labelingFlowCallbacks.js';
import { getViewport } from '../activeMicroscopyViewports';

// This is not used as of now. Need to recheck while implementing contour
// overlapping in microscopy.
const callContourCollisionDialog = (measurementData, overlappedMeasurements) => {
  const { UIDialogService } = ServicesManager.services;
  UIDialogService.dismiss({ id: 'collisionError' });
  const dialogId = UIDialogService.create({
    id: 'collisionError',
    content: FlywheelComponents.ContourCollisionDialog,
    defaultPosition: { x: 420, y: 420 },
    showOverlay: true,
    contentProps: {
      measurementData: { ...measurementData },
      overlappedMeasurements: overlappedMeasurements,
      label:
        'Overlapping contours are not allowed based on viewer configuration. How would you like to proceed?',
      onClose: () => UIDialogService.dismiss({ id: 'collisionError' }),
    },
  });
}

export const addLabel = (event, measurement) => {
  const element = event.currentTarget;
  const doneCallback = {};

  const eventData = {
    event: {
      clientX: element.clientWidth,
      clientY: element.clientHeight,
    },
    element: element,
  };

  const options = {
    skipAddLabelButton: true,
    editLocation: true,
  };
  const toolLabellingFlowCallback = getToolLabellingFlowCallback(callContourCollisionDialog, store);
  toolLabellingFlowCallback(measurement, eventData, doneCallback, options);
}

export const editLabel = (event, measurement, options, viewportsState) => {

  const activeViewportIndex =
    (viewportsState && viewportsState.activeViewportIndex) || 0;

  const { container } = getViewport(activeViewportIndex);
  const doneCallback = {};

  const eventData = {
    event: {
      clientX: event.clientX,
      clientY: event.clientY,
    },
    element: container,
  };
  const toolLabellingFlowCallback = getToolLabellingFlowCallback(callContourCollisionDialog, store);
  toolLabellingFlowCallback(measurement, eventData, doneCallback, options);
}

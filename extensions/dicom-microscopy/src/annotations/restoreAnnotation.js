import store from '@ohif/viewer/src/store';
import { measurements } from '@ohif/core';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import { getToolInstance } from '../tools/utils';
import { getViewport } from '../activeMicroscopyViewports';

const restoreAnnotations = () => {
  const viewportSpecificData =
    store.getState().viewports?.viewportSpecificData || {};
  Object.keys(viewportSpecificData).forEach((dataKey, viewportIndex) => {
    const viewportData = viewportSpecificData[dataKey];
    let microscopyViewportDetails = getViewport(viewportIndex);

    // By the time of this call, viewport may not be loaded with the image,
    // hence checks in an interval and draws annotations when the image is loaded.
    const restoreAnnotationInterval = setInterval(() => {
      if (microscopyViewportDetails) {
        clearInterval(restoreAnnotationInterval);
        drawAnnotations(microscopyViewportDetails, viewportData);
      } else {
        microscopyViewportDetails = getViewport(viewportIndex);
      }
    }, 1000);
  });
};

const restoreAnnotationsOnViewport = viewportIndex => {
  const viewportSpecificData =
    store.getState().viewports?.viewportSpecificData || {};
  const viewportData = viewportSpecificData[viewportIndex];
  if (viewportData) {
    const microscopyViewportDetails = getViewport(viewportIndex);
    drawAnnotations(microscopyViewportDetails, viewportData);
  }
};

const drawAnnotations = (microscopyViewportDetails, viewportData) => {
  const viewerInstance = microscopyViewportDetails.viewerInstance;
  const imagePath = [
    viewportData.StudyInstanceUID,
    viewportData.SeriesInstanceUID,
    viewportData.SOPInstanceUID,
    0,
  ].join('$$$');
  const imageId = measurements.getImageIdForImagePath(imagePath);
  const imageToolState =
    globalDicomMicroscopyToolStateManager.getImageToolState(imageId) || {};

  Object.keys(imageToolState).forEach(tool => {
    imageToolState[tool]?.data.forEach(data => {
      const roi = viewerInstance.getROI(data.uuid);
      if (!roi && data.visible) {
        const toolInstance = getToolInstance(data.toolType);
        toolInstance?.createRoi(data, viewerInstance);
      }
    });
  });
};

export { restoreAnnotations, restoreAnnotationsOnViewport }

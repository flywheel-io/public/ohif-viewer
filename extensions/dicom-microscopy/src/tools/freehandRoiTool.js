import { measurements } from '@ohif/core';
import { FeatureGeometryEvents, FeatureEvents } from 'dicom-microscopy-viewer/src/enums';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import baseAnnotationTool from "./baseAnnotationTool";
import { getSymbol, getPolyBoundingBox, getGeometryName } from "./utils";

export default class freehandRoiTool extends baseAnnotationTool {

  updateFWMeasurementHandles(measurementData, geometryData) {
    const points = [];
    const graphicData = geometryData.scoord3d.graphicData;
    const length = graphicData.length;

    graphicData.forEach((point, pointIndex) => {
      if (pointIndex === length - 1) {
        pointIndex = -1;
      }
      points.push({
        x: point[0],
        y: point[1],
        active: true,
        highlight: true,
        lines: [{
          x: graphicData[pointIndex + 1][0],
          y: graphicData[pointIndex + 1][1]
        }]
      });
    });

    const extent = geometryData.scoord3d.extent;
    if (extent) {
      measurementData.polyBoundingBox = getPolyBoundingBox(extent[0],
        extent[1], extent[3], extent[4]);
    }

    measurementData.handles = {
      points: points,
      textBox: this.createMeasurementTextBox(graphicData),
    }
  }

  createRoi(measurementData, viewerInstance) {
    const imageId = measurements.getImageIdForImagePath(measurementData.imagePath)
    const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId, measurementData.toolType, measurementData.uuid);

    if (smMeasurementData?.geometryData) {
      super.createRoi(smMeasurementData, viewerInstance);
    } else {
      const { handles } = measurementData;
      if (!Array.isArray(handles?.points)) {
        console.error('Measurement data handles are invalid.');
        return;
      }

      const roi = {};
      const styleOptions = {};

      roi.uid = measurementData.uuid;
      roi.properties = { markup: 'measurement' };
      roi.scoord3d = { graphicType: 'POLYGON' };
      roi.geometryName = getGeometryName(measurementData.toolType);
      let graphicData = [];

      const pointsCount = handles?.points?.length || 0;
      handles?.points?.forEach((point, pointIndex) => {
        graphicData.push([point.x, point.y, 0]);
        if (pointsCount - 1 === pointIndex) {
          graphicData.push([point.lines?.[0]?.x, point.lines?.[0]?.y, 0]);
        }
      });
      roi.scoord3d.graphicData = graphicData;

      styleOptions.fill = { color: measurementData.color };
      styleOptions.stroke = {
        color:
          measurementData.color.substring(
            0,
            measurementData.color.lastIndexOf(',')
          ) + ', 1)',
        width: 2,
      };

      roi.properties.styleOptions = styleOptions;
      viewerInstance.addROI(roi, styleOptions);
    }

    const drawingSource = viewerInstance[getSymbol(viewerInstance, 'drawingSource')];
    const annotationManager = viewerInstance[getSymbol(viewerInstance, 'annotationManager')];

    const feature = drawingSource.getFeatureById(measurementData.uuid);
    feature.set('markup', 'measurement');
    feature.getGeometry().dispatchEvent(FeatureGeometryEvents.CHANGE);
    annotationManager.onAdd(feature);
    feature.dispatchEvent(FeatureEvents.PROPERTY_CHANGE);
  }
}

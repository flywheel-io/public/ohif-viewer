/*
Start header
n_tags, uint32, number of tags. Number of tags should apparently be between 1 and 128.
If this is not true we just abort and move to csa_max_pos.
unused, uint32, apparently has value 77

Each tag
name : S64, null terminated string 64 bytes
vm : int32
vr : S4, first 3 characters only
syngodt : int32
nitems : int32
xx : int32 - apparently either 77 or 205
nitems gives the number of items in the tag. The items follow directly after the tag.
*/

import CSA from './csa';

export default class CSA1 extends CSA {
  //readHeader(){}
  readItemLength() {}
  itemLengthValidation(itemLength) {
    return false;
  }
}

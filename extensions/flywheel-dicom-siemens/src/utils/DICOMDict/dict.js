export const DICOMDescriptionsTag = {
  Rows: '00280010',
  Columns: '00280011',
  SOPClassUID: '00080016',
  SOPInstanceUID: '00080018',
  ReferencedSOPClassUID: '00081150',
  ReferencedSOPInstanceUID: '00081155',
  SourceImageSequence: '00082112',
  SourceInstanceSequence: '00420013',
  // private tags
  NumberOfImagesInMosaic: '0019100A',
  CSASequenceIndex: '00290001',
  InPlaneResolution: '0051100B',
};

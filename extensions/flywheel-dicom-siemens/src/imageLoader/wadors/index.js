import getPixelData from './getPixelData.js';
import loadImage from './loadImage.js';
import register from './register.js';
import mosaic from './mosaic';

const { clearAllCachedBlocks } = mosaic;
export default {
  getPixelData,
  loadImage,
  register,
  clearAllCachedBlocks,
};

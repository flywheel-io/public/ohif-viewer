import initExtension from './init';

export default {
  id: 'flywheel-zip-image',
  preRegistration({ commandsManager, servicesManager, appConfig }) {
    initExtension({ commandsManager, servicesManager, appConfig });
  },
};

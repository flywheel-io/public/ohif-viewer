/*
Copyright (c) MONAI Consortium
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import React from 'react';
import MonaiLabelPanel from './components/MonaiLabelPanel.js';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import CornerstoneInfusions from '@flywheel/extension-cornerstone-infusions';

const { selectProjectConfig } = FlywheelRedux.selectors;

function shouldEnableSegmentationPanel() {
  const config = selectProjectConfig(store.getState());
  return !!config?.enableSegmentationPanel;
}

const { getToolbarModule } = CornerstoneInfusions;
const panelModule = ({
  commandsManager,
  servicesManager,
  hotkeysManager,
  appConfig,
}) => {
  const infusionModules = getToolbarModule({
    commandsManager,
    servicesManager,
    hotkeysManager,
    appConfig,
  });

  const segmentationDefinitions = infusionModules.definitions.find(
    segment => segment.id === 'toggleSegmentation'
  );
  const ExtentedMonaiLabel = props => {
    return (
      <MonaiLabelPanel
        {...props}
        commandsManager={commandsManager}
        servicesManager={servicesManager}
        segmentationDefinitions={segmentationDefinitions}
      />
    );
  };

  return {
    menuOptions: [
      {
        icon: 'list',
        label: 'Segmentation',
        from: 'right',
        target: 'monai-label-panel',
        context: [
          'ACTIVE_VIEWPORT::CORNERSTONE',
          'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
          'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
        ],
        isVisible: shouldEnableSegmentationPanel,
      },
    ],
    components: [
      {
        id: 'monai-label-panel',
        component: ExtentedMonaiLabel,
      },
    ],
    defaultContext: ['VIEWER'],
  };
};

export default panelModule;

import { LinkSeriesTool } from '../tools';

const commandsModule = () => {
  const actions = {
    toggleScrollLink: LinkSeriesTool.tools.scrollLink.toggle.bind(
      LinkSeriesTool.tools.scrollLink
    ),
    toggleWwwcLink: LinkSeriesTool.tools.wwwcLink.toggle.bind(
      LinkSeriesTool.tools.wwwcLink
    ),
    toggleZoomLink: LinkSeriesTool.tools.zoomLink.toggle.bind(
      LinkSeriesTool.tools.zoomLink
    ),
    toggleOrientLink: LinkSeriesTool.tools.orientLink.toggle.bind(
      LinkSeriesTool.tools.orientLink
    ),
  };

  const definitions = {
    toggleScrollLink: {
      commandFn: actions.toggleScrollLink,
      storeContexts: [],
      options: {},
    },
    toggleWwwcLink: {
      commandFn: actions.toggleWwwcLink,
      storeContexts: [],
      options: {},
    },
    toggleZoomLink: {
      commandFn: actions.toggleZoomLink,
      storeContexts: [],
      options: {},
    },
    toggleOrientLink: {
      commandFn: actions.toggleOrientLink,
      storeContexts: [],
      options: {},
    },
  };

  return {
    actions,
    definitions,
  };
};

export default commandsModule;

import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';

let fileChooserDialogId = null;
const { SegmentationFileChooser } = FlywheelComponents;

const commandsModule = ({ appConfig, servicesManager, hotkeysManager }) => {
  const actions = {
    toggleSegmentationFileChooser: state => {
      const { UIDialogService } = servicesManager.services;

      if (fileChooserDialogId) {
        return;
      }

      fileChooserDialogId = UIDialogService.create({
        content: SegmentationFileChooser,
        defaultPosition: {
          x: window.innerWidth / 2 - 175,
          y: window.innerHeight / 2 - 220,
        },
        showOverlay: true,
        contentProps: {
          onClose: () => {
            UIDialogService.dismiss({ id: fileChooserDialogId });
            fileChooserDialogId = null;
          },
          onConfirm: () => {},
          hotkeysManager,
        },
      });
    },
  };

  const definitions = {
    toggleSegmentationFileChooser: {
      commandFn: actions.toggleSegmentationFileChooser,
      storeContexts: [],
      options: {},
    },
  };

  return {
    actions,
    definitions,
  };
};

export default commandsModule;

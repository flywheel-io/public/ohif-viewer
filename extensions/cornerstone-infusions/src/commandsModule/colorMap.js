import setColormapId from '../utils/setColormapId';
import OHIF from '@ohif/core';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const { utils } = OHIF;
const { CornerstoneColormapService } = utils;

const _updateViewportsColormap = (
  viewportIndex,
  nextColormap,
  nextColormapName,
  isReset
) => {
  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    viewportIndex
  );

  CornerstoneColormapService.onChangeColormap(
    element,
    isReset,
    nextColormap,
    nextColormapName
  );
};

const commandsModule = ({ appConfig }) => {
  const { store } = appConfig;
  const actions = {
    changeColormap: ({ viewports: viewportsStore, colorId, reset }) => {
      const { viewportSpecificData = {} } = viewportsStore || {};
      const {
        colormapName,
        colormap,
      } = CornerstoneColormapService.getNextColormap(colorId);
      // iterate viewports and update each of them
      for (let keyIndex in viewportSpecificData) {
        if (
          viewportSpecificData[keyIndex].plugin === 'cornerstone' ||
          viewportSpecificData[keyIndex].plugin === 'cornerstone::nifti' ||
          viewportSpecificData[keyIndex].plugin === 'cornerstone::metaimage'
        ) {
          _updateViewportsColormap(keyIndex, colormap, colormapName, reset);
        }
      }
      // set to store
      setColormapId(colorId, store);
    },
    clearColormap: ({}) => {
      setColormapId(null, store);
    },
  };

  const definitions = {
    changeColormap: {
      commandFn: actions.changeColormap,
      storeContexts: ['viewports'],
    },
    resetColormap: {
      commandFn: actions.changeColormap,
      storeContexts: ['viewports'],
      options: { reset: true },
    },
    clearColormap: {
      commandFn: actions.clearColormap,
    },
  };

  return {
    actions,
    definitions,
  };
};

export default commandsModule;

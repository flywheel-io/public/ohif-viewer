import { CrosshairsTool } from '../tools';

const commandsModule = () => {
  const actions = {
    toggleCrosshairs: CrosshairsTool.toggleCrosshairs,
  };

  const definitions = {
    toggleCrosshairs: {
      commandFn: actions.toggleCrosshairs,
      storeContexts: [],
      options: {},
    },
  };

  return {
    actions,
    definitions,
  };
};

export default commandsModule;

import initReactive from './initReactive';
import { Reactive as FlywheelCommonReactive } from '@flywheel/extension-flywheel-common';
import initScrollValidator from './initScrollValidator';

export default function initExtension() {
  initReactive(FlywheelCommonReactive.store.observables.storeObservable$);
  initScrollValidator();
}

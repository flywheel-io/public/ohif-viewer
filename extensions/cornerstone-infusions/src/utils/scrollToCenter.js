import cornerstoneTools from 'cornerstone-tools';
import { getStackData } from './cornerstoneUtils';

const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');

export default function scrollToCenter(elements) {
  for (const element of elements) {
    const stackData = getStackData(element);
    const centerPos = Math.round(stackData.imageIds.length / 2) || 0;
    try {
      scrollToIndex(element, centerPos);
    } catch (error) {
      // out of range
      scrollToIndex(element, stackData.imageIds.length - 1);
    }
  }
}

const orientSynchronizer = (synchronizer, sourceElement, targetElement) => {
  if (targetElement === sourceElement) {
    return;
  }

  const sourceViewport = cornerstone.getViewport(sourceElement);
  let targetViewport = cornerstone.getViewport(targetElement);

  if (
    targetViewport.rotation === sourceViewport.rotation &&
    targetViewport.hflip === sourceViewport.hflip &&
    targetViewport.vflip === sourceViewport.vflip
  ) {
    return;
  }

  if (targetViewport.rotation !== sourceViewport.rotation) {
    targetViewport.rotation = sourceViewport.rotation;
  }

  if (targetViewport.hflip !== sourceViewport.hflip) {
    targetViewport.hflip = sourceViewport.hflip;
  }

  if (targetViewport.vflip !== sourceViewport.vflip) {
    targetViewport.vflip = sourceViewport.vflip;
  }

  synchronizer.setViewport(targetElement, targetViewport);
};

export default orientSynchronizer;

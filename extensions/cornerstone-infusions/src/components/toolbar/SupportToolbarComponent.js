import React from 'react';
import store from '@ohif/viewer/src/store';
import { TICKET_URL } from '@flywheel/extension-flywheel-common/src/http/urls';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import { ToolbarButton } from '@ohif/ui';
import { connect } from 'react-redux';

const {
  selectCurrentNifti,
  selectCurrentWebImage,
} = FlywheelCommonRedux.selectors;
const { getRouteParams } = FlywheelCommonUtils;

const getUrl = (project, sessions, currentFile) => {
  let description = '';
  const state = store.getState();
  const userMail = state.flywheel?.user?.email;

  const { taskId } = getRouteParams();

  if (taskId) {
    description += taskId ? `Reader Task Id%09%09: ${taskId}%0A` : '';
  } else {
    description += project ? `Project%09%09: ${project.label}%0A` : '';

    if (sessions && sessions.length > 0) {
      description += `Session Id%09:`;
      sessions.forEach(session => {
        description += session._id + ', ';
      });
      const trimIndex = description.lastIndexOf(',');
      if (trimIndex > -1) {
        description = description.substring(0, trimIndex);
      }
      description += '%0A';
    } else {
      const containerId = currentFile?.containerId;
      description += containerId ? `Container Id%09: ${containerId}%0A` : '';
      const fileName = currentFile?.filename;
      description += fileName ? `File Name%09: ${fileName}%0A` : '';
    }
  }
  description += '----------------------------';
  const url =
    TICKET_URL +
    '?tf_anonymous_requester_email=' +
    userMail +
    '&tf_description=' +
    description;
  return url;
};

function SupportToolbarComponent({
  parentContext,
  toolbarClickCallback,
  button,
  activeButtons,
  isActive,
  className,
  project,
  sessions,
  currentFile,
}) {
  const { id, label, icon } = button;

  const url = getUrl(project, sessions, currentFile);

  return (
    <a href={url} target="_blank">
      <ToolbarButton
        key={id}
        label={label}
        icon={icon}
        class={button.class}
        isActive={isActive}
      />
    </a>
  );
}

function mapStateToProps(state) {
  const { selectActiveProject, selectSessions } = FlywheelRedux.selectors;
  const project = selectActiveProject(state);
  const currentNifi = selectCurrentNifti(state);
  const currentImage = selectCurrentWebImage(state);
  const sessions = selectSessions(state);

  return {
    project,
    sessions,
    currentFile: currentNifi || currentImage,
  };
}

export default connect(mapStateToProps)(SupportToolbarComponent);

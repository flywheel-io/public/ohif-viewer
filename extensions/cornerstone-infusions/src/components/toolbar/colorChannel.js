import React, { useState, useEffect } from 'react';
import cornerstone from 'cornerstone-core';
import { ToolbarButton, OverlayTrigger, Tooltip } from '@ohif/ui';
import classnames from 'classnames';
import './colorMap.styl';
import './colorChannel.styl';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { useSelector } from 'react-redux';

const ColorChannel = ({ button, className = '', commandsManager }) => {
  const [selectedChannel, setSelectedChannel] = useState('RGB');
  const colors = cornerstone.colors.getColorChannelList();
  const [isExpanded, setIsExpanded] = useState(false);
  const [toolBarButtonVisible, setToolBarButtonVisible] = useState(true);
  const { label, icon, commandName } = button;
  const { activeViewportIndex } = useSelector(state => {
    const { viewports = {} } = state;
    const { activeViewportIndex } = viewports;
    return {
      activeViewportIndex,
    };
  });

  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    activeViewportIndex
  );

  let enabledElement;
  if (element) {
    enabledElement = cornerstone.getEnabledElement(element);
  }

  const isColorMapExist = useSelector(state => {
    const viewports = state.viewports || {};
    return viewports.colormapId;
  });

  const currentChannel = useSelector(state => {
    const viewports = state.viewports || {};
    return viewports.channelId;
  });

  const channelIndex = colors.findIndex(x => x.id == currentChannel);

  useEffect(() => {
    if (enabledElement?.image?.color) {
      setToolBarButtonVisible(true);
    } else if (enabledElement?.image?.color === false) {
      setToolBarButtonVisible(false);
    }
  }, [enabledElement?.image?.color]);

  useEffect(() => {
    const colorChannelName = colors?.[channelIndex]?.name || 'RGB';
    setSelectedChannel(colorChannelName);
  }, [colors?.[channelIndex]?.name]);

  const handleClick = color => {
    if (selectedChannel !== color.name) {
      _runChannelChangeCommand(
        { commandName },
        { channelId: color.id },
        commandsManager
      );
      setSelectedChannel(color.name);
    }
    if (isColorMapExist) {
      _runColorChangeCommand(
        {
          isReset: true,
        },
        commandsManager
      );
    }
  };

  function _runChannelChangeCommand(button, options, commandsManager) {
    if (button.commandName) {
      const _options = Object.assign({}, options, button.commandOptions);
      commandsManager.runCommand(button.commandName, _options);
    }
  }

  function _runColorChangeCommand(options, commandsManager) {
    const _options = Object.assign({}, options);
    commandsManager.runCommand('resetColormap', _options);
  }

  const Item = channel => {
    return (
      <div className="colorItems">
        <input
          type="checkbox"
          name="channelGroup"
          className="inputItem"
          onChange={() => {
            handleClick(channel.color);
          }}
          checked={channel.color.name === selectedChannel}
        />
        <label>{channel.color.name}</label>
      </div>
    );
  };
  function ChannelSelect(colors) {
    return (
      <div>
        {colors.colors.map(item => (
          <Item key={item.id} color={item}></Item>
        ))}
      </div>
    );
  }

  function onExpandableToolClick() {
    setIsExpanded(!isExpanded);
  }

  function onOverlayHide() {
    setIsExpanded(false);
  }

  const getToolBarButtonComponent = () => {
    return (
      <div>
        {toolBarButtonVisible && (
          <ToolbarButton
            key="menu-button"
            type="tool"
            label={label}
            icon={icon}
            class="material-icons"
            className={'toolbar-button expandableToolMenu'}
            isExpandable={true}
            isExpanded={isExpanded}
          />
        )}
      </div>
    );
  };

  const toolbarButtonComponent = getToolBarButtonComponent();

  const toolbarMenuOverlay = () => (
    <Tooltip
      placement="bottom"
      className={classnames(
        'tooltip-toolbar-overlay colorChannelDialog',
        className
      )}
      id={`${Math.random()}_tooltip-toolbar-overlay}`}
    >
      <ChannelSelect colors={colors}></ChannelSelect>
    </Tooltip>
  );

  return (
    <OverlayTrigger
      key="menu-button"
      trigger="click"
      placement="bottom"
      rootClose={true}
      handleHide={onOverlayHide}
      onClick={onExpandableToolClick}
      overlay={toolbarMenuOverlay()}
    >
      {toolbarButtonComponent}
    </OverlayTrigger>
  );
};
export default ColorChannel;

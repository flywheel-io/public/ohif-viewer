import ColorChannel from '../components/toolbar/colorChannel';
import withCommandsManager from '../components/withCommandsManager';

const getDefinitions = (store, commandsManager) => {
  const toolbarDefinitions = [
    {
      id: 'toggleColorChannel',
      label: 'RGB Channel',
      icon: 'gradient',
      class: 'material-icons',
      CustomComponent: withCommandsManager(ColorChannel, commandsManager),
      commandName: 'changeColorChannel',
      context: [
        'ACTIVE_VIEWPORT::CORNERSTONE',
        'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
        'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
      ],
    },
  ];
  return toolbarDefinitions;
};

export default getDefinitions;

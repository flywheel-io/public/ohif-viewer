import { LinkSeriesTool } from '../tools';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const getLinkButtonState = (store, link) => () => {
  const state = store.getState();
  // TODO OHIF way pass viewports state as params
  const viewports = FlywheelCommonRedux.selectors.selectViewports(state) || [];
  return {
    active: link ? link.isEnabled : false,
    visible: viewports.length > 1,
  };
};

const getDefinitions = store => {
  const toolbarDefinitions = [
    {
      id: 'Link',
      label: 'Link',
      icon: 'link',
      getState: getLinkButtonState(store),
      buttons: [
        {
          id: 'ScrollLinkButton',
          label: 'Scroll',
          icon: 'bars',
          type: 'stateful',
          getState: getLinkButtonState(store, LinkSeriesTool.tools.scrollLink),
          commandName: 'toggleScrollLink',
          commandOptions: {},
        },
        {
          id: 'WwwcLinkButton',
          label: 'Levels',
          icon: 'level',
          type: 'stateful',
          getState: getLinkButtonState(store, LinkSeriesTool.tools.wwwcLink),
          commandName: 'toggleWwwcLink',
          commandOptions: {},
        },
        {
          id: 'ZoomLinkButton',
          label: 'Zoom',
          icon: 'search-plus',
          type: 'stateful',
          getState: getLinkButtonState(store, LinkSeriesTool.tools.zoomLink),
          commandName: 'toggleZoomLink',
          commandOptions: {},
        },
        {
          id: 'OrientLinkButton',
          label: 'Orient',
          icon: 'rotate',
          class: 'material-icons',
          type: 'stateful',
          getState: getLinkButtonState(store, LinkSeriesTool.tools.orientLink),
          commandName: 'toggleOrientLink',
          commandOptions: {},
        },
      ],
    },
  ];

  return toolbarDefinitions;
};

export default getDefinitions;

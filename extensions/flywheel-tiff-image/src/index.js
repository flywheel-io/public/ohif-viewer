import initExtension from './init';

export default {
  id: 'flywheel-tiff-image',
  preRegistration({ commandsManager, servicesManager, appConfig }) {
    initExtension({ commandsManager, servicesManager, appConfig });
  },
};

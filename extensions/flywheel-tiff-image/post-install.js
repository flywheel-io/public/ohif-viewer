const saveFile = require('fs').writeFileSync;

const pkgJsonPath = process.env.INIT_CWD + '/node_modules/tiff.js/package.json';
const json = require(pkgJsonPath);

if (!json.hasOwnProperty('browser')) {
  json.browser = {
    fs: false,
    path: false,
    crypto: false,
    stream: false,
  };
}

saveFile(pkgJsonPath, JSON.stringify(json, null, 2));

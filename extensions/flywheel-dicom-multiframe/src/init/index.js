import initImageLoader from './initImageLoader';

export default function initExtension({ appConfig }) {
  initImageLoader(store);
}

import loadImage from './loadImage.js';

export default function(cornerstone) {
  // register Multi-frame wadors scheme, using default OHIF metadata providers
  cornerstone.registerImageLoader('multiFrameWadors', loadImage);
}

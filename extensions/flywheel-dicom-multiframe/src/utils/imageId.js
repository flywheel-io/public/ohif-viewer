export const scheme = 'multiFrameWadors';

/**
 * It changes wadors to multiFrameWadors allowing to use multi-frame specific image loader
 * @param {string} wadorsId Image id
 * @return {string}
 */
export const replaceToMultiFrameIdScheme = wadorsId => {
  return wadorsId.replace('wadors', scheme);
};

/**
 * MultiFrame wadors Image id is based on studyInstanceUID, seriesInstanceUID, source sopInstanceUID and current frame index.
 * It return uri equivalent to source imageID
 * @param {string} multiFrameImageId Image id
 * @return {string}
 */
export const getURI = multiFrameImageId => {
  let uri = multiFrameImageId.replace(`${scheme}:`, '');
  // const anyFrameRegExp = /\/frames\/[\d]*/;
  // const sourceFrame = '/frames/1';
  // uri = uri.replace(anyFrameRegExp, sourceFrame);

  return uri;
};

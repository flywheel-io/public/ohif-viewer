import initExtension from './init';
import adapters from './adapters';

export default {
  id: 'flywheel-dicom-multiframe',
  preRegistration({ commandsManager, servicesManager, appConfig }) {
    initExtension({ commandsManager, servicesManager, appConfig });
  },
};

export { adapters };

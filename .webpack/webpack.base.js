// ~~ ENV
const dotenv = require('dotenv');
//
const path = require('path');
const webpack = require('webpack');
// ~~ RULES
const loadShadersRule = require('./rules/loadShaders.js');
const loadWebWorkersRule = require('./rules/loadWebWorkers.js');
const transpileJavaScriptRule = require('./rules/transpileJavaScript.js');
// ~~ PLUGINS
const TerserJSPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// ~~ ENV VARS
const NODE_ENV = process.env.NODE_ENV;
const QUICK_BUILD = process.env.QUICK_BUILD;
const BUILD_NUM = process.env.CIRCLE_BUILD_NUM || '0';
const NODE_DIR = path.join(__dirname, '../node_modules/');

//
dotenv.config();

/* Determine Sentry release from GitLab CI variables */
const { CI_COMMIT_TAG, CI_COMMIT_REF_NAME, CI_COMMIT_SHORT_SHA } = process.env;
let sentryRelease;
if (CI_COMMIT_TAG) {
  sentryRelease = `flywheel-ohif-viewer@${CI_COMMIT_TAG}`;
} else if (CI_COMMIT_REF_NAME && CI_COMMIT_SHORT_SHA) {
  sentryRelease = `flywheel-ohif-viewer@${CI_COMMIT_REF_NAME}.${CI_COMMIT_SHORT_SHA}`;
}

module.exports = (env, argv, { SRC_DIR, DIST_DIR }) => {
  if (!process.env.NODE_ENV) {
    throw new Error('process.env.NODE_ENV not set');
  }

  const mode = NODE_ENV === 'production' ? 'production' : 'development';
  const isProdBuild = NODE_ENV === 'production';
  const isQuickBuild = QUICK_BUILD === 'true';

  const config = {
    mode: isProdBuild ? 'production' : 'development',
    devtool: isProdBuild ? 'source-map' : 'eval-cheap-module-source-map',
    entry: {
      app: `${SRC_DIR}/index.js`,
    },
    optimization: {
      minimize: false,
      sideEffects: true,
      minimizer: [new TerserJSPlugin()],
    },
    context: SRC_DIR,
    stats: {
      colors: true,
      hash: true,
      timings: true,
      assets: true,
      chunks: false,
      chunkModules: false,
      modules: false,
      children: false,
      warnings: true,
    },
    module: {
      rules: [
        transpileJavaScriptRule(mode),
        loadWebWorkersRule,
        loadShadersRule,
      ],
    },
    resolve: {
      // Which directories to search when resolving modules
      modules: ['./node_modules', SRC_DIR],
      extensions: ['.js', '.jsx', '.json', '.*'],
      symlinks: true,
      mainFields: ['browser', 'main', 'module'],
      alias: {
        'react-dnd': require.resolve('react-dnd'), // This can force resolving the correct version
      },
      fallback: {
        // Add fallbacks for Node.js core modules if necessary
        fs: false, // Explicitly set to `false` to avoid polyfills
        path: require.resolve('path-browserify'),
        stream: require.resolve('stream-browserify'),
        querystring: require.resolve('querystring-es3'),
        'process/browser': require.resolve('process/browser'),
      },
    },
    plugins: [
      new webpack.ProvidePlugin({
        process: 'process/browser',
      }),
      new webpack.DefinePlugin({
        /* Application */
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.DEBUG': JSON.stringify(process.env.DEBUG),
        'process.env.APP_CONFIG': JSON.stringify(process.env.APP_CONFIG || ''),
        'process.env.PUBLIC_URL': JSON.stringify(process.env.PUBLIC_URL || '/'),
        'process.env.VERSION_NUMBER': webpack.DefinePlugin.runtimeValue(() => {
          const packagePath = path.resolve(
            __dirname,
            '../platform/viewer/package.json'
          );
          const package = require(packagePath);
          return JSON.stringify(package.version || '');
        }, [path.resolve(__dirname, '../platform/viewer/package.json')]),
        'process.env.BUILD_NUM': JSON.stringify(BUILD_NUM),
        /* i18n */
        'process.env.USE_LOCIZE': JSON.stringify(process.env.USE_LOCIZE || ''),
        'process.env.LOCIZE_PROJECTID': JSON.stringify(
          process.env.LOCIZE_PROJECTID || ''
        ),
        'process.env.LOCIZE_API_KEY': JSON.stringify(
          process.env.LOCIZE_API_KEY || ''
        ),
        /* Log */
        'process.env.SENTRY_RELEASE': JSON.stringify(sentryRelease),
      }),
      new CopyWebpackPlugin({
        patterns: [
          {
            from:
              NODE_DIR +
              '@cornerstonejs/codec-libjpeg-turbo/dist/libjpegturbowasm.wasm',
            to: DIST_DIR,
          },
          {
            from:
              NODE_DIR + '@cornerstonejs/codec-openjpeg/dist/openjpegwasm.wasm',
            to: DIST_DIR,
          },
        ],
      }),
    ],
  };

  if (isQuickBuild) {
    config.devtool = false;
  }

  return config;
};
